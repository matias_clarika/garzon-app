﻿namespace Garzon.Forms.UI.TemplateSelectors
{
    using System;
    using Garzon.Core.Views.Component;
    using Garzon.DataObjects;
    using Xamarin.Forms;

    public class ProductListTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ProductWithDescriptionTemplate { get; set; }
        public DataTemplate ProductWithoutDescriptionTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            try
            {
                var product = ((SelectableItem<Product>)item).Data;
                if (string.IsNullOrEmpty(product.ShortDescription))
                {
                    return this.ProductWithoutDescriptionTemplate;
                }
                else
                {
                    return this.ProductWithDescriptionTemplate;
                }
            }
            catch
            {
                return default(DataTemplate);
            }
        }

    }
}
