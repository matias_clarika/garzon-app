﻿namespace Garzon.Forms.UI.TemplateSelectors
{
    using System;
    using Garzon.Forms.UI.Views;
    using Xamarin.Forms;

    public class RecommendedTemplateSelector : DataTemplateSelector
    {
        public DataTemplate RecommendedProductTemplate { get; set; }
        public DataTemplate PendingRequestOrderDetailTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            try
            {
                var recommendedItemView = (RecommendedItemView)item;
                switch (recommendedItemView.Type)
                {
                    case RecommendedItemType.ORDER_DETAIL_PENDING_REQUEST:
                        return PendingRequestOrderDetailTemplate;
                    default:
                        return RecommendedProductTemplate;
                }
            }
            catch
            {
                return default(DataTemplate);
            }
        }

    }
}
