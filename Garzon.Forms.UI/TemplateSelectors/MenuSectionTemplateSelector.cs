﻿using System;
using Garzon.DataObjects;
using Xamarin.Forms;

namespace Garzon.Forms.UI.TemplateSelectors
{
    public class MenuSectionTemplateSelector : DataTemplateSelector
    {
        #region templates
        public DataTemplate MenuSectionImageTemplate
        {
            get;
            set;
        }

        public DataTemplate MenuSectionIconTemplate
        {
            get;
            set;
        }

        #endregion  
        #region override
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            MenuSection menuSection = (Garzon.DataObjects.MenuSection)item;

            if (!String.IsNullOrEmpty(menuSection.Image))
            {
                return MenuSectionImageTemplate;
            }
            else
            {
                return MenuSectionIconTemplate;
            }

        }
        #endregion
    }
}
