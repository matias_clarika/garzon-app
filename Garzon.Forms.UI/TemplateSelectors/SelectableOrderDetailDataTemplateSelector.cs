﻿using System;
using Garzon.Core.Views.Component;
using Garzon.DataObjects;
using Xamarin.Forms;

namespace Garzon.Forms.UI.TemplateSelectors
{
    public class SelectableOrderDetailDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ValidTemplate { get; set; }
        public DataTemplate InvalidTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var isValid = false;
            try
            {
                var orderDetail = (SelectableItem<OrderDetail>)item;
                isValid = !orderDetail.Data.IsWaiting();
            }
            catch (Exception e)
            {
                //nothing
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return isValid ? ValidTemplate : InvalidTemplate;
        }
    }
}
