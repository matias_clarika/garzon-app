﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Payments;
    using Garzon.Core.Utils;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Navigation.Result;
    using MvvmCross.Core.ViewModels;
    using Xamarin.Forms;

    public class MercadoPagoPaymentViewModel : GarzonBaseViewModel, IMvxViewModel<MercadoPagoPaymentViewModel.Params, PaymentResult>
    {
        #region dependency
        protected readonly IOAuthSettings OAthSettings;
        protected readonly IMercadoPagoPayment MercadoPagoPayment;
        #endregion

        #region constants
        private const string MP_TOKEN_KEY = "token";
        private const string MP_PAYMENT_METHOD_ID = "payment_method_id";
        private const string MP_CAPTURE = "capture";
        #endregion
        private bool isLoading;
        private UrlWebViewSource url;

        public bool IsLoading
        {
            get => this.isLoading;
            set => this.SetProperty(ref this.isLoading, value);
        }

        public UrlWebViewSource Url
        {
            get => this.url;
            set => SetProperty(ref this.url, value);
        }

        public MercadoPagoPaymentViewModel(ILogger logger,
                                           IAnalytics analytics,
                                           INavigationController navigationController,
                                           AppDefaultConfig appConfig,
                                           IDialogs dialogs,
                                           IOAuthSettings oAthSettings,
                                           IMercadoPagoPayment mercadoPagoPayment) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            this.OAthSettings = oAthSettings;
            this.MercadoPagoPayment = mercadoPagoPayment;
        }

        #region IMvxViewModel
        public void Prepare(Params parameter)
        {
        }
        #endregion
        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            await this.OnLoading();
            Url = new UrlWebViewSource
            {
                Url = GetMercadoPagoUrlByUser()
            };

        }
        #endregion

        #region abstract class
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "mercadopago_payment";
        }
        #endregion

        #region private methods
        private string GetMercadoPagoUrlByUser()
        {
            return string.Format(this.MercadoPagoPayment.PaymentUrl, this.OAthSettings.GetToken());
        }

        #endregion

        #region public methods
        public Task OnLoading()
        {
            return Task.Run(() =>
            {
                this.IsLoading = true;
            });
        }

        public Task OnFinish()
        {
            return Task.Run(() =>
            {
                this.IsLoading = false;
            });
        }


        public async Task<bool> ValidateSuccessUrl(string url)
        {
            if (url.Contains(MP_TOKEN_KEY) && url.Contains(MP_PAYMENT_METHOD_ID) && url.Contains(MP_CAPTURE))
            {
                var parameters = url.Split('&');
                var token = FindValueByParameterName(MP_TOKEN_KEY, parameters);
                var paymentMethodId = FindValueByParameterName(MP_PAYMENT_METHOD_ID, parameters);
                var capture = bool.Parse(FindValueByParameterName(MP_CAPTURE, parameters));
                this.navigationController.Back(this, PaymentResult.Build(token, paymentMethodId, capture));
                return true;
            }
            else
            {
                return await Task.Factory.StartNew(() =>
                {
                    return false;
                });
            }
        }

        /// <summary>
        /// Finds the name of the value by parameter.
        /// </summary>
        /// <returns>The value by parameter name.</returns>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="parameters">Parameters.</param>
        private string FindValueByParameterName(string parameterName, string[] parameters)
        {
            var parameter = parameters.FirstOrDefault((arg) => arg.Contains(parameterName));
            if (!string.IsNullOrEmpty(parameter))
            {
                return parameter.Split('=')[1];
            }
            return string.Empty;
        }
        #endregion

        #region params
        public class Params
        {
            public static Params Build()
            {
                return new Params();
            }
        }
        #endregion
    }
}
