﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using Xamarin.Forms;
    using Garzon.Core.EventBus;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.EventBus.Auth;
    using Garzon.Forms.UI.EventBus.OrderNotifications;
    using Garzon.Forms.UI.EventBus.PlaceChanges;
    using System.Collections.Generic;

    public class MasterViewModel : GarzonBaseViewModel
    {
        #region dependency
        readonly IUserSessionManager _userSessionFacade;
        readonly IOrderManager _orderManager;
        readonly IEventBus _eventBus;
        readonly OrderGlobalEventHandler _orderGlobalEventHandler;
        #endregion

        #region fields to binding
        /// <summary>
        /// current user logged
        /// </summary>
        User _user;

        public User User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public MvxObservableCollection<MenuItem> MenuItems { get; } = new MvxObservableCollection<MenuItem>();

        public MenuItem.Identifiers SelectedMenuItem = MenuItem.Identifiers.PLACES;

        MenuItem _menuItem;

        public MenuItem SelectedMenu
        {
            get => _menuItem;
            set => SetProperty(ref _menuItem, value);
        }

        Place _currentPlace;
        public Place CurrentPlace
        {
            get => _currentPlace;
            set => SetProperty(ref _currentPlace, value);
        }

        bool _showPinNumber;
        public bool ShowPinNumber
        {
            get => _showPinNumber;
            set => SetProperty(ref _showPinNumber, value);
        }

        string _pinNumber;

        public string PinNumber
        {
            get => _pinNumber;
            set => SetProperty(ref _pinNumber, value);
        }
        private bool _firstTime = true;
        #endregion

        #region fields
        public Order Order
        {
            get;
            set;
        }
        #endregion
        #region fields subscribers
        IEventToken _tokenUserSessionEvent;
        IEventToken _tokenOrderEvent;
        IEventToken _tokenPlaceEvent;
        IEventToken _tokenOrderCheckNotificationEvent;
        #endregion

        #region commands
        public IMvxAsyncCommand<MenuItem> ItemMenuSelectedCommand => new MvxAsyncCommand<MenuItem>(ItemSelected);
        #endregion

        #region constants
        private const string FULL_ASSEMBLY_REFERENCE = "?assembly=Garzon.Core";
        #endregion
        #region constructor
        public MasterViewModel(ILogger logger,
                               IAnalytics analytics,
                               INavigationController navigationController,
                               IUserSessionManager userSessionFacade,
                               IOrderManager orderFacade,
                               IEventBus eventBus,
                               OrderGlobalEventHandler orderGlobalEventHandler) : base(logger, analytics, navigationController)
        {
            _userSessionFacade = userSessionFacade;
            _orderManager = orderFacade;
            _eventBus = eventBus;
            _orderGlobalEventHandler = orderGlobalEventHandler;
        }

        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            Subscribe();
        }

        private async Task InitializeViewModel()
        {
            if (_firstTime)
            {
                _firstTime = false;
                await CheckSession();
                await Load();
                await _orderManager.DeletePendingOrderDetails();
            }
        }

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await InitializeViewModel();
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            Unsuscribe();
            base.ViewDestroy(viewFinishing);
        }
        #endregion

        #region events
        private async void Subscribe()
        {
            _tokenUserSessionEvent = await _eventBus
                                                    .SubscribeOnMainThread<UserSessionEvent>(OnChangeUserSession);

            _tokenOrderEvent = await _eventBus
                                              .SubscribeOnMainThread<OrderChangeEvent>(OnChangeOrder);

            _tokenPlaceEvent = await _eventBus
                                              .SubscribeOnMainThread<PlaceChangeEvent>(OnChangePlace);

            _tokenOrderCheckNotificationEvent = await _eventBus
                                                .SubscribeOnMainThread<CheckNotificationEvent>(OnCheckNotification);
        }


        private void Unsuscribe()
        {
            _eventBus.Unsubscribe<UserSessionEvent>(_tokenUserSessionEvent);
            _eventBus.Unsubscribe<OrderChangeEvent>(_tokenOrderEvent);
            _eventBus.Unsubscribe<PlaceChangeEvent>(_tokenPlaceEvent);
            _eventBus.Unsubscribe<PlaceChangeEvent>(_tokenOrderCheckNotificationEvent);
        }

        private void OnChangePlace(PlaceChangeEvent obj)
        {
            CurrentPlace = obj.Place;
            GeneratedMenuItems();
        }

        private void OnChangeOrder(OrderChangeEvent obj)
        {
            if (obj.Order != null && obj.Order.IsClosed())
            {
                NotifyClosedOrder();
            }
            else
            {
                Order = obj.Order;
                CheckOrder();
            }
        }

        private void NotifyClosedOrder()
        {
            Order = null;
            navigationController.ReOpenApp();
        }

        private void OnChangeUserSession(UserSessionEvent message)
        {
            User = message.User;
            GeneratedMenuItems();
        }


        private async void OnCheckNotification(CheckNotificationEvent checkNotificationEvent)
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                await _orderGlobalEventHandler.CheckPendingNotification();
            }
        }
        #endregion
        #region Menu Items

        private void GeneratedMenuItems()
        {
            MenuItems.Clear();

            var hasOrder = Order != null;
            var userLogged = User != null;
            var hasPlace = CurrentPlace != null;
            if (!hasOrder)
            {
                MenuItems.Add(CreateMenuItem(Strings["Places"], "ic_place", MenuItem.Identifiers.PLACES));
            }
            else
            {
                MenuItems.Add(CreateMenuItem(Strings["MenuTitle"], "ic_place", MenuItem.Identifiers.MENU));
            }

            if (hasPlace)
            {
                MenuItems.Add(CreateMenuItem(Strings["CallTheWaiter"], "ic_waiter", MenuItem.Identifiers.WAITER));
            }

            if (hasOrder)
            {
                MenuItems.Add(CreateMenuItem(Strings["CheckOrder"], "ic_request_check", MenuItem.Identifiers.CHECK_ORDER));
            }

            if (userLogged)
            {
                MenuItems.Add(CreateMenuItem(Strings["MyOrderHistory"], "ic_my_order", MenuItem.Identifiers.MY_ORDERS));
            }

            MenuItems.Add(CreateMenuItem(Strings["Faq"], "ic_faq", MenuItem.Identifiers.FAQ));

            if (userLogged && !hasOrder)
            {
                MenuItems.Add(CreateMenuItem(Strings["SignOut"], "ic_logout", MenuItem.Identifiers.SIGN_OUT));
            }
        }
        /// <summary>
        /// Creates the menu item.
        /// </summary>
        /// <returns>The menu item.</returns>
        /// <param name="title">Title.</param>
        /// <param name="iconSVG">Icon svg.</param>
        /// <param name="identifier">Identifier.</param>
        private MenuItem CreateMenuItem(string title, string iconSVG, MenuItem.Identifiers identifier)
        {
            return new MenuItem { Title = title, IconSVG = iconSVG, Identifier = identifier, IsSelected = SelectedMenuItem == identifier };
        }

        public class MenuItem
        {

            public string Title { get; set; }

            public string IconSVG { get; set; }

            public string Icon
            {
                get
                {
                    string extension = ".svg";
                    if (IsSelected)
                    {
                        extension = "_activate.svg";
                    }
                    return $"resource://Garzon.Core.Resources.Images.{IconSVG}{extension}{FULL_ASSEMBLY_REFERENCE}";
                }
            }

            public Color TextColor
            {
                get
                {
                    if (IsSelected)
                    {
                        return GarzonColors.PRIMARY_DARK;
                    }
                    else
                    {
                        return GarzonColors.SECONDARY;
                    }
                }
            }

            public Boolean IsSelected { get; set; } = false;

            public Identifiers Identifier
            {
                get;
                set;
            }

            public enum Identifiers
            {
                PLACES, MENU, RECOVER_ORDER, CHECK_ORDER, MY_ORDERS, SIGN_IN, SIGN_OUT,
                WAITER, FAQ
            };
        }
        #region actions


        private async Task ItemSelected(MenuItem menuItem)
        {
            if (SelectedMenuItem != menuItem.Identifier)
            {
                bool changePage = true;
                switch (menuItem.Identifier)
                {
                    case MenuItem.Identifiers.PLACES:
                        await OpenPlaces();
                        break;
                    case MenuItem.Identifiers.SIGN_IN:
                    case MenuItem.Identifiers.RECOVER_ORDER:
                        await OpenLogin();
                        break;
                    case MenuItem.Identifiers.MENU:
                        await OpenMenu();
                        break;
                    case MenuItem.Identifiers.CHECK_ORDER:
                        await OpenResume();
                        changePage = false;
                        break;
                    case MenuItem.Identifiers.WAITER:
                        await OpenCallWaiter();
                        changePage = false;
                        break;
                    case MenuItem.Identifiers.FAQ:
                        changePage = false;
                        await Faq();
                        break;
                    case MenuItem.Identifiers.MY_ORDERS:
                        await OpenMyOrders();
                        changePage = false;
                        break;
                    case MenuItem.Identifiers.SIGN_OUT:
                        changePage = false;
                        await Logout();
                        break;

                }

                SelectedMenu = menuItem;
                if (changePage)
                {
                    SelectedMenuItem = menuItem.Identifier;
                }
                //refresh menu items
                GeneratedMenuItems();
            }
        }
        #endregion

        private async Task Load()
        {
            await FixMasterDetailAndroid();
            await OpenFirstPage();
            GeneratedMenuItems();
        }

        private static async Task FixMasterDetailAndroid()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                await Task.Delay(250);
            }
        }

        private Task OpenFirstPage()
        {
            if (Order != null)
            {
                SelectedMenuItem = MenuItem.Identifiers.MENU;
                return OpenMenu();
            }
            else
            {
                SelectedMenuItem = MenuItem.Identifiers.PLACES;
                return OpenPlaces();
            }
        }

        private async Task CheckSession()
        {
            User = await _userSessionFacade.RecoveryUser();
            if (User != null)
            {
                await GetAndCheckOrder();
            }
        }

        private async Task GetAndCheckOrder()
        {
            Order = await _orderManager.GetCurrentOrder(true, false);
            await CheckOrder();
        }

        private async Task CheckOrder()
        {
            if (Order != null
                && Order.ServerId.HasValue
                && Order.ServerId.Value > 0)
            {
                CurrentPlace = Order.Place;
                PinNumber = String.Format(Strings["PinNumber"], Order.Pin.Value);
                ShowPinNumber = true;
            }
            else
            {
                ShowPinNumber = false;
                CurrentPlace = null;
                PinNumber = "";

            }
        }


        private Task OpenPlaces()
        {
            return navigationController.OpenPlaces();
        }

        private async Task OpenLogin()
        {
            LoginResult loginResult = await navigationController.OpenLogin(true);
            var result = await _userSessionFacade.ProcessLoginResult(loginResult);
            if (result)
            {
                Load();
            }

        }

        private Task OpenResume()
        {
            return navigationController.OpenResumeCheck();
        }

        private Task OpenCallWaiter()
        {
            return navigationController.OpenCallWaiter();
        }

        private Task Faq()
        {
            return navigationController.OpenWebUrl("http://garzonapp.com/preguntasfrecuentes.html", Strings["Faq"]);
        }

        private async Task Logout()
        {
            await _userSessionFacade.SignOut();
            Load();
        }

        private Task OpenMenu()
        {
            return navigationController.OpenMenuSectionByPlace(Order.Place, true);
        }

        private Task OpenMyOrders()
        {
            return navigationController.OpenMyOrders();
        }
        #endregion


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "master";
        }
        #endregion
    }


}
