﻿namespace Garzon.Forms.UI.ViewModels
{

    using System.Collections.Generic;

    public class RootViewModel : GarzonBaseViewModel
    {
        public RootViewModel()
        {
        }


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "root";
        }
        #endregion
    }
}
