﻿using System;
using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.EventBus;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;

namespace Garzon.Forms.UI.ViewModels
{
    public class ProductWithoutImageViewModel : ProductViewModel
    {
        #region constructor
        public ProductWithoutImageViewModel(ILogger logger,
                                            IAnalytics analytics,
                                            INavigationController navigationController,
                                            AppDefaultConfig appConfig,
                                            IDialogs dialogs,
                                            IProductMenuSectionBL productMenuSectionBL,
                                            IOrderManager orderFacade,
                                            IEventBus eventBus) : base(logger, analytics, navigationController, appConfig, dialogs, productMenuSectionBL, orderFacade, eventBus)
        {
        }
        #endregion
       
    }
}
