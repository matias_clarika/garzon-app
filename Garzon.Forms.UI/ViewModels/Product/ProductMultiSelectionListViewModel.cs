﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Navigation.Result;
    using MvvmCross.Core.ViewModels;

    public class ProductMultiSelectionListViewModel : ProductGroupListViewModel, IMvxViewModel<ProductMultiSelectionParams, ProductGroupResult>
    {
        #region fields
        private List<Product> TempSelectedProducts
        {
            get;
            set;
        }
        #endregion
        public ProductMultiSelectionListViewModel(ILogger logger,
                                                       IAnalytics analytics,
                                                       INavigationController navigationController,
                                                       AppDefaultConfig appConfig,
                                                       IDialogs dialogs,
                                                       IProductGroupBL productGroupBL,
                                                       IOrderManager orderManager) : base(logger, analytics, navigationController, appConfig, dialogs, productGroupBL, orderManager)
        {
        }

        #region IMvxViewModel
        public void Prepare(ProductMultiSelectionParams parameter)
        {
            base.InitializeWithParams(parameter.ParentGroup);
            this.TempSelectedProducts = parameter.SelectedProducts;
        }
        #endregion

        #region override
        protected override void SetDefaultSubtitle()
        {
            Subtitle = string.Format(Strings["ProductMultipleSelectionSubtitle"], this.ParentGroup.Name.ToLower());
        }

        protected override void AddProductToList(List<Product> products)
        {
            if (products != null && products.Any())
            {
                foreach (var product in products)
                {
                    var selected = false;
                    if (this.TempSelectedProducts != null)
                    {
                        selected = this.TempSelectedProducts.Exists((selectedProduct) => selectedProduct.Id == product.Id);
                    }
                    this.ProductList.Add(product, selected);
                }
            }
        }

        #endregion
        #region actions
        protected override async Task OnSave()
        {
            await this.navigationController.Back(this,ProductGroupResult.Build().WithProducts(this.ProductList.SelectedItems.ToList()));
        }
        #endregion
    }

    public class ProductMultiSelectionParams
    {
        /// <summary>
        /// Gets the parent group.
        /// </summary>
        /// <value>The parent group.</value>
        public Group ParentGroup
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the selected products.
        /// </summary>
        /// <value>The selected products.</value>
        public List<Product> SelectedProducts
        {
            get;
            private set;
        }

        private ProductMultiSelectionParams(Group parentGroup, List<Product> selectedProducts = null)
        {
            this.ParentGroup = parentGroup;
            this.SelectedProducts = selectedProducts;
        }

        /// <summary>
        /// Build the specified parentGroup and selectedProducts.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="parentGroup">Parent group.</param>
        /// <param name="selectedProducts">Selected products.</param>
        public static ProductMultiSelectionParams Build(Group parentGroup, List<Product> selectedProducts = null)
        {
            return new ProductMultiSelectionParams(parentGroup, selectedProducts);
        }
    }
}
