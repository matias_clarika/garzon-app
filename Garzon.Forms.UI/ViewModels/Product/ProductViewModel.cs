﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Core.EventBus;
using Garzon.Forms.UI.EventBus.OrderNotifications;
using System.Linq;

namespace Garzon.Forms.UI.ViewModels
{
    public class ProductViewModel : BaseMyOrderViewModel, IMvxViewModel<ParamsToProducts>
    {
        #region dependency
        readonly IProductMenuSectionBL _productMenuSectionBL;
        #endregion

        #region fields to binding
        //products
        public MvxObservableCollection<Product> ProductList { get; } = new MvxObservableCollection<Product>();
        //page
        private string _title;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
        //header
        private string _subtitle;

        public string Subtitle
        {
            get => _subtitle;
            set => SetProperty(ref _subtitle, value);
        }
        //general

        public bool _isPullToRefreshEnabled = true;

        public bool IsPullToRefreshEnabled
        {
            get => _isPullToRefreshEnabled;
            set => SetProperty(ref _isPullToRefreshEnabled, value);
        }

        public bool _isRefreshing = false;

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetProperty(ref _isRefreshing, value);
        }

        public bool _isLoadingMain = false;

        public bool IsLoadingMain
        {
            get => _isLoadingMain;
            set => SetProperty(ref _isLoadingMain, value);
        }

        public bool _isInfiniteEnabled = false;

        public bool IsInfiniteEnabled
        {
            get => _isInfiniteEnabled;
            set => SetProperty(ref _isInfiniteEnabled, value);
        }

        public bool _isLoadingInfinite = false;

        public bool IsLoadingInfinite
        {
            get => _isLoadingInfinite;
            set => SetProperty(ref _isLoadingInfinite, value);
        }

        #region nullables
        public OrderDetail OrderDetailWithoutStock
        {
            get;
            set;
        }
        #endregion

        #endregion

        #region async utils
        CancellationTokenSource productDetailCancellationToken;
        CancellationTokenSource groupByProductCancellationToken;
        #endregion
        #region command

        public IMvxAsyncCommand PullToRefreshCommand => new MvxAsyncCommand(PullToRefresh);
        public IMvxAsyncCommand LoadingCommand => new MvxAsyncCommand(LoadMore);

        public IMvxAsyncCommand<Product> ItemClickCommand => new MvxAsyncCommand<Product>(ItemClick);

        #endregion

        #region fields to pagination
        const int LIMIT = 999;
        #endregion

        #region fields
        public MenuSection MenuSection
        {
            get;
            set;
        }


        #endregion

        #region constants
        private readonly string TAG = typeof(ProductViewModel).FullName.ToString();
        private bool _firstTime = true;
        #endregion

        #region constructor
        public ProductViewModel(ILogger logger,
                                IAnalytics analytics,
                                INavigationController navigationController,
                                AppDefaultConfig appConfig,
                                IDialogs dialogs,
                                IProductMenuSectionBL productMenuSectionBL,
                                IOrderManager orderFacade,
                                IEventBus eventBus) : base(logger, analytics, navigationController, appConfig, orderFacade, dialogs, eventBus)
        {
            _productMenuSectionBL = productMenuSectionBL;
        }
        #endregion

        #region Implementation of IMvxViewModel
        public void Prepare(ParamsToProducts parameter)
        {
            MenuSection = parameter.MenuSection;
            OrderDetailWithoutStock = parameter.OrderDetail;
            //refresh title
            Title = MenuSection.Name;
            Subtitle = MenuSection.Name;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            IsPullToRefreshEnabled = false;
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            CancelToken();
            LoadFirstTime();
        }

        private void LoadFirstTime()
        {
            if (_firstTime)
            {
                _firstTime = false;
                LoadProducts();
            }
        }

        private void CancelToken()
        {
            CancellationTokenSource(this.productDetailCancellationToken);
            CancellationTokenSource(this.groupByProductCancellationToken);
        }
        #endregion

        #region private methods products
        private async Task LoadProducts()
        {
            try
            {
                IsLoadingMain = ProductList.Count == 0;
                IsRefreshing = false;

                IPaggination paggination = new Paggination(ProductList.Count, LIMIT);

                List<Product> newProducts = await _productMenuSectionBL.Find(MenuSection.Id, paggination);
                AddProducts(newProducts);
                CheckIfProductListIsEmpty();
            }
            catch (Exception e)
            {
                base.logger.Error(TAG, e.Message);
            }
            finally
            {
                if (this.ProductList.Any())
                {
                    IsRefreshing = false;
                    IsLoadingMain = false;
                }

            }
        }

        /// <summary>
        /// Adds the products.
        /// </summary>
        /// <param name="newProducts">New products.</param>
        private void AddProducts(List<Product> newProducts)
        {
            CheckInfiniteState(newProducts);
            //add new products
            ProductList.AddRange(newProducts);
        }

        private void CheckInfiniteState(List<Product> newProducts)
        {
            IsInfiniteEnabled = (newProducts.Count == LIMIT);
        }
        #endregion

        #region actions

        private async Task PullToRefresh()
        {
            IsRefreshing = true;
            IsInfiniteEnabled = false;
            ProductList.Clear();
            await LoadProducts();
        }

        private async Task LoadMore()
        {
            if (IsInfiniteEnabled)
            {
                IsLoadingInfinite = true;
                await LoadProducts();
            }
            IsLoadingInfinite = false;
        }

        private async Task ItemClick(Product product)
        {
            if (ValidateProductStock(product))
            {
                if (MenuSection.FastCharge ?? false)
                {
                    await OpenProductFastCharge(product);
                }
                else
                {
                    await OpenProductPageByType(product);
                }
            }
        }

        #endregion

        #region private
        private void CheckEditableOrderDetailWithoutStock()
        {
            if (OrderDetailWithoutStock != null)
            {
                Back();
            }
        }

        private Task Back()
        {
            return navigationController.Back(this);
        }
        /// <summary>
        /// Opens the product detail.
        /// </summary>
        /// <returns>The product detail.</returns>
        /// <param name="product">Product.</param>
        private async Task OpenProductPageByType(Product product)
        {
            if (product.Type == (int)ProductType.SIMPLE)
            {
                this.productDetailCancellationToken = new CancellationTokenSource();
                await navigationController.OpenProductDetail(product,
                                                              OrderDetailWithoutStock,
                                                              this.productDetailCancellationToken.Token);
                this.productDetailCancellationToken = null;
                CheckEditableOrderDetailWithoutStock();
            }
            else
            {
                this.groupByProductCancellationToken = new CancellationTokenSource();
                await navigationController.OpenGroupsByProduct(this.MenuSection, product, null, false, this.groupByProductCancellationToken.Token);
                this.groupByProductCancellationToken = null;
            }

        }
        /// <summary>
        /// Opens the product fast charge.
        /// </summary>
        /// <returns>The product fast charge.</returns>
        /// <param name="product">Product.</param>
        private async Task OpenProductFastCharge(Product product)
        {
            var result = await this.dialogs.ShowQuantityDialog(product.Name,
                                                               base.appConfig.MIN_QUANTITY_PRODUCT_ADD_TO_ORDER,
                                                               base.appConfig.MIN_QUANTITY_PRODUCT_ADD_TO_ORDER,
                                                               base.appConfig.MAX_QUANTITY_PRODUCT_ADD_TO_ORDER,
                                                               Strings["OrderAddButton"],
                                                               Strings["Cancel"]);
            if (result.Ok)
            {
                await orderManager.InsertOrUpdateOrderDetail(product,
                                                              result.Result,
                                                              OrderDetailWithoutStock);
                Back();
            }
        }
        /// <summary>
        /// Validates the product stock.
        /// </summary>
        /// <returns><c>true</c>, if product stock was validated, <c>false</c> otherwise.</returns>
        /// <param name="product">Product.</param>
        private bool ValidateProductStock(Product product)
        {
            if (OrderDetailWithoutStock != null
                && OrderDetailWithoutStock.ProductId == product.Id)
            {
                this.dialogs.ShowAlert(Strings["ProductWithoutStock"]);
                return false;
            }
            else
            {
                return true;
            }
        }


        private async Task CheckIfProductListIsEmpty()
        {
            if (!this.ProductList.Any())
            {
                await this.navigationController.Back(this);
                await Task.Delay(250);
                await this.dialogs.ShowAlert(Strings["ProductListEmptyError"]);
            }
        }
        #endregion


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "product_simple_detail";
        }
        #endregion
    }



    public class ParamsToProducts
    {
        #region inmutables
        readonly MenuSection _menuSection;

        public MenuSection MenuSection
        {
            get => _menuSection;
        }

        readonly OrderDetail _orderDetail;
        public OrderDetail OrderDetail
        {
            get => _orderDetail;
        }

        #endregion

        #region constructors
        private ParamsToProducts(MenuSection menuSection,
                                 OrderDetail orderDetail = null)
        {
            _menuSection = menuSection;
            _orderDetail = orderDetail;
        }
        #endregion

        #region builders
        public static ParamsToProducts Builder(MenuSection menuSection,
                                               OrderDetail orderDetail = null)
        {
            return new ParamsToProducts(menuSection,
                                        orderDetail);
        }
        #endregion
    }

}
