﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Forms.UI.EventBus.OrderNotifications;
using Garzon.Forms.UI.Navigation;
using MvvmCross.Core.ViewModels;

namespace Garzon.Forms.UI.ViewModels
{
    public class ProductCommentViewModel : GarzonBaseViewModel, IMvxViewModel<ProductCommentParams, string>
    {
        #region bindings
        private string comment;
        public string Comment
        {
            get => this.comment;
            set => SetProperty(ref this.comment, value);
        }

        #endregion
        #region commands
        public IMvxAsyncCommand SaveCommentCommand => new MvxAsyncCommand(OnSaveComment);

        #endregion
        public ProductCommentViewModel(ILogger logger,
                                       IAnalytics analytics,
                                       INavigationController navigationController,
                                       AppDefaultConfig appConfig,
                                       IDialogs dialogs) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
        }
        #region IMvxViewModel
        public void Prepare(ProductCommentParams parameter)
        {
            if (!string.IsNullOrEmpty(parameter.Comment))
            {
                this.Comment = parameter.Comment;
            }
        }
        #endregion
        #region actions

        private async Task OnSaveComment()
        {
            await this.navigationController.Back(this, this.Comment);
        }

        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "product_comment";
        }
        #endregion
    }

    public class ProductCommentParams
    {
        readonly string comment;
        private ProductCommentParams(string comment = null)
        {
            this.comment = comment;
        }

        public String Comment
        {
            get => this.comment;
        }
        public static ProductCommentParams Build(string comment = null)
        {
            return new ProductCommentParams(comment);
        }
    }
}
