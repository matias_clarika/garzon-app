﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Navigation.Result;
    using I18NPortable;
    using MvvmCross.Core.ViewModels;
    using Xamarin.Forms;

    public class GroupListViewModel : GarzonBaseViewModel, IMvxViewModel<GroupListParams, ProductAddedResult>
    {
        #region dependency
        protected readonly IGroupBL groupBL;
        protected readonly IOrderManager orderManager;
        #endregion
        #region constants
        private static string TAG = typeof(GroupListViewModel).FullName;
        #endregion
        #region fields
        private Product product;
        private string comment = null;
        private string title;
        private OrderDetail orderDetail;
        private double amount;
        private bool isPending = false;
        private double productPrice;
        private string addButtonTex;
        private bool firstTime = true;
        #endregion
        #region bindings

        private int quantity = 1;

        public int Quantity
        {
            get => this.quantity;
            set => SetProperty(ref this.quantity, value);
        }

        public Product Product
        {
            get => this.product;
            set => SetProperty(ref this.product, value);
        }

        public MvxObservableCollection<GroupItemView> GroupList { get; } = new MvxObservableCollection<GroupItemView>();

        public string Title
        {
            get => this.title;
            set => SetProperty(ref this.title, value);
        }

        public double Amount
        {
            get => this.amount;
            set => SetProperty(ref this.amount, value);
        }

        public double ProductPrice
        {
            get => this.productPrice;
            set => SetProperty(ref this.productPrice, value);
        }

        public OrderDetail OrderDetail
        {
            get => this.orderDetail;
            set
            {
                SetProperty(ref this.orderDetail, value);
                if (this.OrderDetail != null && string.IsNullOrEmpty(this.comment))
                {
                    this.comment = this.OrderDetail.Comment;
                }
            }
        }

        public string AddButtonText
        {
            get => addButtonTex;
            set => SetProperty(ref addButtonTex, value);
        }
        #endregion

        #region commands
        public IMvxAsyncCommand<GroupItemView> GroupItemClickCommand => new MvxAsyncCommand<GroupItemView>(OnGroupItemClick);
        public IMvxAsyncCommand AddToMyOrderCommand => new MvxAsyncCommand(SaveOrderDetail);
        #endregion

        #region tokens
        protected CancellationTokenSource productCancellationToken;
        protected CancellationTokenSource commentCancellationToken;
        #endregion
        public GroupListViewModel(ILogger logger,
                                  IAnalytics analytics,
                                  INavigationController navigationController,
                                  AppDefaultConfig appConfig,
                                  IDialogs dialogs,
                                  IGroupBL groupBL,
                                  IOrderManager orderManager) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            this.groupBL = groupBL;
            this.orderManager = orderManager;
        }

        #region IMvxViewModel
        public void Prepare(GroupListParams parameter)
        {
            SetupTitle(parameter);
            InitializeViewModelWithParams(parameter.Product, parameter.OrderDetail, parameter.IsPending);
        }

        private void SetupTitle(GroupListParams parameter)
        {
            this.Title = parameter.ParentMenuSection.Name;
        }

        /// <summary>
        /// Initializes the view model with parameters.
        /// </summary>
        /// <param name="product">Product.</param>
        /// <param name="orderDetail">Order detail.</param>
        /// <param name="isPending">If set to <c>true</c> is pending.</param>
        protected void InitializeViewModelWithParams(Product product, OrderDetail orderDetail = null, bool isPending = false)
        {
            SetProduct(product);

            if (orderDetail != null)
            {
                AddButtonText = Strings["OrderUpdateButton"];
                if (!orderDetail.IsGroupObserved() && orderDetail.IsObserved())
                {
                    orderDetail.ClearDetailsAndComment();
                    if (!isPending)
                    {
                        orderDetail.Product = this.Product;
                    }
                }
                this.OrderDetail = orderDetail;
                this.Quantity = this.OrderDetail.Quantity;
            }
            else
            {
                AddButtonText = Strings["OrderAddButton"];
                this.OrderDetail = this.orderManager.CreateOrderDetailByProduct(this.Product);
            }

            this.isPending = isPending;
            if (this.isPending)
            {
                this.ProductPrice = 0;
            }
            else
            {
                this.ProductPrice = this.Product.Price;
            }
        }

        #endregion

        #region lifecycle
        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await CancelToken();
            if (this.firstTime)
            {
                this.firstTime = false;
                await Load();
                RefreshAmount();
            }
        }
        #endregion


        #region results

        private async Task CancelToken()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                await Task.Delay(200);
            }
            base.CancellationTokenSource(this.productCancellationToken);
            base.CancellationTokenSource(this.commentCancellationToken);
        }

        private async void OnProductSelected(Group group, ProductGroupResult productGroupResult)
        {
            if (!(group.MultipleSelection.HasValue && group.MultipleSelection.Value))
            {
                if (productGroupResult.Products.Any())
                {
                    this.OrderDetail = await this.orderManager.InsertOrUpdateProductInCompountProduct(this.Product, productGroupResult.Products.FirstOrDefault(), this.OrderDetail, this.isPending, this.Quantity);
                }
                else
                {
                    var detailToRemove = this.OrderDetail.Details.FirstOrDefault((detail) => detail.GroupId == group.Id);
                    if (detailToRemove != null)
                    {
                        this.OrderDetail.Details.Remove(detailToRemove);
                    }
                }

            }
            else
            {
                this.OrderDetail = await this.orderManager.InsertOrUpdateProductsInProduct(this.Product, group, productGroupResult.Products, this.OrderDetail, this.Quantity);
            }

            await Reload();
            RefreshAmount();
        }

        private async void OnProductComment(string newComment)
        {
            this.comment = newComment;
            await Reload();
        }
        #endregion

        #region private methods
        private void SetProduct(Product product)
        {
            this.Product = product;
        }

        private async Task Load()
        {
            var result = await this.groupBL.FindListByProductId(this.Product.Id);
            this.Product.Groups = result;
            var list = GroupItemView.BuildList(result,
                                               this.comment,
                                               this.OrderDetail,
                                               this.isPending,
                                               this.Product.IsCompoundPromo()
                                               && !this.isPending);
            MergeWithOrder(list);
            this.GroupList.AddRange(list);
        }

        protected Task Reload()
        {
            this.GroupList.Clear();
            return Load();
        }

        private void MergeWithOrder(List<GroupItemView> list)
        {
            if (this.OrderDetail != null && this.OrderDetail.Details.Any())
            {
                foreach (var orderDetail in OrderDetail.Details)
                {
                    FindAndSetOrderDetailInGroup(list, orderDetail);
                }
            }
            else if (this.isPending)
            {
                FindAndSetOrderDetailInGroup(list, this.OrderDetail);
            }
        }

        private static void FindAndSetOrderDetailInGroup(List<GroupItemView> list, OrderDetail orderDetail)
        {
            GroupItemView groupItem = FindGroupItemByOrderDetail(list, orderDetail);
            SetOrderDetailInGroupItem(orderDetail, groupItem);
        }

        private static void SetOrderDetailInGroupItem(OrderDetail orderDetail, GroupItemView groupItem)
        {
            if (groupItem != null)
            {
                groupItem.SetProduct(orderDetail.Product, orderDetail.Quantity);
                if (groupItem.Status == 0 && orderDetail.Status.HasValue)
                {
                    groupItem.Status = (int)orderDetail.Status;
                }
            }
        }

        private static GroupItemView FindGroupItemByOrderDetail(List<GroupItemView> list, OrderDetail orderDetail)
        {
            return list.FirstOrDefault((filter) => filter.IsGroup && filter.Group.Id == orderDetail.GroupId);
        }

        protected void RefreshAmount()
        {
            if (this.OrderDetail != null)
            {
                this.OrderDetail.CalculateAmount();
                Amount = this.OrderDetail.Amount.Value;
            }
            else
            {
                Amount = this.Product.Price;
            }
        }

        #endregion

        #region actions

        private async Task OnGroupItemClick(GroupItemView groupItemView)
        {
            if (groupItemView.IsGroup)
            {
                if (this.ValidateGroupType(groupItemView))
                {
                    await OpenProductsByGroup(groupItemView);
                }
            }
            else
            {
                await OpenProductComment();

            }
        }

        private async Task OpenProductsByGroup(GroupItemView groupItemView)
        {
            this.productCancellationToken = new CancellationTokenSource();
            var group = groupItemView.Group;
            List<Product> selectedProducts = null;
            selectedProducts = FindListProductByGroup(group);
            var productGroupResult = await navigationController.OpenProductByGroup(group, this.productCancellationToken.Token, false, null, selectedProducts);
            this.productCancellationToken = null;
            if (productGroupResult != null)
            {
                OnProductSelected(group, productGroupResult);
            }
        }

        private List<Product> FindListProductByGroup(Group group)
        {
            return this.OrderDetail.Details.Where((detail) => detail.GroupId.HasValue && detail.GroupId.Value == group.Id).Select((arg) => arg.Product).ToList();
        }

        private bool ValidateGroupType(GroupItemView groupItemView)
        {
            if (!this.Product.IsCompoundPromoEqual() || !groupItemView.Group.SendLater)
            {
                return true;
            }
            else
            {
                this.dialogs.ShowError(string.Format(Strings["ProductEqualErrorSelectedSendLater"], this.Title.ToLower()));
                return false;
            }
        }

        private async Task OpenProductComment()
        {
            this.commentCancellationToken = new CancellationTokenSource();
            var newComment = await this.navigationController.OpenProductComment(this.comment, this.commentCancellationToken.Token);
            this.commentCancellationToken = null;
            if (newComment != null)
            {
                OnProductComment(newComment);
            }
        }


        private async Task SaveOrderDetail()
        {
            if (ValidateOrderDetail())
            {
                var progress = this.dialogs.ProgressDialog(Strings["OrderDetailSavingMessage"]);
                await progress.Show();
                try
                {
                    this.OrderDetail.Comment = this.comment;
                    if (this.isPending)
                    {
                        await this.orderManager.SelectProductByPendingOrderDetail(this.OrderDetail.Product, this.OrderDetail);
                    }
                    else
                    {
                        await this.orderManager.InsertOrUpdateOrderDetail(this.OrderDetail);
                    }

                    await progress.Hide();
                    await navigationController.Back(this, ProductAddedResult.Build(true));
                }
                catch (Exception e)
                {
                    this.logger.Error(TAG, e.StackTrace);
                    await progress.Hide();
                    await this.dialogs.ShowError(Strings["OrderDetailSaveErrorMessage"]);
                }
            }
        }

        private bool ValidateOrderDetail()
        {
            if (this.isPending)
            {
                if (this.OrderDetail.Product == null)
                {
                    this.dialogs.ShowError(Strings["PendingProductIncomplete"]);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (this.OrderDetail == null)
                {
                    ShowErrorRequiredGroups();
                    return false;
                }
                else if (this.OrderDetail.Details.FirstOrDefault(detail => detail.IsObserved()) != null)
                {
                    this.dialogs.ShowError(Strings["CompoundPendingFixObservation"]);
                    return false;
                }
                else
                {
                    var groupValidation = ValidateGroupsRequired();
                    if (!groupValidation)
                    {
                        ShowErrorRequiredGroups();
                        return groupValidation;
                    }
                }
            }
            return true;
        }

        private void ShowErrorRequiredGroups()
        {
            var addMessage = string.IsNullOrEmpty(this.Title) ? this.Product.Name : this.Title;
            this.dialogs.ShowError(string.Format(Strings["CompoundProductIncompletGroupError"], addMessage.ToLower()));
        }

        private bool ValidateGroupsRequired()
        {
            var groupsRequired = GroupList.Where((arg) => arg.IsGroup && !arg.Group.IsOptional()).Select((arg) => arg.Group).ToList();
            if (groupsRequired.Any())
            {
                foreach (var groupRequired in groupsRequired)
                {
                    var exist = OrderDetail.Details.Exists((detail) => detail.GroupId.HasValue && detail.GroupId.Value == groupRequired.Id);
                    if (!exist)
                    {
                        return false;
                    }
                }

            }
            return true;
        }
        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "product_compound_detail";
        }
        #endregion

    }

    public class GroupListParams
    {

        private readonly Product product;
        private readonly MenuSection parentMenuSection;
        private OrderDetail orderDetail;
        private readonly bool isPending;
        private GroupListParams(MenuSection parentMenuSection, Product product, bool isPending = false)
        {
            this.product = product;
            this.parentMenuSection = parentMenuSection;
            this.isPending = isPending;
        }

        public Product Product
        {
            get => this.product;
        }

        public OrderDetail OrderDetail
        {
            get => this.orderDetail;
        }

        public MenuSection ParentMenuSection
        {
            get => this.parentMenuSection;
        }

        public bool IsPending
        {
            get => this.isPending;
        }

        public static GroupListParams Build(MenuSection parentMenuSection, Product product, bool isPending)
        {
            return new GroupListParams(parentMenuSection, product, isPending);
        }

        public void WithOrderDetail(OrderDetail orderDetail)
        {
            this.orderDetail = orderDetail;
        }
    }

    public class GroupItemView
    {
        public static GroupItemView Build(Group group = null)
        {
            var groupItemView = new GroupItemView();
            if (group != null)
            {
                groupItemView.Group = group;
                groupItemView.Name = group.Name;
            }
            else
            {
                groupItemView.Name = I18N.Current["ProductDetailSpecialTitle"];
            }

            return groupItemView;
        }

        public static List<GroupItemView> BuildList(List<Group> groups, string comment = null, OrderDetail orderDetail = null, bool isPending = false, bool hiddenSendLater = false)
        {
            if (hiddenSendLater)
            {
                groups = groups.Where((filter) => filter.SendLater == false).ToList();
            }
            if (orderDetail != null && isPending)
            {
                groups = groups.FindAll((filter) => filter.Id == orderDetail.GroupId)
                               .Select((group) => { group.SendLater = false; return group; })
                               .ToList();
            }
            var list = groups.Select(GroupItemView.Build).ToList();
            var commentItem = GroupItemView.Build();
            if (!string.IsNullOrWhiteSpace(comment))
            {
                commentItem.Description = comment;
            }
            list.Add(commentItem);
            return list;
        }

        public string Name
        {
            get;
            set;
        }

        public Group Group
        {
            get;
            set;
        }

        private string description;
        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(this.description) && !this.HasProduct)
                {

                    if (Group != null)
                    {
                        if (Group.SendLater)
                        {
                            Description = I18N.Current["SendLaterHelpText"];
                        }
                        else
                        {
                            Description = this.Group.IsOptional() ? I18N.Current["GroupOptionalDescriptionHelpText"] : I18N.Current["GroupDescriptionHelpText"];
                        }
                    }

                }
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }

        public double Price
        {
            get;
            set;
        }

        public bool IsGroup
        {
            get => this.Group != null;
        }

        public int Status
        {
            get;
            set;
        }

        private bool hasProduct = false;
        private bool HasProduct
        {
            set
            {
                if (!this.HasProduct && value)
                {
                    this.Description = string.Empty;
                }
                this.hasProduct = value;
            }
            get
            {
                return this.hasProduct;
            }
        }
        private GroupItemView()
        {
        }

        public void SetProduct(Product product, int quantity = 1)
        {
            FormatDescriptionByProduct(product);
            if (product.Price > 0)
            {
                Price += quantity * product.Price;
            }
        }

        /// <summary>
        /// Formats the description by product.
        /// </summary>
        /// <param name="product">Product.</param>
        private void FormatDescriptionByProduct(Product product)
        {
            this.HasProduct = true;
            var separator = !string.IsNullOrEmpty(Description) ? ", " : string.Empty;
            Description = $"{Description}{separator}{ product.Name}";
        }
    }
}
