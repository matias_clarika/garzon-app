﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Component;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Navigation.Result;
    using MvvmCross.Core.ViewModels;

    public class ProductGroupListViewModel : GarzonBaseViewModel, IMvxViewModel<ProductGroupListParams, ProductGroupResult>
    {
        #region dependency
        protected readonly IProductGroupBL productGroupBL;
        protected readonly IOrderManager orderManager;
        #endregion
        #region constants
        private readonly static string TAG = typeof(ProductGroupListViewModel).FullName;
        #endregion
        #region fields
        private bool firstTime = true;
        private Group parentGroup;
        private string title;
        private string subtitle;
        public bool isPullToRefreshEnabled = false;
        public bool isRefreshing = false;
        public bool isLoadingMain = false;
        public bool isInfiniteEnabled = false;
        public bool _isLoadingInfinite = false;
        private Product SelectedProduct
        {
            get;
            set;
        }
        #endregion

        #region binding
        public Group ParentGroup
        {
            get => this.parentGroup;
            set => SetProperty(ref this.parentGroup, value);
        }

        public SelectableObservableCollection<Product> ProductList { get; } = new SelectableObservableCollection<Product>();
        public IMvxAsyncCommand SaveCommand => new MvxAsyncCommand(OnSave);

        //page
        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }
        //header
        public string Subtitle
        {
            get => subtitle;
            set => SetProperty(ref subtitle, value);
        }
        //general
        public bool IsPullToRefreshEnabled
        {
            get => isPullToRefreshEnabled;
            set => SetProperty(ref isPullToRefreshEnabled, value);
        }
        public bool IsRefreshing
        {
            get => isRefreshing;
            set => SetProperty(ref isRefreshing, value);
        }
        public bool IsLoadingMain
        {
            get => isLoadingMain;
            set => SetProperty(ref isLoadingMain, value);
        }
        public bool IsInfiniteEnabled
        {
            get => isInfiniteEnabled;
            set => SetProperty(ref isInfiniteEnabled, value);
        }
        public bool IsLoadingInfinite
        {
            get => _isLoadingInfinite;
            set => SetProperty(ref _isLoadingInfinite, value);
        }

        #endregion

        #region to order
        private OrderDetail pendingOrderDetail;
        #endregion
        public ProductGroupListViewModel(ILogger logger,
                                             IAnalytics analytics,
                                             INavigationController navigationController,
                                             AppDefaultConfig appConfig,
                                             IDialogs dialogs,
                                             IProductGroupBL productGroupBL,
                                             IOrderManager orderManager) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            this.productGroupBL = productGroupBL;
            this.orderManager = orderManager;
        }

        #region IMvxViewModel
        public void Prepare(ProductGroupListParams parameter)
        {
            InitializeWithParams(parameter.ParentGroup, parameter.PendingOrderDetail, parameter.SelectedProduct);
        }
        /// <summary>
        /// Initializes the with parameters.
        /// </summary>
        /// <param name="parentGroup">Parent group.</param>
        /// <param name="pendingOrderDetail">Pending order detail.</param>
        /// <param name="selectedProduct">Selected product.</param>
        protected void InitializeWithParams(Group parentGroup, OrderDetail pendingOrderDetail = null, Product selectedProduct = null)
        {
            this.ParentGroup = parentGroup;
            this.pendingOrderDetail = pendingOrderDetail;
            this.SelectedProduct = selectedProduct;
        }
        #endregion
        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            SetupUI();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            if (this.firstTime)
            {
                this.firstTime = false;
                Load();
            }
        }

        #endregion

        #region actions
        protected virtual async Task OnSave()
        {
            if (await ValidateSelection())
            {
                if (this.SelectedProduct != null && this.pendingOrderDetail != null)
                {
                    await SelectPendingOrderDetail(this.SelectedProduct);
                }
                await navigationController.Back(this, ProductGroupResult.Build().WithProduct(this.ProductList.SelectedItems.FirstOrDefault()));
            }
        }

        protected virtual async Task<bool> ValidateSelection()
        {
            if (this.ParentGroup.IsOptional() || this.ProductList.SelectedItems.Any())
            {
                return true;
            }
            else
            {
                this.dialogs.ShowError(Strings["ProductGroupRequiredSelected"]);
                return false;
            }
        }
        #endregion
        #region private methods

        protected virtual void SetupUI()
        {
            Title = ParentGroup.Name;
            var labelByLanguage = this.ParentGroup.FindLabelByLenguage(CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);
            if (string.IsNullOrEmpty(labelByLanguage))
            {
                this.SetDefaultSubtitle();
            }
            else
            {
                Subtitle = labelByLanguage;
            }
        }

        protected virtual void SetDefaultSubtitle()
        {
            Subtitle = string.Format(Strings["ProductGroupHelpText"], ParentGroup.Name);
        }

        private async Task Load()
        {
            IsLoadingMain = true;
            try
            {
                var products = await this.productGroupBL.FindListByGroupId(ParentGroup.Id, pendingOrderDetail);
                AddProductToList(products);
            }
            catch (Exception e)
            {
                this.logger.Error(TAG, e.Message);
                await navigationController.Back(this);
            }
            finally
            {
                IsLoadingMain = false;
            }
        }

        protected virtual void AddProductToList(List<Product> products)
        {
            if (products != null && products.Any())
            {
                foreach (var product in products)
                {
                    this.ProductList.Add(product, this.SelectedProduct != null && product.Id == SelectedProduct.Id);
                }
            }
        }

        private async Task SelectPendingOrderDetail(Product product)
        {
            var orderDetail = await this.orderManager.SelectProductByPendingOrderDetail(product, this.pendingOrderDetail);
        }

        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "product_aditional_aditional";
        }
        #endregion
    }

    public class ProductGroupListParams
    {
        private readonly Group parentGroup;
        private readonly bool isPending;
        private OrderDetail pendingOrderDetail;

        public Product SelectedProduct
        {
            get;
            private set;
        }
        private ProductGroupListParams(Group parentGroup, bool isPending)
        {
            this.parentGroup = parentGroup;
            this.isPending = isPending;
        }
        /// <summary>
        /// Build the specified parentGroup and isPending.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="parentGroup">Parent group.</param>
        /// <param name="isPending">If set to <c>true</c> is pending.</param>
        public static ProductGroupListParams Build(Group parentGroup, bool isPending)
        {
            return new ProductGroupListParams(parentGroup, isPending);
        }
        /// <summary>
        /// Withs the selected product.
        /// </summary>
        /// <returns>The selected product.</returns>
        /// <param name="selectedProduct">Selected product.</param>
        public ProductGroupListParams WithSelectedProduct(Product selectedProduct)
        {
            this.SelectedProduct = selectedProduct;
            return this;
        }
        internal void WithPendingOrderDetail(OrderDetail pendingOrderDetail)
        {
            this.pendingOrderDetail = pendingOrderDetail;
        }

        public Group ParentGroup
        {
            get => this.parentGroup;
        }

        public bool IsPending
        {
            get => this.isPending;
        }

        public OrderDetail PendingOrderDetail
        {
            get => this.pendingOrderDetail;
        }

    }
}
