﻿using System;
namespace Garzon.Forms.UI.ViewModels
{
    public class ProductDetailResult
    {
        #region constructor
        private ProductDetailResult(ProductDetailResultBuilder builder)
        {
            _productAdded = builder.ProductAdded;
        }
        #endregion

        #region fields inmutables
        private readonly bool _productAdded;
        #endregion

        #region getters
        public bool ProductAdded
        {
            get => _productAdded;
        }
        #endregion

        #region builder
        public class ProductDetailResultBuilder
        {
            #region constructor
            private ProductDetailResultBuilder(bool productAdded)
            {
                _productAdded = productAdded;
            }
            #endregion
            #region fields

            private readonly bool _productAdded;
            public bool ProductAdded
            {
                get => _productAdded;
            }
            #endregion

            public static ProductDetailResultBuilder Builder(bool productAdded = false)
            {
                return new ProductDetailResultBuilder(productAdded);
            }

            public ProductDetailResult Build()
            {
                return new ProductDetailResult(this);
            }
        }
        #endregion
    }
}
