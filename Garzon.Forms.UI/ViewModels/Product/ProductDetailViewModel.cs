﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.BL;
    using Garzon.Forms.UI.Navigation.Result;

    public class ProductDetailViewModel : GroupListViewModel, IMvxViewModel<ParamsToProductDetail, ProductAddedResult>
    {
        #region commands
        public IMvxAsyncCommand TapLessCounterCommand => new MvxAsyncCommand(CounterLess);
        public IMvxAsyncCommand TapMoreCounterCommand => new MvxAsyncCommand(CounterMore);
        #endregion

        #region constants
        private readonly string TAG = typeof(ProductDetailViewModel).FullName.ToString();
        #endregion

        #region constructor
        public ProductDetailViewModel(ILogger logger,
                                      IAnalytics analytics,
                                      INavigationController navigationController,
                                      AppDefaultConfig appConfig,
                                      IOrderManager orderManager,
                                      IGroupBL groupBL,
                                      IDialogs dialogs) : base(logger, analytics, navigationController, appConfig, dialogs, groupBL, orderManager)
        {
            Quantity = base.appConfig.MIN_QUANTITY_PRODUCT_ADD_TO_ORDER;
        }
        #endregion

        #region Implementation of IMvxViewModel
        public void Prepare(ParamsToProductDetail parameter)
        {
            base.InitializeViewModelWithParams(parameter.Product, parameter.OrderDetail);
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            this.SetupTitle();
            await CalculateTotalAmount();
        }

        private void SetupTitle()
        {
            if (!string.IsNullOrEmpty(this.Product.Image))
            {
                Title = this.Product.Name;
            }
        }
        #endregion

        #region calculate methods
        private async Task CalculateTotalAmount()
        {
            await Task.Factory.StartNew(() =>
             {
                 base.OrderDetail.CalculateAmount();
                 Amount = OrderDetail.Amount.Value;
             });
        }
        #endregion

        #region actions
        private async Task CounterLess()
        {
            if (Quantity > 1)
            {
                base.Quantity--;
                base.OrderDetail.Quantity = Quantity;
                await Reload();
                RefreshAmount();
            }
        }

        private async Task CounterMore()
        {
            base.Quantity++;
            OrderDetail.Quantity = Quantity;
            await Reload();
            RefreshAmount();
        }
        #endregion

        #region private methods
        private async Task Back(bool isAdded = false)
        {
            await this.navigationController.Back(this,
                                                  ProductAddedResult.Build(isAdded));
        }
        #endregion

    }


    #region params to navigate
    public class ParamsToProductDetail
    {
        public Product Product
        {
            get;
            set;
        }

        public OrderDetail OrderDetail
        {
            get;
            set;
        }

        public ParamsToProductDetail(Product product, OrderDetail orderDetail = null)
        {
            Product = product;
            OrderDetail = orderDetail;
        }

    }
    #endregion

}
