﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;
using Garzon.DataObjects;
using System.Linq;
using MvvmCross.Core.ViewModels;
using Garzon.Forms.UI.Facade.Auth;
using System.Threading;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Core.Views.Component;
using Xamarin.Forms;
using Garzon.Core.EventBus;
using Garzon.DAL.DataSource.Remote.Implementation;
using Garzon.DAL.DataSource.Remote.Exceptions;

namespace Garzon.Forms.UI.ViewModels
{
    public class CallWaiterViewModel : OrderUserValidationViewModel
    {
        #region dependency
        readonly IWaiterBL _waiterBL;
        #endregion
        #region fields to binding
        public SelectableObservableCollection<WaiterCallOption> Items { get; } = new SelectableObservableCollection<WaiterCallOption>();
        #endregion

        #region command
        public IMvxAsyncCommand SendCommand => new MvxAsyncCommand(SendOptions);
        #endregion

        #region cancellation token
        CancellationTokenSource _loginCancellationToken;
        IEventToken _tokenRetrySendOption;
        #endregion
        #region constructor
        public CallWaiterViewModel(ILogger logger,
                                   IAnalytics analytics,
                                   INavigationController navigationController,
                                   AppDefaultConfig appConfig,
                                   IDialogs dialogs,
                                   IOrderManager orderManager,
                                   IUserSessionManager userSessionManager,
                                   IWaiterBL waiterBL,
                                   IEventBus eventBus) : base(logger, analytics, navigationController, appConfig, orderManager, dialogs, eventBus, userSessionManager)
        {
            _waiterBL = waiterBL;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            Load();

        }

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            CancelToken();
            await Subscribe();
        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            Unsuscribe();
        }

        private async Task CancelToken()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                await Task.Delay(200);
            }
            CancellationTokenSource(_loginCancellationToken);
        }
        #endregion

        #region events

        private async Task Subscribe()
        {
            _tokenRetrySendOption = await _eventBus.SubscribeOnMainThread<WaiterCallRetryEvent>(RetrySendOptions);
        }

        private void Unsuscribe()
        {
            _eventBus.Unsubscribe<WaiterCallRetryEvent>(_tokenRetrySendOption);
        }
        #endregion
        #region waiter call options
        private async Task Load()
        {
            try
            {
                long placeId = (await orderManager.GetCurrentPlace()).Id;
                var options = await _waiterBL.FindByPlaceId(placeId);
                AddItems(options);
            }
            catch (Exception e)
            {
                Load();
            }
        }
        /// <summary>
        /// Adds the items.
        /// </summary>
        /// <param name="options">Options.</param>
        private void AddItems(List<WaiterCallOption> options)
        {
            if (options != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Items.Add(options);
                    CheckOptions(options);
                });
            }
        }
        /// <summary>
        /// Checks the options.
        /// </summary>
        /// <returns>The options.</returns>
        /// <param name="options">Options.</param>
        private async Task CheckOptions(List<WaiterCallOption> options)
        {
            if (options == null || options.Count == 0)
            {
                await this.dialogs.ShowError(Strings["WaiterCallOptionsNotFound"]);
                await navigationController.Back(this);
            }
        }

        private bool ValidateSelectedOptions()
        {
            if (!Items.SelectedItems.Any())
            {
                this.dialogs.ShowError(Strings["WaiterCallOptionsNotSelected"]);
                return false;
            }
            else
            {
                return true;
            }

        }

        private async Task<bool> ValidateUserAndLocation()
        {

            if (orderManager.GetCurrentUser() != null
                && orderManager.GetCurrentOrder() != null
                && orderManager.GetCurrentTable() > 0)
            {
                return true;
            }
            else
            {
                return await RequestLogin();
            }
        }

        private async Task<bool> RequestLogin()
        {
            _loginCancellationToken = new CancellationTokenSource();
            LoginResult loginResult = await navigationController.OpenLogin(false, _loginCancellationToken.Token);
            _loginCancellationToken = null;

            if (await _userSessionManager.ProcessLoginResult(loginResult))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private async Task SendOptions()
        {
            if (ValidateSelectedOptions())
            {
                if (await ValidateUserAndLocation())
                {
                    var progress = this.dialogs.ProgressDialog(string.Empty);
                    await progress.Show();
                    try
                    {
                        var result = await _waiterBL.Call((await orderManager.GetCurrentPlace()).Id,
                                                          orderManager.GetCurrentSector(),
                                                          orderManager.GetCurrentTable(),
                                                          Items.SelectedItems.ToList());
                        await progress.Hide();
                        if (result)
                        {
                            await navigationController.Back(this);
                            await this.dialogs.ShowSuccess(Strings["CallWaiterSuccess"]);
                        }
                        else
                        {
                            ShowErrorWhenCallWaiter();
                        }
                    }
                    catch (WaiterCallBLException e)
                    {
                        await progress.Hide();
                        System.Diagnostics.Debug.WriteLine(e.Message);
                        ShowErrorWhenCallWaiter(Strings["CallWaiterExistError"]);
                    }
                    catch (TableNumberNotExistBLException e1)
                    {
                        await progress.Hide();
                        System.Diagnostics.Debug.WriteLine(e1.Message);
                        await IncorrectTableNumber();
                    }
                    catch (UserSessionException e2)
                    {
                        await progress.Hide();
                        System.Diagnostics.Debug.WriteLine(e2.Message);
                        await RedirectByReasonUserBlock();
                    }
                    catch (Exception e3)
                    {
                        await progress.Hide();
                        System.Diagnostics.Debug.WriteLine(e3.Message);
                        ShowErrorWhenCallWaiter();
                    }
                }
            }
        }


        private void RetrySendOptions(WaiterCallRetryEvent obj)
        {
            SendOptions();
        }

        private async Task IncorrectTableNumber()
        {
            await orderManager.SetCurrentSectorAndTable(-1, 0);
            await _eventBus.Publish(WaiterCallRetryEvent.Builder(this));
            ShowErrorWhenCallWaiter(Strings["TableNumberMessageError"]);
        }

        /// <summary>
        /// Shows the error when call waiter.
        /// </summary>
        /// <param name="message">Message.</param>
        private void ShowErrorWhenCallWaiter(string message = null)
        {
            if (String.IsNullOrWhiteSpace(message))
            {
                message = Strings["CallWaiterGenericError"];
            }

            this.dialogs.ShowError(message);
        }
        #endregion


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "waiter_call";
        }
        #endregion


    }

    class WaiterCallRetryEvent : Event
    {
        private WaiterCallRetryEvent(object sender) : base(sender)
        {
        }

        public static WaiterCallRetryEvent Builder(object sender)
        {
            return new WaiterCallRetryEvent(sender);
        }
    }

}
