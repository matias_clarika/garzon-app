﻿using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Navigation;
using MvvmCross.Core.ViewModels;

namespace Garzon.Forms.UI.ViewModels
{
    public abstract class ResultBaseViewModel<TResult> : GarzonBaseViewModel, IMvxViewModelResult<TResult> where TResult : class
    {
        protected ResultBaseViewModel(ILogger logger = null,
                                      IAnalytics analytics = null,
                                      INavigationController navigationController = null,
                                      AppDefaultConfig appConfig = null,
                                      IDialogs dialogs = null) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
        }
    }
}
