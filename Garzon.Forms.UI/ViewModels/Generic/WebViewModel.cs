﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Navigation;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;

namespace Garzon.Forms.UI.ViewModels
{
    public class WebViewModel : GarzonBaseViewModel, IMvxViewModel<WebParams>
    {
        private string title;
        private UrlWebViewSource url;
        private bool loading = true;
        private bool successLoading;
        #region bindings
        public string Title
        {
            get => this.title;
            set => SetProperty(ref this.title, value);
        }


        public UrlWebViewSource Url
        {
            get => this.url;
            set => SetProperty(ref this.url, value);
        }

        public bool Loading
        {
            get => this.loading;
            set => SetProperty(ref this.loading, value);
        }

        public bool SuccessLoading
        {
            get => this.successLoading;
            set => SetProperty(ref this.successLoading, value);
        }

        public IMvxAsyncCommand LoadingCommand => new MvxAsyncCommand(OnLoading);
        public IMvxAsyncCommand FinishCommand => new MvxAsyncCommand(OnFinish);
        public IMvxAsyncCommand CloseCommand => new MvxAsyncCommand(OnClose);

        #endregion

        public WebViewModel(ILogger logger,
                               IAnalytics analytics,
                               INavigationController navigationController,
                               AppDefaultConfig appConfig,
                               IDialogs dialogs) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
        }

        #region IMvxViewModel
        public void Prepare(WebParams parameter)
        {
            Title = parameter.Title;
            Url = new UrlWebViewSource
            {
                Url = parameter.Url
            };
        }
        #endregion

        private Task OnLoading()
        {
            return Task.Run(() =>
            {
                this.Loading = true;
                this.SuccessLoading = false;
            });
        }

        private Task OnFinish()
        {
            return Task.Run(() =>
            {
                this.Loading = false;
                this.SuccessLoading = true;
            });
        }


        private Task OnClose()
        {
            return this.navigationController.Back(this);
        }

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "web";
        }
        #endregion

    }

    public class WebParams
    {

        readonly string _title;
        readonly string _url;

        public string Title
        {
            get => _title;
        }

        public string Url
        {
            get => _url;
        }

        private WebParams(string title, string url)
        {
            _title = title;
            _url = url;
        }

        public static WebParams Builder(string title, string url)
        {
            return new WebParams(title, url);
        }

    }

}
