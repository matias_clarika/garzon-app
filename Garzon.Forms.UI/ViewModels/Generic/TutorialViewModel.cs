﻿namespace Garzon.Forms.UI.ViewModels.Generic
{
    using System;
    using System.Collections.Generic;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Forms.UI.Navigation;

    public class TutorialViewModel : GarzonBaseViewModel
    {
        protected TutorialViewModel(ILogger logger,
                                    IAnalytics analytics,
                                    INavigationController navigationController,
                                    AppDefaultConfig appConfig,
                                    IDialogs dialogs) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
        }


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "tutorial";
        }
        #endregion
    }
}
