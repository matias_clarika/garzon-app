﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using Garzon.Core.Analytics;
    using System.Threading;
    using Garzon.Core.Utils;
    using System.Collections.Generic;
    using Xamarin.Forms;
    using Garzon.Core.EventBus;
    using Garzon.Forms.UI.Views;
    using System.Linq;
    using Garzon.Forms.UI.EventBus.OrderNotifications;

    public class MenuSectionViewModel : BaseMyOrderViewModel, IMvxViewModel<Params>
    {
        #region dependency
        protected readonly IMenuSectionBL menuSectionBL;
        protected readonly IProductRecommendedBL productRecommendedBL;
        protected readonly ITutorialBL tutorialBL;
        protected readonly IProductMenuSectionBL productMenuSectionBL;
        #endregion

        #region fields to binding

        //product recommended
        public MvxObservableCollection<RecommendedItemView> RecommendedList { get; } = new MvxObservableCollection<RecommendedItemView>();

        public Boolean _productRecommendedVisibility = false;

        public Boolean ProductRecommendedVisibility
        {
            get => _productRecommendedVisibility;
            set => SetProperty(ref _productRecommendedVisibility, value);
        }

        //menu sections
        public MvxObservableCollection<MenuSection> MenuSectionList { get; } = new MvxObservableCollection<MenuSection>();

        //general
        public Boolean _isLoading = false;

        public Boolean IsLoading
        {
            get => _isLoading;
            set => SetProperty(ref _isLoading, value);
        }

        public Boolean _isLoadingMain = false;

        public Boolean IsLoadingMain
        {
            get => _isLoadingMain;
            set => SetProperty(ref _isLoadingMain, value);
        }

        private string _icon = "res:icon.png";

        public string Icon
        {
            get => _icon;
            set => SetProperty(ref _icon, value);
        }

        private MenuSection _menuSection;

        public MenuSection MenuSection
        {
            get => _menuSection;
            set => SetProperty(ref _menuSection, value);
        }

        private string _title = "";

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private string _subtitle = "";
        public string Subtitle
        {
            get => _subtitle;
            set => SetProperty(ref _subtitle, value);
        }


        #endregion

        #region command

        public IMvxAsyncCommand PullToRefreshCommand => new MvxAsyncCommand(PullToRefresh);

        public IMvxAsyncCommand<MenuSection> ItemClickCommand => new MvxAsyncCommand<MenuSection>(ItemClick);

        public IMvxAsyncCommand<RecommendedItemView> RecommendedItemClickCommand => new MvxAsyncCommand<RecommendedItemView>(RecommendedItemClick);

        #endregion

        #region fields to pagination
        const int LIMIT = 99;
        const int LIMIT_PRODUCT_RECOMMENDED = 6;
        #endregion

        #region field
        protected Place _place;
        private bool _firstTime = true;
        protected new bool _isDailyMenu = false;
        private bool cancelAutoRefreshInRecommended = false;
        #endregion

        #region tokens
        private CancellationTokenSource productDetailCancellationToken;
        protected CancellationTokenSource pendingOrderDetailCancellationToken;
        private IEventToken orderChangeToken;
        private bool itemClickEnabled = true;
        #endregion
        #region constants
        private readonly string TAG = typeof(MenuSectionViewModel).FullName.ToString();
        #endregion

        #region constructor
        public MenuSectionViewModel(ILogger logger,
                                    IAnalytics analytics,
                                    INavigationController navigationController,
                                    AppDefaultConfig appConfig,
                                    IDialogs dialogs,
                                    IMenuSectionBL menuSectionBL,
                                    IProductRecommendedBL productRecommendedBL,
                                    IOrderManager orderManager,
                                    IEventBus eventBus,
                                    ITutorialBL tutorialBL,
                                    IProductMenuSectionBL productMenuSectionBL) : base(logger, analytics, navigationController, appConfig, orderManager, dialogs, eventBus)
        {
            this.menuSectionBL = menuSectionBL;
            this.productRecommendedBL = productRecommendedBL;
            this.tutorialBL = tutorialBL;
            this.productMenuSectionBL = productMenuSectionBL;
        }
        #endregion

        #region implementation of IMvxViewModel
        public void Prepare(Params parameter)
        {
            _place = parameter.Place;
            MenuSection = parameter.MenuSection;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            ConfigureTitle();
        }

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            CancelToken();
            await Subscribe();
            await RefreshUI();
            CheckTutorial();
        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            Unsubscribe();
        }
        #endregion

        #region events
        private async Task Subscribe()
        {
            this.orderChangeToken = await this.EventBus.Subscribe<OrderInKitchentEvent>(OnOrderInKitchen);
        }

        private async void OnOrderInKitchen(OrderInKitchentEvent obj)
        {
            cancelAutoRefreshInRecommended = false;
            await RefreshUI();
        }

        private void Unsubscribe()
        {
            this.EventBus.Unsubscribe<OrderChangeEvent>(this.orderChangeToken);
        }

        private async Task OnOrderDetailPendingSelected(bool isAdded)
        {
            if (isAdded)
            {
                this.cancelAutoRefreshInRecommended = true;
                await GoToMyOrder();
                if (Device.RuntimePlatform == Device.Android)
                {
                    await Task.Delay(500);
                    this.cancelAutoRefreshInRecommended = false;
                }
            }
        }
        #endregion
        #region general
        protected virtual void ConfigureTitle()
        {
            if (MenuSection != null)
            {
                Title = MenuSection.Name;
                Subtitle = MenuSection.Name;
            }
            else
            {
                Title = $"{_place.Name}";
            }
        }

        private async void CheckTutorial()
        {
            bool isInResto = await this.OrderIsInResto();
            if (isInResto && await this.tutorialBL.GetHowToCallCounterTutorial())
            {
                await navigationController.OpenHowToCallCounterTutorial();
            }
        }

        private void CancelToken()
        {
            CancellationTokenSource(this.productDetailCancellationToken);
            CancellationTokenSource(this.pendingOrderDetailCancellationToken);
        }

        private async Task RefreshUI()
        {
            if (_firstTime)
            {
                await LoadProductRecommended();
                await LoadMenuSections();
                _firstTime = false;
            }
            else
            {
                if (!cancelAutoRefreshInRecommended)
                {
                    await LoadProductRecommended();
                }
                else
                {
                    cancelAutoRefreshInRecommended = false;
                }

            }
        }
        #endregion

        #region private methods product recommended

        private async Task LoadProductRecommended()
        {
            if (_place != null)
            {
                IPaggination paggination = new Paggination(0, LIMIT_PRODUCT_RECOMMENDED);
                try
                {
                    List<OrderDetail> pendingRequests = await this.orderManager.GetPendingRequestOrderDetailList();
                    var productsRecommended = await this.productRecommendedBL.Find(_place.Id, paggination);

                    AddProductRecommended(ConvertPendingAndProductsToRecommendedList(pendingRequests, productsRecommended));
                }
                catch (Exception e)
                {
                    CheckProductRecommended();
                    base.logger.Error(TAG, e.Message);
                }
                finally
                {
                }
            }
        }


        private List<RecommendedItemView> ConvertPendingAndProductsToRecommendedList(List<OrderDetail> pendingRequests,
                                                                                      List<Product> productsRecommended)
        {
            List<RecommendedItemView> result = new List<RecommendedItemView>();
            if (pendingRequests != null && pendingRequests.Any())
            {
                result.AddRange(pendingRequests.Select(RecommendedItemView.Build));
            }
            if (productsRecommended != null && productsRecommended.Any())
            {
                result.AddRange(productsRecommended.Select(RecommendedItemView.Build));
            }
            return result;
        }

        private void AddProductRecommended(List<RecommendedItemView> recommendedProducts)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                RecommendedList.Clear();
                RecommendedList.AddRange(recommendedProducts);
                CheckProductRecommended();
            });
        }

        private void CheckProductRecommended()
        {
            ProductRecommendedVisibility = RecommendedList.Count > 0;
        }

        #endregion
        #region private methods menu sections
        protected virtual async Task LoadMenuSections()
        {
            try
            {
                IsLoadingMain = MenuSectionList.Count == 0;
                IsLoading = !IsLoadingMain;

                IPaggination paggination = new Paggination(MenuSectionList.Count, LIMIT);

                long menuSectionId = -1;
                if (MenuSection != null)
                {
                    menuSectionId = MenuSection.Id;
                }
                var menuSections = await menuSectionBL.Find(paggination, menuSectionId);
                AddMenuSections(menuSections);

            }
            catch (Exception e)
            {
                base.logger.Error(TAG, e.Message);
            }
            finally
            {
                IsLoading = false;
                IsLoadingMain = false;
            }
        }

        protected virtual void AddMenuSections(List<MenuSection> list)
        {
            MenuSectionList.AddRange(list);
        }
        #endregion

        #region actions

        private async Task PullToRefresh()
        {
            MenuSectionList.Clear();
            await LoadMenuSections();
        }

        protected async Task ItemClick(MenuSection menuSection)
        {
            if (this.itemClickEnabled)
            {
                this.itemClickEnabled = false;
                await OpenMenuSection(menuSection);
                await Task.Delay(500);
                this.itemClickEnabled = true;
            }
        }

        protected async Task RecommendedItemClick(RecommendedItemView recommendedItemView)
        {
            switch (recommendedItemView.Type)
            {
                case RecommendedItemType.PRODUCT_RECOMMENDED:
                    await OpenProduct(recommendedItemView.Product);
                    break;
                case RecommendedItemType.ORDER_DETAIL_PENDING_REQUEST:
                    await OpenPendingOrderDetail(recommendedItemView.OrderDetail);
                    break;
            }
        }

        private async Task OpenPendingOrderDetail(OrderDetail orderDetail)
        {
            var parentProduct = await this.productMenuSectionBL.FindByID(orderDetail.ParentProductId.Value);
            if (parentProduct.IsCompoundPromoEqual())
            {
                await this.orderManager.SelectProductByPendingOrderDetail(orderDetail.Product, orderDetail);
                await GoToMyOrder();
            }
            else
            {
                var parentMenuSection = await menuSectionBL.FindById(parentProduct.MenuSectionId.Value);
                this.pendingOrderDetailCancellationToken = new CancellationTokenSource();
                var result = await navigationController.OpenGroupsByProduct(parentMenuSection, parentProduct, orderDetail, orderDetail.IsPendingRequest, this.pendingOrderDetailCancellationToken.Token);
                this.pendingOrderDetailCancellationToken = null;
                await OnOrderDetailPendingSelected(result != null && result.IsAdded);
            }

        }

        private Task OpenProduct(Product product)
        {
            if (product.Type == (int)ProductType.SIMPLE)
            {
                return OpenProductDetail(product);
            }
            else
            {
                return OpenProductCompound(product);
            }
        }

        private async Task OpenProductCompound(Product product)
        {
            this.productDetailCancellationToken = new CancellationTokenSource();
            var menuSection = await this.menuSectionBL.FindById(product.MenuSectionId.Value);
            await navigationController.OpenGroupsByProduct(menuSection, product, null, false, this.productDetailCancellationToken.Token);
            this.productDetailCancellationToken = null;
        }

        private Task OpenMenuSection(MenuSection menuSection)
        {
            if (menuSection.DeepLink == DeepLink.GoToProduct
                            && String.IsNullOrEmpty(menuSection.Image))
            {
                menuSection.Image = MenuSection.Image;
            }
            return navigationController.OpenPageByMenuSection(_place, menuSection, null);
        }


        private async Task OpenProductDetail(Product product)
        {
            productDetailCancellationToken = new CancellationTokenSource();
            await navigationController.OpenProductDetail(product,
                                                          null,
                                                          productDetailCancellationToken.Token);
            productDetailCancellationToken = null;
        }

        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "menu_section";
        }
        #endregion
    }

    public class Params
    {
        #region fields
        public MenuSection MenuSection
        {
            get;
            set;
        }

        public Place Place
        {
            get;
            set;
        }

        public OrderDetail OrderDetail
        {
            get;
            set;
        }
        #endregion

        #region constructor
        public Params(Place place,
                      MenuSection menuSection = null,
                      OrderDetail orderDetail = null)
        {
            Place = place;
            MenuSection = menuSection;
            OrderDetail = orderDetail;
        }
        #endregion
    }
}
