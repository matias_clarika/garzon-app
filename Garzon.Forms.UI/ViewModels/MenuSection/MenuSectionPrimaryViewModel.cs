﻿using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.EventBus;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;

namespace Garzon.Forms.UI.ViewModels
{
    public class MenuSectionPrimaryViewModel : MenuSectionViewModel
    {
        #region constructor
        public MenuSectionPrimaryViewModel(ILogger logger,
                                           IAnalytics analytics,
                                           INavigationController navigationController,
                                           AppDefaultConfig appConfig,
                                           IDialogs dialogs,
                                           IMenuSectionBL menuSectionBL,
                                           IProductRecommendedBL productRecommendedBL,
                                           IOrderManager orderManager,
                                           IEventBus eventBus,
                                           ITutorialBL tutorial,
                                           IProductMenuSectionBL productMenuSectionBL) : base(logger, analytics, navigationController, appConfig, dialogs, menuSectionBL, productRecommendedBL, orderManager, eventBus, tutorial, productMenuSectionBL)
        {
        }
        #endregion
    }
}
