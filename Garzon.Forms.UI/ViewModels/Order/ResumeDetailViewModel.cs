﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Xamarin.Forms;
    using Garzon.Core.Views.Format;
    using Garzon.BL;
    using Garzon.Core.EventBus;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Garzon.Core.Managers;

    public class ResumeDetailViewModel : OrderUserValidationViewModel
    {
        #region dependency
        readonly IFormatOrderDetail formatOrderDetail;
        readonly IFormatPrice formatPrice;
        readonly IRedirectManager redirectManager;
        #endregion

        #region constants
        const int LIMIT_MENU_SECTION_SUGGESTION = 5;
        readonly string TAG = typeof(MyOrderDetailViewModel).FullName;
        #endregion
        #region protected
        protected Order order;
        #endregion
        #region private
        private bool isLoadingMenuSectionSuggestion = false;
        private bool filterMyOrders = false;
        private string tableNumber;
        private string pinNumber;
        private bool isRefreshOrder = false;
        private bool checkOrderVisibility;
        private bool isVisibleMenuSuggestion = true;
        #endregion

        #region fields to binding
      
        public bool IsLoadingMenuSectionSuggestion
        {
            get => isLoadingMenuSectionSuggestion;
            set => SetProperty(ref isLoadingMenuSectionSuggestion, value);
        }

        public bool FilterMyOrders
        {
            get => filterMyOrders;
            set => SetProperty(ref filterMyOrders, value);
        }

        public MvxObservableCollection<OrderDetail> OrderDetailList { get; } = new MvxObservableCollection<OrderDetail>();

        public MvxObservableCollection<MenuSection> MenuSectionSuggestionList { get; } = new MvxObservableCollection<MenuSection>();

        public string TableNumber
        {
            get => tableNumber;
            set => SetProperty(ref tableNumber, value);
        }

        public string PinNumber
        {
            get => pinNumber;
            set => SetProperty(ref pinNumber, value);
        }

        public bool CheckOrderVisibility
        {
            get => checkOrderVisibility;
            set => SetProperty(ref checkOrderVisibility, value);
        }

        public bool IsRefreshOrder
        {
            get => isRefreshOrder;
            set => SetProperty(ref isRefreshOrder, value);
        }

        private bool isVisibleShowMyOrder = true;

        public bool IsVisibleShowMyOrder
        {
            get => isVisibleShowMyOrder;
            set => SetProperty(ref isVisibleShowMyOrder, value);
        }

        public bool IsVisibleMenuSuggestion
        {
            get => isVisibleMenuSuggestion;
            set => SetProperty(ref isVisibleMenuSuggestion, value);
        }

        #endregion

        #region command
        public IMvxAsyncCommand<OrderDetail> ItemClickCommand => new MvxAsyncCommand<OrderDetail>(ItemClick);
        public IMvxAsyncCommand<MenuSection> MenuSectionItemClickCommand => new MvxAsyncCommand<MenuSection>(MenuSectionItemClick);
        public IMvxAsyncCommand CheckOrderCommand => new MvxAsyncCommand(OnCheckOrder);

        public IMvxAsyncCommand ToggleSwitchCommand => new MvxAsyncCommand(OnFilterMyOrders);
        #endregion

        #region constructor
        public ResumeDetailViewModel(ILogger logger,
                                     IAnalytics analytics,
                                     INavigationController navigationController,
                                     AppDefaultConfig appConfig,
                                     IDialogs dialogs,
                                     IOrderManager orderManager,
                                     IFormatOrderDetail formatOrderDetail,
                                     IEventBus eventBus,
                                     IFormatPrice formatPrice,
                                     IUserSessionManager userSessionManager,
                                     IRedirectManager redirectManager) : base(logger, analytics, navigationController, appConfig, orderManager, dialogs, eventBus, userSessionManager)
        {
            this.formatOrderDetail = formatOrderDetail;
            this.formatPrice = formatPrice;
            this.redirectManager = redirectManager;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            GetInfo();
        }
        public override void ViewAppeared()
        {
            base.ViewAppeared();
            RefreshOrder();
        }
        #endregion

        #region order

        protected virtual async Task GetInfo()
        {
            TableNumber = String.Format(Strings["TableNumberInOrder"], orderManager.GetCurrentTable());
            PinNumber = String.Format(Strings["PinNumber"], (await orderManager.GetCurrentOrder()).Pin);
        }

        protected virtual async Task RefreshOrder()
        {
            if (!IsRefreshOrder)
            {
                var progress = this.dialogs.ProgressDialog("");
                await progress.Show();
                try
                {
                    order = await orderManager.CheckOrderStatus();
                    await progress.Hide();
                    IsRefreshOrder = true;
                }
                catch (UserSessionException e)
                {
                    base.logger.Error(TAG, e.Message);
                    await progress.Hide();
                    await RedirectByReasonUserBlock();
                }
                catch (Exception e1)
                {
                    base.logger.Error(TAG, e1.Message);
                    await progress.Hide();
                    await navigationController.Back(this);
                    await this.dialogs.ShowError(Strings["GetOrderErrorConnection"]);
                }
            }
            if (IsRefreshOrder)
            {
                await ReLoad();
                LoadMenuSectionSuggestion();
            }
        }

        protected virtual async Task Load()
        {
            try
            {
                List<OrderDetail> orderDetails = await orderManager.FindConfirmedOrderDetail(FilterMyOrders);
                await AddOrderDetails(orderDetails);
                CheckOrderButtonVisibility(orderDetails);
            }
            catch (Exception e)
            {
                base.logger.Error(TAG, e.Message);
                await this.dialogs.ShowError("Error");
            }
        }

        protected virtual void CheckOrderButtonVisibility(List<OrderDetail> orderDetails)
        {
            /*var pendingToSend = !orderDetails.Exists((filter) => filter.IsWaiting());
            Device.BeginInvokeOnMainThread(() =>
            {
                CheckOrderVisibility = pendingToSend;
            });*/
            CheckOrderVisibility = true;
        }

        protected Task AddOrderDetails(List<OrderDetail> orderDetails)
        {
            return Task.Factory.StartNew(() =>
            {
                orderDetails = formatOrderDetail.FormatToList(orderDetails);
                Device.BeginInvokeOnMainThread(() =>
                {
                    OrderDetailList.Clear();
                    OrderDetailList.AddRange(orderDetails);
                });
            });
        }

        private async Task ReLoad()
        {
            await Load();
            await RefreshTotalAmount();
        }

        protected virtual async Task RefreshTotalAmount()
        {
            var result = await orderManager.GetTotalAmount(FilterMyOrders);
            this.CalculateSubtotalAndDiscount(result);
        }
        #endregion

        #region actions

        private Task OnCheckOrder()
        {
            return CheckOrder();
        }

        async Task CheckOrder(bool force = false)
        {
            if (await ValidateCheckOrder() && await this.CheckConnectionWithAlert())
            {
                var progress = this.dialogs.ProgressDialog("");

                try
                {
                    await progress.Show();
                    var result = await orderManager.CheckOrder(force);
                    await progress.Hide();
                    await this.dialogs.ShowAlert(Strings["CheckingOrderSuccess"]);
                    if (result != null)
                    {
                        await this.redirectManager.AnalyzeRedirectByUrl(result.RedirectUrl, result.PlaceId);
                    }
                }
                catch (CheckOrderBLExistException e)
                {
                    await ShowOrderExistError(progress, e);
                }
                catch (CheckOrderBLPendingException e1)
                {
                    await ShowPendingDetailError(progress, e1);
                }
                catch (CheckOrderBLPendingRequestException e2)
                {
                    await ShowPendingRequestDetailError(progress, e2);
                }
                catch (Exception e3)
                {
                    System.Diagnostics.Debug.WriteLine(e3.Message);
                    await progress.Hide();
                    base.logger.Error(TAG, e3.Message);
                    await this.dialogs.ShowAlert(Strings["CheckingOrderError"]);
                }
            }
        }

        private async Task ShowOrderExistError(IProgressDialog progress, CheckOrderBLExistException e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            progress.Hide();
            await this.dialogs.ShowAlert(Strings["CheckingOrderExistError"]);
        }

        private async Task ShowPendingDetailError(IProgressDialog progress, CheckOrderBLPendingException e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            progress.Hide();
            await this.dialogs.ShowAlert(Strings["CheckingOrderErrorNotKitchen"]);
        }

        private async Task ShowPendingRequestDetailError(IProgressDialog progress, CheckOrderBLPendingRequestException e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            await progress.Hide();
            await QuestionCheckOrderWhenExistPendingDetails();
        }

        private async Task QuestionCheckOrderWhenExistPendingDetails()
        {
            var result = await this.dialogs.ShowConfirmDialog(Strings["CheckingOrderErrorPendingRequestTitle"],
                                                                          Strings["CheckingOrderErrorPendingRequestMessage"],
                                                                          Strings["CheckingOrderErrorPendingRequestYes"],
                                                                          Strings["CheckingOrderErrorPendingRequestNo"]);
            if (result)
            {
                CheckOrder(true);
            }
            else
            {
                this.navigationController.Back(this);
            }
        }

        private async Task<bool> ValidateCheckOrder()
        {
            if (order != null && !order.IsClosed())
            {
                var progress = this.dialogs.ProgressDialog("");
                try
                {
                    await progress.Show();
                    var order = await orderManager.CheckOrderStatus();
                    await progress.Hide();
                    if (order.Amount.Value.Equals(this.SubtotalAmount))
                    {
                        return true;
                    }
                    else
                    {
                        await ReLoad();
                        return await QuestionCheckOrderDifferentAmount(order);
                    }
                }
                catch (Exception e)
                {
                    await progress.Hide();
                    await this.dialogs.ShowError(Strings["CheckingOrderError"]);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private async Task<bool> ValidateHasInKitchen()
        {
            var result = await orderManager.FindConfirmedOrderDetail();
            var hasInKitchen = result.Exists((filter) => filter.InKitchen());

            if (!hasInKitchen)
            {
                this.dialogs.ShowAlert(Strings["CheckingOrderErrorNotKitchen"]);
            }

            return hasInKitchen;
        }

        private async Task<bool> QuestionCheckOrderDifferentAmount(Order order)
        {
            return await this.dialogs.ShowConfirmDialog(Strings["OrderUpdatedTitle"],
                                                    String.Format(Strings["OrderUpdatedMessage"],
                                                                  formatPrice.FormatPriceWithSymbol(order.Amount.Value)),
                                                    Strings["OrderUpdatedAcceptButton"],
                                                    Strings["OrderUpdatedCancelButton"]);
        }

        async Task OnFilterMyOrders()
        {
            await ReLoad();
        }
        #endregion

        #region suggestions
        protected virtual async Task LoadMenuSectionSuggestion()
        {
            if (MenuSectionSuggestionList.Count == 0)
            {
                try
                {
                    IsLoadingMenuSectionSuggestion = true;
                    IPaggination paggination = new Paggination(MenuSectionSuggestionList.Count, LIMIT_MENU_SECTION_SUGGESTION);
                    var suggestion = await orderManager.FindSuggestionMenuSection(paggination);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MenuSectionSuggestionList.AddRange(suggestion);
                    });
                }
                catch (Exception e)
                {
                    base.logger.Error(TAG, e.Message);
                }
                finally
                {
                    IsLoadingMenuSectionSuggestion = false;
                }
            }
        }

        #region actions
        async Task MenuSectionItemClick(MenuSection menuSection)
        {
            Place currentPlace = await orderManager.GetCurrentPlace();
            await navigationController.OpenPageByMenuSection(currentPlace, menuSection);
        }

        async Task ItemClick(OrderDetail orderDetail)
        {

        }
        #endregion
        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "check_order_detail";
        }
        #endregion
    }
}
