﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;
using Garzon.Forms.UI.Navigation.Result;
using MvvmCross.Core.ViewModels;

namespace Garzon.Forms.UI.ViewModels
{
    public class OrderQuantityDinersViewModel : ResultBaseViewModel<QuantityDinersResult>
    {
        #region dependency
        readonly IOrderManager _orderManage;
        #endregion
        #region fields to binding
        private string _quantityDiners = "";

        public string QuantityDiners
        {
            get => _quantityDiners;
            set => SetProperty(ref _quantityDiners, value);
        }

        #endregion
        #region command
        public IMvxAsyncCommand SendCommand => new MvxAsyncCommand(OnSend);
        #endregion
        #region constructor
        public OrderQuantityDinersViewModel(IOrderManager orderManager,
                                    ILogger logger,
                                    IAnalytics analytics,
                                    INavigationController navigationController,
                                    AppDefaultConfig appConfig,
                                    IDialogs dialogs) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            _orderManage = orderManager;
        }
        #endregion


        #region actions
        public async Task OnSend()
        {
            if (await ValidateQuantity())
            {
                await navigationController.Back(this,
                                                 QuantityDinersResult.QuantityDinersResultBuilder(int.Parse(_quantityDiners)));
            }
        }

        #endregion

        #region private

        private async Task<bool> ValidateQuantity()
        {
            int number = 0;
            if (String.IsNullOrEmpty(_quantityDiners) 
                || !int.TryParse(_quantityDiners, out number))
            {
                await dialogs.ShowError(Strings["QuantityDinersRequired"]);
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "quantity_diners";
        }
        #endregion
    }
}
