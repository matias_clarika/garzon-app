﻿using System;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.EventBus;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Facade.Auth;
using Garzon.Forms.UI.Navigation;

namespace Garzon.Forms.UI.ViewModels
{
    public abstract class OrderUserValidationViewModel : BaseMyOrderViewModel
    {
        #region dependency
        protected readonly IUserSessionManager _userSessionManager;
        #endregion

        protected OrderUserValidationViewModel(ILogger logger,
                                            IAnalytics analytics,
                                            INavigationController navigationController,
                                            AppDefaultConfig appConfig,
                                            IOrderManager orderManager,
                                            IDialogs dialogs,
                                            IEventBus eventBus,
                                            IUserSessionManager userSessionManager) : base(logger, analytics, navigationController, appConfig, orderManager, dialogs, eventBus)
        {
            _userSessionManager = userSessionManager;
        }


        protected async Task RedirectByReasonUserBlock()
        {
            await _userSessionManager.ProcessUserBlock();
            await navigationController.ReOpenApp();
            await dialogs.ShowAlert(Strings["UserBlockNotification"]);
        }
    }
}
