﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;
using Garzon.Forms.UI.Navigation.Result;
using MvvmCross.Core.ViewModels;

namespace Garzon.Forms.UI.ViewModels
{
    public class OrderPinViewModel : ResultBaseViewModel<PinResult>
    {
        #region dependency
        readonly IOrderManager _orderManage;
        #endregion
        #region fields to binding
        private string _pinNumber = "";

        public string PinNumber
        {
            get => _pinNumber;
            set => SetProperty(ref _pinNumber, value);
        }

        private string _helperStringToPin;

        public string HelperStringToPin
        {
            get => _helperStringToPin;
            set => SetProperty(ref _helperStringToPin, value);
        }

        #endregion
        #region command
        public IMvxAsyncCommand SendPinCommand => new MvxAsyncCommand(SendPin);
        #endregion
        #region constructor
        public OrderPinViewModel(IOrderManager orderManager,
                                    ILogger logger,
                                    IAnalytics analytics,
                                    INavigationController navigationController,
                                    AppDefaultConfig appConfig,
                                    IDialogs dialogs) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            _orderManage = orderManager;
        }
        #endregion

        public override async Task Initialize()
        {
            await base.Initialize();
            CreateMessageToHelpPin();
        }

        #region actions
        public async Task SendPin()
        {
            if (PinNumber.Length == 4)
            {
                await navigationController.Back(this,
                                                 PinResult.PinResultBuilder(PinNumber));
            }
            else
            {
                await this.dialogs.ShowError(Strings["PinNumberError"]);
            }
        }
        #endregion
        #region private
        private void CreateMessageToHelpPin()
        {
            string message = Strings["PinHelp"];
            HelperStringToPin =  String.Format(message, _orderManage.GetCurrentTable());
        }
        #endregion


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "order_pin";
        }
        #endregion
    }

}
