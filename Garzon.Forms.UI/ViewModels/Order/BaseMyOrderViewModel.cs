﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.DataObjects;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Navigation;
using MvvmCross.Core.ViewModels;
using Garzon.Core.EventBus;
using Garzon.Forms.UI.EventBus.OrderNotifications;

namespace Garzon.Forms.UI.ViewModels
{
    public abstract class BaseMyOrderViewModel : GarzonBaseViewModel
    {
        #region dependency
        protected readonly IOrderManager orderManager;
        protected readonly IEventBus _eventBus;
        #endregion
        #region private fields

        double subtotalAmount = 0;
        private double discountAmount = 0;
        private double totalAmount = 0;
        #endregion
        #region fields to binding
        private Double _pendingTotalAmount;

        public Double PendingTotalAmount
        {
            get => _pendingTotalAmount;
            set => SetProperty(ref _pendingTotalAmount, value);
        }


        protected bool _isDailyMenu;

        public bool IsDailyMenu
        {
            get => _isDailyMenu;
            set => SetProperty(ref _isDailyMenu, value);
        }

        private bool _pendingTotalAmountVisibility;

        public bool PendingTotalAmountVisibility
        {
            get => _pendingTotalAmountVisibility;
            set => SetProperty(ref _pendingTotalAmountVisibility, value);
        }

        private bool _sendNowVisibility = false;
        public bool SendNowVisibility
        {
            get => _sendNowVisibility;
            set => SetProperty(ref _sendNowVisibility, value);
        }

        private bool _existPinNumber;

        public bool ExistPinNumber
        {
            get => _existPinNumber;
            set => SetProperty(ref _existPinNumber, value);
        }

        public double TotalAmount
        {
            get => totalAmount;
            set => SetProperty(ref totalAmount, value);
        }

        public double SubtotalAmount
        {
            get => this.subtotalAmount;
            set => this.SetProperty(ref this.subtotalAmount, value);
        }

        public double DiscountAmount
        {
            get => this.discountAmount;
            set => this.SetProperty(ref this.discountAmount, value);
        }
        #endregion

        #region tokens
        IEventToken _tokenOrderToken;
        #endregion
        #region command
        public IMvxAsyncCommand ResumeCheckCommand => new MvxAsyncCommand(GoToResumeCheck);
        public IMvxAsyncCommand MyOrderCommand => new MvxAsyncCommand(GoToMyOrder);
        public IMvxAsyncCommand PinNumberCommand => new MvxAsyncCommand(ShowPinNumber);

        #endregion
        #region constructor
        public BaseMyOrderViewModel(ILogger logger,
                                    IAnalytics analytics,
                                    INavigationController navigationController,
                                    AppDefaultConfig appConfig,
                                    IOrderManager orderManager,
                                    IDialogs dialogs,
                                    IEventBus eventBus) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            this.orderManager = orderManager;
            _eventBus = eventBus;
        }
        #endregion

        #region lifecycle
        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await CheckUI();
            Subscribe();
            await Refresh();

        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            Unsuscribe();
        }

        #endregion

        #region events
        private async void Subscribe()
        {
            _tokenOrderToken = await _eventBus.SubscribeOnMainThread<OrderChangeEvent>(OnOrderChange);

        }

        private void Unsuscribe()
        {
            _eventBus.Unsubscribe<OrderChangeEvent>(_tokenOrderToken);
        }

        private void OnOrderChange(OrderChangeEvent obj)
        {
            Refresh();
        }
        #endregion
        #region order methods
        protected async Task Refresh()
        {
            var orderDetails = await orderManager.FindPendingOrderDetail();
            CalculatePendingTotalAmount(orderDetails);
        }

        protected virtual void CalculatePendingTotalAmount(List<OrderDetail> orderDetails)
        {
            double totalAmount = 0;

            foreach (OrderDetail orderDetail in orderDetails)
            {
                totalAmount += orderDetail.Amount.Value;
            }
            PendingTotalAmount = totalAmount;
            PendingTotalAmountVisibility = totalAmount > 0;
        }


        protected void CalculateSubtotalAndDiscount(double newTotalAmount)
        {
            this.SubtotalAmount = newTotalAmount;
            this.DiscountAmount = newTotalAmount * 0.1;
            this.TotalAmount = this.SubtotalAmount - this.DiscountAmount;
        }

        protected virtual async Task<bool> OrderIsInResto()
        {
            var mode = await this.orderManager.GetOrderMode();
            var isInResto = mode == (int)PlaceMode.ServerPlaceModeIds.RESTO;
            return isInResto;
        }
        #endregion

        #region actions
        protected Task GoToMyOrder()
        {
            return navigationController.OpenMyOrderDetail();
        }

        protected Task GoToResumeCheck()
        {
            return navigationController.OpenResumeCheck();
        }

        private async Task ShowPinNumber()
        {
            var currentOrder = await orderManager.GetCurrentOrder();
            if (currentOrder != null && currentOrder.Pin.HasValue && currentOrder.Pin.Value > 0)
            {
                await this.dialogs.ShowAlert(String.Format(Strings["PinNumber"],
                                                       currentOrder.Pin.Value));
            }
        }

        #endregion

        #region private

        private async Task CheckUI()
        {
            await CheckExistPinNumber();
        }

        private async Task CheckExistPinNumber()
        {
            var currentOrder = await orderManager.GetCurrentOrder(false, false);
            ExistPinNumber = currentOrder != null && currentOrder.Pin.HasValue;
        }
        #endregion
    }
}
