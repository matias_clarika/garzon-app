﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;

    public class TakeAwayExistViewModel : LoginWithoutTableViewModel, IMvxViewModel<TakeAwayExistViewModel.Params>
    {
        #region private fields
        private string orderNumber;
        private Place place;
        #endregion

        #region binding fields
        public string OrderNumber
        {
            get => this.orderNumber;
            set => this.SetProperty(ref this.orderNumber, value);
        }
        #endregion
        public TakeAwayExistViewModel(ILogger logger,
                                      IAnalytics analytics,
                                      INavigationController navigationController,
                                      AppDefaultConfig appConfig,
                                      IDialogs dialogs,
                                      IUserSessionManager userSessionManager,
                                      IOrderManager orderManager) : base(logger, analytics, navigationController, appConfig, dialogs, userSessionManager, orderManager)
        {
        }

        #region IMvxViewModel
        public void Prepare(Params parameter)
        {
            this.place = parameter.Place;
        }
        #endregion

        #region override
        protected override Task ConfigureByPlace()
        {
            return Task.Factory.StartNew(() =>
            {
                this.Title = this.place.Name;
            });
        }

        protected override async Task SuccessLogin(User user)
        {
            if (await this.ValidateFields() && await this.CheckConnectionWithAlert())
            {
                var progress = this.dialogs.ProgressDialog(string.Empty);
                try
                {
                    await progress.Show();
                    var result = await this.orderManager.TakeAwayExistOrder(this.place, int.Parse(this.OrderNumber));
                    await progress.Hide();
                    if (result)
                    {
                        await this.dialogs.ShowError(Strings["TakeAwayExistOrderSuccessMessage"]);
                        await this.navigationController.Back(this);
                        await this.navigationController.OpenMyOrders();
                    }
                    else
                    {
                        await this.dialogs.ShowError(Strings["TakeAwayExistOrderGenericErrorMessage"]);
                    }

                }
                catch (Exception)
                {
                    await progress.Hide();
                    if (await this.CheckConnectionWithAlert())
                    {
                        await this.dialogs.ShowError(Strings["TakeAwayExistOrderGenericErrorMessage"]);
                    }
                }

            }

        }

        private async Task<bool> ValidateFields()
        {
            if (this.OrderNumber.Trim().Length == 0)
            {
                await this.dialogs.ShowError(Strings["TakeAwayExistOrderNumberIsRequiredMessage"]);
                return false;
            }
            else
            {
                try
                {
                    int.Parse(this.OrderNumber);
                    return true;
                }
                catch (Exception)
                {
                    await this.dialogs.ShowError(Strings["TakeAwayExistOrderNumberInvalidErrorMessage"]);
                    return false;
                }
            }
        }
        #endregion
        #region params
        public class Params
        {
            public Place Place
            {
                get;
                private set;
            }

            private Params(Place place)
            {
                this.Place = place;
            }

            /// <summary>
            /// Build the specified place.
            /// </summary>
            /// <returns>The build.</returns>
            /// <param name="place">Place.</param>
            public static Params Build(Place place)
            {
                return new Params(place);
            }
        }
        #endregion
    }
}
