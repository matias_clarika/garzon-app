﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.EventBus;
using Garzon.Core.Utils;
using Garzon.Forms.UI.EventBus.OrderNotifications;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Facade.Auth;
using Garzon.Forms.UI.Navigation;
using Xamarin.Forms;
using MvvmCross.Core.ViewModels;
using Garzon.DAL.DataSource.Remote.Exceptions;
using Garzon.BL;
using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.Forms.UI.ViewModels
{
    public abstract class SendToKitchenViewModel : OrderUserValidationViewModel
    {
        #region dependency
        private readonly ITutorialBL tutorialBL;
        #endregion
        #region private
        bool _awaitUsers = false;
        //flag to show alert with pin if the user is owner and if the first time to send.
        bool _isOwnUserAndFirstTimeSend = false;
        #endregion
        #region tokens
        CancellationTokenSource _loginCancellationToken;
        CancellationTokenSource _pinCancellationToken;
        CancellationTokenSource _quantityDinersToken;
        IEventToken _tokenSendToKitchen;
        IEventToken _tokenOrderTableAuth;
        IEventToken _tokenOrderPinAuth;
        IEventToken _tokenOrderQuantityDiners;
        private CancellationTokenSource mercadopagoToken;
        #endregion
        #region command
        public IMvxAsyncCommand SendToKitchenCommand => new MvxAsyncCommand(TrySendToKitchen);
        #endregion

        public SendToKitchenViewModel(ILogger logger,
                                      IAnalytics analytics,
                                      INavigationController navigationController,
                                      AppDefaultConfig appConfig,
                                      IOrderManager orderManager,
                                      IUserSessionManager userSessionManager,
                                      IDialogs dialogs,
                                      IEventBus eventBus,
                                      ITutorialBL tutorialBL) : base(logger, analytics, navigationController, appConfig, orderManager, dialogs, eventBus, userSessionManager)
        {
            this.tutorialBL = tutorialBL;
        }

        #region lifecycle

        public override async Task Initialize()
        {
            await base.Initialize();
            CheckFirstSendInOrder();

        }

        public override async void ViewAppearing()
        {
            base.ViewAppearing();
            await Subscribe();
        }
        public override void ViewDisappearing()
        {
            base.ViewDisappearing();
            Unsuscribe();
        }
        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await CancelToken();
        }


        #endregion
        #region events
        private async Task CancelToken()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                await Task.Delay(300);
            }
            CancellationTokenSource(_loginCancellationToken);
            CancellationTokenSource(_pinCancellationToken);
            CancellationTokenSource(_quantityDinersToken);
            CancellationTokenSource(this.mercadopagoToken);
        }

        private async Task Subscribe()
        {
            _tokenSendToKitchen = await _eventBus.Subscribe<SendToKitchenEvent>(OnSendToKichen);
            _tokenOrderTableAuth = await _eventBus.Subscribe<OrderTableAuthEvent>(OnTableAuthEvent);
            _tokenOrderPinAuth = await _eventBus.Subscribe<OrderPinAuthEvent>(OnPinAuthEvent);
            _tokenOrderQuantityDiners = await _eventBus.Subscribe<OrderQuantityDinersEvent>(OnOrderQuantityDinersEvent);
        }

        private void Unsuscribe()
        {
            _eventBus.Unsubscribe<SendToKitchenEvent>(_tokenSendToKitchen);
            _eventBus.Unsubscribe<OrderTableAuthEvent>(_tokenOrderTableAuth);
            _eventBus.Unsubscribe<OrderPinAuthEvent>(_tokenOrderPinAuth);
            _eventBus.Unsubscribe<OrderQuantityDinersEvent>(_tokenOrderQuantityDiners);
        }

        private void OnPinAuthEvent(OrderPinAuthEvent obj)
        {
            RequestPin();
        }

        private void OnTableAuthEvent(OrderTableAuthEvent obj)
        {
            RequestTableNumber();
        }

        void OnSendToKichen(SendToKitchenEvent sendToKitchenEvent)
        {
            TrySendToKitchen();
        }

        private void OnOrderQuantityDinersEvent(OrderQuantityDinersEvent obj)
        {
            RequestQuantityDiners();
        }

        private async Task FireSendToKitchen()
        {
            await _eventBus.Publish(SendToKitchenEvent.Build(this));
        }
        #endregion

        #region send to kitchen
        public async Task TrySendToKitchen()
        {
            if (await CheckUser() && await CheckPaymentMethod())
            {
                SendToKitchen();
            }
        }

        /// <summary>
        /// Sends to kitchen.
        /// </summary>
        /// <returns>The to kitchen.</returns>
        /// <param name="awaitUsers">If set to <c>true</c> await users.</param>
        private async Task SendToKitchen()
        {
            var dialog = this.dialogs.ProgressDialog("");
            var source = new CancellationTokenSource();
            try
            {
                var result = await orderManager.SendToKitchen(source,
                                                               _awaitUsers);
                await dialog.Hide();
                if (result != null)
                {
                    await navigationController.ReOpenApp();

                    if (result.IsInResto())
                    {
                        ShowSuccessAndTutorial();
                    }
                    else
                    {
                        await OpenSuccessTakeAway();
                    }
                }
            }
            catch (SentToKitchenException se)
            {
                CancellationTokenSource(source);
                await dialog.Hide();
                AnalyzeSendToKitchenException(se);
            }
            catch (Exception)
            {
                CancellationTokenSource(source);
                await dialog.Hide();
                await ShowGenericError();
            }
        }

        private async Task OpenSuccessTakeAway()
        {
            await this.dialogs.ShowError(Strings["SendToKitchenTakeAwaySuccessMessage"]);
            await this.navigationController.OpenMyOrders();
        }

        private async Task ShowGenericError()
        {
            await this.dialogs.ShowError(Strings["SendToKitchenGenericError"]);
        }

        /// <summary>
        /// Analyzes the send to kitchen exception.
        /// </summary>
        /// <returns>The send to kitchen exception.</returns>
        /// <param name="se">Se.</param>
        private async Task AnalyzeSendToKitchenException(SentToKitchenException se)
        {
            switch (se.Error)
            {
                case SentToKitchenException.ERROR.BLOCK_USER:
                    RedirectByReasonUserBlock();
                    break;
                case SentToKitchenException.ERROR.TABLE_NOT_FOUND:
                    _eventBus.Publish(OrderTableAuthEvent.Build(this));
                    break;
                case SentToKitchenException.ERROR.PIN:
                    _eventBus.Publish(OrderPinAuthEvent.Build(this));
                    break;
                case SentToKitchenException.ERROR.QUANTITY_REQUIRED:
                    _eventBus.Publish(OrderQuantityDinersEvent.Build(this));
                    break;
                case SentToKitchenException.ERROR.ORDER_INVALID_PRODUCTS:
                    ErrorInvalidProducts(se.InvalidProducts);
                    break;
                case SentToKitchenException.ERROR.PAYMENT_REJECTED:
                    ErrorPaymentRejected();
                    break;
                case SentToKitchenException.ERROR.PAYMENT_INVALID_CARD:
                    ErrorPaymentInvalidCard();
                    break;
                default:
                    await ShowGenericError();
                    break;

            }
        }

        /// <summary>
        /// Shows the alert toa wait order confirmed.
        /// </summary>
        private async void ShowSuccessAndTutorial()
        {
            if (!await ShowTutorial())
            {
                await ShowSuccess();
            }
        }

        private async Task ShowSuccess()
        {
            if (_isOwnUserAndFirstTimeSend)
            {
                await ShowSuccessWithPinNumber();
            }
            else
            {
                await ShowSuccessDefault();
            }
        }

        private async Task ShowSuccessDefault()
        {
            await this.dialogs.ShowAlert(Strings["SendToKitchenSuccess"]);
        }

        private async Task<bool> ShowTutorial()
        {
            var show = await this.tutorialBL.GetPinNumberAndCheckOrderTutorial();
            if (show)
            {
                await ShowSuccess();
                await navigationController.OpenPinNumberAndCheckOrderTutorial();
            }
            return show;
        }

        private async Task ShowSuccessWithPinNumber()
        {
            await this.dialogs.ShowAlert(String.Format(Strings["SendToKitchenSuccessPinNumber"],
                                                   (await orderManager.GetCurrentOrder(true, false)).Pin.Value));
        }
        #endregion

        #region login
        public async Task<bool> CheckUser()
        {
            if (await orderManager.UserIsRequired())
            {
                _loginCancellationToken = new CancellationTokenSource();
                var result = await navigationController.OpenLogin(false, _loginCancellationToken.Token);
                _loginCancellationToken = null;
                if (result != null)
                {
                    return await _userSessionManager.ProcessLoginResult(result);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        private async Task RequestTableNumber()
        {
            //clear
            await orderManager.SetCurrentTable(0);
            TrySendToKitchen();
            await this.dialogs.ShowError(Strings["TableNumberMessageError"]);

        }
        #endregion

        #region pin

        private async void CheckFirstSendInOrder()
        {
            var currentOrder = await orderManager.GetCurrentOrder(false, false);
            _isOwnUserAndFirstTimeSend = currentOrder == null || !currentOrder.Pin.HasValue;
        }

        private async Task RequestPin()
        {
            //if the API request PIN number, user not is an owner
            _isOwnUserAndFirstTimeSend = false;

            _pinCancellationToken = new CancellationTokenSource();
            var result = await navigationController.OpenOrderPin(_pinCancellationToken.Token);
            _pinCancellationToken = null;
            if (result != null)
            {
                orderManager.SetPin(result.Pin);
                TrySendToKitchen();
            }
        }
        #endregion

        #region diners
        private async Task RequestQuantityDiners()
        {
            _quantityDinersToken = new CancellationTokenSource();
            var result = await navigationController.OpenOrderQuantityDiners(_quantityDinersToken.Token);
            _quantityDinersToken = null;
            if (result != null)
            {
                orderManager.SetQuantityDiners(result.Quantity);
                TrySendToKitchen();
            }
        }
        #endregion

        #region payment

        protected virtual async Task<bool> CheckPaymentMethod()
        {
            if (await this.orderManager.GetOrderMode() == (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY)
            {
                return await GeneratePayment();
            }
            return await Task.Factory.StartNew(() =>
            {
                return true;
            });
        }

        private async Task<bool> GeneratePayment()
        {
            this.mercadopagoToken = new CancellationTokenSource();
            var paymentResult = await this.navigationController.OpenMercadoPagoPayment(mercadopagoToken.Token);
            this.mercadopagoToken = null;
            if (paymentResult != null)
            {
                this.orderManager.SetPayment(paymentResult);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        private async Task ErrorInvalidProducts(List<Product> invalidProducts)
        {
            await orderManager.RemoveInvalidProducts(invalidProducts);
            ShowErrorInvalidProducts(invalidProducts);
        }

        internal abstract Task ShowErrorInvalidProducts(List<Product> invalidProducts);


        private Task ErrorPaymentInvalidCard()
        {
            return this.dialogs.ShowAlert(Strings["PaymentInvalidCardErrorMessage"]);
        }

        private Task ErrorPaymentRejected()
        {
            return this.dialogs.ShowAlert(Strings["PaymentRejectedErrorMessage"]);
        }

    }
}
