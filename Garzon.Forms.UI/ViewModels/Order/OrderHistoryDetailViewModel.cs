﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.EventBus;
    using Garzon.Core.Managers;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Format;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;

    public class OrderHistoryDetailViewModel : ResumeDetailViewModel, IMvxViewModel<OrderHistoryDetailViewModel.Params>
    {
        #region private fields
        private bool isOrderTakeAwayAndRejected = false;
        #endregion
        #region public fields
        public bool IsOrderTakeAwayAndRejected
        {
            get => this.isOrderTakeAwayAndRejected;
            set => this.SetProperty(ref this.isOrderTakeAwayAndRejected, value);
        }
        #endregion
        public OrderHistoryDetailViewModel(ILogger logger,
                                           IAnalytics analytics,
                                           INavigationController navigationController,
                                           AppDefaultConfig appConfig,
                                           IDialogs dialogs,
                                           IOrderManager orderManager,
                                           IFormatOrderDetail formatOrderDetail,
                                           IEventBus eventBus,
                                           IFormatPrice formatPrice,
                                           IUserSessionManager userSessionManager,
                                           IRedirectManager redirectManager) : base(logger, analytics, navigationController, appConfig, dialogs, orderManager, formatOrderDetail, eventBus, formatPrice, userSessionManager, redirectManager)
        {
        }

        #region IMvxViewModel
        public void Prepare(Params parameter)
        {
            order = parameter.Order;
        }
        #endregion
        #region lifecycle
        public override Task Initialize()
        {
            this.IsRefreshOrder = true;
            this.IsVisibleShowMyOrder = false;
            this.IsVisibleMenuSuggestion = false;
            this.IsOrderTakeAwayAndRejected = !this.order.IsInResto() && this.order.IsObserved();
            return base.Initialize();
        }
        #endregion

        #region override

        protected override Task GetInfo()
        {
            return Task.Factory.StartNew(() =>
            {
                if (order.IsInResto())
                {
                    TableNumber = String.Format(Strings["TableNumberInOrder"], order.TableNumber.Value);
                    PinNumber = String.Format(Strings["PinNumber"], order.Pin.Value);
                }
                else
                {
                    if (order.NumberToWithdraw.HasValue && order.NumberToWithdraw.Value > 0)
                    {
                        PinNumber = String.Format(Strings["OrderNumberToFormat"], order.NumberToWithdraw.Value);
                    }
                    else
                    {
                        PinNumber = Strings["OrderTakeAwayPendingToConfirmationLabelText"];
                    }
                }

            });

        }
        protected override Task Load()
        {
            return this.AddOrderDetails(order.Details);
        }

        protected override void CheckOrderButtonVisibility(List<OrderDetail> orderDetails)
        {
            this.CheckOrderVisibility = false;
        }

        protected override async Task RefreshTotalAmount()
        {
            this.TotalAmount = await this.orderManager.GetTotalAmountByOrderDetails(this.order.Details);
            this.CalculateSubtotalAndDiscount(this.TotalAmount);
        }

        protected override Task LoadMenuSectionSuggestion()
        {
            return Task.Factory.StartNew(() =>
            {

            });
        }
        #endregion

        #region params
        public class Params
        {
            public Order Order
            {
                get;
                private set;
            }
            private Params(Order order)
            {
                this.Order = order;
            }

            /// <summary>
            /// Build the specified order.
            /// </summary>
            /// <returns>The build.</returns>
            /// <param name="order">Order.</param>
            public static Params Build(Order order)
            {
                return new Params(order);
            }
        }
        #endregion
    }
}
