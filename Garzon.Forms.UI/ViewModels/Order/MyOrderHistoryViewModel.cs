﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.EventBus;
    using Garzon.Core.Utils;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.EventBus.OrderNotifications;
    using Garzon.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;

    public class MyOrderHistoryViewModel : GarzonBaseViewModel
    {
        #region dependencies
        protected readonly IOrderBL OrderBL;
        #endregion
        #region private fields
        private bool isInfiniteEnabled = true;
        private bool isLoadingInfinite;
        private bool isRefreshing;
        private bool isMainLoading;
        private IEventToken tokenRefreshTakeAway;
        private readonly int LIMIT = 50;
        #endregion
        #region binding
        public bool IsInfiniteEnabled
        {
            get => this.isInfiniteEnabled;
            set => this.SetProperty(ref this.isInfiniteEnabled, value);
        }

        public bool IsLoadingInfinite
        {
            get => this.isLoadingInfinite;
            set => this.SetProperty(ref this.isLoadingInfinite, value);
        }

        public bool IsRefreshing
        {
            get => this.isRefreshing;
            set => this.SetProperty(ref this.isRefreshing, value);
        }

        public bool IsMainLoading
        {
            get => this.isMainLoading;
            set => this.SetProperty(ref this.isMainLoading, value);
        }

        public MvxObservableCollection<Order> List { get; } = new MvxObservableCollection<Order>();
        public IMvxAsyncCommand<Order> ItemClickCommand => new MvxAsyncCommand<Order>(this.OrderClick);

        public IMvxAsyncCommand LoadingCommand => new MvxAsyncCommand(this.OnLoading);
        public IMvxAsyncCommand PullToRefreshCommand => new MvxAsyncCommand(this.OnPullToRefresh);

        #endregion
        public MyOrderHistoryViewModel(ILogger logger,
                                       IAnalytics analytics,
                                       INavigationController navigationController,
                                       AppDefaultConfig appConfig,
                                       IDialogs dialogs,
                                       IOrderBL orderBL) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            this.OrderBL = orderBL;
        }

        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "my_order_history";
        }
        #endregion

        #region lifecycle
        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await Subscribe();
            await CheckAndLoad();
        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            Unsuscribe();
        }

        private async Task CheckAndLoad()
        {
            if (!this.IsMainLoading)
            {
                this.List.Clear();
                this.IsMainLoading = true;
                await this.Load();
            }
        }
        #endregion

        #region private

        private async Task Subscribe()
        {
            this.tokenRefreshTakeAway = await this.EventBus.SubscribeOnMainThread<RefreshTakeAwayEvent>(OnRefreshTakeAway);

        }
        private void Unsuscribe()
        {
            this.EventBus.Unsubscribe<RefreshTakeAwayEvent>(this.tokenRefreshTakeAway);
        }

        private async void OnRefreshTakeAway(RefreshTakeAwayEvent obj)
        {
            await CheckAndLoad();
        }

        private async Task Load(bool loadingInfinite = false)
        {
            if (await this.CheckConnectionWithAlert())
            {
                this.IsLoadingInfinite = loadingInfinite;
                this.IsRefreshing = !loadingInfinite;

                try
                {
                    List<Order> result = await this.OrderBL.GetMyOrderHistory(0, LIMIT);

                    if (result != null && result.Any())
                    {
                        this.List.AddRange(result);
                    }
                    this.HideLoadings();
                    this.IsInfiniteEnabled = result != null && result.Count == LIMIT;
                }
                catch (Exception)
                {
                    if (await this.CheckConnectionWithAlert())
                    {
                        await this.dialogs.ShowError(Strings["MyOrderHistoryGenericErrorMessage"]);
                        await this.navigationController.Back(this);
                    }
                }
                finally
                {
                    this.HideLoadings();
                }
            }
            else if (!this.List.Any())
            {
                await this.navigationController.Back(this);
            }
        }

        private void HideLoadings()
        {
            this.IsLoadingInfinite = false;
            this.IsRefreshing = false;
            this.IsMainLoading = false;
        }

        private async Task OrderClick(Order order)
        {
            if (order.Details.Any())
            {
                await this.navigationController.OpenOrderDetail(order);
            }
            else
            {
                await this.dialogs.ShowAlert(Strings["OrderDetailTakeAwayWithoutDetailMessage"]);
            }

        }

        private Task OnLoading()
        {
            return this.Load(true);
        }

        private Task OnPullToRefresh()
        {
            this.List.Clear();
            return this.Load();
        }
        #endregion


    }
}
