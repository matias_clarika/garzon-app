﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using System.Linq;
    using Garzon.BL;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Component;
    using Garzon.Core.EventBus;
    using Xamarin.Forms;
    using Garzon.Core.Views.Format;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.Forms.UI.EventBus.OrderNotifications;
    using static Garzon.DataObjects.PlaceMode;

    public class MyOrderDetailViewModel : SendToKitchenViewModel
    {
        #region dependency
        readonly IMenuSectionBL menuSectionBL;
        readonly IProductMenuSectionBL productBL;
        readonly IFormatOrderDetail formatOrderDetail;
        readonly IGroupBL groupBL;
        #endregion

        #region private fields
        private bool isTakeAway = false;
        private string acceptButtonText;
        private bool visibilityDeleteButton = false;
        private bool isLoadingMenuSectionSuggestion = false;
        #endregion

        #region fields to binding
        public SelectableObservableCollection<OrderDetail> MyOrderDetailList { get; } = new SelectableObservableCollection<OrderDetail>();

        public MvxObservableCollection<MenuSection> MenuSectionSuggestionList { get; } = new MvxObservableCollection<MenuSection>();

        public bool IsLoadingMenuSectionSuggestion
        {
            get => this.isLoadingMenuSectionSuggestion;
            set => SetProperty(ref this.isLoadingMenuSectionSuggestion, value);
        }

        public bool VisibilityDeleteButton
        {
            get => this.visibilityDeleteButton;
            set => SetProperty(ref this.visibilityDeleteButton, value);
        }

        public string AcceptButtonText
        {
            get => this.acceptButtonText;
            set => SetProperty(ref this.acceptButtonText, value);
        }

        public bool IsTakeAway
        {
            get => this.isTakeAway;
            set => this.SetProperty(ref this.isTakeAway, value);
        }
        #endregion

        #region command
        public IMvxAsyncCommand<OrderDetail> OrderDetailClickCommand => new MvxAsyncCommand<OrderDetail>(OrderDetailClick);
        public IMvxAsyncCommand<MenuSection> MenuSectionItemClickCommand => new MvxAsyncCommand<MenuSection>(MenuSectionItemClick);
        public IMvxAsyncCommand<SelectableItem<OrderDetail>> ItemOrderDetailClickCommand => new MvxAsyncCommand<SelectableItem<OrderDetail>>(ItemOrderDetailClick);

        public IMvxAsyncCommand DeleteOrderDetailCommand => new MvxAsyncCommand(QuestionDeleteOrderDetails);
        public new IMvxAsyncCommand SendToKitchenCommand => new MvxAsyncCommand(SendToKitchen);
        #endregion

        #region constants
        const int LIMIT_MENU_SECTION_SUGGESTION = 5;
        private readonly string TAG = typeof(MyOrderDetailViewModel).FullName;
        #endregion

        #region async utils
        CancellationTokenSource productDetailCancellationToken;
        CancellationTokenSource groupByProductCancellationToken;
        private IEventToken tokenOrderObserved;
        #endregion

        #region constructor
        public MyOrderDetailViewModel(ILogger logger,
                                    IAnalytics analytics,
                                    INavigationController navigationController,
                                    AppDefaultConfig appConfig,
                                    IOrderManager orderManager,
                                    IUserSessionManager userSessionManager,
                                    IDialogs dialogs,
                                    IEventBus eventBus,
                                    IFormatOrderDetail formatOrderDetail,
                                    IProductMenuSectionBL productBL,
                                    IMenuSectionBL menuSectionBL,
                                    ITutorialBL tutorialBL,
                                    IGroupBL groupBL) : base(logger,
                                                               analytics,
                                                               navigationController,
                                                               appConfig,
                                                               orderManager,
                                                               userSessionManager,
                                                               dialogs,
                                                               eventBus,
                                                               tutorialBL)
        {
            this.menuSectionBL = menuSectionBL;
            this.productBL = productBL;
            this.formatOrderDetail = formatOrderDetail;
            this.groupBL = groupBL;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            this.IsTakeAway = (await this.orderManager.GetOrderMode()) == (int)ServerPlaceModeIds.TAKE_AWAY;
        }

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await CancelToken();
            await SetupButton();
            await Subscribe();
            await ReLoad();
            LoadMenuSectionSuggestion();
        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            Unsuscribe();
        }
        #region events
        private async Task Subscribe()
        {
            this.tokenOrderObserved = await _eventBus.Subscribe<OrderObservedEvent>(OnObservedEvent);
        }

        private async void OnObservedEvent(OrderObservedEvent obj)
        {
            await ReLoad();
        }

        private void Unsuscribe()
        {
            this._eventBus.Unsubscribe<OrderObservedEvent>(this.tokenOrderObserved);
        }
        #endregion
        private async Task SetupButton()
        {
            var currentOrder = await orderManager.GetCurrentOrder();
            if (orderManager.GetCurrentUser() != null && await orderManager.GetOrderMode() != (int)PlaceMode.ServerPlaceModeIds.RESTO)
            {
                AcceptButtonText = Strings["PayAndSend"];
            }
            else if (currentOrder != null && currentOrder.ServerId > 0)
            {
                AcceptButtonText = Strings["Send"];
            }
            else
            {
                AcceptButtonText = Strings["Next"];
            }
        }
        #endregion


        #region order

        protected override void CalculatePendingTotalAmount(List<OrderDetail> orderDetails)
        {
            base.CalculatePendingTotalAmount(orderDetails);
            if (this.IsTakeAway)
            {
                this.CalculateSubtotalAndDiscount(this.PendingTotalAmount);
            }
        }
        /// <summary>
        /// Load this instance.
        /// </summary>
        /// <returns>The load.</returns>
        private async Task Load()
        {
            try
            {
                List<OrderDetail> orderDetails = await orderManager.FindPendingOrderDetail();
                AddToList(orderDetails);
                CalculatePendingTotalAmount(orderDetails);
            }
            catch (Exception e)
            {
                base.logger.Error(TAG, e.Message);
            }
        }

        /// <summary>
        /// Adds to list.
        /// </summary>
        /// <param name="orderDetails">Order details.</param>
        /// <param name="isEmptyBack">If set to <c>true</c> is empty back.</param>
        private void AddToList(List<OrderDetail> orderDetails, bool isEmptyBack = true)
        {
            if (orderDetails != null && orderDetails.Any())
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    MyOrderDetailList.Add(FormatOrderDetails(orderDetails));
                });

            }
            if (isEmptyBack && !orderDetails.Any() && !MyOrderDetailList.Any())
            {
                navigationController.Back(this);
            }
        }

        private List<OrderDetail> FormatOrderDetails(List<OrderDetail> orderDetails)
        {
            return formatOrderDetail.FormatToList(orderDetails);
        }

        /// <summary>
        /// Res the load.
        /// </summary>
        /// <returns>The load.</returns>
        private async Task ReLoad()
        {
            Device.BeginInvokeOnMainThread(MyOrderDetailList.Clear);

            await Load();
            VisibilityDeleteButton = false;
        }

        [Obsolete("Order detial waiting deprecated in v 1.0.4")]
        private async Task LoadWaitOrderDetails()
        {
            var result = await orderManager.FindWaitingOrderDetails();
            if (result != null
                && result.Any())
            {
                AddToList(result, false);
            }
        }

        private async Task CancelToken()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                await Task.Delay(200);
            }
            CancellationTokenSource(productDetailCancellationToken);
            CancellationTokenSource(groupByProductCancellationToken);
        }

        #region actions
        private async Task OrderDetailClick(OrderDetail orderDetail)
        {
            if (orderDetail.IsObserved())
            {
                await OpenOrderDetailWithObservation(orderDetail);
            }
            else
            {
                await OpenProductDetail(orderDetail);
            }
        }

        private async Task OpenOrderDetailWithObservation(OrderDetail orderDetail)
        {
            if (orderDetail.IsSimple())
            {
                await OpenMenuSectionByProductObserved(orderDetail);
            }
            else
            {
                await OpenProductCompoundDetail(orderDetail);
            }
        }

        private async Task OpenMenuSectionByProductObserved(OrderDetail orderDetail)
        {
            if (orderDetail.IsGroupObserved())
            {
                await OpenProductDetail(orderDetail);
            }
            else
            {
                await OpenMenuSectionByOrderDetail(orderDetail);
            }

        }

        private async Task OpenMenuSectionByOrderDetail(OrderDetail orderDetail, bool passOrderDetail = true)
        {
            var place = await orderManager.GetCurrentPlace();
            var product = await productBL.FindByID(orderDetail.ProductId);
            var menuSection = await menuSectionBL.FindById(product.MenuSectionId.Value);
            await navigationController.OpenPageByMenuSection(place, menuSection, passOrderDetail ? orderDetail : null);
        }

        private async Task OpenFastOrder(OrderDetail orderDetail)
        {
            var acceptText = Device.RuntimePlatform == Device.Android ? Strings["Accept"] : Strings["OrderUpdateButton"];
            IResult<int> result = await this.dialogs.ShowQuantityDialog(Strings["EditQuantityToOrder"],
                                                                               orderDetail.Quantity,
                                                                               base.appConfig.MIN_QUANTITY_PRODUCT_ADD_TO_ORDER,
                                                                               base.appConfig.MAX_QUANTITY_PRODUCT_ADD_TO_ORDER,
                                                                               acceptText,
                                                                               Strings["Cancel"]);
            if (result.Ok)
            {
                orderDetail.Quantity = result.Result;
                await InsertOrderDetail(orderDetail);
            }
        }

        private async Task OpenProductDetail(OrderDetail orderDetail)
        {
            if (orderDetail.Product.FastOrder.HasValue && orderDetail.Product.FastOrder.Value)
            {
                await OpenFastOrder(orderDetail);
            }
            else if (!orderDetail.IsSimple())
            {
                await OpenProductCompoundDetail(orderDetail);
            }
            else
            {
                productDetailCancellationToken = new CancellationTokenSource();
                var product = await this.productBL.FindByID(orderDetail.ProductId);
                var result = await navigationController.OpenProductDetail(product,
                                                                           orderDetail,
                                                                           productDetailCancellationToken.Token);
                productDetailCancellationToken = null;
            }
        }

        private async Task OpenProductCompoundDetail(OrderDetail orderDetail)
        {
            var possiblePendingOrderDetail = orderDetail.Details.Count == 1 ? orderDetail.Details.FirstOrDefault() : orderDetail.Details.FirstOrDefault((arg) => arg.IsPendingRequest);
            long productId = orderDetail.ProductId;
            var isPending = possiblePendingOrderDetail != null || orderDetail.Details.Count == 1;

            if (isPending)
            {
                orderDetail = possiblePendingOrderDetail;
                productId = orderDetail.ParentProductId.Value;
            }

            if (!isPending && orderDetail.IsGroupObserved())
            {
                OpenMenuSectionByOrderDetail(orderDetail);
                this.orderManager.DeleteOrderDetail(orderDetail);
            }
            else
            {
                var product = await productBL.FindByID(productId);
                if (!isPending || orderDetail.IsObserved() || !product.IsCompoundPromoEqual())
                {
                    var menuSection = await menuSectionBL.FindById(product.MenuSectionId.Value);
                    this.groupByProductCancellationToken = new CancellationTokenSource();
                    await navigationController.OpenGroupsByProduct(menuSection, product, orderDetail, isPending, this.groupByProductCancellationToken.Token);
                    this.groupByProductCancellationToken = null;
                }
            }
        }

        private async Task ItemOrderDetailClick(SelectableItem<OrderDetail> orderDetail)
        {
            ValidateSelectedItems();
        }

        private async Task InsertOrderDetail(OrderDetail orderDetail)
        {
            try
            {
                await orderManager.InsertOrUpdateOrderDetail(orderDetail);
                await ReLoad();
            }
            catch (Exception e)
            {
                base.logger.Error(TAG, e.Message);
            }
        }

        public async Task QuestionDeleteOrderDetails()
        {
            if (GetOrderDetailsToDeleted().Count > 0)
            {
                bool result = await this.dialogs.ShowConfirmDialog(Strings["OrderDetailDeleteTitle"],
                                                               Strings["OrderDetailDeleteMessage"],
                                                               Strings["Delete"],
                                                               Strings["Cancel"]);
                if (result)
                {
                    await DeleteOrderDetails();
                }
            }
        }

        private async Task DeleteOrderDetails()
        {
            var dialog = this.dialogs.ProgressDialog(Strings["OrderDetailDeleteProgress"]);
            await dialog.Show();
            var deleteResult = await orderManager.DeleteOrderDetails(GetOrderDetailsToDeleted());
            await dialog.Hide();
            if (deleteResult)
            {
                await ReLoad();
            }
        }

        private bool CheckOrderDetails()
        {
            if (MyOrderDetailList.Any())
            {
                return true;
            }
            else
            {
                this.dialogs.ShowError(Strings["OrderDetailSendToKitchenEmptyProductsError"]);
                return false;
            }
        }

        private List<OrderDetail> GetOrderDetailsToDeleted()
        {
            return MyOrderDetailList
                        .SelectedItems
                        .Where((filter) => !filter.IsWaiting())
                        .ToList();
        }
        #endregion
        #endregion

        #region suggestions
        private async Task LoadMenuSectionSuggestion()
        {
            if (MenuSectionSuggestionList.Count == 0)
            {
                try
                {
                    IsLoadingMenuSectionSuggestion = true;
                    IPaggination paggination = new Paggination(MenuSectionSuggestionList.Count, LIMIT_MENU_SECTION_SUGGESTION);
                    var suggestionMenu = await orderManager.FindSuggestionMenuSection(paggination);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MenuSectionSuggestionList.AddRange(suggestionMenu);
                    });
                }
                catch (Exception e)
                {
                    base.logger.Error(TAG, e.Message);
                }
                finally
                {
                    IsLoadingMenuSectionSuggestion = false;
                }
            }
        }

        #region actions
        private async Task MenuSectionItemClick(MenuSection menuSection)
        {
            Place currentPlace = await orderManager.GetCurrentPlace();
            await navigationController.OpenPageByMenuSection(currentPlace, menuSection);
        }
        #endregion
        #endregion

        #region events
        /// <summary>
        /// Validates the selected items.
        /// </summary>
        private async Task ValidateSelectedItems()
        {
            VisibilityDeleteButton = MyOrderDetailList != null
                                          && MyOrderDetailList.SelectedItems != null
                                                              && GetOrderDetailsToDeleted().Count > 0;
        }
        #endregion

        protected async Task SendToKitchen()
        {
            if (await CheckOrder())
            {
                await this.orderManager.ClearCurrentTable();
                TrySendToKitchen();
            }
        }


        private async Task<bool> CheckOrder()
        {
            var orderDetails = await orderManager.FindPendingOrderDetail();

            var hasObserved = orderDetails.Exists(s => s.IsObserved());
            var hasNotObserved = orderDetails.Exists(s => !s.IsObserved() && s.ParentId == null);

            if (!hasNotObserved)
            {
                await this.dialogs.ShowAlert(Strings["OrderDetailStatusErrorAllObserved"]);
                return false;
            }
            else if (hasObserved)
            {
                var result = await this.dialogs.ShowConfirmDialog(Strings["OrderDetailStatusTitle"],
                                                              Strings["OrderDetailStatusMessage"],
                                                              Strings["Accept"],
                                                              Strings["Cancel"]);
                return result;
            }
            else
            {
                return true;
            }
        }

        internal override async Task ShowErrorInvalidProducts(List<Product> invalidProducts)
        {
            await ReLoad();
            var listNameProducts = String.Join("\n", invalidProducts.Select(product => product.Name).ToArray());
            await this.dialogs.ShowAlert(String.Format(Strings["SendToKitchenInvalidProductsError"], listNameProducts));
        }

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "my_order_detail";
        }
        #endregion
    }
}
