﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Garzon.BL;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Facade;
    using Garzon.Config;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Core.Views.Behaviours;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.EventBus;
    using Garzon.Forms.UI.EventBus.PlaceChanges;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Garzon.BL.Common;
    using Garzon.Forms.UI.EventBus;
    using System.Collections.Generic;
    using System.Linq;

    public class PlaceListViewModel : OrderUserValidationViewModel, IDinamicBehaviourView
    {
        #region dependency
        readonly IPlaceBL _placeBL;
        readonly IMenuBL _menuBL;
        readonly ICheckVersionBL checkVersionBL;
        #endregion

        #region fields to binding

        public MvxObservableCollection<Place> PlaceList { get; } = new MvxObservableCollection<Place>();

        public Boolean _isFirstLoading = true;

        public Boolean IsFirstLoading
        {
            get => _isFirstLoading;
            set => SetProperty(ref _isFirstLoading, value);
        }

        public Boolean _isLoading = false;

        public Boolean IsLoading
        {
            get => _isLoading;
            set => SetProperty(ref _isLoading, value);
        }

        public Boolean _isLoadingMain = false;
        private bool _firstTime = true;

        public Boolean IsLoadingMain
        {
            get => _isLoadingMain;
            set => SetProperty(ref _isLoadingMain, value);
        }

        #endregion

        #region command
        public IMvxAsyncCommand PullToRefreshCommand => new MvxAsyncCommand(PullToRefresh);
        public IMvxAsyncCommand<Place> ItemClickCommand => new MvxAsyncCommand<Place>(ItemClick);
        #endregion

        #region fields
        const int LIMIT = 40;
        private IEventToken onResumeAppEventToken;
        #endregion

        #region constants
        private readonly string TAG = typeof(PlaceListViewModel).FullName;
        #endregion

        #region constructor
        public PlaceListViewModel(ILogger logger,
                                  IAnalytics analytics,
                                  INavigationController navigationController,
                                  AppDefaultConfig appConfig,
                                  IDialogs dialogs,
                                  IPlaceBL placeBL,
                                  IOrderManager orderManager,
                                  IUserSessionManager userSessionManager,
                                  IMenuBL menuBL,
                                  IEventBus eventBus,
                                  ICheckVersionBL checkVersionBL) : base(logger, analytics, navigationController, appConfig, orderManager, dialogs, eventBus, userSessionManager)
        {
            _placeBL = placeBL;
            _menuBL = menuBL;
            this.checkVersionBL = checkVersionBL;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            ConfigureDinamicView();
            await Subscribe();
            CheckAppVersion();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            _eventBus.Publish(PlaceChangeEvent.Builder(this));
            LoadFirstTime();
        }
        #endregion

        #region events

        private async Task Subscribe()
        {
            this.onResumeAppEventToken = await this.EventBus.Subscribe<OnResumeAppEvent>(OnResumeApp);
        }

        private async void OnResumeApp(OnResumeAppEvent obj)
        {
            try
            {
                if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.Android)
                {
                    await Task.Delay(250);
                }
                await CheckAppVersion();
            }
            catch
            {
                await CheckAppVersion();
            }

        }

        #endregion
        #region private methods

        private async Task LoadFirstTime()
        {
            if (_firstTime)
            {
                _firstTime = false;
                Load();
            }
        }

        private async Task Load(bool isPullToRefresh = false)
        {
            SetDinamicBehaviourVisibility(false);
            try
            {
                IsLoadingMain = PlaceList.Count == 0;
                IsLoading = !IsLoadingMain;
                var position = await GetLocation();
                int offset = isPullToRefresh ? 0 : PlaceList.Count;

                double? latitud = null;
                double? longitude = null;

                if (position != null)
                {
                    latitud = position.Latitude;
                    longitude = position.Longitude;
                }
                IPaggination paggination = new Paggination(offset, LIMIT);
                var items = await _placeBL.Find(paggination, latitud, longitude);
                if (isPullToRefresh)
                {
                    PlaceList.Clear();
                }

                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    PlaceList.AddRange(items);
                    CheckStateUI();
                });

            }
            catch (UserSessionException e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                RedirectByReasonUserBlock();
            }
            catch (Exception e1)
            {
                base.logger.Error(TAG, e1.Message);
                ErrorConnectionUI(PlaceList.Count > 0);
            }
            finally
            {
                IsLoading = false;
                IsLoadingMain = false;
            }
        }

        private void CheckStateUI()
        {
            if (PlaceList.Count == 0)
            {
                EmptyUI(Strings["PlacesNotFoundTitle"], Strings["PlacesNotFoundDescription"]);
            }
            else
            {
                SetDinamicBehaviourVisibility(false);
            }
        }

        #endregion

        #region actions

        private async Task PullToRefresh()
        {
            await Load(true);
        }

        private async Task ItemClick(Place place)
        {
            if (await orderManager.ValidateCurrentOrderByPlace(place.Id)
                || await QuestionChangePlace())
            {
                await GoToPlace(place);
            }

        }

        private async Task<bool> QuestionChangePlace()
        {
            var currentPlace = await orderManager.GetCurrentPlace();
            var result = await this.dialogs.ShowConfirmDialog(Strings["QuestionChangePlaceTitle"],
                                                          String.Format(Strings["QuestionChangePlaceMessage"], currentPlace.Name),
                                                          Strings["QuestionChangePlaceAccept"],
                                                          Strings["QuestionChangePlaceCancel"]);

            if (result)
            {
                await orderManager.ClearCurrentOrder();
            }

            return result;
        }

        private async Task GoToPlace(Place place)
        {
            if (place.Modes != null && place.Modes.Any())
            {
                PlaceMode placeMode = null;
                if (place.Modes.Count > 1 || place.Modes.FirstOrDefault().Id != (int)PlaceMode.ServerPlaceModeIds.RESTO)
                {
                    placeMode = await this.SelectPlaceModeByPlace(place);
                }
                else
                {
                    placeMode = place.Modes.FirstOrDefault();
                }

                if (placeMode != null)
                {
                    if (placeMode.Id != (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY_EXIST_ORDER)
                    {
                        await this.orderManager.SetOrderMode((int)placeMode.Id);
                        Menu menu = await DownloadMenuByPlace(place);
                        if (menu != null)
                        {
                            await OpenPlace(place, placeMode);
                        }
                    }
                    else
                    {
                        await this.navigationController.OpenTakeAwayExistOrder(place);
                    }
                }
            }
        }

        private Task<PlaceMode> SelectPlaceModeByPlace(Place place)
        {
            return this.navigationController.OpenSelectPlaceMode(place.Modes);
        }

        private async Task OpenPlace(Place place, PlaceMode placeMode)
        {
            await orderManager.SetCurrentPlace(place);
            await _eventBus.Publish(PlaceChangeEvent.Builder(this, place));
            await navigationController.OpenMenuSectionByPlace(place);
        }

        #endregion

        #region DinamicBehaviourView
        private void ConfigureDinamicView()
        {
            DinamicBehaviourView = this;
        }
        #region Implementation of IDinamicBehaviourView


        public async Task TapView()
        {
            await Load();
        }
        #endregion
        #endregion

        #region DownloadMenu
        /// <summary>
        /// Downloads the menu by place.
        /// </summary>
        /// <returns>The menu by place.</returns>
        /// <param name="place">Place.</param>
        private async Task<Menu> DownloadMenuByPlace(Place place)
        {
            var dialog = this.dialogs.ProgressDialog(Strings["DownloadingMenu"]);
            await dialog.Show();
            //download menu
            try
            {
                return await _menuBL.FindMenuByPlace(place.Id);
            }
            catch (Exception e)
            {
                logger.Error(TAG, e.Message);
                ShowErrorDownloadMenu(e);
                return null;
            }
            finally
            {
                dialog.Hide();
            }
        }
        /// <summary>
        /// Shows the error download menu.
        /// </summary>
        /// <param name="e">E.</param>
        private void ShowErrorDownloadMenu(Exception e)
        {
            this.dialogs.ShowError(Strings["ErrorDownloadMenu"]);
        }


        private async Task CheckAppVersion()
        {
            try
            {
                var result = await this.checkVersionBL.ValidateAppVersion();
                if (!result)
                {
                    await ShowAppVersionDialog();
                }
            }
            catch (Exception e)
            {
                this.logger.Error(TAG, e.Message);
            }
        }

        private async Task ShowAppVersionDialog()
        {
            var confirm = await this.dialogs.ShowConfirmDialog(Strings["CheckVersionAlertTitle"],
                                                               Strings["CheckVersionAlertMessage"],
                                                               Strings["CheckVersionAlertUpdateButton"],
                                                               Strings["CheckVersionAlertOkButton"]);
            if (confirm)
            {
                var StoreUrl = Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.iOS ? base.appConfig.IOSStoreURL : base.appConfig.AndroidStoreURL;
                Xamarin.Forms.Device.OpenUri(new Uri(StoreUrl));
            }
            else
            {
                CheckAppVersion();
            }

        }

        #endregion

        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "places_list";
        }
        #endregion

    }
}
