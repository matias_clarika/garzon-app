﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;
    using Rg.Plugins.Popup.Services;

    public class SelectPlaceModeViewModel : GarzonBaseViewModel, IMvxViewModel<SelectPlaceModeViewModel.Params>
    {
        #region private
        private TaskCompletionSource<PlaceMode> taskCompletionSource;
        private bool isCompleted = false;
        #endregion
        #region bindings
        public MvxObservableCollection<PlaceMode> Modes { get; } = new MvxObservableCollection<PlaceMode>();
        public IMvxCommand<PlaceMode> ItemClickCommand => new MvxAsyncCommand<PlaceMode>(this.OnItemClick);
        #endregion
        public SelectPlaceModeViewModel(ILogger logger = null,
                                        IAnalytics analytics = null,
                                        INavigationController navigationController = null,
                                        AppDefaultConfig appConfig = null,
                                        IDialogs dialogs = null) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
        }

        #region IMvxViewModel
        public void Prepare(Params parameter)
        {
            this.AddNewModes(parameter.Modes);
            this.taskCompletionSource = parameter.TaskCompletetionSource;
        }
        #endregion
        #region lifecycle
        public override void ViewDisappearing()
        {
            base.ViewDisappearing();
            if (!this.isCompleted && this.taskCompletionSource != null)
            {
                this.taskCompletionSource.TrySetResult(null);
            }

        }
        #endregion
        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "select_place_mode";
        }
        #endregion

        #region private methods

        private void AddNewModes(List<PlaceMode> modes)
        {

            this.Modes.AddRange(modes);
            if (modes.Exists((obj) => obj.Id == (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY))
            {
                this.Modes.Add(PlaceMode.BuildTakeAwayExist());
            }
        }

        private async Task OnItemClick(PlaceMode placeMode)
        {
            this.isCompleted = true;
            await PopupNavigation.Instance.PopAsync();
            this.taskCompletionSource.TrySetResult(placeMode);
            this.taskCompletionSource = null;

        }

        #endregion
        #region params
        public class Params
        {
            public List<PlaceMode> Modes
            {
                get;
                private set;
            }

            public TaskCompletionSource<PlaceMode> TaskCompletetionSource
            {
                get;
                private set;
            }

            private Params(List<PlaceMode> modes, TaskCompletionSource<PlaceMode> taskCompletetionSource)
            {
                this.Modes = modes;
                this.TaskCompletetionSource = taskCompletetionSource;
            }

            /// <summary>
            /// Build the specified modes.
            /// </summary>
            /// <returns>The build.</returns>
            /// <param name="modes">Modes.</param>
            public static Params Build(List<PlaceMode> modes, TaskCompletionSource<PlaceMode> taskCompletetionSource)
            {
                return new Params(modes, taskCompletetionSource);
            }
        }
        #endregion
    }
}
