﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.EventBus;
    using Garzon.Core.Utils;
    using Garzon.Core.ViewModels;
    using Garzon.Core.Views.Behaviours;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.EventBus.OrderNotifications;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Utils;
    using MvvmCross.Core.ViewModels;
    using MvvmCross.Platform;
    using Plugin.Connectivity;
    using Plugin.Geolocator;
    using Plugin.Geolocator.Abstractions;
    using Plugin.Permissions.Abstractions;
    using Xamarin.Forms;

    public abstract class GarzonBaseViewModel : BaseViewModel
    {
        #region dependency
        public readonly INavigationController navigationController;

        private IEventBus _eventBus;

        protected IEventBus EventBus
        {
            get
            {
                if (_eventBus == null)
                {
                    _eventBus = Mvx.Resolve<IEventBus>();
                }
                return _eventBus;
            }
        }
        #endregion

        #region fields to binding
        private ImageSource _backImageSource;

        public ImageSource BackImageSource
        {
            get => _backImageSource;
            set => SetProperty(ref _backImageSource, value);
        }

        private bool showWaiterCall = true;

        public bool ShowWaiterCall
        {
            get => this.showWaiterCall;
            set => this.SetProperty(ref this.showWaiterCall, value);
        }
        #endregion
        #region command

        public IMvxAsyncCommand PMyOrderCommand => new MvxAsyncCommand(GoToMyOrder);

        public IMvxAsyncCommand TapDinamicBehaviourCommand => new MvxAsyncCommand(TapView);
        public IMvxAsyncCommand CallWaiterCommand => new MvxAsyncCommand(OpenCallWaiter);
        #endregion

        #region behaviour view
        public IDinamicBehaviourView DinamicBehaviourView
        {
            get;
            set;
        }

        private Boolean _dinamicBehaviourVisibility = false;

        public Boolean DinamicBehaviourVisibility
        {
            get => _dinamicBehaviourVisibility;
            set => SetProperty(ref _dinamicBehaviourVisibility, value);
        }

        private string _dinamicBehaviourTitle = "";

        public string DinamicBehaviourTitle
        {
            get => _dinamicBehaviourTitle;
            set => SetProperty(ref _dinamicBehaviourTitle, value);
        }

        private string _dinamicBehaviourDescription = "";

        public string DinamicBehaviourDescription
        {
            get => _dinamicBehaviourDescription;
            set => SetProperty(ref _dinamicBehaviourDescription, value);
        }

        #endregion
        #region fields
        protected bool _isActive;
        #endregion
        #region constructor
        protected GarzonBaseViewModel(ILogger logger = null,
                                      IAnalytics analytics = null,
                                      INavigationController navigationController = null,
                                      AppDefaultConfig appConfig = null,
                                      IDialogs dialogs = null) : base(logger,
                                                                      analytics,
                                                                      appConfig,
                                                                      dialogs)
        {
            this.navigationController = navigationController;
        }

        #endregion

        #region lifecycle

        public override async Task Initialize()
        {
            await CheckWaiterCallVisibility();
            await base.Initialize();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            EventBus.Publish(CheckNotificationEvent.Builder(this));
        }

        #endregion

        #region private
        private async Task CheckWaiterCallVisibility()
        {
            var orderManager = Mvx.Resolve<IOrderManager>();

            if (orderManager != null)
            {
                var mode = await orderManager.GetOrderMode();
                this.ShowWaiterCall = mode == (int)PlaceMode.ServerPlaceModeIds.RESTO;
            }
        }

        #endregion
        #region action

        private async Task GoToMyOrder()
        {
            await navigationController.OpenMyOrderDetail();
        }


        protected async Task OpenCallWaiter()
        {
            await navigationController.OpenCallWaiter();
        }
        #endregion

        #region Implementation of IDinamicBehaviourView
        protected async Task TapView()
        {
            SetDinamicBehaviourVisibility(false);

            if (DinamicBehaviourView != null)
            {
                await DinamicBehaviourView.TapView();
            }
        }
        #region view
        /// <summary>
        /// Sets the dinamic behaviour view icon.
        /// </summary>
        /// <param name="icon">Icon.</param>
        public void SetDinamicBehaviourViewIcon(string icon)
        {
            //TODO when design is done
        }
        /// <summary>
        /// Sets the dinamic behaviour view title.
        /// </summary>
        /// <param name="title">Title.</param>
        public void SetDinamicBehaviourViewTitle(string title)
        {
            DinamicBehaviourTitle = title;
        }
        /// <summary>
        /// Sets the dinamic behaviour description.
        /// </summary>
        /// <param name="description">Description.</param>
        public void SetDinamicBehaviourDescription(string description)
        {
            DinamicBehaviourDescription = description;
        }
        /// <summary>
        /// Sets the dinamic behaviour visibility.
        /// </summary>
        /// <param name="isVisible">If set to <c>true</c> is visible.</param>
        public void SetDinamicBehaviourVisibility(bool isVisible = false)
        {
            DinamicBehaviourVisibility = isVisible;
        }
        /// <summary>
        /// Changes the dinamic behaviour user interface.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="description">Description.</param>
        private void ChangeDinamicBehaviourUI(string title = "", string description = "")
        {
            SetDinamicBehaviourVisibility(true);
            SetDinamicBehaviourViewTitle(title);
            SetDinamicBehaviourDescription(description);
        }

        /// <summary>
        /// Empties the user interface.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="description">Description.</param>
        public void EmptyUI(string title = null, string description = null)
        {
            ChangeDinamicBehaviourUI(String.IsNullOrEmpty(title) ? Strings["EmptyTitle"] : title,
                                     String.IsNullOrEmpty(description) ? Strings["EmptyDescription"] : description);
        }

        /// <summary>
        /// Errors the connection user interface.
        /// </summary>
        /// <param name="showAlert">If set to <c>true</c> show alert.</param>
        /// <param name="title">Title.</param>
        /// <param name="description">Description.</param>
        public void ErrorConnectionUI(bool showAlert = false, string title = null, string description = null)
        {
            if (showAlert)
            {
                if (base.dialogs != null)
                {
                    this.dialogs.ShowAlert(Strings["ErrorConnectionToAlert"]);
                }
            }
            else
            {
                ChangeDinamicBehaviourUI(String.IsNullOrEmpty(title) ? Strings["ErrorConnectionTitle"] : title,
                                         String.IsNullOrEmpty(description) ? Strings["ErrorConnectionDescription"] : description);
            }
        }
        #endregion
        #endregion

        #region utils
        protected void CancellationTokenSource(CancellationTokenSource cancellationTokenSource)
        {
            if (cancellationTokenSource != null)
            {

                if (!cancellationTokenSource.IsCancellationRequested)
                {
                    try
                    {
                        cancellationTokenSource.Cancel();
                        cancellationTokenSource.Dispose();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                    }
                }
                cancellationTokenSource = null;
            }
        }
        #endregion

        #region geo
        protected async Task<bool> IsLocationAvailable()
        {
            if (!CrossGeolocator.IsSupported)
                return false;

            if (!await PermissionsUtils.CheckPermissions(Permission.Location))
            {
                return false;
            }
            return CrossGeolocator.Current.IsGeolocationAvailable;
        }

        protected async Task<Position> GetLocation()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                var available = await IsLocationAvailable();
                var enabled = locator.IsGeolocationEnabled;
                if (!available || !enabled)
                {
                    //not available or enabled
                    return null;
                }

                return await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region connection

        /// <summary>
        /// Checks the connection.
        /// </summary>
        /// <returns><c>true</c>, if connection was checked, <c>false</c> otherwise.</returns>
        protected bool CheckConnection()
        {
            return CrossConnectivity.Current.IsConnected;
        }

        /// <summary>
        /// Checks the connection with alert.
        /// </summary>
        /// <returns>The connection with alert.</returns>
        protected async Task<bool> CheckConnectionWithAlert()
        {
            if (!this.CheckConnection())
            {
                await this.dialogs.ShowAlert(Strings["ErrorConnectionDeviceAfterTime"]);
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
