﻿using System;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Facade.Auth;
using Garzon.Forms.UI.Navigation;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using System.Collections.ObjectModel;
using Garzon.DataObjects.Social;
using Garzon.DAL.DataSource.Remote.Exceptions;
using System.Collections.Generic;

namespace Garzon.Forms.UI.ViewModels
{
    public class LoginViewModel : GarzonBaseViewModel, IMvxViewModelResult<LoginResult>
    {
        #region dependency
        protected readonly IOrderManager orderManager;
        protected readonly IUserSessionManager userSessionManager;
        #endregion

        #region fields to binding
        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private bool _googleSignInVisible = false;

        public bool GoogleSignInVisible
        {
            get => _googleSignInVisible;
            set => SetProperty(ref _googleSignInVisible, value);
        }

        private string _tableNumber;
        public string TableNumber
        {
            get => _tableNumber;
            set => SetProperty(ref _tableNumber, value);
        }

        private User _user;

        public User User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        private bool _isUserLogged;

        public bool IsUserLogged
        {
            get => _isUserLogged;
            set => SetProperty(ref _isUserLogged, value);
        }

        private Sector _selectedSector;
        public Sector SelectedSector
        {
            get => _selectedSector;
            set => SetProperty(ref _selectedSector, value);
        }


        private ObservableCollection<Sector> _sectorList = new ObservableCollection<Sector>();
        public ObservableCollection<Sector> SectorList
        {
            get => _sectorList;
            set => SetProperty(ref _sectorList, value);
        }

        protected bool NeedSectors = true;
        #endregion

        #region command
        public IMvxAsyncCommand LoginFacebookCommand => new MvxAsyncCommand(LoginFacebook);
        public IMvxAsyncCommand LoginGoogleCommand => new MvxAsyncCommand(LoginGoogle);
        public IMvxAsyncCommand ContinueCommand => new MvxAsyncCommand(SendTableNumber);
        public IMvxAsyncCommand LogoutCommand => new MvxAsyncCommand(Logout);
        #endregion

        #region constants
        private readonly string TAG = typeof(LoginViewModel).FullName;
        #endregion

        #region constructor
        public LoginViewModel(ILogger logger,
                              IAnalytics analytics,
                              INavigationController navigationController,
                              AppDefaultConfig appConfig,
                              IDialogs dialogs,
                              IUserSessionManager userSessionManager,
                              IOrderManager orderManager) : base(logger, analytics, navigationController, appConfig, dialogs)
        {
            this.userSessionManager = userSessionManager;
            this.orderManager = orderManager;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            await ConfigureByPlace();
            await CheckUI();
        }
        #endregion

        #region general
        private async Task CheckUI()
        {
            GoogleSignInVisible = Device.RuntimePlatform == Device.Android;
            User = orderManager.GetCurrentUser();
            IsUserLogged = User != null;
        }

        protected virtual async Task ConfigureByPlace()
        {
            if (orderManager != null)
            {
                Place currentPlace = await orderManager.GetCurrentPlace();
                if (currentPlace != null)
                {
                    Title = currentPlace.Name;
                    AddSectors(currentPlace);
                }
            }
        }

        /// <summary>
        /// Adds the sectors.
        /// </summary>
        /// <param name="currentPlace">Current place.</param>
        private void AddSectors(Place currentPlace)
        {
            if (this.NeedSectors && currentPlace.Sectors != null)
            {
                foreach (var sector in currentPlace.Sectors)
                {
                    SectorList.Add(sector);
                }
            }
        }

        #endregion
        private async Task SocialLogin(SocialPlatform socialPlatform)
        {
            if (ValidateTableNumber())
            {
                IProgressDialog progress = null;
                try
                {
                    SocialUser socialUser = await SocialLoginNativePlatform(socialPlatform);
                    progress = this.dialogs.ProgressDialog(Strings["SignInLoading"]);
                    await progress.Show();
                    User user = await userSessionManager.SignInGarzonSocial(socialPlatform, socialUser);
                    await progress.Hide();
                    if (user != null)
                    {
                        await SignLocal(user);
                    }
                }
                catch (UserSessionException e)
                {
                    await HideProgressDialog(progress);
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    await AnalyzeUserException(e);
                }
                catch (Exception e)
                {
                    await HideProgressDialog(progress);
                    base.logger.Error(TAG, e.Message);
                    await ShowErrorByPlayform(socialPlatform);
                }
            }
        }

        private async Task HideProgressDialog(IProgressDialog progress)
        {
            if (progress != null)
            {
                await progress.Hide();
            }
        }

        private async Task<SocialUser> SocialLoginNativePlatform(SocialPlatform socialPlatform)
        {
            return await userSessionManager.SignInNativeSocial(socialPlatform);
        }

        private async Task ShowErrorByPlayform(SocialPlatform socialPlatform)
        {
            switch (socialPlatform)
            {
                case SocialPlatform.Facebook:
                    await this.dialogs.ShowError(Strings["SignInFacebookError"]);
                    break;
                case SocialPlatform.Google:
                    await this.dialogs.ShowError(Strings["SignInGoogleError"]);
                    break;

            }
        }
        #region facebook
        public async Task LoginFacebook()
        {
            await SocialLogin(SocialPlatform.Facebook);
        }

        #endregion

        #region google
        public async Task LoginGoogle()
        {
            await SocialLogin(SocialPlatform.Google);
        }
        #endregion

        #region local
        private async Task SignLocal(User user)
        {
            if (user != null)
            {
                try
                {
                    user = await userSessionManager.SignInLocalUser(user);
                    await ProcessUser(user);
                }
                catch (Exception e1)
                {
                    System.Diagnostics.Debug.WriteLine(e1.Message);
                    await this.dialogs.ShowError(Strings["SignInLocalError"]);
                }
            }
        }

        private async Task AnalyzeUserException(UserSessionException e)
        {
            string message;
            switch (e.Error)
            {
                case UserSessionException.ERROR.BLOCK_USER_TEMPORALY:
                    await RefreshUIByUserBlocked();
                    string pendingTime = FormatPrettyPendingTime(e);
                    message = String.Format(Strings["UserBlockTemporalyMessage"], pendingTime);
                    break;
                case UserSessionException.ERROR.BLOCK_USER_PERMANENT:
                    await RefreshUIByUserBlocked();
                    message = Strings["UserBlockPermanentMessage"];
                    break;
                case UserSessionException.ERROR.GENERAL_ERROR:
                    message = Strings["UserLoginInvalidAccount"];
                    break;
                default:
                    message = Strings["UserLoginInvalidAccount"];
                    break;
            }

            await this.dialogs.ShowError(message);
        }

        private string FormatPrettyPendingTime(UserSessionException e)
        {
            string pendingTime = "";
            if (e.PendingTime.HasValue)
            {
                if (e.PendingTime.Value == 60)
                {
                    pendingTime = $"1 {Strings["Minute"]}";
                }
                else if (e.PendingTime.Value > 60)
                {
                    pendingTime = $"{(int)(e.PendingTime.Value / 60)} {Strings["Minutes"]}";
                }
                else
                {
                    pendingTime = $"{e.PendingTime.Value} {Strings["Seconds"]}";
                }
            }

            return pendingTime.ToLower();
        }

        private async Task RefreshUIByUserBlocked()
        {
            await userSessionManager.SignOut();
            await CheckUI();
        }

        #endregion
        #region user logged
        public async Task SendTableNumber()
        {
            if (ValidateTableNumber())
            {
                await ProcessUser(User);
            }
        }

        public async Task Logout()
        {
            await userSessionManager.SignOut();
            ClearUser();
            await CheckUI();
        }

        private void ClearUser()
        {
            User = null;
        }

        #endregion
        private bool ValidateTableNumber()
        {
            if (!this.NeedSectors)
            {
                return true;
            }
            else
            {
                if (SelectedSector == null)
                {
                    this.dialogs.ShowAlert(Strings["SectorIsRequired"]);
                    return false;
                }

                var result = true;
                try
                {
                    result = !String.IsNullOrEmpty(TableNumber) && int.Parse(TableNumber) > 0;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    result = false;
                }

                if (!result)
                {
                    this.dialogs.ShowAlert(Strings["TableNumberIsRequired"]);
                }

                return result;
            }

        }

        private async Task ProcessUser(User user)
        {
            if (user != null)
            {
                await SuccessLogin(user);
            }
        }

        protected virtual async Task SuccessLogin(User user)
        {
            var result = LoginResult.Builder()
                                    .WithUser(user);
            if (this.NeedSectors)
            {
                result.WithTableNumber(SelectedSector.Id, int.Parse(TableNumber));
            }

            await navigationController.Back(this, result);
        }


        #region analytics
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "login";
        }
        #endregion
    }
    public class LoginResult
    {
        private LoginResult()
        {

        }

        #region inmutable fields
        private int _tableNumber;

        public int TableNumber
        {
            get => _tableNumber;

        }

        private long _sectorId;

        public long SectorId
        {
            get => _sectorId;
        }

        private User _user;

        public User User
        {
            get => _user;
        }

        public bool IsValid(bool tableNumberIsRequired = true)
        {
            return User != null && (!tableNumberIsRequired || (SectorId > 0 && TableNumber > 0));
        }
        #endregion

        #region builder
        public static LoginResult Builder()
        {
            return new LoginResult();
        }

        public LoginResult WithTableNumber(long sectorId,
                                           int tableNumber)
        {
            _sectorId = sectorId;
            _tableNumber = tableNumber;
            return this;
        }

        public LoginResult WithUser(User user)
        {
            _user = user;
            return this;
        }
        #endregion

    }
}
