﻿namespace Garzon.Forms.UI.ViewModels
{
    using System;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.Forms.UI.Navigation;

    public class LoginWithoutTableViewModel : LoginViewModel
    {
        public LoginWithoutTableViewModel(ILogger logger,
                                          IAnalytics analytics,
                                          INavigationController navigationController,
                                          AppDefaultConfig appConfig,
                                          IDialogs dialogs,
                                          IUserSessionManager userSessionManager,
                                          IOrderManager orderManager) : base(logger, analytics, navigationController, appConfig, dialogs, userSessionManager, orderManager)
        {
            this.NeedSectors = false;
        }
    }
}
