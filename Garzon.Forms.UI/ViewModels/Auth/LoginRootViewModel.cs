﻿using Clarika.Xamarin.Base.Helpers;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using Garzon.Forms.UI.Facade.Auth;
using Garzon.Forms.UI.Navigation;

namespace Garzon.Forms.UI.ViewModels
{
    public class LoginRootViewModel : LoginViewModel
    {
        public LoginRootViewModel(ILogger logger,
                                  IAnalytics analytics,
                                  INavigationController navigationController,
                                  AppDefaultConfig appConfig,
                                  IDialogs dialogs,
                                  IUserSessionManager userSessionFacade,
                                  IOrderManager orderFacade) : base(logger, analytics, navigationController, appConfig, dialogs, userSessionFacade, orderFacade)
        { 
        }

    }
}
