﻿using System;
using Garzon.Forms.UI.ViewModels;
using MvvmCross.Forms.Views;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public class ContentPage<TViewModel> : MvxContentPage<TViewModel>
        where TViewModel : GarzonBaseViewModel

    {

        public ContentPage(string backButtonTitle = "")
        {
            NavigationPage.SetBackButtonTitle(this, backButtonTitle);
        }
    }
}

