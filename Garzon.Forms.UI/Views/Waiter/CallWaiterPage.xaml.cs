﻿using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.Views
{
    public partial class CallWaiterPage : MasterDetailContentPage<CallWaiterViewModel>
    {
        public CallWaiterPage()
        {
            InitializeComponent();
        }
    }
}
