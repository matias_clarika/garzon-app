﻿using Garzon.Forms.UI.ViewModels;
using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms.Xaml;

namespace Garzon.Forms.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxMasterDetailPagePresentation(Position = MasterDetailPosition.Detail, WrapInNavigationPage = true , NoHistory = true, Animated = true)]
    public class MasterDetailRootContentPage<TViewModel> : MasterDetailContentPage<TViewModel>
        where TViewModel : GarzonBaseViewModel
    {
        public MasterDetailRootContentPage()
        {
        }
    }
}

