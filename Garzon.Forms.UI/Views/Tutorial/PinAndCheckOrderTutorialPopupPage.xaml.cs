﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using I18NPortable;
    using Rg.Plugins.Popup.Pages;
    using Rg.Plugins.Popup.Services;
    using Xamarin.Forms;

    public partial class PinAndCheckOrderTutorialPopupPage : PopupPage
    {
        public II18N Strings => I18N.Current;

        public PinAndCheckOrderTutorialPopupPage()
        {
            InitializeComponent();

            pinNumberLabel.Text = Strings["PinNumberTutorialText"];
            checkOrderLabel.Text = Strings["CheckOrderTutorialText"];
            pinAndCheckoutTutorialButton.Text = Strings["TutorialButtonText"];
            pinAndCheckoutTutorialButton.Clicked += OnTapped;
        }

        public void OnTapped(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
    }
}
