﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using I18NPortable;
    using Rg.Plugins.Popup.Pages;
    using Rg.Plugins.Popup.Services;
    using Xamarin.Forms;

    public partial class HowToCallCounterPopupPage : PopupPage
    {
        public II18N Strings => I18N.Current;

        public HowToCallCounterPopupPage()
        {
            InitializeComponent();
            counterTutorialText.Text = Strings["CounterTutorialText"];
            counterTutorialButton.Text = Strings["TutorialButtonText"];
            counterTutorialButton.Clicked += OnTapped;
        }

        public void OnTapped(object sender, EventArgs e){
            PopupNavigation.Instance.PopAsync();
        }
    }
}
