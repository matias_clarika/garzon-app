﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class NavigationMenuItemsView : ContentView
    {
        public NavigationMenuItemsView()
        {
            InitializeComponent();
        }

        #region private

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            list.SelectedItem = null;

        }
        #endregion
    }
}
