﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Garzon.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;
    using MvvmCross.Forms.Views.Attributes;
    using Xamarin.Forms;

    [MvxModalPresentation(WrapInNavigationPage = false)]
    public partial class WebPage : MvxContentPage<WebViewModel>
    {
        private const string FIRE_EVENT_SUCCESS = "fire_event=success";
        public WebPage()
        {
            InitializeComponent();
        }

        #region handlers

        async void Handle_Navigating(object sender, WebNavigatingEventArgs e)
        {
            if (await CheckUrlEventAsync(e))
            {
                if (this.ViewModel is WebViewModel)
                {
                    await this.ViewModel.LoadingCommand.ExecuteAsync();
                }
            }

        }

        private async Task<bool> CheckUrlEventAsync(WebNavigatingEventArgs e)
        {
            if (e.Url.Contains(FIRE_EVENT_SUCCESS))
            {
                try
                {
                    e.Cancel = true;
                    await this.Navigation.PopModalAsync();
                    return false;
                }
                catch(Exception e1)
                {
                    System.Diagnostics.Debug.WriteLine(e1.Message);
                    return true;
                }


            }
            else
            {
                return true;
            }
        }

        void Handle_Navigated(object sender, WebNavigatedEventArgs e)
        {
            if (this.ViewModel is WebViewModel)
            {
                this.ViewModel.FinishCommand.ExecuteAsync();
            }
        }

        #endregion
    }
}
