﻿using System;
using System.Collections.Generic;
using Garzon.Forms.UI.ViewModels;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class ProductCommentPage : MasterDetailContentPage<ProductCommentViewModel>
    {
        public ProductCommentPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            editorComment.Focus();
        }
    }
}
