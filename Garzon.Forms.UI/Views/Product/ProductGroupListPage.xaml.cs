﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Garzon.Forms.UI.ViewModels;
    using Xamarin.Forms;

    public partial class ProductGroupListPage : MasterDetailContentPage<ProductGroupListViewModel>
    {
        public ProductGroupListPage()
        {
            InitializeComponent();
        }
    }
}
