﻿using Garzon.Forms.UI.ViewModels;
using MvvmCross.Forms.Views;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class ProductWithoutImagePage : MasterDetailContentPage<ProductWithoutImageViewModel>
    {
        public ProductWithoutImagePage()
        {
            InitializeComponent();
        }
    }
}
