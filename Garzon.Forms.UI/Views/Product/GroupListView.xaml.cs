﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class GroupListView : ContentView
    {
        public GroupListView()
        {
            InitializeComponent();
        }

        #region private


        async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            listGroup.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }

        #endregion
    }
}
