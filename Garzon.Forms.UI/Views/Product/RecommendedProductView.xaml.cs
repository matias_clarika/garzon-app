﻿using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class RecommendedProductView : ContentView
    {
        public RecommendedProductView()
        {
            InitializeComponent();
        }
        #region private
        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            listRecommendedProduct.SelectedItem = null;
        }
        #endregion
    }
}
