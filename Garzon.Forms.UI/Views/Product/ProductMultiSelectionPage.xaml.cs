﻿namespace Garzon.Forms.UI.Views
{
    using Garzon.Forms.UI.ViewModels;

    public partial class ProductMultiSelectionPage : MasterDetailContentPage<ProductMultiSelectionListViewModel>
    {
        public ProductMultiSelectionPage()
        {
            InitializeComponent();
        }
    }
}
