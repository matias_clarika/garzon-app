﻿using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.Views
{
    public partial class ProductPage : MasterDetailContentPage<ProductViewModel>
    {
        public ProductPage()
        {
            InitializeComponent();
        }
    }
}
