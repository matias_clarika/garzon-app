﻿using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class ProductWithoutImageListView : ContentView
    {
        public ProductWithoutImageListView()
        {
            InitializeComponent();
        }

        #region private

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            // listProduct.SelectedItem = null;
        }
        #endregion
    }
}
