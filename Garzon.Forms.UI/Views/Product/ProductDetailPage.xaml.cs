﻿using Garzon.Forms.UI.ViewModels;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class ProductDetailPage : MasterDetailContentPage<ProductDetailViewModel>
    {
        public ProductDetailPage()
        {
            InitializeComponent();
        }
    }
}
