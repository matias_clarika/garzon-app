﻿using Xamarin.Forms;
using Garzon.Forms.UI.ViewModels;
using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms.Xaml;

namespace Garzon.Forms.UI.Views
{
    public partial class PlaceListPage : MasterDetailRootContentPage<PlaceListViewModel>
    {
        public PlaceListPage()
        {
            InitializeComponent();
        }

    }
}
