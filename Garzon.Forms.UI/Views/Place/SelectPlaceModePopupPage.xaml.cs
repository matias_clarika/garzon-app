﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.DataObjects;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.ViewModels;
    using MvvmCross.Core.ViewModels;
    using MvvmCross.Platform;
    using Rg.Plugins.Popup.Pages;
    using Xamarin.Forms;

    public partial class SelectPlaceModePopupPage : PopupPage
    {
        private SelectPlaceModeViewModel viewModel;

        public SelectPlaceModePopupPage(SelectPlaceModeViewModel.Params parameters)
        {
            InitializeComponent();
            this.viewModel = new SelectPlaceModeViewModel(Mvx.Resolve<ILogger>(),
                                                          Mvx.Resolve<IAnalytics>(),
                                                          Mvx.Resolve<INavigationController>(),
                                                          Mvx.Resolve<AppDefaultConfig>(),
                                                          Mvx.Resolve<IDialogs>());
            this.BindingContext = this.viewModel;

            if (this.viewModel is IMvxViewModel<SelectPlaceModeViewModel.Params>)
            {
                ((IMvxViewModel<SelectPlaceModeViewModel.Params>)this.viewModel).Prepare(parameters);
                this.viewModel.Initialize();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (this.viewModel != null)
            {
                this.viewModel.ViewAppearing();
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (this.viewModel != null)
            {
                this.viewModel.ViewDisappearing();
            }
        }

    }
}
