﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Garzon.Forms.UI.ViewModels;
    using Xamarin.Forms;

    public partial class ResumeOrderDetailView : ContentView
    {
        public ResumeOrderDetailView()
        {
            InitializeComponent();
        }

        void Handle_Toggled(object sender, ToggledEventArgs e)
        {
            if (this.BindingContext is ResumeDetailViewModel viewModel)
            {
                viewModel.ToggleSwitchCommand.ExecuteAsync();
            }
        }
    }
}