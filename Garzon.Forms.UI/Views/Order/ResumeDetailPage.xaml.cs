﻿namespace Garzon.Forms.UI.Views
{
    using Garzon.Forms.UI.ViewModels;
    using Xamarin.Forms;

    public partial class ResumeDetailPage : MasterDetailContentPage<ResumeDetailViewModel>
    {
        public ResumeDetailPage()
        {
            InitializeComponent();
        }
    }
}
