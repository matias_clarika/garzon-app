﻿using System;
using System.Collections.Generic;
using Garzon.Forms.UI.ViewModels;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class QuantityDinersPage : MasterDetailContentPage<OrderQuantityDinersViewModel>
    {
        public QuantityDinersPage()
        {
            InitializeComponent();
        }
    }
}
