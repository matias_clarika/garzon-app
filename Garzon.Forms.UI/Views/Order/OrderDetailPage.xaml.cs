﻿using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.Views
{
    public partial class OrderDetailPage : MasterDetailContentPage<MyOrderDetailViewModel>
    {
        public OrderDetailPage()
        {
            InitializeComponent();
        }
    }
}
