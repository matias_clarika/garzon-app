﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Garzon.Forms.UI.ViewModels;
    using Xamarin.Forms;

    public partial class OrderHistoryDetailPage : MasterDetailContentPage<OrderHistoryDetailViewModel>
    {
        public OrderHistoryDetailPage()
        {
            InitializeComponent();
        }
    }
}
