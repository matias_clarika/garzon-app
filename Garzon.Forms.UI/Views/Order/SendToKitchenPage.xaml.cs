﻿using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.Views
{
    public partial class SendToKitchenPage : MasterDetailContentPage<SendToKitchenViewModel>
    {
        public SendToKitchenPage()
        {
            InitializeComponent();
        }
    }
}
