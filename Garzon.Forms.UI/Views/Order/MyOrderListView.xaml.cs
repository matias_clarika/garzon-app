﻿using System;
using Garzon.Forms.UI.ViewModels;
using Garzon.DataObjects;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class MyOrderListView : ContentView
    {
        public MyOrderListView()
        {
            InitializeComponent();
        }
        private void Item_OnTapped(object sender, EventArgs e)
        {
            if (e is TappedEventArgs tappedEventArgs)
            {
                var orderDetail = tappedEventArgs.Parameter as OrderDetail;
                if (this.BindingContext is MyOrderDetailViewModel viewModel)
                {
                    viewModel.OrderDetailClickCommand.ExecuteAsync(orderDetail);
                }
            }
        }
    }



}
