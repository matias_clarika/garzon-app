﻿using System;
using System.Collections.Generic;
using Garzon.Core.Views.Component;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class OrderDetailInServerViewCell : ViewCell
    {
        public OrderDetailInServerViewCell()
        {
            InitializeComponent();
            content.SetBinding(BindingContextProperty, nameof(SelectableItem.Data));
        }
    }
}
