﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class OrderDetailListView : ContentView
    {
        public OrderDetailListView()
        {
            InitializeComponent();
        }

        #region private

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            list.SelectedItem = null;
        }
        #endregion
    }
}
