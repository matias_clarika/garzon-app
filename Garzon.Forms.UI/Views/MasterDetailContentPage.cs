﻿using Garzon.Forms.UI.ViewModels;
using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms.Xaml;

namespace Garzon.Forms.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxMasterDetailPagePresentation(Position = MasterDetailPosition.Detail, NoHistory = false)]
    public class MasterDetailContentPage<TViewModel> : ContentPage<TViewModel>
        where TViewModel : GarzonBaseViewModel
    {
    }
}

