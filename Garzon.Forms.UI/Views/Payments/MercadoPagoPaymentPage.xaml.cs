﻿namespace Garzon.Forms.UI.Views
{
    using System;
    using System.Threading.Tasks;
    using Garzon.Forms.UI.ViewModels;
    using Xamarin.Forms;

    public partial class MercadoPagoPaymentPage : MasterDetailContentPage<MercadoPagoPaymentViewModel>
    {
        public MercadoPagoPaymentPage()
        {
            InitializeComponent();
        }

        void Handle_Navigated(object sender, WebNavigatedEventArgs e)
        {
            this.ViewModel.OnFinish();
        }

        async void Handle_Navigating(object sender, WebNavigatingEventArgs e)
        {
            if (await this.CheckSuccessPayment(e))
            {

            }
        }

        private async Task<bool> CheckSuccessPayment(WebNavigatingEventArgs e)
        {
            if (await this.ViewModel.ValidateSuccessUrl(e.Url))
            {
                e.Cancel = true;
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
