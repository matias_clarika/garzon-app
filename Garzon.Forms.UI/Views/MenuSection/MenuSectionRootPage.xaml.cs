﻿using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.Views
{
    
    public partial class MenuSectionRootPage : MasterDetailRootContentPage<MenuSectionRootViewModel>
    {
        public MenuSectionRootPage()
        {
            InitializeComponent();
        }
    }
}
