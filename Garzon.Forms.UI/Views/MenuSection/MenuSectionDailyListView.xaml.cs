﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class MenuSectionDailyListView : ContentView
    {
        public MenuSectionDailyListView()
        {
            InitializeComponent();
        }

        #region private


        async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            listMenuSection.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }

        #endregion
    }
}
