﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class MenuSectionSuggestionListView : ContentView
    {
        public MenuSectionSuggestionListView()
        {
            InitializeComponent();
        }

        #region private
        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            listMenuSectionSuggestion.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }
        #endregion
    }
}
