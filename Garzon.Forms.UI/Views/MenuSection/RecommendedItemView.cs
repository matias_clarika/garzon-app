﻿using System;
using Garzon.DataObjects;
using I18NPortable;

namespace Garzon.Forms.UI.Views
{
    public class RecommendedItemView
    {
        public II18N Strings => I18N.Current;

        private RecommendedItemView()
        {
        }


        public Product Product
        {
            get;
            private set;
        }

        public OrderDetail OrderDetail
        {
            get;
            private set;
        }

        public string Title
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public string Description
        {
            get;
            private set;
        }

        public double Price
        {
            get;
            private set;
        }

        public string Image
        {
            get;
            private set;
        }
        public RecommendedItemType Type
        {
            get;
            private set;
        }

        public static RecommendedItemView Build(Product product)
        {
            var item = new RecommendedItemView
            {
                Product = product
            };

            item.BuildByProduct();
            return item;

        }

        private void BuildByProduct()
        {
            if (this.Product != null)
            {
                this.Title = Product.Label;
                this.Name = Product.Name;
                this.Description = Product.ShortDescription;
                this.Image = Product.Image;
                this.Type = RecommendedItemType.PRODUCT_RECOMMENDED;
                this.Price = Product.Price;
            }
        }

        public static RecommendedItemView Build(OrderDetail orderDetail)
        {
            var item = new RecommendedItemView
            {
                OrderDetail = orderDetail
            };
            item.BuildByOrderDetail();
            return item;
        }

        private void BuildByOrderDetail()
        {
            if (this.OrderDetail != null)
            {
                this.Title = this.OrderDetail.MenuSectionName;
                this.Name = this.OrderDetail.Product.Name;
                this.Description = $"{this.OrderDetail.GroupName} {Strings["Pending"]}";
                this.Type = RecommendedItemType.ORDER_DETAIL_PENDING_REQUEST;
                this.Image = this.OrderDetail.GroupImage;
            }
        }

        internal Product GetProduct()
        {
            throw new NotImplementedException();
        }
    }

    public enum RecommendedItemType
    {
        PRODUCT_RECOMMENDED, ORDER_DETAIL_PENDING_REQUEST
    }
}
