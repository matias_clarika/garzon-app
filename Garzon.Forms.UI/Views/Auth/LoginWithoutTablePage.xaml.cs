﻿namespace Garzon.Forms.UI.Views
{
    using Garzon.Forms.UI.ViewModels;

    public partial class LoginWithoutTablePage : MasterDetailContentPage<LoginWithoutTableViewModel>
    {
        public LoginWithoutTablePage()
        {
            InitializeComponent();
        }
    }
}
