﻿using System;
using System.Collections.Generic;
using Garzon.Forms.UI.ViewModels;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    
    public partial class LoginRootPage : MasterDetailRootContentPage<LoginRootViewModel>
    {
        public LoginRootPage()
        {
            InitializeComponent();
        }
    }
}
