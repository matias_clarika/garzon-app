﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Views
{
    public partial class LoginView : ContentView
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void TermCondition_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://garzonapp.com/terminosycondiciones.html"));
        }
    }
}
