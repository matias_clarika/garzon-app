﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.Navigation.EventArguments;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;

namespace Garzon.Forms.UI.CustomMvvmCross
{
    public class CustomMvxNavigationService : MvxNavigationService, IMvxNavigationService
    {
        public CustomMvxNavigationService(IMvxNavigationCache navigationCache, IMvxViewModelLoader viewModelLoader) : base(navigationCache, viewModelLoader)
        {
        }

        //HACK This override fix doble navigation on Android when pass a CancellationToken
        protected override async Task<TResult> Navigate<TParameter, TResult>(MvxViewModelRequest request, IMvxViewModel<TParameter, TResult> viewModel, TParameter param, IMvxBundle presentationBundle = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var args = new NavigateEventArgs(viewModel);
            OnBeforeNavigate(this, args);

            if (cancellationToken != default(CancellationToken))
            {
                cancellationToken.Register(async () =>
                {
                    var result = default(TResult);
                    if (Device.RuntimePlatform == Device.iOS && result != null)
                    {
                        await Close(viewModel, result);
                    }
                    else
                    {
                        await CancelResult(viewModel, result);

                    }
                });
            }

            var tcs = new TaskCompletionSource<object>();
            viewModel.CloseCompletionSource = tcs;
            _tcsResults.Add(viewModel, tcs);

            ViewDispatcher.ShowViewModel(request);

            if (viewModel.InitializeTask?.Task != null)
                await viewModel.InitializeTask.Task.ConfigureAwait(false);

            OnAfterNavigate(this, args);

            try
            {
                return (TResult)await tcs.Task;
            }
            catch (Exception e)
            {
                return default(TResult);
            }
        }

        private async Task CancelResult<TParameter, TResult>(IMvxViewModel<TParameter, TResult> viewModel, TResult result)
        {
            _tcsResults.TryGetValue(viewModel, out TaskCompletionSource<object> _tcs);

            //Disable cancelation of the Task when closing ViewModel through the service
            viewModel.CloseCompletionSource = null;

            try
            {

                _tcs?.TrySetResult(result);
                _tcsResults.Remove(viewModel);

            }
            catch (Exception ex)
            {
                _tcs?.TrySetException(ex);

            }
        }
    }
}
