﻿namespace Garzon.Forms.UI.Utils
{
    using System.Threading.Tasks;
    using I18NPortable;
    using Plugin.Permissions;
    using Plugin.Permissions.Abstractions;
    using Xamarin.Forms;

    public abstract class PermissionsUtils
    {
        public static async Task<bool> CheckPermissions(Permission permission)
        {
            var Strings = I18N.Current;

            var permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
            bool request = false;

            if (permissionStatus == PermissionStatus.Denied)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
                    return await OpenPermissionDialog(permission, Strings);
                }

                request = true;

            }

            if (request || permissionStatus != PermissionStatus.Granted)
            {
                var newStatus = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                if (newStatus.ContainsKey(permission) && newStatus[permission] != PermissionStatus.Granted)
                {
                    return await OpenPermissionDialog(permission, Strings);
                }
            }

            return true;
        }

        private static async Task<bool> OpenPermissionDialog(Permission permission, II18N Strings)
        {
            var title = string.Format(Strings["Permission"], permission);
            var question = string.Format(Strings["PermissionQuestion"], permission);
            var positive = Strings["Settings"];
            var negative = Strings["MaybeLater"];
            var task = Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
            if (task == null)
                return false;

            var result = await task;
            if (result)
            {
                CrossPermissions.Current.OpenAppSettings();
            }

            return false;
        }
    }
}
