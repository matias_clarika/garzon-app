﻿using System.Threading.Tasks;
using Garzon.Forms.UI.DI;
using Garzon.Core;
using Garzon.Forms.UI.Facade.Auth;
using Garzon.Forms.UI.Navigation;
using Garzon.Core.ViewModels;
using Garzon.Core.DI;

namespace Garzon.Forms.UI
{
    public class CoreApp : MvvmCross.Core.ViewModels.MvxApplication, ICoreApp
    {
        private IDependencyInjector _dependencyInjector;

        public override void Initialize()
        {
            //IoC
            _dependencyInjector = DependencyManager.Builder();
            _dependencyInjector.Inject();
            _dependencyInjector.Resolve<IUserSessionManager>().OnTokenReceivedSubscription();
            _dependencyInjector.Resolve<INavigationController>().OpenFirstPage(this);
        }

        public async Task RegisterFirstPage<TViewModel>() where TViewModel : BaseViewModel
        {
            RegisterNavigationServiceAppStart<TViewModel>();
        }
    }
}
