﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;

    public class SeeDetailVisibilityByOrderValueConverter : MvxValueConverter<Order, bool>
    {
        protected override bool Convert(Order value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.Details.Any();
        }
    }
}
