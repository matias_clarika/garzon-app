﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using I18NPortable;
    using MvvmCross.Platform.Converters;

    public class OrderHistoryLabelTextByOrderValueConverter : MvxValueConverter<Order, string>
    {
        protected override string Convert(Order value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value.IsInResto())
            {
                if (value.IsClosed())
                {
                    return I18N.Current["OrderInRestoClosedLabelText"].ToUpper();
                }
                else if (value.InCounter())
                {
                    return I18N.Current["OrderInRestoPendingToConfirmationLabelText"].ToUpper();
                }
                else if (value.IsObserved())
                {
                    return I18N.Current["OrderInRestoObservedLabelText"].ToUpper();
                }
                else if (value.AnyInKitchen())
                {
                    return I18N.Current["OrderInRestoInKitchenLabelText"].ToUpper();
                }
            }
            else
            {
                if (value.IsClosed())
                {
                    return I18N.Current["OrderTakeAwayClosedLabelText"].ToUpper();
                }
                else if (value.ReadyToWithdraw())
                {
                    return I18N.Current["OrderTakeAwayReadyToWithdrawLabelText"].ToUpper();
                }
                else if (value.InCounter())
                {
                    return I18N.Current["OrderTakeAwayPendingToConfirmationLabelText"].ToUpper();
                }
                else if (value.IsObserved())
                {
                    return I18N.Current["OrderTakeAwayObservedLabelText"].ToUpper();
                }
                else if (value.AnyInKitchen())
                {
                    return I18N.Current["OrderTakeAwayInKitchenLabelText"].ToUpper();
                }
            }

            return string.Empty;
        }
    }
}
