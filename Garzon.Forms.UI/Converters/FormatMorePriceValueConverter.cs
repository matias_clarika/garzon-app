﻿using System;
using System.Globalization;
using Garzon.Core.Utils;
using MvvmCross.Platform;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class FormatMorePriceValueConverter : MvxValueConverter<double, string>
    {

        #region dependency
        readonly IFormatPrice _formatPrice;
        #endregion

        #region constructor
        public FormatMorePriceValueConverter()
        {
            _formatPrice = Mvx.Resolve<IFormatPrice>();
        }
        #endregion

        #region override of MvxValueConverter
        protected override string Convert(double value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            if (value != null && value > 0)
            {
                return $"(+{_formatPrice.FormatPriceWithSymbol(value)})";
            }
            else
            {
                return string.Empty;
            }

        }
        #endregion
    }
}
