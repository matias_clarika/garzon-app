﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using I18NPortable;
    using MvvmCross.Platform.Converters;

    public class AdditionalAndCommentValueConverter : MvxValueConverter<string, string>
    {
        #region override of MvxValueConverter
        protected override string Convert(string value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            if (!String.IsNullOrEmpty(value))
            {
                return $"{I18N.Current["ProductDetailSpecialTitle"]}: {value}";
            }
            else
            {
                return value;
            }
        }
        #endregion
    }
}
