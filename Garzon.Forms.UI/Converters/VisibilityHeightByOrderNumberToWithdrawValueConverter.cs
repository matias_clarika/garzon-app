﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;

    public class VisibilityHeightByOrderNumberToWithdrawValueConverter : MvxValueConverter<int?, double>
    {
        protected override double Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.HasValue && value.Value > 0 ? System.Convert.ToDouble(parameter) : 0;
        }
    }
}
