﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;

    public class OrderHistoryLabelVisibilityByOrderValueConverter : MvxValueConverter<Order, bool>
    {
        #region MvxValueConverter
        protected override bool Convert(Order value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }
        #endregion
    }
}
