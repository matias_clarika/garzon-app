﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;
    using Xamarin.Forms;

    public class OrderHistoryLabelBackgroundColorByOrderValueConverter : MvxValueConverter<Order, Color>
    {
        #region MvxValueConverter
        protected override Color Convert(Order value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorName = string.Empty;
            if (value.IsInResto())
            {
                if (value.IsClosed())
                {
                    colorName = "OrderHistoryClosedColor";
                }
                else if (value.InCounter())
                {
                    colorName = "OrderHistoryInCounterColor";
                }
                else if (value.IsObserved())
                {
                    colorName = "OrderHistoryObservedColor";
                }
                else if (value.AnyInKitchen())
                {
                    colorName = "OrderHistoryConfirmedColor";
                }
            }
            else
            {
                if (value.IsClosed())
                {
                    colorName = "OrderHistoryClosedColor";
                }
                else if (value.ReadyToWithdraw())
                {
                    colorName = "OrderHistoryReadyToWithdrawColor";
                }
                else if (value.InCounter())
                {
                    colorName = "OrderHistoryInCounterColor";
                }
                else if (value.IsObserved())
                {
                    colorName = "OrderHistoryObservedColor";
                }
                else if (value.AnyInKitchen())
                {
                    colorName = "OrderHistoryTakeAwayConfirmedColor";
                }
            }

            return (Color)Application.Current.Resources[colorName];
        }
        #endregion
    }
}
