﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Garzon.DataObjects;
    using I18NPortable;
    using MvvmCross.Platform.Converters;

    public class FormatOrderDetailsToListByOrderValueConverter : MvxValueConverter<Order, string>
    {
        private readonly int MAX_ITEM_DETAILS = 5;

        protected override string Convert(Order value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.Details.Any())
            {
                var newDetails = value.Details.Take(MAX_ITEM_DETAILS).Select((arg) => GetLabelByDetail(arg)).ToList();
                if (value.Details.Count > MAX_ITEM_DETAILS)
                {
                    newDetails.Add("...");
                }
                return String.Join(" \n", newDetails);
            }
            return string.Empty;
        }

        private static string GetLabelByDetail(OrderDetail detail)
        {
            var observedLabel = detail.IsObserved() ? $"({I18N.Current["WithoutStock"]})" : string.Empty;
            return $"{detail.Quantity}x {detail.Product.Name} {observedLabel}";
        }
    }
}
