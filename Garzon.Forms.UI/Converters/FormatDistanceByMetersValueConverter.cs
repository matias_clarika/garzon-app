﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;

    public class FormatDistanceByMetersValueConverter : MvxValueConverter<int?, string>
    {
        #region MvxValueConverter
        protected override string Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.HasValue)
            {
                if (value >= 1000)
                {
                    double km = value.Value / 1000;
                    return $"{km.ToString("#,#")} Km.";
                }
                else
                {
                    return $"{value} Mts.";
                }
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

    }
}
