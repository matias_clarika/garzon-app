﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;

    public class VisibilityHeightByStringValueConverter : MvxValueConverter<string, double>
    {
        #region override of MvxValueConverter
        protected override double Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!String.IsNullOrEmpty(value))
            {
                if (parameter != null)
                {
                    return System.Convert.ToDouble(parameter);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return 0;
            }
        }

        #endregion
    }
}
