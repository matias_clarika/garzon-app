﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class VisibilityByPriceValueConverter : MvxValueConverter<double, bool>
    {
        #region override of MvxValueConverter
        protected override bool Convert(double value, Type targetType, object parameter, CultureInfo culture)
        {
            return value > 0;
        }

        #endregion
    }
}
