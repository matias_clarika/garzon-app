﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class VisibilityHeightByDoubleValueConverter: MvxValueConverter<Double, Double>
    {
        #region override of MvxValueConverter
        protected override Double Convert(Double value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value>0)
                {
                    return System.Convert.ToDouble(parameter);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        #endregion
    }

}
