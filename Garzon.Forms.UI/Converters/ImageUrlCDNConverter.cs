﻿using System;
using System.Globalization;
using Garzon.Core.Views;
using MvvmCross.Platform;
using Xamarin.Forms;

namespace Garzon.Forms.UI.Converters
{
    public class ImageUrlCDNConverter : IValueConverter
    {
        #region constants parameter
        public static string PLACE_LIST = "placeList";
        #endregion
        private IDisplay _display;

        IDisplay Display
        {
            get
            {
                if (_display == null)
                {
                    _display = Mvx.Resolve<IDisplay>();
                }
                return _display;
            }
        }

        #region IValueConverter
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string url = (string)value;
                return GenerateUrlByDisplayParameter(url, (string)parameter);
            }
            else
            {
                return null;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion

        private string GenerateUrlByDisplayParameter(string url, string parameter)
        {
            string result = url;
            var cdnSize = -1;
            if (Display != null)
            {
                switch (Display.Scale)
                {
                    case 1:
                        cdnSize = 220;
                        break;
                    case 2:
                        cdnSize = 380;
                        break;
                    default:
                        cdnSize = 550;
                        break;
                }
            }

            if (cdnSize > 0)
            {
                result = $"{result}=s{cdnSize}";
            }
            return result;
        }

    }
}
