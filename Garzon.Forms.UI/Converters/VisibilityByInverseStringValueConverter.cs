﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;

    public class VisibilityByInverseStringValueConverter : MvxValueConverter<string, bool>
    {
        #region override of MvxValueConverter
        protected override bool Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return String.IsNullOrEmpty(value);
        }

        #endregion
    }
}
