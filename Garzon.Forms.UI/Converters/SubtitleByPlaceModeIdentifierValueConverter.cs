﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using I18NPortable;
    using MvvmCross.Platform.Converters;

    public class SubtitleByPlaceModeIdentifierValueConverter : MvxValueConverter<long, string>
    {

        #region MvxValueConverter
        protected override string Convert(long value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY:
                    return I18N.Current["PlaceModeTakeAwaySubtitle"];
                case (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY_EXIST_ORDER:
                    return I18N.Current["PlaceModeTakeAwayExistOrderSubtitle"];
                default:
                    return string.Empty;
            }
        }
        #endregion
    }
}
