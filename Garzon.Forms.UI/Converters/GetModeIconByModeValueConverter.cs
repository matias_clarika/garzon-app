﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using FFImageLoading.Svg.Forms;
    using Garzon.Core.Resources;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;
    using Xamarin.Forms;

    public class GetModeIconByModeValueConverter : MvxValueConverter<Order, Style>
    {
        protected override Style Convert(Order value, Type targetType, object parameter, CultureInfo culture)
        {
            var styleName = string.Empty;
            switch (value.Mode)
            {
                case (int)PlaceMode.ServerPlaceModeIds.RESTO:
                    styleName = "IconResto";
                    break;
                case (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY:
                    styleName = "IconTakeAway";
                    break;
            }
            return (Style)Application.Current.Resources[styleName];
        }
    }
}
