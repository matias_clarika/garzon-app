﻿using System;
using System.Globalization;
using Garzon.DataObjects;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class VisibilityByOrderDetailPendingStatus : MvxValueConverter<int, bool>
    {
        #region override of MvxValueConverter
        protected override bool Convert(int value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value == (int)OrderDetailStatus.INPROGRESS);
        }

        #endregion
    }
}
