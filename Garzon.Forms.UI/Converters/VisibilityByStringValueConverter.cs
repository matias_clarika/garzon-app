﻿using System;
using System.Globalization;
using Garzon.DataObjects;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class VisibilityByStringValueConverter : MvxValueConverter<string, bool>
    {
        #region override of MvxValueConverter
        protected override bool Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return !String.IsNullOrEmpty(value);
        }

        #endregion
    }
}
