﻿using System;
using System.Globalization;
using Garzon.DataObjects;
using I18NPortable;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class WelcomeValueConverter : MvxValueConverter<User, string>
    {
        #region override of MvxValueConverter
        protected override string Convert(User value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value ==null){
                return I18N.Current["MessageDefaultWelcome"];
            }else{
                string messageWelcome = I18N.Current["MessageWelcome"];
                return String.Format(messageWelcome, value.FirstName);
            }
        }

        #endregion
    }
}
