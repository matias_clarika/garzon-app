﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;

    public class VisibilityHeightByOrderDetailsValueConverter : MvxValueConverter<List<OrderDetail>, double>
    {
        #region override of MvxValueConverter
        protected override double Convert(List<OrderDetail> value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value != null && value.Any())
                {
                    return System.Convert.ToDouble(parameter);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
            }

        }

        #endregion
    }
}
