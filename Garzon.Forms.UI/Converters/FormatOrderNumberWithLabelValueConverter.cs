﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using I18NPortable;
    using MvvmCross.Platform.Converters;

    public class FormatOrderNumberWithLabelValueConverter : MvxValueConverter<int?, string>
    {
        protected override string Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.HasValue)
            {
                return string.Format(I18N.Current["OrderNumberToFormat"], value.Value);
            }
            return string.Empty;
        }
    }
}
