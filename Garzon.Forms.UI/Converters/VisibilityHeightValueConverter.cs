﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class VisibilityHeightValueConverter : MvxValueConverter<Boolean, Double>
    {
        #region override of MvxValueConverter
        protected override Double Convert(Boolean value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value)
                {
                    return System.Convert.ToDouble(parameter);
                }
                else
                {
                    return 0;
                }
            }
            catch(Exception e)
            {
                return 0;
            }

        }

        #endregion
    }
}
