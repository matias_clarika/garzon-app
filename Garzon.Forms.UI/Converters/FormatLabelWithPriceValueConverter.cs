﻿using System;
using System.Globalization;
using Garzon.Core.Utils;
using MvvmCross.Platform;
using MvvmCross.Platform.Converters;

namespace Garzon.Forms.UI.Converters
{
    public class FormatLabelWithPriceValueConverter : MvxValueConverter<string, string>
    {

        #region dependency
        readonly IFormatPrice _formatPrice;
        #endregion

        #region constructor
        public FormatLabelWithPriceValueConverter()
        {
            _formatPrice = Mvx.Resolve<IFormatPrice>();
        }
        #endregion


        #region override of MvxValueConverter
        protected override string Convert(string value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            //get amount
            double amount = (double)parameter;

            //all caps
            //value = value.ToUpper();

            if (amount > 0)
            {
                return value + " (" + _formatPrice.FormatPriceWithSymbol(amount) + ")";
            }
            else
            {
                return value;
            }

        }
        #endregion
    }
}
