﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;
    using Xamarin.Forms;

    public class IconByPlaceModeIdentifierValueConverter : IValueConverter
    {

        #region IValueConverter
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var styleName = string.Empty;
            int id = int.Parse($"{value}");
            switch (id)
            {
                case (int)PlaceMode.ServerPlaceModeIds.RESTO:
                    styleName = "IconRestoSelectPlaceMode";
                    break;
                case (int)PlaceMode.ServerPlaceModeIds.DELIVERY:
                    styleName = "IconDeliverySelectPlaceMode";
                    break;
                case (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY:
                    styleName = "IconTakeAwaySelectPlaceMode";
                    break;
                case (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY_EXIST_ORDER:
                    styleName = "IconTakeAwayExistSelectPlaceMode";
                    break;
            }

            if (Application.Current.Resources.ContainsKey(styleName))
            {
                return (Style)Application.Current.Resources[styleName];
            }
            else
            {
                return default(Style);
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
