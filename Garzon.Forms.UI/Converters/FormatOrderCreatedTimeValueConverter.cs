﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;

    public class FormatOrderCreatedTimeValueConverter : MvxValueConverter<DateTime?, string>
    {
        protected override string Convert(DateTime? value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.HasValue)
            {
                return value.Value.ToLocalTime().ToString("dd/MM/yy, hh:mm");
            }

            return string.Empty;
        }
    }
}
