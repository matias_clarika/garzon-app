﻿namespace Garzon.Forms.UI.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;
    using static Garzon.DataObjects.PlaceMode;

    public class IsVisibleByTakeAwayMode : MvxValueConverter<List<PlaceMode>, bool>
    {
        #region MvxValueConverter
        protected override bool Convert(List<PlaceMode> value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && value.Select((arg) => arg.Id).Contains((int)ServerPlaceModeIds.TAKE_AWAY);
        }
        #endregion
    }
}
