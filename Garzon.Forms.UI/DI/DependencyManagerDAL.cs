﻿namespace Garzon.Forms.UI.DI
{
    using Garzon.DAL.DataSource.Local;
    using Garzon.DAL.DataSource.Local.Implementation;
    using Garzon.DAL.DataSource.Local.LocalStorage;
    using Garzon.DAL.DataSource.Local.LocalStorage.Implementation;
    using Garzon.DAL.DataSource.Remote;
    using Garzon.DAL.DataSource.Remote.Implementation;
    using Garzon.DAL.Repository;
    using Garzon.DAL.Repository.Implementation;
    using MvvmCross.Platform;

    public partial class DependencyManager
    {
        protected override void InjectDAL()
        {
            base.InjectDAL();
            InjectLocalDataSource();
            InjectRemoteDataSource();
            InjectRepository();
        }

        private void InjectLocalDataSource()
        {
            Mvx.LazyConstructAndRegisterSingleton<ITutorialSharedPreferences, TutorialSharedPreferences>();
            Mvx.LazyConstructAndRegisterSingleton<IMenuSectionLocalDataSource, MenuSectionLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<ITutorialLocalDataSource, TutorialLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IGroupLocalDataSource, GroupLocalDataSource>();
        }

        private void InjectRemoteDataSource()
        {
            Mvx.LazyConstructAndRegisterSingleton<IMenuRemoteDataSource, MenuRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<ICheckVersionRemoteDataSource, CheckVersionRemoteDataSource>();
        }

        private void InjectRepository()
        {
            Mvx.LazyConstructAndRegisterSingleton<IPlaceRepository, PlaceDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<IMenuRepository, MenuDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<IMenuSectionRepository, MenuSectionDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<ITutorialRepository, TutorialDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<IGroupRepository, GroupDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<ICheckVersionRepository, CheckVersionDataRepository>();
        }
    }
}
