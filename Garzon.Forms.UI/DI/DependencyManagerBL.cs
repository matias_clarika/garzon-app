﻿namespace Garzon.Forms.UI.DI
{
    using Garzon.BL;
    using Garzon.BL.Common;
    using Garzon.BL.Implementation;
    using Garzon.BL.Implementation.Common;
    using Garzon.BL.Tutorial;
    using Garzon.Core.DI;
    using MvvmCross.Platform;

    public partial class DependencyManager
    {
        protected override void InjectBL()
        {
            base.InjectBL();
            Mvx.LazyConstructAndRegisterSingleton<ISocialUserBL, SocialUserBL>();
            Mvx.LazyConstructAndRegisterSingleton<ITutorialBL, TutorialBL>();
            Mvx.LazyConstructAndRegisterSingleton<IGroupBL, GroupBL>();
            Mvx.LazyConstructAndRegisterSingleton<IProductGroupBL, ProductGroupBL>();
            Mvx.LazyConstructAndRegisterSingleton<ICheckVersionBL, CheckVersionBL>();

        }
    }
}
