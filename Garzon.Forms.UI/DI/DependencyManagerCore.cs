﻿namespace Garzon.Forms.UI.DI
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Garzon.Config;
    using Garzon.Config.Facebook;
    using Garzon.Config.Google;
    using Garzon.Core.Factory;
    using Garzon.Core.Factory.Implementation;
    using Garzon.Core.Managers;
    using Garzon.Core.Views.Format;
    using Garzon.Forms.UI.EventBus.Notifications;
    using Garzon.Forms.UI.EventBus.OrderNotifications;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Facade.Auth;
    using Garzon.Forms.UI.Managers;
    using Garzon.Forms.UI.Navigation;
    using Garzon.Forms.UI.Views.Format;
    using MvvmCross.Platform;
    using Newtonsoft.Json;

    public partial class DependencyManager
    {
        protected override void InjectCore()
        {
            base.InjectCore();
            InjectSettings();
            InjectFacebook();
            InjectGoogle();
            InjectFactory();
            InjectNavigation();
            InjectManagers();
            InjectGlobals();
            InjectFormat();

        }


        private void InjectFacebook()
        {
            string buildEnvironment = Mvx.Resolve<IEnvironment>().BuildEnvironment;

            switch (buildEnvironment)
            {
                case EnvironmentConstants.BUILD_ENVIRONMENT_RELEASE:
                case EnvironmentConstants.BUILD_ENVIRONMENT_PROD:
                    Mvx.ConstructAndRegisterSingleton<IFacebookAppConfig, FacebookAppProdConfig>();
                    break;
                default:
                    Mvx.ConstructAndRegisterSingleton<IFacebookAppConfig, FacebookAppDevConfig>();
                    break;
            }
        }


        private void InjectGoogle()
        {
            string buildEnvironment = Mvx.Resolve<IEnvironment>().BuildEnvironment;

            switch (buildEnvironment)
            {
                case EnvironmentConstants.BUILD_ENVIRONMENT_RELEASE:
                case EnvironmentConstants.BUILD_ENVIRONMENT_PROD:
                    Mvx.ConstructAndRegisterSingleton<IGoogleAppConfig, GoogleAppProdConfig>();
                    break;
                default:
                    Mvx.ConstructAndRegisterSingleton<IGoogleAppConfig, GoogleAppDevConfig>();
                    break;
            }
        }

        private void InjectFactory()
        {
            Mvx.RegisterType<IOrderDetailFactory, OrderDetailFactory>();
        }

        private void InjectNavigation()
        {
            Mvx.LazyConstructAndRegisterSingleton<INavigationController, NavigationController>();
        }

        private void InjectManagers()
        {
            Mvx.LazyConstructAndRegisterSingleton<IOrderManager, MyOrderManager>();
            Mvx.LazyConstructAndRegisterSingleton<IUserSessionManager, UserSessionManager>();
            Mvx.LazyConstructAndRegisterSingleton<INotifyManager, NotifyManager>();
            Mvx.LazyConstructAndRegisterSingleton<IRedirectManager, WebRedirectManager>();
        }

        private void InjectGlobals()
        {
            Mvx.ConstructAndRegisterSingleton<RegistrationNotificationGlobalEventHandler, RegistrationNotificationGlobalEventHandler>();
            Mvx.ConstructAndRegisterSingleton<OrderGlobalEventHandler, OrderGlobalEventHandler>();
        }

        private void InjectSettings(){
            Mvx.RegisterSingleton<JsonSerializerSettings>(new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                ContractResolver = new UnderscorePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            });
        }

        private void InjectFormat()
        {
            Mvx.RegisterType<IFormatOrderDetail, FormatOrderDetail>();
        }
    }
}
