﻿using System;
using Garzon.Core.DI;
using Garzon.Forms.UI.CustomMvvmCross;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace Garzon.Forms.UI.DI
{
    public partial class DependencyManager : SharedDependencyInjector, IDependencyInjector
    {
        const bool MockEnabled = true;

        #region builder
        public static IDependencyInjector Builder(){
            //HACK navigation
            Mvx.RegisterSingleton<IMvxNavigationService>(new CustomMvxNavigationService(null, Mvx.Resolve<IMvxViewModelLoader>()));

            Mvx.ConstructAndRegisterSingleton<IDependencyInjector, DependencyManager>();
            return Mvx.Resolve<IDependencyInjector>();
        }
        #endregion

        public static IDependencyInjector GetInstance(){
            return Mvx.Resolve<IDependencyInjector>();
        }
    }
}
