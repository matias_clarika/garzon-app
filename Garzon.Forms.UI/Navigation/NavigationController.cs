﻿namespace Garzon.Forms.UI.Navigation
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Garzon.Forms.UI.ViewModels;
    using Garzon.DataObjects;
    using MvvmCross.Core.Navigation;
    using MvvmCross.Core.ViewModels;
    using Garzon.Core;
    using Garzon.Core.ViewModels;
    using Garzon.Forms.UI.Navigation.Result;
    using Rg.Plugins.Popup.Services;
    using Garzon.Forms.UI.Views;
    using System.Linq;
    using Garzon.Forms.UI.Facade;
    using MvvmCross.Platform;

    public class NavigationController : INavigationController
    {
        #region dependency
        readonly IMvxNavigationService navigationService;
        #endregion
        #region fields
        private bool currentResolvedObservedOrder = false;
        #endregion
        #region constructor
        public NavigationController(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        #endregion

        #region Implementation of INavigationController

        public async Task OpenFirstPage(ICoreApp coreApp)
        {
            await coreApp.RegisterFirstPage<MasterViewModel>();
        }

        public async Task ReOpenApp()
        {
            this.currentResolvedObservedOrder = false;
            await navigationService.Navigate<MasterViewModel>();
        }

        public Task OpenPlaces()
        {
            return navigationService.Navigate<PlaceListViewModel>();
        }

        public Task OpenMenuSectionByPlace(Place place, bool root = false)
        {
            if (!root)
            {
                return navigationService.Navigate<MenuSectionViewModel, Params>(new Params(place));
            }
            else
            {
                return navigationService.Navigate<MenuSectionRootViewModel, Params>(new Params(place));
            }
        }

        public Task OpenPageByMenuSection(Place place,
                                          MenuSection menuSection = null,
                                          OrderDetail orderDetail = null)
        {
            Params parameters = new Params(place, menuSection, orderDetail);

            return ResolveMenuSectionDeepLink(parameters);

        }

        #region MenuSection Deep links
        /// <summary>
        /// Resolves the menu section deep link.
        /// </summary>
        /// <returns>The menu section deep link.</returns>
        /// <param name="parameters">Parameters.</param>
        private Task ResolveMenuSectionDeepLink(Params parameters)
        {
            switch (parameters.MenuSection.DeepLink)
            {
                case DeepLink.GoToMenu:
                    return NavigateToMenuSectionMenu(parameters);
                case DeepLink.GoToMenuSectionImage:
                    return NavigateMenuSectionWithImage(parameters);
                case DeepLink.GoToMenuSectionIcon:
                    return NavigateMenuSectionWithIcon(parameters);
                case DeepLink.GoToProduct:
                    return NavigateToProductsWithIcon(parameters);
                default:
                    return NavigateToProductsWithImage(parameters);
            }
        }

        private Task NavigateToMenuSectionMenu(Params parameters)
        {
            return navigationService.Navigate<MenuSectionViewModel, Params>(parameters);
        }

        private Task NavigateMenuSectionWithImage(Params parameters)
        {
            return navigationService.Navigate<MenuSectionPrimaryViewModel, Params>(parameters);
        }

        private Task NavigateMenuSectionWithIcon(Params parameters)
        {
            return navigationService.Navigate<MenuSectionSecondaryViewModel, Params>(parameters);
        }

        private Task NavigateToProductsWithIcon(Params parameters)
        {
            return navigationService.Navigate<ProductWithoutImageViewModel, ParamsToProducts>(ParamsToProducts
                                                                                               .Builder(parameters.MenuSection,
                                                                                                        parameters.OrderDetail));
        }

        private Task NavigateToProductsWithImage(Params parameters)
        {
            return navigationService.Navigate<ProductViewModel, ParamsToProducts>(ParamsToProducts
                                                                                   .Builder(parameters.MenuSection,
                                                                                            parameters.OrderDetail));
        }
        #endregion

        public Task<ProductAddedResult> OpenProductDetail(Product product,
                                                           OrderDetail orderDetail = null,
                                                           CancellationToken cancellationToken = default(CancellationToken))
        {
            return navigationService.Navigate<ProductDetailViewModel,
                                               ParamsToProductDetail,
                ProductAddedResult>(new ParamsToProductDetail(product, orderDetail), null, cancellationToken);
        }

        public Task Back(BaseViewModel viewModel)
        {
            return navigationService.Close(viewModel);
        }

        public Task Back<TViewModel, TResult>(TViewModel viewModel,
                                              TResult result) where TViewModel : IMvxViewModelResult<TResult> where TResult : class
        {
            return navigationService.Close(viewModel, result);
        }

        public async Task OpenMyOrderDetail(bool isObservedOrder = false)
        {
            if (!isObservedOrder || !this.currentResolvedObservedOrder)
            {
                if (isObservedOrder)
                {
                    this.currentResolvedObservedOrder = true;
                }
                await this.navigationService.Navigate<MyOrderDetailViewModel>();
            }
        }

        public Task OpenCallWaiter()
        {
            return navigationService.Navigate<CallWaiterViewModel>();
        }

        public async Task<LoginResult> OpenLogin(bool root = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (root)
            {
                return await navigationService.Navigate<LoginRootViewModel, LoginResult>(null, cancellationToken);
            }
            else
            {
                if (await Mvx.Resolve<IOrderManager>().TableNumberIsRequired())
                {
                    return await navigationService.Navigate<LoginViewModel, LoginResult>(null, cancellationToken);
                }
                else
                {
                    return await navigationService.Navigate<LoginWithoutTableViewModel, LoginResult>(null, cancellationToken);
                }
            }
        }

        public Task OpenResumeCheck()
        {
            return navigationService.Navigate<ResumeDetailViewModel>();
        }

        public Task<PinResult> OpenOrderPin(CancellationToken cancellationToken = default(CancellationToken))
        {
            return navigationService.Navigate<OrderPinViewModel, PinResult>(null, cancellationToken);
        }

        public Task OpenSendToKitchen()
        {
            return navigationService.Navigate<SendToKitchenViewModel>();
        }

        public Task OpenWebUrl(string url, string title = "")
        {
            return navigationService.Navigate<WebViewModel, WebParams>(WebParams.Builder(title, url));
        }

        public Task<QuantityDinersResult> OpenOrderQuantityDiners(CancellationToken cancellationToken = default(CancellationToken))
        {
            return navigationService.Navigate<OrderQuantityDinersViewModel, QuantityDinersResult>(null, cancellationToken);
        }

        public Task OpenHowToCallCounterTutorial()
        {
            return PopupNavigation.Instance.PushAsync(new HowToCallCounterPopupPage());
        }

        public Task OpenPinNumberAndCheckOrderTutorial()
        {
            return PopupNavigation.Instance.PushAsync(new PinAndCheckOrderTutorialPopupPage());
        }

        public Task<ProductAddedResult> OpenGroupsByProduct(MenuSection parentMenuSection, Product product, OrderDetail editOrderDetail = null, bool isPending = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            var param = GroupListParams.Build(parentMenuSection, product, isPending);
            if (editOrderDetail != null)
            {
                param.WithOrderDetail(editOrderDetail);
            }
            return navigationService.Navigate<GroupListViewModel, GroupListParams, ProductAddedResult>(param, null, cancellationToken);
        }

        public Task<ProductGroupResult> OpenProductByGroup(Group parentGroup,
                                                           CancellationToken cancellationToken = default(CancellationToken),
                                                           bool isPending = false,
                                                           OrderDetail orderDetailPending = null,
                                                           List<Product> selectedProducts = null)
        {

            if (parentGroup.MultipleSelection.HasValue && parentGroup.MultipleSelection.Value)
            {
                var param = ProductMultiSelectionParams.Build(parentGroup, selectedProducts);
                return navigationService.Navigate<ProductMultiSelectionListViewModel, ProductMultiSelectionParams, ProductGroupResult>(param, null, cancellationToken);
            }
            else
            {
                var param = ProductGroupListParams.Build(parentGroup, isPending);
                if (orderDetailPending != null)
                {
                    param.WithPendingOrderDetail(orderDetailPending);
                }
                if (selectedProducts.Any())
                {
                    param.WithSelectedProduct(selectedProducts.FirstOrDefault());
                }
                return this.navigationService.Navigate<ProductGroupListViewModel, ProductGroupListParams, ProductGroupResult>(param, null, cancellationToken);
            }

        }

        public Task<string> OpenProductComment(string comment = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return navigationService.Navigate<ProductCommentViewModel, ProductCommentParams, string>(ProductCommentParams.Build(comment), null, cancellationToken);
        }

        public async Task<PlaceMode> OpenSelectPlaceMode(List<PlaceMode> modes)
        {
            var taskCompletionSource = new TaskCompletionSource<PlaceMode>();
            var popupPage = new SelectPlaceModePopupPage(SelectPlaceModeViewModel.Params.Build(modes, taskCompletionSource));
            await PopupNavigation.Instance.PushAsync(popupPage);
            return await taskCompletionSource.Task;
        }

        public Task OpenTakeAwayExistOrder(Place place)
        {
            var p = TakeAwayExistViewModel.Params.Build(place);
            return navigationService.Navigate<TakeAwayExistViewModel, TakeAwayExistViewModel.Params>(p);
        }

        public Task OpenMyOrders()
        {
            return navigationService.Navigate<MyOrderHistoryViewModel>();
        }

        public Task OpenOrderDetail(Order order)
        {
            var p = OrderHistoryDetailViewModel.Params.Build(order);
            return navigationService.Navigate<OrderHistoryDetailViewModel, OrderHistoryDetailViewModel.Params>(p);
        }

        public Task<PaymentResult> OpenMercadoPagoPayment(CancellationToken cancellationToken = default(CancellationToken))
        {
            var p = MercadoPagoPaymentViewModel.Params.Build();
            return navigationService.Navigate<MercadoPagoPaymentViewModel, MercadoPagoPaymentViewModel.Params, PaymentResult>(p, null, cancellationToken);
        }
        #endregion

    }
}
