﻿using System;
namespace Garzon.Forms.UI.Navigation.Result
{
    public class PinResult
    {

        private readonly string _pin;

        private PinResult(string pin)
        {
            _pin = pin;
        }

        public string Pin
        {
            get => _pin;
        }

        /// <summary>
        /// Pins the result builder.
        /// </summary>
        /// <returns>The result builder.</returns>
        /// <param name="pin">Pin.</param>
        public static PinResult PinResultBuilder(string pin)
        {
            return new PinResult(pin);
        }
    }
}
