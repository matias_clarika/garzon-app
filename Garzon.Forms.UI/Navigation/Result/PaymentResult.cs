﻿namespace Garzon.Forms.UI.Navigation.Result
{
    using System;
    using Garzon.DataObjects;

    public class PaymentResult
    {
        private PaymentResult(string token, string paymentMethodId, bool capture)
        {
            this.Token = token;
            this.PaymentMethodId = paymentMethodId;
            this.Capture = capture;
        }

        public string Token
        {
            get;
            private set;
        }

        public string PaymentMethodId
        {
            get;
            private set;
        }

        public bool Capture
        {
            get;
            private set;
        }

        public static PaymentResult Build(string token, string paymentMethodId, bool capture)
        {
            return new PaymentResult(token, paymentMethodId, capture);
        }

        /// <summary>
        /// Tos the payment.
        /// </summary>
        /// <returns>The payment.</returns>
        public Payment ToPayment()
        {
            return new Payment
            {
                Token = this.Token,
                PaymentMethodId = this.PaymentMethodId,
                Capture = this.Capture
            };
        }
    }
}
