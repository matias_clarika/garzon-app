﻿using System;
namespace Garzon.Forms.UI.Navigation.Result
{
    public class QuantityDinersResult
    {
        private readonly int _quantity;

        private QuantityDinersResult(int quantity)
        {
            _quantity = quantity;
        }

        public int Quantity
        {
            get => _quantity;
        }

        /// <summary>
        /// Quantities the diners result builder.
        /// </summary>
        /// <returns>The diners result builder.</returns>
        /// <param name="quantity">Quantity.</param>
        public static QuantityDinersResult QuantityDinersResultBuilder(int quantity)
        {
            return new QuantityDinersResult(quantity);
        }
    }
}
