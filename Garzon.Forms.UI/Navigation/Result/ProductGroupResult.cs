﻿namespace Garzon.Forms.UI.Navigation.Result
{
    using System;
    using System.Collections.Generic;
    using Garzon.DataObjects;

    public class ProductGroupResult
    {
        public List<Product> Products
        {
            get;
            private set;
        }

        private ProductGroupResult()
        {
            this.Products = new List<Product>();
        }

        /// <summary>
        /// Build this instance.
        /// </summary>
        /// <returns>The build.</returns>
        public static ProductGroupResult Build()
        {
            return new ProductGroupResult();
        }

        /// <summary>
        /// Withs the product.
        /// </summary>
        /// <returns>The product.</returns>
        /// <param name="product">Product.</param>
        public ProductGroupResult WithProduct(Product product)
        {
            if (product != null)
            {
                this.Products.Add(product);
            }

            return this;
        }
        /// <summary>
        /// Withs the products.
        /// </summary>
        /// <returns>The products.</returns>
        /// <param name="products">Products.</param>
        public ProductGroupResult WithProducts(List<Product> products)
        {
            this.Products.AddRange(products);
            return this;
        }


    }
}
