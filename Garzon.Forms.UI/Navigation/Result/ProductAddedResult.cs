﻿using System;
namespace Garzon.Forms.UI.Navigation.Result
{
    public class ProductAddedResult
    {
        private readonly bool isAdded;

        private ProductAddedResult(bool isAdded)
        {
            this.isAdded = isAdded;
        }

        public bool IsAdded
        {
            get => this.isAdded;
        }

        /// <summary>
        /// Groups the product result build.
        /// </summary>
        /// <returns>The product result build.</returns>
        /// <param name="isAdded">If set to <c>true</c> is added.</param>
        public static ProductAddedResult Build(bool isAdded = false)
        {
            return new ProductAddedResult(isAdded);
        }
    }
}
