﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Garzon.Core.Navigation;
using Garzon.DataObjects;
using Garzon.Forms.UI.Navigation.Result;
using Garzon.Forms.UI.ViewModels;
using MvvmCross.Core.ViewModels;

namespace Garzon.Forms.UI.Navigation
{
    public interface INavigationController : INavigationCoreController
    {
        #region restaurant

        /// <summary>
        /// Opens the places.
        /// </summary>
        /// <returns>The places.</returns>
        Task OpenPlaces();

        /// <summary>
        /// Opens the menu section by place.
        /// </summary>
        /// <returns>The menu section by place.</returns>
        /// <param name="place">Place.</param>
        /// <param name="root">If set to <c>true</c> root.</param>
        Task OpenMenuSectionByPlace(Place place, bool root = false);

        /// <summary>
        /// Opens the page by menu section.
        /// </summary>
        /// <returns>The page by menu section.</returns>
        /// <param name="place">Place.</param>
        /// <param name="menuSection">Menu section.</param>
        /// <param name="orderDetail">Order detail.</param>
        Task OpenPageByMenuSection(Place place,
                                   MenuSection menuSection = null,
                                   OrderDetail orderDetail = null);

        /// <summary>
        /// Opens the product detail.
        /// </summary>
        /// <returns>The product detail.</returns>
        /// <param name="product">Product.</param>
        /// <param name="orderDetail">Order detail.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<ProductAddedResult> OpenProductDetail(Product product,
                                                    OrderDetail orderDetail = null,
                                                    CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Opens my order detail.
        /// </summary>
        /// <returns>The my order detail.</returns>
        /// <param name="isObservedOrder">If set to <c>true</c> is observed order.</param>
        Task OpenMyOrderDetail(bool isObservedOrder = false);

        /// <summary>
        /// Opens the resume check.
        /// </summary>
        /// <returns>The resume check.</returns>
        Task OpenResumeCheck();
        /// <summary>
        /// Opens the login.
        /// </summary>
        /// <returns>The login.</returns>
        /// <param name="root">If set to <c>true</c> root.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<LoginResult> OpenLogin(bool root = false,
                                    CancellationToken cancellationToken = default(CancellationToken));
        /// <summary>
        /// Opens the call waiter.
        /// </summary>
        /// <returns>The call waiter.</returns>
        Task OpenCallWaiter();

        /// <summary>
        /// Opens the order pin.
        /// </summary>
        /// <returns>The order pin.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<PinResult> OpenOrderPin(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Opens the order pin.
        /// </summary>
        /// <returns>The order pin.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<QuantityDinersResult> OpenOrderQuantityDiners(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Opens the send to kitchen.
        /// </summary>
        /// <returns>The send to kitchen.</returns>
        Task OpenSendToKitchen();

        /// <summary>
        /// Opens the web URL.
        /// </summary>
        /// <returns>The web URL.</returns>
        /// <param name="url">URL.</param>
        /// <param name="title">Title.</param>
        Task OpenWebUrl(string url, string title = "");

        /// <summary>
        /// Opens the how to call counter tutorial.
        /// </summary>
        /// <returns>The how to call counter tutorial.</returns>
        Task OpenHowToCallCounterTutorial();

        /// <summary>
        /// Opens the pin number and check order tutorial.
        /// </summary>
        /// <returns>The pin number and check order tutorial.</returns>
        Task OpenPinNumberAndCheckOrderTutorial();

        /// <summary>
        /// Opens the groups by product.
        /// </summary>
        /// <returns>The groups by product.</returns>
        /// <param name="parentMenuSection">Parent menu section.</param>
        /// <param name="product">Product.</param>
        /// <param name="editOrderDetail">Edit order detail.</param>
        /// <param name="isPending">If set to <c>true</c> is pending.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<ProductAddedResult> OpenGroupsByProduct(MenuSection parentMenuSection,
                                       Product product,
                                       OrderDetail editOrderDetail = null,
                                       bool isPending = false,
                                       CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Opens the product by group.
        /// </summary>
        /// <returns>The product by group.</returns>
        /// <param name="parentGroup">Parent group.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <param name="isPending">If set to <c>true</c> is pending.</param>
        /// <param name="orderDetailPending">Order detail pending.</param>
        /// <param name="selectedProducts">Selected products.</param>
        Task<ProductGroupResult> OpenProductByGroup(Group parentGroup,
                                                    CancellationToken cancellationToken = default(CancellationToken),
                                                    bool isPending = false,
                                                    OrderDetail orderDetailPending = null,
                                                    List<Product> selectedProducts = null);

        /// <summary>
        /// Opens the product comment.
        /// </summary>
        /// <returns>The product comment.</returns>
        /// <param name="comment">Comment.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<string> OpenProductComment(string comment = null,
                                        CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Selects the place mode.
        /// </summary>
        /// <returns>The place mode.</returns>
        /// <param name="modes">Modes.</param>
        Task<PlaceMode> OpenSelectPlaceMode(List<PlaceMode> modes);

        /// <summary>
        /// Opens the take away exist order.
        /// </summary>
        /// <returns>The take away exist order.</returns>
        /// <param name="place">Place.</param>
        Task OpenTakeAwayExistOrder(Place place);

        /// <summary>
        /// Opens my orders.
        /// </summary>
        /// <returns>The my orders.</returns>
        Task OpenMyOrders();

        /// <summary>
        /// Opens the order detail.
        /// </summary>
        /// <returns>The order detail.</returns>
        /// <param name="order">Order.</param>
        Task OpenOrderDetail(Order order);

        /// <summary>
        /// Opens the mercado pago payment.
        /// </summary>
        /// <returns>The mercado pago payment.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<PaymentResult> OpenMercadoPagoPayment(CancellationToken cancellationToken = default(CancellationToken));
        #endregion
    }
}
