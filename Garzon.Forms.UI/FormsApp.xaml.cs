﻿using DLToolkit.Forms.Controls;
using Garzon.Core.EventBus;
using Garzon.Core.i18n;
using Garzon.Forms.UI.EventBus;
using MvvmCross.Forms.Platform;
using MvvmCross.Platform;

namespace Garzon.Forms.UI
{
    public partial class FormsApp : MvxFormsApplication
    {
        public FormsApp()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            FlowListView.Init();
            I18nSetup.Setup();
        }

        protected override void OnResume()
        {
            base.OnResume();
            var eventBus = Mvx.Resolve<IEventBus>();
            eventBus.Publish<OnResumeAppEvent>(OnResumeAppEvent.Build(this));
        }

    }
}
