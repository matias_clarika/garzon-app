﻿using System.Threading.Tasks;
using Clarika.Xamarin.Base.Auth;
using Garzon.BL;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;
using Garzon.Forms.UI.ViewModels;
using Garzon.Core.Social.Facebook;
using Garzon.Core.Social.Google;
using Garzon.Core.EventBus;
using Garzon.Core.DI;
using Garzon.Core.EventBus.Auth;

namespace Garzon.Forms.UI.Facade.Auth
{
    public class UserSessionManager : IUserSessionManager
    {
        #region dependency
        readonly ISocialUserBL _socialUserBL;
        readonly IUserBL _userBL;
        readonly IFacebookManager _facebookManager;
        readonly IGoogleManager _googleManager;
        readonly IDependencyInjector _dependencyInjector;
        readonly IEventBus _eventBus;
        readonly IOrderManager _orderManager;
        #endregion

        #region utils
        private Task<IEventToken> _token;
        #endregion

        public UserSessionManager(ISocialUserBL socialUserBL,
                                 IUserBL userBL,
                                 IFacebookManager facebookManager,
                                  IDependencyInjector dependencyInjector,
                                 IEventBus eventBus,
                                 IOrderManager orderFacade,
                                 IGoogleManager googleManager = null)
        {
            _socialUserBL = socialUserBL;
            _userBL = userBL;
            _facebookManager = facebookManager;
            _dependencyInjector = dependencyInjector;
            _eventBus = eventBus;
            _orderManager = orderFacade;
            _googleManager = googleManager;
        }

        #region Implementation of IUserSessionFacade

        public async Task<SocialUser> SignInNativeSocial(SocialPlatform platform)
        {
            switch (platform)
            {
                case SocialPlatform.Facebook:
                    return await _facebookManager.Login();
                case SocialPlatform.Google:
                    return await _googleManager.SignIn();
                default:
                    return null;
            }
        }

        public async Task<User> SignInGarzonSocial(SocialPlatform socialPlatform,
                                                   SocialUser socialUser)
        {
            if (socialUser != null)
            {
                User user = null;

                switch (socialPlatform)
                {
                    case SocialPlatform.Facebook:
                        user = await _socialUserBL.SignInFacebook(socialUser);
                        break;
                    case SocialPlatform.Google:
                        user = await _socialUserBL.SignInGoogle(socialUser);
                        break;
                }

                return await ProcessSocialUser(user);
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> SignOut()
        {
            var result = await _userBL.Logout();

            if (result)
            {
                //notify
                await _eventBus.Publish(UserSessionEvent.Builder(this));
                await _orderManager.SetCurrentUser(null);

                _facebookManager.LogOut();
            }


            return result;
        }

        public async Task<User> RecoveryUser()
        {
            var user = await _userBL.FindCurrentUser();
            await ProcessSocialUser(user);
            _orderManager.SetCurrentUser(user);
            return user;
        }

        public async void OnTokenReceivedSubscription()
        {
            _token = _eventBus.SubscribeOnThreadPoolThread<AuthNotificationMessage>(OnTokenReceived);
        }

        public Task<User> SignInLocalUser(User user)
        {
            return _socialUserBL.SignInLocal(user);
        }
        #endregion

        private async Task<User> ProcessSocialUser(User user)
        {
            if (user != null)
            {
                //update token
                _dependencyInjector.Resolve<IOAuthSettings>()
                                  .SetToken(user.Token);
            }
            //publish change state
            _eventBus.Publish(UserSessionEvent.Builder(this).WithUser(user));

            return user;
        }

        public async Task<bool> ProcessLoginResult(LoginResult loginResult)
        {
            if (loginResult != null)
            {
                var tableNumberIsRequired = await _orderManager.TableNumberIsRequired();
                if (loginResult.IsValid(tableNumberIsRequired))
                {
                    await _orderManager.SetCurrentUser(loginResult.User);
                    if (tableNumberIsRequired)
                    {
                        await _orderManager.SetCurrentSectorAndTable(loginResult.SectorId,
                                                                loginResult.TableNumber);
                    }

                    return true;
                }
            }
            return false;
        }

        private void OnTokenReceived(AuthNotificationMessage authNotificationMessage)
        {
            _userBL.SetDeviceToken(authNotificationMessage.RegistrationToken);
        }

        public async Task ProcessUserBlock()
        {
            await SignOut();
            await _orderManager.ClosedOrder();
        }
    }
}
