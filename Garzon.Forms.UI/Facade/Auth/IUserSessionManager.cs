﻿using System.Threading.Tasks;
using Garzon.Forms.UI.ViewModels;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;

namespace Garzon.Forms.UI.Facade.Auth
{
    public interface IUserSessionManager
    {
        /// <summary>
        /// Signs the in native social.
        /// </summary>
        /// <returns>The in native social.</returns>
        /// <param name="platform">Platform.</param>
        Task<SocialUser> SignInNativeSocial(SocialPlatform platform);

        /// <summary>
        /// Signs the in garzon social.
        /// </summary>
        /// <returns>The in garzon social.</returns>
        /// <param name="socialPlatform">Social platform.</param>
        /// <param name="socialUser">Social user.</param>
        Task<User> SignInGarzonSocial(SocialPlatform socialPlatform,
                                      SocialUser socialUser);

        /// <summary>
        /// Signs the in local user.
        /// </summary>
        /// <returns>The in local user.</returns>
        /// <param name="user">User.</param>
        Task<User> SignInLocalUser(User user);

        /// <summary>
        /// Signs the out.
        /// </summary>
        /// <returns>The out.</returns>
        Task<bool> SignOut();
        /// <summary>
        /// Recoveries the user.
        /// </summary>
        /// <returns>The user.</returns>
        Task<User> RecoveryUser();

        /// <summary>
        /// Processes the login result.
        /// </summary>
        /// <returns>The login result.</returns>
        /// <param name="loginResult">Login result.</param>
        Task<bool> ProcessLoginResult(LoginResult loginResult);


        /// <summary>
        /// Ons the token received subscription.
        /// </summary>
        void OnTokenReceivedSubscription();

        /// <summary>
        /// Processes the user block.
        /// </summary>
        /// <returns>The user block.</returns>
        Task ProcessUserBlock();

    }

    public enum SocialPlatform{
        Facebook, Google
    }
}
