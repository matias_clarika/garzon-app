﻿namespace Garzon.Forms.UI.Facade
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.DataObjects;
    using System.Linq;
    using System.Threading;
    using Garzon.Core.EventBus;
    using Garzon.Forms.UI.EventBus.OrderNotifications;
    using Garzon.Core.Factory;
    using Garzon.Forms.UI.Navigation.Result;

    public class MyOrderManager : IOrderManager
    {
        #region dependency
        readonly IOrderBL orderBL;
        readonly IOrderDetailBL _orderDetailBL;
        readonly IMenuSectionBL _menuSectionBL;
        readonly IEventBus _eventBus;
        readonly IOrderDetailFactory _orderDetailFactory;
        #endregion

        #region fields
        Order _currentOrder;
        Place _currentPlace;
        User _currentUser;
        long? _currentSectorId;
        int? currentTable;
        int? _currentQuantityDiners;
        int _orderMode;
        IEventToken _orderTableChangeToken;
        PaymentResult paymentResult;
        #endregion

        #region constructor
        public MyOrderManager(IOrderBL orderBL,
                              IOrderDetailBL orderDetailBL,
                              IMenuSectionBL menuSectionBL,
                              IEventBus eventBus,
                              IOrderDetailFactory orderDetailFactory)
        {
            this.orderBL = orderBL;
            _orderDetailBL = orderDetailBL;
            _menuSectionBL = menuSectionBL;
            _eventBus = eventBus;
            _orderDetailFactory = orderDetailFactory;
            Subscribe();
        }
        #endregion

        #region event

        private async Task Subscribe()
        {
            _orderTableChangeToken = await _eventBus.Subscribe<OrderTableChangeEvent>(OnOrderTableChange);
        }

        private void OnOrderTableChange(OrderTableChangeEvent obj)
        {
            GetCurrentOrder(true);
        }
        #endregion

        #region Implementation of IOrderFacade

        public async Task<Order> GetCurrentOrder(bool force = false, bool notify = true)
        {
            if (_currentUser != null && (force || _currentOrder == null))
            {
                _currentOrder = await orderBL.FindCurrentOrder();

                if (_currentOrder != null)
                {
                    await SetCurrentPlace(_currentOrder.Place);
                    if (_currentOrder.TableNumber > 0)
                    {
                        currentTable = (int)_currentOrder.TableNumber;
                    }
                    if (_currentOrder.SectorId > 0)
                    {
                        _currentSectorId = _currentOrder.SectorId;
                    }
                }
            }
            if (_currentOrder != null
                && _currentOrder.Place == null
                && _currentPlace != null)
            {
                _currentOrder.Place = _currentPlace;
            }

            if (notify)
            {
                await NotifyOrderChange();
            }


            if (_currentOrder != null &&
                _currentOrder.IsClosed())
            {
                await ClosedOrder();
            }
            return _currentOrder;
        }

        #region pending
        public async Task<List<OrderDetail>> FindPendingOrderDetail()
        {
            var result = await _orderDetailBL.FindPendingByUser(_currentUser);
            return result;
        }
        #endregion

        #region confirmed
        public Task<List<OrderDetail>> FindConfirmedOrderDetail(bool filterByUserLogged = false)
        {
            if (filterByUserLogged)
            {
                return _orderDetailBL.FindConfirmedByUserId(_currentUser.Id);
            }
            else
            {
                return _orderDetailBL.FindConfirmed();
            }
        }

        public async Task<double> GetTotalAmount(bool filterByUserLogged = false)
        {
            List<OrderDetail> orderDetails = await FindConfirmedOrderDetail(filterByUserLogged);
            return await GetTotalAmountByOrderDetails(orderDetails);
        }

        public Task<double> GetTotalAmountByOrderDetails(List<OrderDetail> orderDetails)
        {
            return Task.Factory.StartNew(() =>
            {
                double result = 0;
                foreach (OrderDetail orderDetail in orderDetails)
                {
                    result += orderDetail.Amount.Value;
                }

                return result;
            });
        }
        #endregion

        #region place
        public async Task<Place> GetCurrentPlace()
        {
            return _currentPlace;
        }

        public async Task SetCurrentPlace(Place place)
        {
            await Task.Factory.StartNew(() =>
            {
                if (place != null)
                {
                    _currentPlace = place;
                    currentTable = null;
                    _currentSectorId = null;

                }
            });
        }
        #endregion

        #region order detail
        public async Task InsertOrUpdateOrderDetail(Product product,
                                                    int quantity,
                                                    OrderDetail orderDetail = null)
        {
            var newOrderDetail = _orderDetailFactory.Create(product, quantity);
            if (orderDetail != null)
            {
                newOrderDetail.Id = orderDetail.Id;
            }
            await InsertOrUpdateOrderDetail(newOrderDetail);
        }

        private OrderDetail MergeOrderDetails(OrderDetail newOrderDetail, OrderDetail orderDetail = null)
        {
            if (orderDetail != null && orderDetail.Id != newOrderDetail.Id)
            {
                if (newOrderDetail.Id > 0)
                {
                    //if edit order detail
                    _orderDetailBL.Delete(newOrderDetail);
                }
                else
                {
                    //if not change quantity to main order detail
                    newOrderDetail.Quantity += orderDetail.Quantity;
                }

                newOrderDetail.Id = orderDetail.Id;
            }
            return newOrderDetail;
        }

        public Task<OrderDetail> FindOrderDetailByProduct(Product product)
        {
            return _orderDetailBL.FindPendingByProductId(product.Id, _currentUser);
        }

        public async Task InsertOrUpdateOrderDetail(OrderDetail orderDetail)
        {
            orderDetail.SetPending();

            if (!orderDetail.IsCompound && String.IsNullOrEmpty(orderDetail.Comment))
            {
                var existOrderDetail = await FindOrderDetailByProduct(orderDetail.Product);
                orderDetail = MergeOrderDetails(orderDetail, existOrderDetail);
            }
            orderDetail.CalculateAmount();

            if (_currentUser != null)
            {
                orderDetail.UserId = _currentUser.Id;
            }
            await _orderDetailBL.InsertOrUpdate(orderDetail);
        }

        #endregion
        public async Task<Order> SendToKitchen(CancellationTokenSource source,
                                               bool wait = false)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    await GenerateOrderToKitchen(wait);
                    var order = await orderBL.SendToKitchen(_currentOrder);
                    this.ClearCurrentPayment();
                    return order;
                }, source.Token);
            }
            catch (Exception e)
            {
                this.ClearCurrentPayment();
                throw e;
            }
        }

        public async Task SetCurrentTable(int tableNumber)
        {
            await Task.Factory.StartNew(() =>
            {
                currentTable = tableNumber;
            });
        }

        public async Task SetCurrentSectorAndTable(long sectorId, int table)
        {
            await Task.Factory.StartNew(() =>
            {
                _currentSectorId = sectorId;
                currentTable = table;
            });
        }

        public long GetCurrentSector()
        {
            return _currentSectorId.HasValue ? _currentSectorId.Value : -1;
        }

        public int GetCurrentTable()
        {
            return currentTable.HasValue ? currentTable.Value : -1;
        }

        public async Task SetCurrentUser(User user)
        {
            await Task.Factory.StartNew(() => _currentUser = user);
        }

        public User GetCurrentUser()
        {
            return _currentUser;
        }

        public Task<List<MenuSection>> FindSuggestionMenuSection(IPaggination paggination = null)
        {
            return _menuSectionBL.Find(paggination);
        }

        public async Task<Order> CheckOrder(bool force = false)
        {
            await GenerateOrderToKitchen();
            return await orderBL.CheckOrder(_currentOrder, force);
        }

        public void SetPin(string pin)
        {
            if (_currentOrder != null)
            {
                _currentOrder.Pin = int.Parse(pin);
            }
        }

        public async Task<Order> CheckOrderStatus()
        {
            _currentOrder = await orderBL.CheckOrderStatus(_currentOrder.ServerId.Value);
            await NotifyOrderChange();
            return _currentOrder;
        }

        public Task<bool> DeleteOrderDetail(OrderDetail orderDetail)
        {
            return DeleteOrderDetails(new List<OrderDetail> { orderDetail });
        }

        public Task<bool> DeleteOrderDetails(IEnumerable<OrderDetail> orderDetails)
        {
            return _orderDetailBL.Delete(orderDetails);
        }

        public async Task SyncOrder(string data)
        {
            var syncOrder = await orderBL.SyncOrder(data);
            if (syncOrder != null && syncOrder.IsInResto())
            {
                _currentOrder = syncOrder;
                NotifyOrderChange();
            }
        }

        public Task<OrderDetail> InsertOrUpdateProductsInProduct(Product mainProduct,
                                                                 Group group,
                                                                 List<Product> productsToAdd,
                                                                 OrderDetail orderDetail = null,
                                                                 int quantity = 1)
        {
            return Task<OrderDetail>.Factory.StartNew(() =>
            {
                if (orderDetail == null)
                {
                    orderDetail = _orderDetailFactory.Create(mainProduct, quantity);
                }

                if (orderDetail.IsObserved())
                {
                    orderDetail.SetPending();
                }

                FindAndRemoveChildrenByGroup(orderDetail, group);
                AddProductsInOrderDetailByGroup(productsToAdd, orderDetail, group, mainProduct.Id, quantity);

                return orderDetail;
            });
        }

        private void AddProductsInOrderDetailByGroup(List<Product> productsToAdd, OrderDetail orderDetail, Group group, long parentProductId, int quantity)
        {
            if (productsToAdd != null && productsToAdd.Any())
            {
                foreach (var product in productsToAdd)
                {
                    var newDetail = new OrderDetail { Quantity = quantity };
                    orderDetail.Details.Add(newDetail);
                    AddProductInOrderDetail(product, parentProductId, newDetail);
                }
            }
        }

        private void FindAndRemoveChildrenByGroup(OrderDetail orderDetail, Group group)
        {
            if (orderDetail.Details.Any())
            {
                orderDetail.Details.RemoveAll((detail) => detail.GroupId.HasValue && detail.GroupId.Value == group.Id);
            }
        }

        public Task<OrderDetail> InsertOrUpdateProductInCompountProduct(Product mainProduct,
                                                                        Product productToAdd,
                                                                        OrderDetail orderDetail = null,
                                                                        bool isPending = false,
                                                                        int quantity = 1)
        {
            return Task<OrderDetail>.Factory.StartNew(() =>
            {
                if (orderDetail == null)
                {
                    orderDetail = _orderDetailFactory.Create(mainProduct, quantity);
                }

                if (!isPending)
                {
                    OrderDetail orderDetailToEdit = this.AddDetailInOrderDetail(productToAdd, orderDetail, mainProduct.Id);
                    if (orderDetailToEdit.IsObserved())
                    {
                        orderDetailToEdit.SetPending();
                    }
                    else
                    {
                        CheckAutoGeneratedOrderDetailByProductType(mainProduct, productToAdd, orderDetail);
                    }
                }
                else
                {
                    if (orderDetail.IsObserved())
                    {
                        orderDetail.SetPending();
                    }
                    AddProductInOrderDetail(productToAdd, mainProduct.Id, orderDetail);
                }
                return orderDetail;
            });
        }

        private OrderDetail AddDetailInOrderDetail(Product productToAdd, OrderDetail orderDetail, long? parentProductId = null, bool sendLater = false)
        {
            var orderDetailToEdit = orderDetail.Details.FirstOrDefault((OrderDetail arg) =>
            {
                return arg.Product.GroupId.Value == productToAdd.GroupId.Value;
            });

            if (orderDetailToEdit == null)
            {
                orderDetailToEdit = new OrderDetail { Quantity = orderDetail.Quantity };
                orderDetail.Details.Add(orderDetailToEdit);
            }

            orderDetailToEdit.SendLater = sendLater;
            AddProductInOrderDetail(productToAdd, parentProductId, orderDetailToEdit);
            return orderDetailToEdit;
        }

        private static void AddProductInOrderDetail(Product productToAdd, long? parentProductId, OrderDetail orderDetailToEdit)
        {
            if (parentProductId.HasValue)
            {
                orderDetailToEdit.ParentProductId = (int)parentProductId.Value;
            }

            orderDetailToEdit.Product = productToAdd;
        }

        private void CheckAutoGeneratedOrderDetailByProductType(Product mainProduct, Product productToAdd, OrderDetail orderDetail)
        {
            if (mainProduct.IsCompoundPromo())
            {
                if (mainProduct.Groups != null)
                {
                    foreach (var group in mainProduct.Groups)
                    {
                        var newProductToAdd = productToAdd.Clone();
                        newProductToAdd.GroupId = group.Id;
                        AddDetailInOrderDetail(newProductToAdd, orderDetail, mainProduct.Id, group.SendLater);
                    }
                }
            }
        }

        public async Task<bool> ValidateCurrentOrderByPlace(long placeId)
        {
            var currentPlace = await GetCurrentPlace();
            return !(currentPlace != null && currentPlace.Id != placeId && await HasDetails());
        }

        private async Task<bool> HasDetails()
        {
            return (await FindPendingOrderDetail()).Count > 0;
        }

        public async Task<bool> ClosedOrder()
        {
            if (_currentOrder != null)
            {
                bool result = await orderBL.ClosedOrder(_currentOrder);
                if (result)
                {
                    ClearTempOrderValues();
                }
                return result;
            }
            else
            {
                return false;
            }
        }

        private void ClearTempOrderValues()
        {
            _currentOrder = null;
            currentTable = null;
            _currentSectorId = null;
            _currentPlace = null;
            _currentQuantityDiners = null;
            this.paymentResult = null;
        }

        public async Task ClearCurrentOrder()
        {
            bool result = await orderBL.ClearCurrentOrder();
            if (result)
            {
                ClearTempOrderValues();
            }
        }

        public async Task<OrderDetail> FindOrCreateOrderDetailByProduct(Product product)
        {
            var orderDetail = await FindOrderDetailByProduct(product);
            if (orderDetail == null)
            {
                orderDetail = CreateOrderDetailByProduct(product);
            }
            return orderDetail;
        }

        public OrderDetail CreateOrderDetailByProduct(Product product)
        {
            return _orderDetailFactory.Create(product, 1);
        }


        public async Task<bool> OrderPendingToSync()
        {
            return (await GetCurrentOrder(true)).PendingToSync;
        }

        public async Task<List<OrderDetail>> FindWaitingOrderDetails()
        {
            if (_currentUser != null)
            {
                return await _orderDetailBL.FindWaitingOrderDetails(_currentUser.Id);
            }
            else
            {
                return null;
            }
        }
        #endregion

        /// <summary>   
        /// Generates the order to kitchen.
        /// </summary>
        /// <returns>The order to kitchen.</returns>
        /// <param name="wait">If set to <c>true</c> wait.</param>
        private async Task GenerateOrderToKitchen(bool wait = false)
        {
            if (_currentOrder == null)
            {
                _currentOrder = Activator.CreateInstance<Order>();
            }

            _currentOrder.TableNumber = currentTable;
            _currentOrder.SectorId = _currentSectorId;
            _currentOrder.PlaceId = _currentPlace.Id;
            _currentOrder.QuantityDiners = _currentQuantityDiners;
            _currentOrder.Mode = _orderMode;

            if (this.paymentResult != null)
            {
                _currentOrder.Payment = this.paymentResult.ToPayment();
            }
            _currentOrder.Await = wait;
            _currentOrder.Details = await FindPendingToServerOrderDetail();
        }

        private async Task<List<OrderDetail>> FindPendingToServerOrderDetail()
        {
            var ordersDetails = await FindPendingOrderDetail();
            ordersDetails = ordersDetails.Where((orderDetail, predicate) => !orderDetail.IsObserved()).ToList();

            return ordersDetails;
        }

        /// <summary>
        /// Sets the current order.
        /// </summary>
        /// <param name="order">Order.</param>
        private void SetCurrentOrder(Order order)
        {
            GetCurrentOrder(true);
        }


        private async Task NotifyOrderChange()
        {
            await _eventBus.Publish(OrderChangeEvent.Build(this,
                                                             _currentOrder));
        }

        public void SetQuantityDiners(int quantity)
        {
            _currentQuantityDiners = quantity;
        }

        public async Task<List<OrderDetail>> GetPendingRequestOrderDetailList()
        {
            if (_currentUser != null)
            {
                return await this._orderDetailBL.FindPendingRequestListByUserId(_currentUser.Id);
            }
            else
            {
                return default(List<OrderDetail>);
            }

        }

        public Task<OrderDetail> SelectProductByPendingOrderDetail(Product product,
                                                                         OrderDetail orderDetail)
        {
            return this._orderDetailBL.SelectProductByPendingOrderDetail(product, orderDetail);
        }

        public async Task<bool> RemoveInvalidProducts(List<Product> invalidProducts)
        {
            try
            {
                if (invalidProducts != null)
                {
                    foreach (var invalidProduct in invalidProducts)
                    {
                        await _orderDetailBL.RemovePendingOrderDetailByProductId(invalidProduct.Id);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        public async Task DeletePendingOrderDetails()
        {
            await _orderDetailBL.DeletePendingOrderDetails();
        }

        public Task ClearCurrentTable()
        {
            return Task.Factory.StartNew(() =>
            {
                if (this._currentOrder == null
                || !this._currentOrder.ServerId.HasValue)
                {
                    this.currentTable = null;
                }
            });

        }

        public Task SetOrderMode(int modeIdentifier)
        {
            return Task.Factory.StartNew(() =>
            {
                this._orderMode = modeIdentifier;
            });
        }

        public Task<int> GetOrderMode()
        {
            return Task.Factory.StartNew(() =>
            {
                return this._orderMode;
            });
        }

        public async Task<bool> UserIsRequired()
        {
            return (GetCurrentUser() == null ||
                    await TableNumberIsRequired());
        }

        public async Task<bool> TableNumberIsRequired()
        {
            return await GetOrderMode() == (int)PlaceMode.ServerPlaceModeIds.RESTO && GetCurrentTable() <= 0;
        }

        public Task<Order> GetSyncOrder(string data)
        {
            return this.orderBL.GetSyncOrder(data);
        }

        public Task<bool> TakeAwayExistOrder(Place place, int orderNumber)
        {
            return this.orderBL.SendTakeAwayExistOrder(place.Id, orderNumber);
        }

        public void SetPayment(PaymentResult paymentResult)
        {
            this.paymentResult = paymentResult;
        }

        private void ClearCurrentPayment()
        {
            if (this._currentOrder != null)
            {
                this._currentOrder.Payment = null;
            }
            this.paymentResult = null;
        }
    }
}
