﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;
using Garzon.Forms.UI.Navigation.Result;

namespace Garzon.Forms.UI.Facade
{
    public interface IOrderManager
    {
        /// <summary>
        /// Gets the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        /// <param name="force">If set to <c>true</c> force.</param>
        /// <param name="notify">If set to <c>true</c> notify.</param>
        Task<Order> GetCurrentOrder(bool force = false, bool notify = true);

        /// <summary>
        /// Creates the order detail by product.
        /// </summary>
        /// <returns>The order detail by product.</returns>
        /// <param name="product">Product.</param>
        OrderDetail CreateOrderDetailByProduct(Product product);


        /// <summary>
        /// Finds the or create order detail by product.
        /// </summary>
        /// <returns>The or create order detail by product.</returns>
        /// <param name="product">Product.</param>
        Task<OrderDetail> FindOrCreateOrderDetailByProduct(Product product);

        /// <summary>
        /// Finds the order detail by product.
        /// </summary>
        /// <returns>The order detail by product.</returns>
        /// <param name="product">Product.</param>
        Task<OrderDetail> FindOrderDetailByProduct(Product product);

        /// <summary>
        /// Inserts the or update order detail.
        /// </summary>
        /// <returns>The or update order detail.</returns>
        /// <param name="orderDetail">Order detail.</param>
        Task InsertOrUpdateOrderDetail(OrderDetail orderDetail);

        /// <summary>
        /// Inserts the or update order detail.
        /// </summary>
        /// <returns>The or update order detail.</returns>
        /// <param name="product">Product.</param>
        /// <param name="quantity">Quantity.</param>
        /// <param name="orderDetail">Order detail.</param>
        Task InsertOrUpdateOrderDetail(Product product,
                                       int quantity,
                                       OrderDetail orderDetail = null);
        /// <summary>
        /// Syncs the order.
        /// </summary>
        /// <param name="data">Data.</param>
        Task SyncOrder(string data);

        /// <summary>
        /// Finds the orders detail.
        /// </summary>
        /// <returns>The orders detail.</returns>
        Task<List<OrderDetail>> FindPendingOrderDetail();
        /// <summary>
        /// Finds the confirmed order detail.
        /// </summary>
        /// <returns>The confirmed order detail.</returns>
        /// <param name="filterByUserLogged">If set to <c>true</c> filter by user logged.</param>
        Task<List<OrderDetail>> FindConfirmedOrderDetail(bool filterByUserLogged = false);
        /// <summary>
        /// Gets the total amount.
        /// </summary>
        /// <returns>The total amount.</returns>
        /// <param name="filterByUserLogged">If set to <c>true</c> filter by user logged.</param>
        Task<Double> GetTotalAmount(bool filterByUserLogged = false);


        /// <summary>
        /// Finds the waiting order details.
        /// </summary>
        /// <returns>The waiting order details.</returns>
        Task<List<OrderDetail>> FindWaitingOrderDetails();

        /// <summary>
        /// Sets the current place.
        /// </summary>
        /// <param name="place">Place.</param>
        Task SetCurrentPlace(Place place);

        /// <summary>
        /// Gets the current place.
        /// </summary>
        /// <returns>The current place.</returns>
        Task<Place> GetCurrentPlace();

        /// <summary>
        /// Sends to kitchen.
        /// </summary>
        /// <returns>The to kitchen.</returns>
        /// <param name="source">Source.</param>
        /// <param name="wait">If set to <c>true</c> wait.</param>
        Task<Order> SendToKitchen(CancellationTokenSource source,
                                  bool wait = false);

        /// <summary>
        /// Sets the current table.
        /// </summary>
        /// <returns>The current table.</returns>
        /// <param name="tableNumber">Table number.</param>
        Task SetCurrentTable(int tableNumber);

        /// <summary>
        /// Sets the current sector and table.
        /// </summary>
        /// <returns>The current sector and table.</returns>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="table">Table.</param>
        Task SetCurrentSectorAndTable(long sectorId,
                                      int table);

        /// <summary>
        /// Gets the current sector.
        /// </summary>
        /// <returns>The current sector.</returns>
        long GetCurrentSector();

        /// <summary>
        /// Gets the current table.
        /// </summary>
        /// <returns>The current table.</returns>
        int GetCurrentTable();
        /// <summary>
        /// Sets the user.
        /// </summary>
        /// <param name="user">User.</param>
        Task SetCurrentUser(User user);

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <returns>The current user.</returns>
        User GetCurrentUser();

        /// <summary>
        /// Validates the current order by place.
        /// </summary>
        /// <returns><c>true</c>, if current order by place was validated, <c>false</c> otherwise.</returns>
        /// <param name="id">Identifier.</param>
        Task<bool> ValidateCurrentOrderByPlace(long id);

        /// <summary>
        /// Finds the suggestion menu section.
        /// </summary>
        /// <returns>The suggestion menu section.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<MenuSection>> FindSuggestionMenuSection(IPaggination paggination = null);

        /// <summary>
        /// Checks the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="force">If set to <c>true</c> force.</param>
        Task<Order> CheckOrder(bool force = false);

        /// <summary>
        /// Clears the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task ClearCurrentOrder();

        /// <summary>
        /// Sets the pin.
        /// </summary>
        /// <param name="pin">Pin.</param>
        void SetPin(string pin);
        Task<List<OrderDetail>> GetPendingRequestOrderDetailList();

        /// <summary>
        /// Checks the order status.
        /// </summary>
        /// <returns>The order status.</returns>
        Task<Order> CheckOrderStatus();

        /// <summary>
        /// Deletes the order detail.
        /// </summary>
        /// <returns>The order detail.</returns>
        /// <param name="orderDetail">Order detail.</param>
        Task<bool> DeleteOrderDetail(OrderDetail orderDetail);

        /// <summary>
        /// Deletes the order details.
        /// </summary>
        /// <returns>The order details.</returns>
        /// <param name="orderDetails">Order details.</param>
        Task<bool> DeleteOrderDetails(IEnumerable<OrderDetail> orderDetails);

        /// <summary>
        /// Closeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        Task<bool> ClosedOrder();

        /// <summary>
        /// Inserts the or update products in product.
        /// </summary>
        /// <returns>The or update products in product.</returns>
        /// <param name="mainProduct">Main product.</param>
        /// <param name="group">Group.</param>
        /// <param name="productsToAdd">Products to add.</param>
        /// <param name="orderDetail">Order detail.</param>
        /// <param name="quantity">Quantity.</param>
        Task<OrderDetail> InsertOrUpdateProductsInProduct(Product mainProduct,
                                                          Group group,
                                                          List<Product> productsToAdd,
                                                          OrderDetail orderDetail = null,
                                                          int quantity = 1);

        /// <summary>
        /// Inserts the or update product in compount product.
        /// </summary>
        /// <returns>The or update product in compount product.</returns>
        /// <param name="compoundProduct">Compound product.</param>
        /// <param name="productToAdd">Product to add.</param>
        /// <param name="orderDetail">Order detail.</param>
        /// <param name="isPending">If set to <c>true</c> is pending.</param>
        /// <param name="quantity">Quantity.</param>
        Task<OrderDetail> InsertOrUpdateProductInCompountProduct(Product compoundProduct,
                                                                 Product productToAdd,
                                                                 OrderDetail orderDetail = null,
                                                                 bool isPending = false,
                                                                 int quantity = 1);

        /// <summary>
        /// Orders the pending to sync.
        /// </summary>
        /// <returns><c>true</c>, if pending to sync was ordered, <c>false</c> otherwise.</returns>
        Task<bool> OrderPendingToSync();

        /// <summary>
        /// Sets the quantity diners.
        /// </summary>
        /// <param name="quantity">Quantity.</param>
        void SetQuantityDiners(int quantity);

        /// <summary>
        /// Selects the product by pending order detail.
        /// </summary>
        /// <returns>The product by pending order detail.</returns>
        /// <param name="product">Product.</param>
        /// <param name="orderDetail">Order detail.</param>
        Task<OrderDetail> SelectProductByPendingOrderDetail(Product product,
                                                            OrderDetail orderDetail);

        /// <summary>
        /// Removes the invalid products.
        /// </summary>
        /// <returns>The invalid products.</returns>
        /// <param name="invalidProducts">Invalid products.</param>
        Task<bool> RemoveInvalidProducts(List<Product> invalidProducts);
        /// <summary>
        /// Deletes the pending order details.
        /// </summary>
        /// <returns>The pending order details.</returns>
        Task DeletePendingOrderDetails();

        /// <summary>
        /// Clears the current table.
        /// </summary>
        /// <returns>The current table.</returns>
        Task ClearCurrentTable();

        /// <summary>
        /// Sets the order mode.
        /// </summary>
        /// <returns>The order mode.</returns>
        /// <param name="modeIdentifier">Mode identifier.</param>
        Task SetOrderMode(int modeIdentifier);

        /// <summary>
        /// Gets the order mode.
        /// </summary>
        /// <returns>The order mode.</returns>
        Task<int> GetOrderMode();

        /// <summary>
        /// Users the is required.
        /// </summary>
        /// <returns>The is required.</returns>
        Task<bool> UserIsRequired();

        /// <summary>
        /// Tables the number is required.
        /// </summary>
        /// <returns>The number is required.</returns>
        Task<bool> TableNumberIsRequired();

        /// <summary>
        /// Gets the sync order.
        /// </summary>
        /// <returns>The sync order.</returns>
        /// <param name="data">Data.</param>
        Task<Order> GetSyncOrder(string data);

        /// <summary>
        /// Takes the away exist order.
        /// </summary>
        /// <returns>The away exist order.</returns>
        /// <param name="place">Place.</param>
        /// <param name="orderNumber">Order number.</param>
        Task<bool> TakeAwayExistOrder(Place place, int orderNumber);

        /// <summary>
        /// Gets the total amount by order details.
        /// </summary>
        /// <returns>The total amount by order details.</returns>
        /// <param name="orderDetails">Order details.</param>
        Task<double> GetTotalAmountByOrderDetails(List<OrderDetail> orderDetails);

        /// <summary>
        /// Sets the payment.
        /// </summary>
        /// <param name="paymentResult">Payment result.</param>
        void SetPayment(PaymentResult paymentResult);
    }
}
