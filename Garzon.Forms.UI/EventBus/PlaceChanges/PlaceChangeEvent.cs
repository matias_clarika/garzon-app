﻿using System;
using Garzon.Core.EventBus;
using Garzon.DataObjects;

namespace Garzon.Forms.UI.EventBus.PlaceChanges
{
    public class PlaceChangeEvent : Event
    {
        readonly Place _place;

        public Place Place
        {
            get=>_place;
        }

        private PlaceChangeEvent(object sender,
                                Place place = null) : base(sender)
        {
            _place = place;
        }

        /// <summary>
        /// Builder the specified sender and currentPlace.
        /// </summary>
        /// <returns>The builder.</returns>
        /// <param name="sender">Sender.</param>
        /// <param name="currentPlace">Current place.</param>
        public static PlaceChangeEvent Builder(object sender,
                                               Place currentPlace = null){
            return new PlaceChangeEvent(sender, currentPlace);
        }
    }
}
