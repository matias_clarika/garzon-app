﻿using System;
using System.Threading.Tasks;
using Garzon.Core.EventBus;
using Garzon.Core.Utils;
using Garzon.Forms.UI.Facade;
using I18NPortable;
using MvvmCross.Platform;

namespace Garzon.Forms.UI.EventBus.Notifications
{
    public class RegistrationNotificationGlobalEventHandler
    {
        #region dependency
        readonly IEventBus _eventBus;
        private IDialogs _dialogs;
        private IDialogs Dialogs
        {
            get
            {
                if (_dialogs == null)
                {
                    _dialogs = Mvx.Resolve<IDialogs>();
                }
                return _dialogs;
            }

        }
        #endregion

        #region tokens
        Task<IEventToken> _token;
        #endregion

        IProgressDialog _mainProgressDialog;

        public RegistrationNotificationGlobalEventHandler(IEventBus eventBus)
        {
            //DI
            _eventBus = eventBus;
            //Subscribe
            _token = _eventBus.SubscribeOnMainThread<RegistrationNotificationEvent>(OnRegistrationNotificationChange);
        }

        private void OnRegistrationNotificationChange(RegistrationNotificationEvent obj)
        {
            switch (obj.Action)
            {
                case Action.START:
                    ShowLoader();
                    break;
                case Action.SUCCESS:
                case Action.ERROR:
                    HideLoader();
                    break;
            }
        }

        private void ShowLoader()
        {
            if (_mainProgressDialog != null)
            {
                _mainProgressDialog = _dialogs
                                        .ProgressDialog(I18N.Current["NotificationRegistrationStart"]);
            }
            _mainProgressDialog.Show();
        }

        private void HideLoader()
        {
            if (_mainProgressDialog != null)
            {
                _mainProgressDialog.Hide();
                _mainProgressDialog = null;
            }

        }
    }
}
