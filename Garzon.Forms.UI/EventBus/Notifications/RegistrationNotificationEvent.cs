﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus.Notifications
{
    public class RegistrationNotificationEvent : Event
    {
        readonly Action _action;

        public Action Action
        {
            get=>_action;
        }

        private RegistrationNotificationEvent(object sender,
                                             Action action) : base(sender)
        {
            _action = action;
        }

        /// <summary>
        /// Builder the specified sender and action.
        /// </summary>
        /// <returns>The builder.</returns>
        /// <param name="sender">Sender.</param>
        /// <param name="action">Action.</param>
        public static RegistrationNotificationEvent Builder(object sender,
                                                            Action action){
            return new RegistrationNotificationEvent(sender, action);
        }
    }

    public enum Action{
        START, ERROR, SUCCESS
    }
}
