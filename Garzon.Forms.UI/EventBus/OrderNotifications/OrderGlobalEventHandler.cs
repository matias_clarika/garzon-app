﻿namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Garzon.Core;
    using Garzon.Core.EventBus;
    using Garzon.Core.EventBus.Auth;
    using Garzon.Core.Managers;
    using Garzon.Core.Utils;
    using Garzon.Forms.UI.DI;
    using Garzon.Forms.UI.Facade;
    using Garzon.Forms.UI.Navigation;
    using I18NPortable;
    using MvvmCross.Platform;
    using Newtonsoft.Json;

    public class OrderGlobalEventHandler
    {
        #region dependency
        private IDialogs dialogs;
        private IDialogs Dialogs
        {
            get
            {
                if (this.dialogs == null)
                {
                    this.dialogs = Mvx.Resolve<IDialogs>();
                }
                return this.dialogs;
            }

        }

        private IAppLifecycle appLifecycle;

        public IAppLifecycle AppLifecycle
        {
            get
            {
                if (this.appLifecycle == null)
                {
                    this.appLifecycle = Mvx.Resolve<IAppLifecycle>();
                }
                return this.appLifecycle;
            }

        }

        readonly INavigationController navigationController;
        readonly IEventBus eventBus;
        readonly IOrderManager orderManager;
        readonly INotifyManager notifyManager;
        readonly IRedirectManager redirectManager;
        #endregion

        #region tokens
        Task<IEventToken> token;
        #endregion
        public OrderGlobalEventHandler(INavigationController navigationController,
                                       IEventBus eventBus,
                                       IOrderManager orderManager,
                                       INotifyManager notifyManager,
                                       IRedirectManager redirectManager)
        {
            //DI
            this.navigationController = navigationController;
            this.eventBus = eventBus;
            this.orderManager = orderManager;
            this.notifyManager = notifyManager;
            this.redirectManager = redirectManager;
            //subscribe
            this.token = eventBus.SubscribeOnMainThread<OrderEvent>(OnOrderReceived);
        }

        private async void OnOrderReceived(OrderEvent orderEvent)
        {
            if (orderEvent != null)
            {
                var orderNotification = OrderNotification.Builder(orderEvent);
                await ReadAndWriteByOrderNotification(orderNotification);
            }
        }

        private async Task ReadAndWriteByOrderNotification(OrderNotification orderNotification)
        {
            await PersistData(orderNotification);
            Notify(orderNotification);
        }

        private async Task PersistData(OrderNotification orderNotification)
        {
            if (!String.IsNullOrEmpty(orderNotification.Data))
            {
                switch (orderNotification.Action)
                {
                    case OrderNotificationAction.ORDER_CONFIRMED:
                    case OrderNotificationAction.ORDER_OBSERVED:
                    case OrderNotificationAction.ORDER_CLOSED:
                    case OrderNotificationAction.ORDER_CHANGE_TABLE_NUMBER:
                        await SyncDataWithOrder(orderNotification);
                        break;
                }
            }
        }

        private async Task SyncDataWithOrder(OrderNotification orderNotification)
        {
            IProgressDialog progressDialog = null;
            if (IsForeground())
            {
                progressDialog = this.Dialogs.ProgressDialog("");
                await progressDialog.Show();
            }
            await orderManager.SyncOrder(orderNotification.Data);
            if (progressDialog != null)
            {
                await progressDialog.Hide();
            }
        }

        public async Task CheckPendingNotification()
        {
            string pending = await notifyManager.Get(OrderNotification.KEY_ORDER_COUNTER_NOTIFICATION);
            if (!String.IsNullOrEmpty(pending))
            {
                //clear
                await notifyManager.Put(OrderNotification.KEY_ORDER_COUNTER_NOTIFICATION, null);
                var orderNotification = OrderNotification.Builder(pending);
                ReadAndWriteByOrderNotification(orderNotification);
            }
        }

        #region notify
        private void Notify(OrderNotification orderNotification,
                            bool withoutAlert = false)
        {
            if (IsForeground())
            {
                switch (orderNotification.Action)
                {
                    case OrderNotificationAction.ORDER_OBSERVED:
                        NotifyObserved(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_CONFIRMED:
                        NotifyInKitchen(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_CLOSED:
                        NotifyClosed(orderNotification);
                        break;
                    case OrderNotificationAction.WAITER_ONTHEWAY:
                        NotifyWaiter(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_IN_COUNTER:
                        NotifyOrderInCounter(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_CHANGE_TABLE_NUMBER:
                        NotifyOrderChangeTableNumber(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_TAKE_AWAY_READY_FOR_WITHDRAW:
                        NotifyOrderReadyForWithdraw(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_TAKE_AWAY_CONFIRM:
                        NotifyOrderTakeAwayConfirm(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_TAKE_AWAY_DELIVERED:
                        NotifyTakeAwayDelivered(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_TAKE_AWAY_OBSERVED:
                        NotifyTakeAwayObserved(orderNotification);
                        break;
                    case OrderNotificationAction.ORDER_PAYMENT_REJECTED:
                        NotifyTakeAwayObserved(orderNotification, true);
                        break;
                }
            }
            else if (orderNotification.IsPendingRouter())
            {
                PendingNotify(orderNotification);
            }
        }

        /// <summary>
        /// Pendings the notify.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private void PendingNotify(OrderNotification orderNotification)
        {
            notifyManager.Put(orderNotification.GetKey(), orderNotification.ToString());
        }

        /// <summary>
        /// Notifies the closed.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyClosed(OrderNotification orderNotification)
        {
            await navigationController.ReOpenApp();
            await this.Dialogs.ShowAlert(I18N.Current["OrderClosedMessage"],
                                    I18N.Current["OrderClosedTitle"],
                                    I18N.Current["Accept"]);
            await this.redirectManager.AnalyzeRedirectByData(orderNotification.Data);
        }

        /// <summary>
        /// Notifies the in kitchen.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyInKitchen(OrderNotification orderNotification)
        {
            await this.eventBus.Publish<OrderInKitchentEvent>(OrderInKitchentEvent.Build(this));
            await Dialogs.ShowAlert(I18N.Current["OrderInKitchenMessage"],
                                    I18N.Current["OrderInKitchenTitle"],
                                    I18N.Current["Accept"]);

        }

        /// <summary>
        /// Notifies the waiter.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyWaiter(OrderNotification orderNotification)
        {
            await Dialogs.ShowAlert(I18N.Current["WaiterOnTheWay"]);
        }

        /// <summary>
        /// Notifies the observed.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyObserved(OrderNotification orderNotification)
        {
            await navigationController.OpenMyOrderDetail(true);
            await Dialogs.ShowAlert(I18N.Current["OrderObservedMessage"],
                              I18N.Current["OrderObservedTitle"],
                              I18N.Current["Accept"]);
            await this.eventBus.Publish(OrderObservedEvent.Build(this));
        }

        /// <summary>
        /// Notifies the order in counter.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyOrderInCounter(OrderNotification orderNotification)
        {
            await Dialogs.ShowAlert(I18N.Current["SendToKitchenSuccess"]);
        }

        /// <summary>
        /// Notifies the order change table number.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyOrderChangeTableNumber(OrderNotification orderNotification)
        {
            this.eventBus.Publish(OrderTableChangeEvent.Builder(this));
        }

        /// <summary>
        /// Notifies the order take away confirm.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyOrderTakeAwayConfirm(OrderNotification orderNotification)
        {
            var order = await orderManager.GetSyncOrder(orderNotification.Data);
            var message = string.Format(I18N.Current["OrderTakeAwayConfirmedMessage"], order.NumberToWithdraw.Value, order.Place != null ? order.Place.Name : string.Empty, order.TimeDelay.Value);
            await Dialogs.ShowAlert(message, GetTitleToTakeAwayAlert(order));
            NotifyRefreshTakeAway();
        }

        ///summary>
        /// Notifies the order ready for withdraw.
        /// </summary>
        /// <param name="orderNotification">Order notification.</param>
        private async void NotifyOrderReadyForWithdraw(OrderNotification orderNotification)
        {
            var order = await orderManager.GetSyncOrder(orderNotification.Data);
            var message = string.Format(I18N.Current["OrderReadyForWithdrawMessage"], order.NumberToWithdraw);
            await Dialogs.ShowAlert(message, GetTitleToTakeAwayAlert(order));
            NotifyRefreshTakeAway();
        }

        private async void NotifyTakeAwayDelivered(OrderNotification orderNotification)
        {
            var order = await orderManager.GetSyncOrder(orderNotification.Data);
            var message = string.Format(I18N.Current["OrderTakeAwayDeliveredMessage"], order.NumberToWithdraw.Value);
            await Dialogs.ShowAlert(message, GetTitleToTakeAwayAlert(order));
            NotifyRefreshTakeAway();
        }


        private async void NotifyTakeAwayObserved(OrderNotification orderNotification, bool withPayment = false)
        {
            var order = await orderManager.GetSyncOrder(orderNotification.Data);
            string detailsObserved = ObservedDetailMessageByOrder(order);
            var message = string.Format(I18N.Current["OrderTakeAwayObservedMessage"], order.NumberToWithdraw.Value, detailsObserved);
            if (withPayment)
            {
                message = $"{message}.\n{I18N.Current["PaymentOrderRejectedAndCloseReasonMessage"]}";
            }
            else
            {
                message = $"{message}.\n{I18N.Current["PaymentReintegrateMessage"]}";
            }

            var confirm = await Dialogs.ShowConfirmDialog(GetTitleToTakeAwayAlert(order), message, I18N.Current["ChangeOrder"], I18N.Current["Accept"]);
            NotifyRefreshTakeAway();
            if (confirm)
            {
                await this.navigationController.OpenMenuSectionByPlace(order.Place);
            }
        }

        private static string ObservedDetailMessageByOrder(DataObjects.Order order)
        {

            List<string> detailsObserved = new List<string>();
            foreach (var detail in order.Details)
            {
                if (detail.IsObserved())
                {
                    var detailMessage = detail.Product.Name;
                    if (detail.IsCompound)
                    {
                        var childrenObserved = detail.Details.Where((arg) => arg.IsObserved()).Select((arg) => arg.Product.Name).ToArray();
                        if (childrenObserved.Any())
                        {
                            detailMessage = $"{detailMessage}: {string.Join(", ", childrenObserved)}";
                        }
                    }
                    detailsObserved.Add(detailMessage);
                }
            }
            return string.Join("\n", detailsObserved);
        }

        static string GetTitleToTakeAwayAlert(DataObjects.Order order)
        {
            return order.Place != null ? order.Place.Name : I18N.Current["TakeAwayTitle"];
        }

        private async Task NotifyRefreshTakeAway()
        {
            this.eventBus.Publish(RefreshTakeAwayEvent.Build(this));
        }

        #endregion

        private bool IsForeground()
        {
            return this.AppLifecycle != null && this.AppLifecycle.Foreground;
        }
    }

    public class OrderNotification
    {

        public static string KEY_ORDER_COUNTER_NOTIFICATION = "KEY_ORDER_COUNTER_NOTIFICATION";

        public OrderNotificationAction Action
        {
            get;
            set;
        }

        public string Data
        {
            get;
            set;
        }

        public OrderNotification()
        {

        }

        private OrderNotification(OrderNotificationAction action, string data)
        {
            Action = action;
            Data = data;
        }

        public static OrderNotification Builder(OrderEvent orderEvent)
        {
            return new OrderNotification(orderEvent.Action, orderEvent.Data);
        }

        public static OrderNotification Builder(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<OrderNotification>(json);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public bool IsPendingRouter()
        {
            return Action > 0
                && Action != OrderNotificationAction.WAITER_ONTHEWAY
                                                    && Action != OrderNotificationAction.ORDER_UPDATED_BY_OTHER_USER
                                                    && Action != OrderNotificationAction.ORDER_TAKE_AWAY_CONFIRM
                                                    && Action != OrderNotificationAction.ORDER_TAKE_AWAY_DELIVERED
                                                    && Action != OrderNotificationAction.ORDER_TAKE_AWAY_READY_FOR_WITHDRAW;
        }

        public string GetKey()
        {
            return KEY_ORDER_COUNTER_NOTIFICATION;
        }

    }
}
