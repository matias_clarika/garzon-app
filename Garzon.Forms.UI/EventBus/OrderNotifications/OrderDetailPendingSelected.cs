﻿using System;
using Garzon.Core.EventBus;
using Garzon.DataObjects;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderDetailPendingSelected : Event
    {
        #region immutable
        readonly OrderDetail pendingOrderDetail;
        readonly Group parentGroup;
        #endregion

        private OrderDetailPendingSelected(object sender,
                                           OrderDetail pendingOrderDetail) : base(sender)
        {
            this.pendingOrderDetail = pendingOrderDetail;
        }

        #region getters
        public OrderDetail PendingOrderDetail
        {
            get => this.pendingOrderDetail;
        }
        #endregion

        #region builder
        public static OrderDetailPendingSelected Builder(object sender,
                                                         OrderDetail pendingOrderDetail)
        {
            return new OrderDetailPendingSelected(sender, pendingOrderDetail);
        }
        #endregion

    }
}
