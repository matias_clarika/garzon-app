﻿using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class SendToKitchenEvent : Event
    {
        private SendToKitchenEvent(object sender) : base(sender)
        {
        }

        public static SendToKitchenEvent Build(object sender){
            return new SendToKitchenEvent(sender);
        }
    }
}
