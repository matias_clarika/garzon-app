﻿namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    using System;
    using Garzon.Core.EventBus;

    public class RefreshTakeAwayEvent : Event
    {
        public RefreshTakeAwayEvent(object sender) : base(sender)
        {
        }

        /// <summary>
        /// Build the specified sender.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="sender">Sender.</param>
        public static RefreshTakeAwayEvent Build(object sender)
        {
            return new RefreshTakeAwayEvent(sender);
        }
    }
}
