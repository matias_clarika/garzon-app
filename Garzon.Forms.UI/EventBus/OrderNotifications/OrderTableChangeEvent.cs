﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderTableChangeEvent : Event
    {
        private OrderTableChangeEvent(object sender) : base(sender)
        {
        }

        public static OrderTableChangeEvent Builder(object sender){
            return new OrderTableChangeEvent(sender);
        }
    }
}
