﻿using System;
using Garzon.Core.EventBus;
using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderQuantityDinersEvent : Event
    {
        public OrderQuantityDinersEvent(object sender) : base(sender)
        {
        }

        public static OrderQuantityDinersEvent Build(object sender)
        {
            return new OrderQuantityDinersEvent(sender);
        }
    }
}
