﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderInCounterEvent : Event
    {
        private OrderInCounterEvent(object sender) : base(sender)
        {
        }

        public static OrderInCounterEvent Build(object sender){
            return new OrderInCounterEvent(sender);
        }
    }
}
