﻿using System;
using Garzon.Core.EventBus;
using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderTableAuthEvent : Event
    {
        public OrderTableAuthEvent(object sender) : base(sender)
        {
        }

        public static OrderTableAuthEvent Build(object sender)
        {
            return new OrderTableAuthEvent(sender);
        }
    }
}
