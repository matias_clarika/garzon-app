﻿namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    using Garzon.Core.EventBus;
    using Garzon.DataObjects;

    public class ProductSelectedEvent : Event
    {
        #region immutable
        readonly Product product;
        readonly Group parentGroup;
        #endregion

        private ProductSelectedEvent(object sender,
                                     Product product,
                                     Group parentGroup) : base(sender)
        {
            this.product = product;
            this.parentGroup = parentGroup;
        }

        #region getters
        public Product Product
        {
            get => this.product;
        }

        public Group ParentGroup{
            get => this.parentGroup;
        }
        #endregion

        #region builder
        public static ProductSelectedEvent Builder(object sender,
                                                   Product product,
                                                   Group parentGroup)
        {
            return new ProductSelectedEvent(sender, product, parentGroup);
        }
        #endregion

    }
}
