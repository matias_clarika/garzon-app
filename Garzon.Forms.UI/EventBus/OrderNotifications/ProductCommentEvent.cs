﻿namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    using System;
    using Garzon.Core.EventBus;

    public class ProductCommentEvent : Event
    {
        readonly string comment;

        private ProductCommentEvent(object sender, string comment) : base(sender)
        {
            this.comment = comment;
        }

        public string Comment{
            get => this.comment;
        }
        public static ProductCommentEvent Build(object sender, string comment){
            return new ProductCommentEvent(sender, comment);
        }

    }
}
