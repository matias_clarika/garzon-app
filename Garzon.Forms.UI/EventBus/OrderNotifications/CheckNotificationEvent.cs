﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class CheckNotificationEvent : Event
    {
        private CheckNotificationEvent(object sender) : base(sender)
        {
        }

        public static CheckNotificationEvent Builder(object sender){
            return new CheckNotificationEvent(sender);
        }
    }
}
