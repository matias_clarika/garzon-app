﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderInKitchentEvent : Event
    {
        private OrderInKitchentEvent(object sender) : base(sender)
        {
        }

        public static OrderInKitchentEvent Build(object sender)
        {
            return new OrderInKitchentEvent(sender);
        }
    }
}
