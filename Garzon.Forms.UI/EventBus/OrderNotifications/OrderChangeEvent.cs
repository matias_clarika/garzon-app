﻿using System;
using Garzon.Core.EventBus;
using Garzon.DataObjects;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderChangeEvent : Event
    {
        readonly Order _order;

        public Order Order
        {
            get=>_order;
        }

        private OrderChangeEvent(object sender,
                                 Order order = null) : base(sender)
        {
            _order = order;   
        }

        public static OrderChangeEvent Build(object sender,
                                               Order order = null){
            return new OrderChangeEvent(sender,
                                        order);
        }
    }

}
