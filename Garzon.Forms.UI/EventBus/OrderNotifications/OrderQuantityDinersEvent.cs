﻿using System;
using Garzon.Core.EventBus;
using Garzon.Forms.UI.ViewModels;

namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    public class OrderPinAuthEvent : Event
    {
        public OrderPinAuthEvent(object sender) : base(sender)
        {
        }

        public static OrderPinAuthEvent Build(object sender)
        {
            return new OrderPinAuthEvent(sender);
        }
    }
}
