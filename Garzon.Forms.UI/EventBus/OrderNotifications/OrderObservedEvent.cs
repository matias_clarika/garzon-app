﻿namespace Garzon.Forms.UI.EventBus.OrderNotifications
{
    using System;
    using Garzon.Core.EventBus;

    public class OrderObservedEvent : Event
    {
        private OrderObservedEvent(object sender) : base(sender)
        {
        }

        /// <summary>
        /// Build the specified sender.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="sender">Sender.</param>
        public static OrderObservedEvent Build(object sender)
        {
            return new OrderObservedEvent(sender);
        }
    }
}
