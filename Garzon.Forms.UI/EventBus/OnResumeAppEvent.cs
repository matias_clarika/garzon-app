﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Forms.UI.EventBus
{
    public class OnResumeAppEvent : Event
    {
        private OnResumeAppEvent(object sender) : base(sender)
        {
        }

        public static OnResumeAppEvent Build(object sender)
        {
            return new OnResumeAppEvent(sender);
        }
    }
}
