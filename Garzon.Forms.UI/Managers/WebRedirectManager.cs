﻿namespace Garzon.Forms.UI.Managers
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Garzon.Core.Managers;
    using Garzon.Forms.UI.Navigation;
    using Newtonsoft.Json;

    public class WebRedirectManager : IRedirectManager
    {
        private JsonSerializerSettings jsonSerializerSettings;
        private INavigationController navigationController;
        private IOAuthSettings oauthSettings;

        public WebRedirectManager(JsonSerializerSettings jsonSerializerSettings,
                                  INavigationController navigationController,
                                  IOAuthSettings oauthSettings)
        {
            this.jsonSerializerSettings = jsonSerializerSettings;
            this.navigationController = navigationController;
            this.oauthSettings = oauthSettings;
        }

        #region IRedirectManager
        public async Task AnalyzeRedirectByData(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var redirectInfo = JsonConvert.DeserializeObject<RedirectInfo>(data);
                    await OpenRedirect(redirectInfo);
                }
                catch { }
            }
        }

        private async Task OpenRedirect(RedirectInfo redirectInfo)
        {
            if (redirectInfo != null
                                    && redirectInfo.IsValid()
                                    && !string.IsNullOrEmpty(this.oauthSettings.GetToken()))
            {
                await this.navigationController.OpenWebUrl($"{redirectInfo.RedirectUrl}?token={this.oauthSettings.GetToken()}&place_id={redirectInfo.PlaceId}");
            }
        }

        public async Task AnalyzeRedirectByUrl(string url, long? placeId)
        {
            if (!string.IsNullOrEmpty(url) && placeId.HasValue)
            {
                var redirectInfo = new RedirectInfo { RedirectUrl = url, PlaceId = $"{placeId.Value}" };
                await OpenRedirect(redirectInfo);
            }
        }
        #endregion
    }

    public class RedirectInfo
    {

        public RedirectInfo()
        {
        }
        [JsonProperty(PropertyName = "redirect_url")]
        public string RedirectUrl
        {
            get;
            set;
        }
        [JsonProperty(PropertyName = "place_id")]
        public string PlaceId
        {
            get;
            set;
        }
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(this.RedirectUrl);
        }
    }
}
