﻿using System;
namespace Garzon.Core
{
    public interface IAppLifecycle
    {
        bool Foreground
        {
            get;
            set;
        }

        bool ExistInstance{
            get;
            set;
        }
    }
}
