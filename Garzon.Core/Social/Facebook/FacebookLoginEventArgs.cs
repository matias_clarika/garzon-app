﻿using System;
using Garzon.DataObjects.Social;

namespace Garzon.Core.Social.Facebook
{
    public class FacebookLoginEventArgs : EventArgs
    {
        public SocialUser User { get; set; }
    }
}
