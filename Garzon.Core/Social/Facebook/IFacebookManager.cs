﻿using System;
using System.Threading.Tasks;
using Garzon.DataObjects.Social;
using Xamarin.Forms;

namespace Garzon.Core.Social.Facebook
{
    public interface IFacebookManager
    {
        /// <summary>
        /// Login this instance.
        /// </summary>
        /// <returns>The login.</returns>
        Task<SocialUser> Login();
        /// <summary>
        /// Logs the out.
        /// </summary>
        /// <returns>The out.</returns>
        Task LogOut();
        /// <summary>
        /// Validates the token.
        /// </summary>
        /// <returns>The token.</returns>
        Task<bool> ValidateToken();
        /// <summary>
        /// Posts the text.
        /// </summary>
        /// <returns>The text.</returns>
        /// <param name="message">Message.</param>
        Task<bool> PostText(string message);
        /// <summary>
        /// Posts the photo.
        /// </summary>
        /// <returns>The photo.</returns>
        /// <param name="image">Image.</param>
        Task<bool> PostPhoto(ImageSource image);
    }
}
