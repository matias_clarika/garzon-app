﻿using System;
namespace Garzon.Core.Social.Facebook
{
    public static class FacebookSettings
    {
        public static string[] ReadPermissions = new string[] {
            "public_profile", "email",
        };

        public static string[] PublishPermissions = new string[] {
            "publish_actions"
        };

        public static string AppName { get; } = "Clar­i­k­a­_­test";
    }
}
