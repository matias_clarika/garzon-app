﻿using System;
using System.Threading.Tasks;
using Garzon.DataObjects.Social;

namespace Garzon.Core.Social.Google
{
    public interface IGoogleManager
    {
        /// <summary>
        /// Signs the in.
        /// </summary>
        /// <returns>The in.</returns>
        Task<SocialUser> SignIn();
        /// <summary>
        /// Cancels the sign in.
        /// </summary>
        void CancelSignIn();

    }
}
