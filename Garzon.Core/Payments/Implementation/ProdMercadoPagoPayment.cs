﻿namespace Garzon.Core.Payments.Implementation
{
    using System;

    public class ProdMercadoPagoPayment : IMercadoPagoPayment
    {
        #region IMercadoPagoPayment
        public string PaymentUrl => "https://mp-dot-garzon-184613.appspot.com/?user={0}";
        #endregion
    }
}
