﻿namespace Garzon.Core.Payments.Implementation
{
    using System;

    public class QaMercadoPagoPayment : IMercadoPagoPayment
    {
        #region IMercadoPagoPayment
        public string PaymentUrl => "http://mp-staging-dot-garzon-dev.appspot.com/?user={0}";
        #endregion
    }
}
