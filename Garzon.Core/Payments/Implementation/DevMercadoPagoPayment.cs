﻿namespace Garzon.Core.Payments.Implementation
{
    using System;

    public class DevMercadoPagoPayment : IMercadoPagoPayment
    {
        #region IMercadoPagoPayment
        public string PaymentUrl => "http://mp-dev-dot-garzon-dev.appspot.com/?user={0}";
        #endregion
    }
}
