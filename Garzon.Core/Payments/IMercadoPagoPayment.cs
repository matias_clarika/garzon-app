﻿namespace Garzon.Core.Payments
{
    public interface IMercadoPagoPayment
    {
        string PaymentUrl
        {
            get;
        }
    }
}
