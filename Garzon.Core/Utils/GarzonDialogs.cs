﻿using System.Threading.Tasks;
using Acr.UserDialogs;
using Garzon.Core.Resources;

namespace Garzon.Core.Utils
{
    public class GarzonDialogs : IDialogs
    {
        #region dependency
        readonly IUserDialogs _userDialogs;

        #endregion
        public GarzonDialogs(IUserDialogs userDialogs)
        {
            _userDialogs = userDialogs;

        }
        #region Implementation of IDialogs

        public Task ShowAlert(string message, string title = null, string okText = "Ok")
        {
            return _userDialogs.AlertAsync(message, title, okText);
        }

        public IProgressDialog ProgressDialog(string message)
        {
            return new ProgressDialog(_userDialogs.Loading(message));
        }

        public Task ShowError(string message)
        {
            return ShowAlert(message);
        }

        public Task ShowSuccess(string message)
        {
            return ShowAlert(message);
        }

        public async Task<IResult<int>> ShowQuantityDialog(string title,
                                                      int number,
                                                      int minNumber = 0,
                                                      int maxNumber = 10,
                                                      string okText = "Ok",
                                                      string cancelText = "Cancel")
        {
            NumberPromptConfig config = new NumberPromptConfig();
            config.Title = title;
            config.SelectedNumber = number;
            config.OkText = okText;
            config.CancelText = cancelText;
            config.MinNumber = minNumber;
            config.MaxNumber = maxNumber;
            NumberPromptResult result = await _userDialogs.NumberPromptAsync(config);

            return new ResultQuantityDialog(result);

        }

        public async Task<bool> ShowConfirmDialog(string title,
                                                      string message,
                                                      string okText = null,
                                                      string cancelText = null)
        {
            ConfirmConfig config = new ConfirmConfig()
                                        .SetTitle(title)
                                        .SetMessage(message)
                                        .SetOkText(okText);

            if (cancelText != null)
            {
                config.SetCancelText(cancelText);
            }else{
                config.SetCancelText("");
            }

            return await _userDialogs.ConfirmAsync(config);
             

        }

        public async Task<string> ShowCommentDialog(string message,
                                                    string title = null,
                                                    string okText = null,
                                                    string cancelText = null,
                                                    string placeHolder = "")
        {
            var promptResult = await _userDialogs.PromptAsync(message,
                                                              title,
                                                              okText,
                                                              cancelText,
                                                              placeHolder);
            if(promptResult.Ok){
                return promptResult.Text;
            }else{
                return null;
            }
        }

        #endregion
    }

    #region IProgressDialog wrapper
    public class ProgressDialog : IProgressDialog
    {

        readonly Acr.UserDialogs.IProgressDialog _acrProgressDialog;

        public ProgressDialog(Acr.UserDialogs.IProgressDialog acrProgressDialog)
        {
            _acrProgressDialog = acrProgressDialog;
        }

        public Task Hide()
        {
            return Task.Factory.StartNew(() => _acrProgressDialog.Hide());
        }

        public Task Show()
        {
            return Task.Factory.StartNew(() => _acrProgressDialog.Show());

        }
    }
    #endregion

    #region IResult wrapper
    public class ResultQuantityDialog : IResult<int>
    {
        #region fields
        private bool _result = false;
        private int _quantity = 0;
        #endregion

        #region constructor
        public ResultQuantityDialog(NumberPromptResult numberPromptResult)
        {
            if (numberPromptResult != null)
            {
                _result = numberPromptResult.Ok;
                _quantity = numberPromptResult.Value;
            }
        }
        #endregion

        #region Implementation of IResult
        public bool Ok => _result;

        public int Result => _quantity;
        #endregion
    }

    #endregion
}
