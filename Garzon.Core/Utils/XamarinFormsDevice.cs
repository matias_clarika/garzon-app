﻿using System;
using Garzon.DAL;
using Xamarin.Forms;

namespace Garzon.Core.Utils
{
    public class XamarinFormsDevice : IDevice
    {
        private Platform _platform;

        public XamarinFormsDevice()
        {

        }

        public Platform GetPlatform()
        {
            if (_platform == 0)
            {
                _platform = Device.RuntimePlatform == Device.Android ? Platform.ANDROID : Platform.IOS;
            }
            return _platform;
        }
    }
}
