﻿using System;
namespace Garzon.Core.Utils
{
    public interface IFormatPrice
    {
        /// <summary>
        /// Formats the price.
        /// </summary>
        /// <returns>The price.</returns>
        /// <param name="value">Value.</param>
        string FormatPrice(Double value);

        /// <summary>
        /// Formats the price with symbol.
        /// </summary>
        /// <returns>The price with symbol.</returns>
        /// <param name="value">Value.</param>
        string FormatPriceWithSymbol(Double value);
    }
}
