﻿using System;
using Garzon.BL.Currency;
using I18NPortable;

namespace Garzon.Core.Utils
{
    public class DefaultFormatPrice : IFormatPrice
    {
        #region dependency
        readonly ICurrentCurrency _currentCurrency;
        #endregion
        public DefaultFormatPrice(ICurrentCurrency currentCurrency){
            _currentCurrency = currentCurrency;
        }
        #region Implementation of IFormatPrice
        public string FormatPrice(double value)
        {
            if (double.IsNaN(value) || double.IsInfinity(value))
            {
                value = 0.0;
            }

            var formatMoney = I18N.Current["FormatMoney"];
            return(value * _currentCurrency.Ratio()).ToString(formatMoney);
        }

        public string FormatPriceWithSymbol(double value)
        {
            return _currentCurrency.Symbol() + FormatPrice(value);
        }

        #endregion
    }
}
