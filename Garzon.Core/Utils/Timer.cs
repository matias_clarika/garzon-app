﻿using System;
using System.Threading;
using Xamarin.Forms;

namespace Garzon.Core.Utils
{
    public class Timer
    {
        private readonly TimeSpan timespan;
        private readonly Action callback;

        private CancellationTokenSource cancellation;

        private Timer(TimeSpan timespan, Action callback)
        {
            this.timespan = timespan;
            this.callback = callback;
            this.cancellation = new CancellationTokenSource();
        }

        /// <summary>
        /// Starts the new.
        /// </summary>
        /// <returns>The new.</returns>
        /// <param name="timespan">Timespan.</param>
        /// <param name="callback">Callback.</param>
        public static Timer StartNew(TimeSpan timespan, Action callback)
        {
            Timer timer = new Timer(timespan, callback);
            timer.Start();
            return timer;
        }

        /// <summary>
        /// Start this instance.
        /// </summary>
        private void Start()
        {
            CancellationTokenSource cts = this.cancellation; // safe copy
            Device.StartTimer(this.timespan,
                () => {
                    if (cts.IsCancellationRequested) return false;
                    this.callback.Invoke();
                    return true; // or true for periodic behavior
            });
        }
        /// <summary>
        /// Stop this instance.
        /// </summary>
        public void Stop()
        {
            Interlocked.Exchange(ref this.cancellation, new CancellationTokenSource()).Cancel();
        }
       
    }
}
