﻿using System;
using Xamarin.Forms;

namespace Garzon.Core.Utils
{
    public abstract class GarzonColors
    {
        #region Theme
        public static readonly Color PRIMARY_DARK = Color.FromHex("#FF7C00");
        public static readonly Color SECONDARY = Color.FromHex("#424242");
        public static readonly Color COUNTER_ITEM_ALERT_COLOR = Color.FromHex("#b71c1c");
        public static readonly Color COUNTER_ITEM_DEFAULT = Color.FromHex("#9e9e9e");
        public static readonly Color COUNTER_ITEM_HIGHLIGHT_DEFAULT = Color.FromHex("#424242");
        #endregion


    }
}
