﻿using System;
using System.Threading.Tasks;

namespace Garzon.Core.Utils
{
    public interface IDialogs
    {
        /// <summary>
        /// Shows the alert.
        /// </summary>
        /// <returns>The alert.</returns>
        /// <param name="message">Message.</param>
        /// <param name="title">Title.</param>
        /// <param name="okText">Ok text.</param>
        Task ShowAlert(string message, string title = null, string okText = "Ok");
        /// <summary>
        /// Shows the success.
        /// </summary>
        /// <returns>The success.</returns>
        /// <param name="message">Message.</param>
        Task ShowSuccess(string message);

        /// <summary>
        /// Shows the error.
        /// </summary>
        /// <returns>The error.</returns>
        /// <param name="message">Message.</param>
        Task ShowError(string message);

        /// <summary>
        /// Loading the specified message.
        /// </summary>
        /// <returns>The loading.</returns>
        /// <param name="message">Message.</param>
        IProgressDialog ProgressDialog(string message);

        /// <summary>
        /// Shows the quantity dialog.
        /// </summary>
        /// <returns>The quantity dialog.</returns>
        /// <param name="title">Title.</param>
        /// <param name="number">Number.</param>
        /// <param name="minNumber">Minimum number.</param>
        /// <param name="maxNumber">Max number.</param>
        /// <param name="okText">Ok text.</param>
        /// <param name="cancelText">Cancel text.</param>
        Task<IResult<int>> ShowQuantityDialog(string title,
                                                      int number,
                                                      int minNumber = 0,
                                                      int maxNumber = 10,
                                                      string okText = null,
                                                      string cancelText = null);
        /// <summary>
        /// Shows the confirm dialog.
        /// </summary>
        /// <returns>The confirm dialog.</returns>
        /// <param name="title">Title.</param>
        /// <param name="message">Message.</param>
        /// <param name="okText">Ok text.</param>
        /// <param name="cancelText">Cancel text.</param>
        Task<bool> ShowConfirmDialog(string title,
                                               string message,
                                               string okText = null,
                                               string cancelText= null);

        /// <summary>
        /// Shows the comment dialog.
        /// </summary>
        /// <returns>The comment dialog.</returns>
        /// <param name="message">Message.</param>
        /// <param name="title">Title.</param>
        /// <param name="okText">Ok text.</param>
        /// <param name="cancelText">Cancel text.</param>
        /// <param name="placeHolder">Place holder.</param>
        Task<string> ShowCommentDialog(string message,
                                       string title = null,
                                       string okText = null,
                                       string cancelText = null,
                                       string placeHolder = "");
    }

    public interface IProgressDialog
    {
        /// <summary>
        /// Show this instance.
        /// </summary>
        /// <returns>The show.</returns>
        Task Show();
        /// <summary>
        /// Hide this instance.
        /// </summary>
        /// <returns>The hide.</returns>
        Task Hide();
    }

    public interface IResult<T>
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Garzon.Core.Utils.IResult"/> is ok.
        /// </summary>
        /// <value><c>true</c> if ok; otherwise, <c>false</c>.</value>
        bool Ok { get; }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <value>The result.</value>
        T Result { get; }
    }
}
