﻿
namespace Garzon.Core.Resources
{
    public abstract class ImagesPath
    {
        const string ASSEMBLY = "?assembly=Garzon.Core";
        #region Material Icons
        public static readonly string BACK_BUTTON_ICON = "ic_arrow_back_white.png";
        #endregion

        #region Native Icons
        public static readonly string IC_HAMBURGER = "hamburger.png";

        public static readonly string MY_ORDER_BUTTON_ICON = "my_order_white.png";

        public static readonly string IC_WAITER = "ic_waiter.png";
        public static readonly string IC_WAITER_WHITE = "ic_waiter_white.png";

        public static readonly string IC_RESUME_CHECK = "ic_check_order.png";
        public static readonly string IC_RESUME_CHECK_WHITE = "ic_check_order_white.png";
        public static readonly string IC_PIN = "ic_pin.png";

        public static readonly string IC_USER = "ic_user.png";
        #endregion

        #region Counter
        public static readonly string IC_REMOVE_COUNTER = "resource://Garzon.Core.Resources.Images.ic_remove.svg" + ASSEMBLY;
        public static readonly string IC_ADD_COUNTER = "resource://Garzon.Core.Resources.Images.ic_add.svg" + ASSEMBLY;
        public static readonly string IC_OK_WHITE = "ic_ok_white.png";
        public static readonly string IC_SEND_WHITE = "ic_send_white.png";
        public static readonly string IC_DINERS = "resource://Garzon.Core.Resources.Images.ic_diners.svg" + ASSEMBLY;
        #endregion

        #region Defaults
        public static readonly string IC_DEFAULT_IMAGE = "resource://Garzon.Core.Resources.Images.ic_default_image.svg" + ASSEMBLY;
        #endregion

        #region checked
        public static readonly string IC_CHECK = "resource://Garzon.Core.Resources.Images.ic_check.svg" + ASSEMBLY;
        public static readonly string IC_CHECKED = "resource://Garzon.Core.Resources.Images.ic_checked.svg" + ASSEMBLY;
        public static readonly string IC_WITHOUT_STOCK = "ic_without_stock.png";
        public static readonly string IC_WITHOUT_STOCK_ACTIVATE = "ic_without_stock_activate.png";
        public static readonly string IC_RADIOBUTTON = "resource://Garzon.Core.Resources.Images.ic_radiobutton.svg" + ASSEMBLY;
        public static readonly string IC_RADIOBUTTON_SELECTED = "resource://Garzon.Core.Resources.Images.ic_radiobutton_selected.svg" + ASSEMBLY;
        #endregion

        #region alert
        public static readonly string IC_CLOCK = "ic_clock_grey.png";
        public static readonly string IC_CLOCK_ALERT = "ic_clock_red.png";
        #endregion

        #region toolbar
        public static readonly string IC_ORDER_TITLE_TOOLBAR = "ic_order_title_toolbar.png";
        #endregion

        #region tutorial
        public static readonly string TUTORIAL_COUNTER_ICON = "tutorial_counter_icon.png";
        public static readonly string TUTORIAL_PIN_NUMBER = "tutorial_pin_number.png";
        public static readonly string TUTORIAL_CHECK_ORDER = "tutorial_check_order.png";
        #endregion

        #region arrows
        public static readonly string IC_ARROW_RIGHT = "resource://Garzon.Core.Resources.Images.ic_next.svg" + ASSEMBLY;
        #endregion

        #region general
        public static readonly string IC_CLOSE = "resource://Garzon.Core.Resources.Images.ic_close.svg" + ASSEMBLY;
        #endregion

        #region place modes
        public static readonly string IC_RESTO = "resource://Garzon.Core.Resources.Images.ic_resto.svg" + ASSEMBLY;
        public static readonly string IC_DELIVERY = "resource://Garzon.Core.Resources.Images.ic_delivery.svg" + ASSEMBLY;
        public static readonly string IC_TAKE_AWAY = "resource://Garzon.Core.Resources.Images.ic_take_away.svg" + ASSEMBLY;

        public static readonly string IC_RESTO_PRIMARY_DARK = "resource://Garzon.Core.Resources.Images.ic_resto_primary_dark.svg" + ASSEMBLY;
        public static readonly string IC_DELIVERY_PRIMARY_DARK = "resource://Garzon.Core.Resources.Images.ic_delivery_primary_dark.svg" + ASSEMBLY;
        public static readonly string IC_TAKE_AWAY_PRIMARY_DARK = "resource://Garzon.Core.Resources.Images.ic_take_away_primary_dark.svg" + ASSEMBLY;
        public static readonly string IC_TAKE_AWAY_EXIST_PRIMARY_DARK = "resource://Garzon.Core.Resources.Images.ic_take_away_exist_primary_dark.svg" + ASSEMBLY;
        #endregion



    }
}
