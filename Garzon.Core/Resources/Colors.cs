﻿using System;
namespace Garzon.Core.Resources
{
    public abstract class Colors
    {
        public static readonly string BUTTON_ICON_COLOR = "#FFFFFF";

        #region states
        public static readonly string ORDER_DETAIL_STATUS_OBSERVED_HEX = "#b71c1c";
        public static readonly string ORDER_DETAIL_STATUS_OBSERVED_INCOUNTER_HEX = "#9c27b0";
        public static readonly string ORDER_DETAIL_STATUS_OBSERVED_PENDING_HEX = "#283593";
        #endregion
    }
}
