﻿using System;
using System.Threading.Tasks;

namespace Garzon.Core.EventBus
{
    public interface IEventBus
    {
        /// <summary>
        /// Publish the specified eventToSend.
        /// </summary>
        /// <returns>The publish.</returns>
        /// <param name="eventToSend">Event to send.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task Publish<T>(T eventToSend) where T : Event;
        /// <summary>
        /// Subscribe the specified subscriber.
        /// </summary>
        /// <returns>The subscribe.</returns>
        /// <param name="subscriber">Subscriber.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<IEventToken> Subscribe<T>(Action<T> subscriber) where T : Event;
        /// <summary>
        /// Subscribes the on main thread.
        /// </summary>
        /// <returns>The on main thread.</returns>
        /// <param name="subscriber">Subscriber.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<IEventToken> SubscribeOnMainThread<T>(Action<T> subscriber) where T : Event;
        /// <summary>
        /// Subscribes the on thread pool thread.
        /// </summary>
        /// <returns>The on thread pool thread.</returns>
        /// <param name="subscriber">Subscriber.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<IEventToken> SubscribeOnThreadPoolThread<T>(Action<T> subscriber) where T : Event;

        /// <summary>
        /// Unsubscribe the specified eventToken.
        /// </summary>
        /// <returns>The unsubscribe.</returns>
        /// <param name="eventToken">Event token.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        void Unsubscribe<T>(IEventToken eventToken) where T : Event;
    }
}
