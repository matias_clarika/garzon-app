﻿
namespace Garzon.Core.EventBus.Auth
{
    public class AuthNotificationMessage : Event
    {
        readonly string _registrationToken;

        public AuthNotificationMessage(object sender, string registrationToken) : base(sender)
        {
            _registrationToken = registrationToken;
        }

        #region builder
        public static AuthNotificationMessage Builder(object sender, string registrationToken)
        {
            return new AuthNotificationMessage(sender, registrationToken);
        }
        #endregion
        #region fields
        public string RegistrationToken
        {
            get=>_registrationToken;
        }
        #endregion
    }
}
