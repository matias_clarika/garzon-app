﻿using Garzon.Core.EventBus;
using Garzon.DataObjects;

namespace Garzon.Core.EventBus.Auth
{
    public class UserSessionEvent : Event
    {
        private UserSessionEvent(object sender, User user = null) : base(sender)
        {
            User = user;
        }

        #region builder
        public static UserSessionEvent Builder(object sender){
            return new UserSessionEvent(sender);
        }

        public UserSessionEvent WithUser(User user){
            User = user;
            return this;
        }
        #endregion
        #region fields
        public User User
        {
            get;
            set;
        }
        #endregion
    }
}
