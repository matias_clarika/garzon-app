﻿using System;
using MvvmCross.Plugins.Messenger;

namespace Garzon.Core.EventBus
{
    public interface IEventToken
    {
        void Dispose();

        MvxSubscriptionToken GetToken();

    }
}
