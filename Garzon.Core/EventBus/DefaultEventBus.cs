﻿using System;
using System.Threading.Tasks;
using MvvmCross.Plugins.Messenger;

namespace Garzon.Core.EventBus
{
    public class DefaultEventBus : IEventBus
    {
        #region dependency
        private readonly IMvxMessenger _messenger;
        #endregion
        public DefaultEventBus(IMvxMessenger messenger)
        {
            _messenger = messenger;
        }

        #region Implementation of IEventBus
        public async Task Publish<T>(T eventToSend) where T : Event
        {
            _messenger.Publish(eventToSend);
        }

        public async Task<IEventToken> Subscribe<T>(Action<T> subscriber) where T : Event
        {
            var token = _messenger.Subscribe(subscriber);
            return EventToken.Builder().WithToken(token);
        }

        public async Task<IEventToken> SubscribeOnMainThread<T>(Action<T> subscriber) where T : Event
        {
            var token = _messenger.SubscribeOnMainThread(subscriber);
            return EventToken.Builder().WithToken(token);
        }

        public async Task<IEventToken> SubscribeOnThreadPoolThread<T>(Action<T> subscriber) where T : Event
        {
            var token = _messenger.SubscribeOnThreadPoolThread(subscriber);
            return EventToken.Builder().WithToken(token);
        }

        public void Unsubscribe<T>(IEventToken eventToken) where T : Event
        {
            if (eventToken != null)
            {
                var token = eventToken.GetToken();
                if (token != null)
                {
                    _messenger.Unsubscribe<T>(token);
                }
            }

        }

        #endregion
    }
}
