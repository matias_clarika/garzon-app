﻿using MvvmCross.Plugins.Messenger;

namespace Garzon.Core.EventBus
{
    public class Event : MvxMessage
    {
        public Event(object sender) : base(sender)
        {
        }
    }
}
