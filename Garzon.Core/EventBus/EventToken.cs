﻿using System;
using MvvmCross.Plugins.Messenger;

namespace Garzon.Core.EventBus
{
    public class EventToken : IEventToken
    {
        private MvxSubscriptionToken _mvxSubscriptionToken;

        private EventToken()
        {

        }

        #region builder
        public static EventToken Builder()
        {
            return new EventToken();
        }

        public EventToken WithToken(MvxSubscriptionToken token)
        {
            _mvxSubscriptionToken = token;
            return this;
        }
        #endregion
        #region Implementation of IEventToken
        public void Dispose()
        {
            if (_mvxSubscriptionToken != null)
            {
                _mvxSubscriptionToken.Dispose();
            }
        }

        public MvxSubscriptionToken GetToken()
        {
            return _mvxSubscriptionToken;
        }
        #endregion
    }
}
