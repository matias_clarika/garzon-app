﻿
using System.Collections.Generic;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;
using Newtonsoft.Json;

namespace Garzon.Core.EventBus.Auth
{
    public class OrderEvent : Event
    {
        static readonly JsonSerializerSettings _serializerSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            ContractResolver = new UnderscorePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };

        public OrderNotificationAction Action
        {
            get;
            set;
        }

        public string Data
        {
            get;
            set;
        }

        private OrderEvent(object sender,
                          OrderNotificationAction action,
                          string data) : base(sender)
        {
            Action = action;
            Data = data;
        }
        /// <summary>
        /// Builder the specified sender and dictionary.
        /// </summary>
        /// <returns>The builder.</returns>
        /// <param name="sender">Sender.</param>
        /// <param name="dictionary">Dictionary.</param>
        public static OrderEvent Builder(object sender,
                                         Dictionary<string, string> dictionary)
        {
            string data = "";
            OrderNotificationAction action = 0;
            if (dictionary.ContainsKey("data"))
            {
                data = dictionary["data"];
            }
            if (dictionary.ContainsKey("action"))
            {
                action = (OrderNotificationAction)int.Parse(dictionary["action"]);
            }
            return new OrderEvent(sender, action, data);
        }
        /// <summary>
        /// Builder the specified json.
        /// </summary>
        /// <returns>The builder.</returns>
        /// <param name="json">Json.</param>
        public static OrderEvent Builder(string json)
        {
            return JsonConvert.DeserializeObject<OrderEvent>(json, _serializerSettings);
        }

        public OrderPayCounter ToOrderPayCounter()
        {
            try
            {
                return JsonConvert.DeserializeObject<OrderPayCounter>(this.Data, _serializerSettings);
            }
            catch
            {
                return new OrderPayCounter();
            }

        }

        public WaiterCounter ToWaiterCounter()
        {
            try
            {
                return JsonConvert.DeserializeObject<WaiterCounter>(this.Data, _serializerSettings);
            }
            catch
            {
                return new WaiterCounter();
            }
        }

        public OrderCounter ToOrderCounter()
        {
            try
            {
                return JsonConvert.DeserializeObject<OrderCounter>(this.Data, _serializerSettings);
            }
            catch
            {
                return new OrderCounter();
            }
        }

        #region override
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
        #endregion
    }
}
