﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace Garzon.Core.Views.Behaviours
{
    public interface IDinamicBehaviourView
    {
        /// <summary>
        /// Taps the view.
        /// </summary>
        /// <returns>The view.</returns>
        Task TapView();
    }
}
