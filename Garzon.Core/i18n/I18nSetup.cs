﻿using System;
using System.Reflection;
using I18NPortable;

namespace Garzon.Core.i18n
{
    public class I18nSetup
    {
        private I18nSetup()
        {
            var currentAssembly = GetType().GetTypeInfo().Assembly;

            I18N
                .Current
                .SetFallbackLocale("es")
                .SetResourcesFolder("i18n")
                .Init(currentAssembly);
        }

        public static I18nSetup Setup()
        {
            return new I18nSetup();
        }
    }
}



