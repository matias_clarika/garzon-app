﻿# general
Back = Volver
Quantity = Cantidad
QuantityAb = Cant
Total = Total
Accept = Aceptar
Cancel = Cancelar
Send = Enviar
Continue = Continuar
Delete = Eliminar
Wait = Esperar
Or = ó
Minutes = Minutos
Minute = Minuto
Seconds = Segundos
Products = Productos
Charged = Cargado 
SendObservation = Enviar observación
Options = Opciones
Closed = Cerrar
GenericError = Ups! Ocurrió un error. Intenta nuevamente.
Pending = Pendiente
Pendings = Pendientes
Save = Guardar
Sector = Sector
Sectors = Sectores
Linked = Vinculado
Tables = Mesas
#menu
MenuTitle = Menu

# product
ProductDetailSpecialTitle = Aclaraciones
ProductDetailSpecialComment = Por ejemplo: sin mayonesa, sin queso, etc.
ProductWithoutStock = Producto sin stock

# order
MyOrderTitle = Mi Pedido
OrderAddButton = Agregar a mi pedido
OrderUpdateButton = Actualizar en mi pedido
SuggestionQuestion = ¿Te gustaría agregar otro producto?
SendOrderToKitchen = Enviar pedido a cocina
SendToKitchenProcessMessage = Enviando orden a cocina, esperando confirmación...
SendToKitchenProcessMessageConfirm = ¡Orden confirmada!
SendToKitchenGenericError = ¡Ups! Ocurrió un error al enviar el pedido a cocina. Por favor intentá nuevamente.
AddToOrder = Agregar al pedido
MyOrder = Mi Pedido
AddQuantityToOrder = Agregar a mi pedido
EditQuantityToOrder = Editar cantidad
CheckOrder = Pedir cuenta
ResumeTitle = Resumen
CheckOrderSending = ¡Ok!
OrderPinTitle = Número de PIN
InsertPin = Ingresa el número de PIN
PinHelp = Ingresá el PIN para agregar \n tu pedido a la mesa N {0}
PinHelpWhereFind = Solicitá el PIN a los \n usuarios que ya pidieron.
PinNumberError = Pin incorrecto.
ShowMyOrder = Solo mi pedido
OrderDetailDeleteTitle = Eliminar items
OrderDetailDeleteMessage = Eliminará los items de tu pedido
OrderDetailDeleteProgress = Eliminando items...
OrderDetailSendToKitchenEmptyProductsError = Elegí un producto primero
OrderDetailWaitAnotherUsersTitle = Esperar a otro usuario para enviar a cocina
TableNumberInOrder = M {0}
PinNumber = PIN: {0}
Pin = PIN
OrderPendingConfirmMessage = Recibirás una notificación cuando tu pedido haya ingresado a la cocina. \n \n PIN: {0}.
OrderPendingConfirmTitle = ¡Tu pedido fue enviado!
OrderObservedTitle = ¡Necesitamos que revises tu pedido!
OrderObservedMessage = Uno de los items de tu pedido se encuentra sin stock, por favor elegí otra opción.
OrderDetailStatusTitle = Items sin stock
OrderDetailStatusMessage = Los items sin stock no serán incluidos en tu pedido. \n ¿Deseas continuar?
OrderInKitchenTitle = ¡Pedido confirmado!
OrderInKitchenMessage = Tu pedido será llevado a tu mesa cuando este listo
OrderClosedTitle = ¡Orden cerrada!
OrderClosedMessage = La orden fue cerrada. \n \n ¡Gracias por tu compra!
ChekingOrder = Pidiendo la cuenta...
CheckingOrder = Solicitando cuenta...

# order detail
OrderDetailValidationError = Ingresar una cantidad
OrderDetailSavingMessage = Guardando pedido...
OrderDetailSaveErrorMessage = ¡Ups! Ocurrió un error al guardar su pedido, por favor intentá nuevamente.
OrderConfirmByKitchen = ¡Orden confirmada!
WithoutStock = Sin stock
#Auth
SignIn = Ingresar
SignInHelp = Para poder confirmar tu pedido debes estar registrado.
SignInWith = Ingresar con
SignInLoading = Iniciando sesión...
ChangeAccount = Cambiar cuenta

#Table
InsertTableNumber = Ingresá el número de mesa
TableNumberIsRequired = El número de mesa es requerido
SignInContinueUserLoggedText = ¿Querés continuar con esta cuenta?

#Social
Facebook = Facebook
Google = Google

#Navigation Menu
DevelopedBy = Desarrollado por
Clarika = Clarika
MessageDefaultWelcome = ¡Bienvenido!
MessageWelcome = ¡Hola {0}!
RecoverMyOrder = Recuperar mi pedido
MyOrders = Mis pedidos
SignOut = Cerrar sesión

#Places
Places = Restaurantes
DownloadingMenu = Descargando menu...
ErrorDownloadMenu = Error descargando el menu
#Connection
ErrorConnectionToAlert = Revisá tu conexión
ErrorConnectionTitle = ¡Ups!
ErrorConnectionDescription = Revisá tu conexión \n Presiona para intentarlo nuevamente.
ErrorConnectionDeviceAfterTime = ¡Ups! Detectamos que no tienes conexión a internet. Por favor revise su dispositivo.
#Empty
EmptyTitle = ¡Ups!
EmptyDescription = ¡Nada por aquí!

#Waiter
Waiter = Mozo
WhatDoYouNeedQuestion = ¿Qué necesitas?
CallTheWaiter = Llamar al mozo
WaiterCallOptionsNotFound = El restaurante no tiene configurada esta opción
WaiterCallOptionsNotSelected = Por favor seleccioná una opción
CallWaiterGenericError = ¡Ups! Ocurrió un error al intentar llamar al mozo, por favor intentá nuevamente.
CallWaiterExistError = Debe esperar a que el llamado anterior sea atendido.
CallWaiterSuccess = ¡Solicitud enviada!
WaiterOnTheWay = ¡El mozo esta en camino!

PinMessageError = Número de Pin incorrecto.
HelpToOrderDailyMenu = Cree su menu {0}/{1}

#Question cancel order by place
QuestionChangePlaceTitle = ¿Desea comenzar un nuevo pedido?
QuestionChangePlaceMessage = Tiene un pedido cargado en {0}. ¿Te gustaría eliminar ese pedido y comenzar uno nuevo?
QuestionChangePlaceAccept = Aceptar
QuestionChangePlaceCancel = Cancelar

#Formats
FormatMoney = 0.##

CheckingOrderSuccess = ¡Cuenta pedida! \n Si recibiste una buena atención, ¡no te olvides de dejarle la propina al mozo!
CheckingOrderError = Ocurrió un error al pedir la cuenta, intentelo nuevamente en unos momentos.
CheckingOrderExistError = Ya se pidio la cuenta, pronto será llevada a su mesa.

AddWithSpecialComment = Agregar
AddWithoutSpecialComment = Continuar

Table = Mesa

GetOrderErrorConnection = Ups! No pudimos obtener su cuenta, revise su conexión a internet e intente nuevamente.

SendNow = Enviar
Next = Siguiente
WaitToAnotherUserToSend = Esperar
SendToKitchen = Enviar
WaitToAnotherUserToSendHelpText = ¿Esperar a otra persona y dejar tu pedido pendiente? \n \n El último en pedir podrá enviar el de toda la mesa
SendToKitchenTitle = Enviar Pedido
SendToKitchenHelpText = ¿Querés enviar el pedido ahora?
SendToKitchenSuccess = ¡Tu pedido fue enviado correctamente!
SendToKitchenSuccessPinNumber = ¡Tu pedido fue enviado correctamente! \n \n PIN: {0}
SendToKitchenWaitSuccess = ¡El pedido queda pendiente de envío!
SectorIsRequired = Debe seleccionar un sector
SectorPlaceTitle = Sector del lugar
SignInFacebookError = Se produjo un error al intentar logearse en Facebook.
SignInGoogleError = Se produjo un error al intentar logearse en Google.
TableNumberMessageError = Sector/Número de mesa incorrecto.
CheckingOrderErrorNotKitchen = No es posible pedir la cuenta, hay pedidos pendientes.

AcceptTermsCondition_1 = Al registrarme acepto los
AcceptTermsCondition_2 = Términos y Condiciones
AcceptTermsCondition_3 = y las
AcceptTermsCondition_4 = Políticas de Privacidad
AcceptTermsCondition_5 = de Garzon

Faq = FAQ

OrderDetailStatusErrorAllObserved = Por favor, revise los productos sin stock.

OrderUpdatedTitle = Tu cuenta se actualizó.
OrderUpdatedMessage = El importe a pagar es de {0}
OrderUpdatedAcceptButton = Pedir cuenta
OrderUpdatedCancelButton = Cancelar

Confirm = Confirmar
QuantityDinersTitle = Comensales
QuantityDinersRequired = Cantidad es requerida
QuantityDinersHelpText = Para que tu mesa tenga todo lo necesario, ingresa la cantidad de comensales que van a ser
InsertQuantityDiners = Cantidad de comensales

UserBlockTemporalyMessage = Usuario bloqueado temporalmente por uso incorrecto de la App. \n Tiempo restante: {0}
UserBlockPermanentMessage = Usuario bloqueado permanentemente por uso incorrecto de la App. \n Contactarse con el equipo de Garzon
UserLoginInvalidAccount = Usuario no valido
UserBlockNotification = Su usuario fue bloqueado por mal uso de la App

#counter
ErrorWhenLoadOrderPayCounter = Ocurrió un error al actualizar los pedidos de cuenta. \n \n Revise su conexión a internet.
ClosedOrder = Cerrar cuenta
ActiveTables = Mesas activas
TableNumber = Número de mesa
SelectSector = Seleccione el sector
ChangeTable = Cambiar mesa
BlockUsers = Bloquear usuarios
User = Usuario
Password = Contraseña
UserAndPasswordIsRequired = Usuario y contraseña son requeridos
UserOrPasswordIncorrect = Usuario y/o contraseña incorrecto.

#tutorial
CounterTutorialText = Si necesitas llamar al mozo \n o pedir adicionales
PinNumberTutorialText = Por si alguien quiere \n sumarse a la mesa
CheckOrderTutorialText = Ver y pedir \n la cuenta
TutorialButtonText = ENTENDIDO

CheckingOrderErrorPendingRequestTitle = Pedidos pendientes
CheckingOrderErrorPendingRequestMessage = Su mesa tiene pedidos pendientes, desea pedir la cuenta igualmente?
CheckingOrderErrorPendingRequestYes = Pedir igualmente
CheckingOrderErrorPendingRequestNo = Pedir luego

Additional = Adicional
CheckVersionAlertTitle = Existe una nueva versión 
CheckVersionAlertMessage = Existe una nueva versión obligatoria, actualiza Garzon y disfruta de todas sus novedades
CheckVersionAlertUpdateButton = Actualizar
CheckVersionAlertOkButton = Aceptar

SendToKitchenInvalidProductsError = Los siguientes productos no se encuentran disponibles en este horario: \n {0}
ProductListEmptyError = Ups! No existen productos disponibles en este horario.

GroupDescriptionHelpText = Seleccionar
SendLaterHelpText = Elegilo ahora y pedilo despues de comer
ProductGroupHelpText = Elige una opción de {0}
CommentIsEmptyError = Debe ingresar algun comentario
CompoundProductIncompletGroupError = Debe completar su {0} primero
CompoundPendingFixObservation = Por favor cambie el producto sin stock primero.
ProductEqualErrorSelectedSendLater = Solo puede seleccionar el primer producto en este {0}

Permission = Permiso {0}
PermissionQuestion = {0} es requerido por la aplicación para una mejor experiencia.
Settings = Configurar
MaybeLater = Quizas luego

FirstUnit = Primera
SecondUnit = Segunda

ProductMultipleSelectionSubtitle = ¿Qué {0} te gustaría agregar?
GroupOptionalDescriptionHelpText = Opcional
ProductGroupRequiredSelected = Por favor seleccione el producto que desea agregar

GettingTheSectorAvailables = Cargando...
SetupSectors = Configurando sectores...
SectorsHelpTextForMainCounters = El mostrador principal no puede desvincularse de sectores.
ChooseSectorSuccessMessage = Los sectores se configuraron correctamente.
QuestionForceChooseSectorTitle = Sectores
QuestionForceShooseSectorMessage = Existen sectores seleccionados que estan vinculados a otro mostrador, ¿Quiere forzar su vinculación?
CounterWithoutSectorMessage = El mostrador no posee sectores vinculados debe configurar al menos un sector para recibir pedidos.
OrderCounterWasProcessed = La orden ya fue procesada por otro mostrador
SignOutTitle = Cerrar sesión
SignOutMessage = ¿Está seguro que desea cerrar sesión?

SelectPlaceModeTitle = ¿De qué forma querés pedir?
PlaceModeRestoLabel = Consumir en el local
PlaceModeDeliveryLabel = Delivery
PlaceModeTakeAwayLabel = Retirar en el local
PlaceModeTakeAwaySubtitle = Solo pago online
PlaceModeTakeAwayExistOrderLabel = Tengo un número de pedido
PlaceModeTakeAwayExistOrderSubtitle = Ingresar número de seguimiento
SendToKitchenTakeAwaySuccessMessage = La orden se envio correctamente.

Orders = Pedidos
TakeAwayTitle = Take Away
Order = Pedido
OrderNumber = NUMERO DE PEDIDO
TimeOrderToDelivery = TIEMPO DE DEMORA APROXIMADO
TimeDelayIsRequired = Debe seleccionar un tiempo aproximado.
ToWithdraw = PARA RETIRAR
Delivered = ENTREGADO
OrderReadyForWithdrawMessage = ¡Su pedido Nº {0} está listo para retirar!
OrderTakeAwayDeliveredMessage = ¡Su pedido Nº {0} fue entregado!
OrderTakeAwayConfirmedMessage = ¡Su pedido Nº {0} en {1} se está preparando. Tiempo estimado de entrega {2} min. Te avisaremos cuando este listo!
OrderTakeAwayObservedMessage = Su pedido Nº {0} fue rechazado.\n Producto/s sin stock:\n {1}
InsertOrderNumber = Ingresa el número de pedido
TakeAwayExistOrderNumberIsRequiredMessage = Debe ingresar el número de pedido
TakeAwayExistOrderNumberLengthErrorMessage = El número de pedido debe contener 3 digitos.
TakeAwayExistOrderNumberInvalidErrorMessage = El número de pedido no es valido.
TakeAwayExistOrderSuccessMessage = ¡El pedido se vinculo correctamente, le avisaremos cuando este listo!
TakeAwayExistOrderGenericErrorMessage = Ups! Al parecer no es posible vincularse a un pedido con el número ingresado.
PayAndSend = Pagar y Enviar
OrderNumberToFormat = Pedido Nº {0}
SeeDetail = Ver Detalle
MyOrderHistory = Mis Pedidos
OrderDetailTakeAwayWithoutDetailMessage = No se puede acceder al detalle de la orden, debido a que el pedido no se realizo con Garzon.
MyOrderHistoryGenericErrorMessage = Ups! Al parecer no pudimos obtener su historial de pedidos, por favor intente de nuevo más tarde.

OrderInRestoPendingToConfirmationLabelText = Pedido recibido
OrderInRestoInKitchenLabelText = Pedido confirmado
OrderInRestoObservedLabelText = Observado
OrderInRestoClosedLabelText = Finalizado

OrderTakeAwayPendingToConfirmationLabelText = Pedido recibido
OrderTakeAwayInKitchenLabelText = En preparación
OrderTakeAwayObservedLabelText = Rechazado
OrderTakeAwayReadyToWithdrawLabelText = Listo para retirar
OrderTakeAwayClosedLabelText = Entregado

OrderListEmptyText = Aún no existen mesas activas
TakeAwayListEmptyText = Aún no existen pedidos por entregar

TakeAwayListEmptyTextSecondaryCounter = Los Take Away son enviados al mostrador principal

TakeAwayObservedButtonText = Rechazar pedido

PaymentErrorGenericMessage = Ups! Al parecer ocurrio un error al intentar realizar el pago. Por favor verifique que los datos de su tarjeta son correctos.
PaymentRejectedErrorMessage = Ups! El cobro fue rechazado, por favor verifique los datos de la tarjeta.
PaymentInvalidCardErrorMessage = Ups! Al parecer los datos de la tarjeta no son validos.
PaymentOrderRejectedAndCloseReasonMessage = Causa: El pago no se pudo realizar.
OrderCounterPaymentRejectedWhenConfirmOrderErrorMessage = La orden no se pudo confirmar debido a que el pago del usuario fue rechazado. La misma se cerrara automaticamente y se le notificara al usuario.
PaymentReintegrateMessage = Su pago fue reintegrado.
PaymentTitle = Tarjetas 
ChangeOrder = Cambiar pedido
PaymentSetup = Configurar Mercado Pago
SetupPaymentAccountErrorMessage = Necesitamos que cierre sesión e inicie nuevamente para poder configurar los datos de mercado pago.
SetupPaymentAccountSuccessMessage = La cuenta se vinculo correctamente. Ya puede recibir pagos de manera online.
Subtotal = Subtotal
DiscountWithPercent = Descuento Garzon (10%)
TipHelperMessage = No te olvides de dejar tu propina.
OrderTotalAmountRejectedMessage = El total fue reintegrado a su tarjeta.
