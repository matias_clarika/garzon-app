﻿using System;
using Garzon.DataObjects;

namespace Garzon.Core.Factory.Implementation
{
    public class OrderDetailFactory : IOrderDetailFactory
    {
        #region IOrderDetailFactory
        public OrderDetail Create(Product product, int quantity)
        {
            var orderDetail = new OrderDetail { Product = product, Quantity = quantity };
            if (product.MenuSectionId.HasValue)
            {
                orderDetail.MenuSectionId = (int)product.MenuSectionId.Value;
            }
            orderDetail.SetPending();
            return orderDetail;
        }
        #endregion
    }
}
