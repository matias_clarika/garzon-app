﻿using System;
using Garzon.DataObjects;

namespace Garzon.Core.Factory
{
    public interface IOrderDetailFactory
    {
        /// <summary>
        /// Create the specified product and quantity.
        /// </summary>
        /// <returns>The create.</returns>
        /// <param name="product">Product.</param>
        /// <param name="quantity">Quantity.</param>
        OrderDetail Create(Product product, int quantity);
    }
}
