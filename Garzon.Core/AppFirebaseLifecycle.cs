﻿using System;
namespace Garzon.Core
{
    public class AppFirebaseLifecycle : IAppLifecycle
    {
        private bool _foreground = true;

        public bool Foreground
        {
            get { return _foreground; }
            set { _foreground = value; }
        }

        private bool _existInstance = false;
        public bool ExistInstance { get => _existInstance; set { _existInstance = value; }}
    }
}
