﻿using System;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;

namespace Garzon.Core.ViewModels
{
    public class OverrideAttributeViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public OverrideAttributeViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;

            CloseCommand = new MvxAsyncCommand(async () => await _navigationService.Close(this));
        }

        public IMvxAsyncCommand ShowTabsCommand { get; private set; }

        public IMvxAsyncCommand CloseCommand { get; private set; }

        public IMvxAsyncCommand ShowSecondChildCommand { get; private set; }
    }
}
