﻿namespace Garzon.Core.ViewModels
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using I18NPortable;
    using MvvmCross.Core.ViewModels;

    public abstract class BaseViewModel : MvxViewModel
    {
        #region dependency
        protected readonly AppDefaultConfig appConfig;
        protected readonly ILogger logger;
        protected readonly IAnalytics analytics;
        protected readonly IDialogs dialogs;
        #endregion

        #region fields to binding
        public II18N Strings => I18N.Current;
        #endregion

        protected BaseViewModel(ILogger logger = null,
                             IAnalytics analytics = null,
                             AppDefaultConfig appConfig = null,
                             IDialogs dialogs = null)
        {
            //DI
            this.logger = logger;
            this.analytics = analytics;
            this.appConfig = appConfig;
            this.dialogs = dialogs;
        }

        public TaskCompletionSource<object> CloseCompletionSource { get; set; }

        #region lifecycle
        public override void ViewAppeared()
        {
            base.ViewAppeared();
            this.analytics.SendView(GetViewName(), GetViewInformation());
        }

        protected abstract Dictionary<string, string> GetViewInformation();
        protected abstract string GetViewName();

        public override void ViewDestroy(bool viewFinishing = true)
        {
            if (viewFinishing && CloseCompletionSource != null && !CloseCompletionSource.Task.IsCompleted && !CloseCompletionSource.Task.IsFaulted)
                CloseCompletionSource?.TrySetCanceled();

            base.ViewDestroy(viewFinishing);
        }
        #endregion

    }
}
