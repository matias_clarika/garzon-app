﻿using System;
using System.Threading.Tasks;
using Garzon.Core.ViewModels;

namespace Garzon.Core
{
    public interface ICoreApp
    {
        Task RegisterFirstPage<TViewModel>() where TViewModel : BaseViewModel;
    }
}
