﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace Garzon.Core.Views.Component
{
    public static class MultiSelectListView
    {
        public static readonly BindableProperty IsMultiSelectProperty =
            BindableProperty.CreateAttached("IsMultiSelect", typeof(bool), typeof(Xamarin.Forms.ListView), false, propertyChanged: OnIsMultiSelectChanged);

        public static bool GetIsMultiSelect(BindableObject view) => (bool)view.GetValue(IsMultiSelectProperty);

        public static void SetIsMultiSelect(BindableObject view, bool value) => view.SetValue(IsMultiSelectProperty, value);

        public static readonly BindableProperty ForceSingleSelectProperty =
            BindableProperty.CreateAttached("ForceSingleSelect", typeof(bool), typeof(Xamarin.Forms.ListView), false, propertyChanged: OnForceSingleSelectChanged);

        public static bool GetIsForceSingleSelect(BindableObject view) => (bool)view.GetValue(ForceSingleSelectProperty);

        public static void SetForceSingleSelect(BindableObject view, bool value) => view.SetValue(ForceSingleSelectProperty, value);


        public static readonly BindableProperty IsUncheckedEnabledProperty =
            BindableProperty.CreateAttached("IsUncheckedEnabled", typeof(bool), typeof(Xamarin.Forms.ListView), true, propertyChanged: OnIsUncheckedEnabledChanged);

        public static bool GetIsUncheckedEnabled(BindableObject view) => (bool)view.GetValue(IsUncheckedEnabledProperty);

        public static void SetIsUncheckedEnabled(BindableObject view, bool value) => view.SetValue(IsUncheckedEnabledProperty, value);

        private static void OnIsMultiSelectChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var listView = bindable as Xamarin.Forms.ListView;
            if (listView != null)
            {
                // always remove event
                listView.ItemSelected -= OnItemSelected;

                // add the event if true
                if (true.Equals(newValue))
                {
                    listView.ItemSelected += OnItemSelected;
                }
            }
        }

        private static void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var listview = ((Xamarin.Forms.ListView)sender);
            var currentSelectedItem = e.SelectedItem as SelectableItem;
            ForceSingleSelection(listview, currentSelectedItem);

            if (currentSelectedItem != null && (GetIsUncheckedEnabled(listview) || !currentSelectedItem.IsSelected || currentSelectedItem.IsUnChecked))
            {
                currentSelectedItem.IsUnChecked = true;
                // toggle the selection property
                currentSelectedItem.IsSelected = !currentSelectedItem.IsSelected;
            }

            // deselect the item
            listview.SelectedItem = null;
        }

        private static void ForceSingleSelection(Xamarin.Forms.ListView listview, SelectableItem currentSelectedItem)
        {
            if (currentSelectedItem != null && GetIsForceSingleSelect(listview))
            {
                var source = listview.ItemsSource;
                if (source != null)
                {
                    foreach (var item in source)
                    {
                        var selectedItem = item as SelectableItem;
                        if (selectedItem != null && selectedItem.Data != currentSelectedItem.Data)
                        {
                            selectedItem.IsSelected = false;
                        }
                    }
                }
            }
        }

        private static void OnForceSingleSelectChanged(BindableObject bindable, object oldValue, object newValue)
        {

        }

        private static void OnIsUncheckedEnabledChanged(BindableObject bindable, object oldValue, object newValue)
        {

        }
    }

}
