﻿using System;
using Xamarin.Forms;

namespace Garzon.Core.Views.Component
{
    [ContentProperty(nameof(DataView))]
    public class SelectableViewCellSecondary : SelectableViewCell
    {
        private readonly StackLayout contentView;

        public SelectableViewCellSecondary()
        {
            root = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand
            };

            content = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
            };

            contentView = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            contentChecked = new ContentChecked
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start,
            };

            content.Children.Add(contentView);
            content.Children.Add(contentChecked);

            root.Children.Add(content);

            var separator = new BoxView
            {
                VerticalOptions = LayoutOptions.End,
                HeightRequest = 1,
                BackgroundColor = Color.FromHex("#EDEDED")
            };

            root.Children.Add(separator);
            View = root;

            AddChecked();

            DefaultLabelView();
        }

        public View DataView
        {
            get { return dataView; }
            set
            {
                // jump out if the value is the same or something happened to our layout
                if (dataView == value || View != root)
                    return;

                OnPropertyChanging();

                // remove the old view
                if (dataView != null)
                {
                    dataView.RemoveBinding(BindingContextProperty);
                    contentView.Children.Remove(dataView);
                }

                dataView = value;

                // add the new view
                if (dataView != null)
                {
                    dataView.SetBinding(BindingContextProperty, nameof(SelectableItem.Data));
                    contentView.Children.Add(dataView);
                }

                OnPropertyChanged();
            }
        }

    }
}
