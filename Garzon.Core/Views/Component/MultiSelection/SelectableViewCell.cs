﻿using System;
using Xamarin.Forms;

namespace Garzon.Core.Views.Component
{
    [ContentProperty(nameof(DataView))]
    public class SelectableViewCell : ViewCell
    {
        protected StackLayout root;
        protected StackLayout content;
        protected View dataView;
        protected View checkView;
        protected View unCheckView;
        protected ContentChecked contentChecked;

        public SelectableViewCell()
        {
            root = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand
            };

            content = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
            };

            contentChecked = new ContentChecked
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
            };

            content.Children.Add(contentChecked);

            root.Children.Add(content);

            var separator = new BoxView
            {
                VerticalOptions = LayoutOptions.End,
                HeightRequest = 1,
                BackgroundColor = Color.FromHex("#EDEDED")
            };

            root.Children.Add(separator);
            View = root;

            AddChecked();

            DefaultLabelView();
        }

        protected void AddChecked()
        {
            DefaultCheckView();
            DefaultUnCheckView();
        }

        protected void DefaultLabelView()
        {
            var text = new Label();
            text.SetBinding(Label.TextProperty, ".");
            DataView = text;
        }

        protected void DefaultUnCheckView()
        {
            var uncheck = new BoxView
            {
                Color = Color.Cornsilk,
                WidthRequest = 12,
                HeightRequest = 12,
            };
            UnCheckView = uncheck;
        }

        protected void DefaultCheckView()
        {
            var check = new BoxView
            {
                Color = Color.CornflowerBlue,
                WidthRequest = 12,
                HeightRequest = 12,
            };
            CheckView = check;
        }

        public View CheckView
        {
            get { return checkView; }
            set
            {
                // jump out if the value is the same or something happened to our layout
                if (checkView == value || View != root)
                    return;

                OnPropertyChanging();

                // remove the old binding
                if (checkView != null)
                {
                    checkView.RemoveBinding(VisualElement.IsVisibleProperty);
                    contentChecked.Children.Remove(checkView);
                }

                checkView = value;

                // add the new binding
                if (checkView != null)
                {
                    checkView.SetBinding(VisualElement.IsVisibleProperty, nameof(SelectableItem.IsSelected));
                    contentChecked.Children.Add(checkView);
                }

                OnPropertyChanged();
            }
        }

        public View UnCheckView
        {
            get { return unCheckView; }
            set
            {
                // jump out if the value is the same or something happened to our layout
                if (unCheckView == value || View != root)
                    return;

                OnPropertyChanging();

                // remove the old binding
                if (unCheckView != null)
                {
                    unCheckView.RemoveBinding(VisualElement.IsVisibleProperty);
                    contentChecked.Children.Remove(unCheckView);
                }

                unCheckView = value;

                // add the new binding
                if (unCheckView != null)
                {
                    unCheckView.SetBinding(VisualElement.IsVisibleProperty, nameof(SelectableItem.IsUnSelected));
                    contentChecked.Children.Add(unCheckView);
                }

                OnPropertyChanged();
            }
        }


        public View DataView
        {
            get { return dataView; }
            set
            {
                // jump out if the value is the same or something happened to our layout
                if (dataView == value || View != root)
                    return;

                OnPropertyChanging();

                // remove the old view
                if (dataView != null)
                {
                    dataView.RemoveBinding(BindingContextProperty);
                    content.Children.Remove(dataView);
                }

                dataView = value;

                // add the new view
                if (dataView != null)
                {
                    dataView.SetBinding(BindingContextProperty, nameof(SelectableItem.Data));
                    content.Children.Add(dataView);
                }

                OnPropertyChanged();
            }
        }

    }

    public class ContentChecked : StackLayout
    {
        
    }
}
