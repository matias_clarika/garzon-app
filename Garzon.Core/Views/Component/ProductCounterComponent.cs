﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Garzon.Core.Views.Component
{
    public class ProductCounterComponent : ICounterComponent
    {
        #region dependency
        readonly Label _label;

        private int _counter = 0;

        #endregion
        public ProductCounterComponent(Label label, int counterValue)
        {
            _label = label;
        }

        #region Implementation of ICounterComponent
        public double GetCounterValue()
        {
            throw new NotImplementedException();
        }

        public Task<double> OnLess()
        {
            throw new NotImplementedException();
        }

        public Task<double> OnMore()
        {
            throw new NotImplementedException();
        }

        public void SetMaxCounter(double maxCounter)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
