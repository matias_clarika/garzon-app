﻿using System;
using System.Threading.Tasks;

namespace Garzon.Core.Views.Component
{
    public interface ICounterComponent
    {
        /// <summary>
        /// Ons the less.
        /// </summary>
        /// <returns>The less.</returns>
        Task<Double> OnLess();

        /// <summary>
        /// Ons the more.
        /// </summary>
        /// <returns>The more.</returns>
        Task<Double> OnMore();

        /// <summary>
        /// Sets the max counter.
        /// </summary>
        /// <param name="maxCounter">Max counter.</param>
        void SetMaxCounter(double maxCounter);

        /// <summary>
        /// Gets the counter value.
        /// </summary>
        /// <returns>The counter value.</returns>
        Double GetCounterValue();

    }
}
