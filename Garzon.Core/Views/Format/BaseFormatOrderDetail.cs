﻿using System;
using System.Collections.Generic;
using Garzon.DataObjects;
using System.Linq;
using Garzon.BL.Currency;
using I18NPortable;

namespace Garzon.Core.Views.Format
{
    public class BaseFormatOrderDetail : IFormatOrderDetail
    {
        protected readonly ICurrentCurrency currentCurrency;
        public II18N Strings => I18N.Current;

        public BaseFormatOrderDetail(ICurrentCurrency currentCurrency)
        {
            this.currentCurrency = currentCurrency;
        }

        public List<OrderDetail> FormatToList(List<OrderDetail> orderDetails)
        {
            if (orderDetails != null)
            {
                var parentList = orderDetails.Where(filter => !filter.ParentId.HasValue);
                FormatOrderDetailWithChildrens(orderDetails, parentList);
                return parentList.ToList();
            }
            else
            {
                return new List<OrderDetail>();
            }
        }

        /// <summary>
        /// Finds the children to create comment.
        /// </summary>
        /// <param name="orderDetails">Order details.</param>
        /// <param name="parentList">Parent list.</param>
        protected virtual void FormatOrderDetailWithChildrens(List<OrderDetail> orderDetails, IEnumerable<OrderDetail> parentList)
        {
            foreach (var parentDetail in parentList)
            {
                var childrenList = orderDetails.Where(filter => filter.ParentId == parentDetail.Id);
                if (childrenList.Any())
                {
                    parentDetail.Details = childrenList.ToList();
                    if (parentDetail.Details.Where((arg) => arg.IsPendingRequest).Count() == parentDetail.Details.Count())
                    {
                        parentDetail.SetPendingRequest();
                    }
                }

                parentDetail.CalculateAmount();
                if (parentDetail.Details != null && parentDetail.Details.Any())
                {
                    CreateCommentToParent(parentDetail);
                    var comments = parentDetail.Details
                                               .Select((arg) => arg.Comment)
                                               .Where((arg) => !string.IsNullOrEmpty(arg))
                                               .ToList();
                    
                    if (comments.Any())
                    {
                        parentDetail.Comment = string.Join(", ", comments);
                    }
                    else if (parentDetail.Details.Where((arg) => arg.IsSendLater()).Count() == parentDetail.Details.Count())
                    {
                        parentDetail.Comment = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Creates the comment to parent.
        /// </summary>
        /// <param name="parentDetail">Parent detail.</param>
        protected virtual void CreateCommentToParent(OrderDetail parentDetail)
        {
            var details = parentDetail.Details;
            if (parentDetail.Details.Count > 1 && parentDetail.Product != null && parentDetail.Product.IsCompoundPromo())
            {
                details = parentDetail.Details.Where((filter) => !(filter.SendLater.HasValue && filter.SendLater.Value)).ToList();
            }
            string productDetails = GetProductDetailLabels(parentDetail, details);
            parentDetail.ChildrenDetail = productDetails;

        }

        /// <summary>
        /// Gets the product detail labels.
        /// </summary>
        /// <returns>The product detail labels.</returns>
        /// <param name="parentDetail">Parent detail.</param>
        /// <param name="details">Details.</param>
        protected virtual string GetProductDetailLabels(OrderDetail parentDetail, List<OrderDetail> details)
        {
            return String.Join("\n", details.Select(item => item.GetLabelToChildrenDetail(currentCurrency.Symbol(), Strings["Pending"], Strings["FirstUnit"], Strings["SecondUnit"], parentDetail.Product)));
        }
    }
}
