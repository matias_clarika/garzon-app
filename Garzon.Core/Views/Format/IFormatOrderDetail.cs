﻿using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.Core.Views.Format
{
    public interface IFormatOrderDetail
    {
        List<OrderDetail> FormatToList(List<OrderDetail> orderDetails);
    }
}
