﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Core.Converters
{
    public class InverseBoolValueConverter : MvxValueConverter<bool, bool>
    {
     
        #region override of MvxValueConverter
        protected override bool Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return !value;
        }
        #endregion
    }
}
