﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Core.Converters
{
    public class VisibilityHeightByDoubleValueConverter: MvxValueConverter<Double, Double>
    {
        #region override of MvxValueConverter
        protected override Double Convert(Double value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value>0)
                {
                    return System.Convert.ToDouble(parameter);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return 0;
            }

        }

        #endregion
    }

}
