﻿using System;
using System.Threading.Tasks;

namespace Garzon.Core.Managers
{
    public interface INotifyManager
    {
        /// <summary>
        /// Put the specified key and data.
        /// </summary>
        /// <returns>The put.</returns>
        /// <param name="key">Key.</param>
        /// <param name="data">Data.</param>
        Task Put(string key, string data);
        /// <summary>
        /// Get the specified key.
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="key">Key.</param>
        Task<string> Get(string key);

    }
}
