﻿namespace Garzon.Core.Managers
{
    using System;
    using System.Threading.Tasks;

    public interface IRedirectManager
    {
        /// <summary>
        /// Analyzes the redirect.
        /// </summary>
        /// <returns>The redirect.</returns>
        /// <param name="data">Data.</param>
        Task AnalyzeRedirectByData(string data);

        /// <summary>
        /// Analyzes the redirect by URL.
        /// </summary>
        /// <returns>The redirect by URL.</returns>
        /// <param name="url">URL.</param>
        /// <param name="placeId">Place identifier.</param>
        Task AnalyzeRedirectByUrl(string url, long? placeId = null);
    }
}
