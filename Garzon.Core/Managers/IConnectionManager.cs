﻿using System;
namespace Garzon.Core.Managers
{
    public interface IConnectionManager
    {
        /// <summary>
        /// Ises the online.
        /// </summary>
        /// <returns><c>true</c>, if online was ised, <c>false</c> otherwise.</returns>
        Boolean IsOnline();
    }
}
