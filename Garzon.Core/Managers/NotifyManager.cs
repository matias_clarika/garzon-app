﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local.LocalStorage;

namespace Garzon.Core.Managers
{
    public class NotifyManager : INotifyManager
    {
        readonly ISharedPreferences _sharedPreferences;

        public NotifyManager(ISharedPreferences sharedPreferences)
        {
            _sharedPreferences = sharedPreferences;
        }

        #region INotifyManager
        public async Task<string> Get(string key)
        {
            return _sharedPreferences.Get(key, "");
        }

        public async Task Put(string key, string data)
        {
            _sharedPreferences.Put(key, data);
        }
        #endregion
    }
}
