﻿using System;
namespace Garzon.Core.Managers
{
    public class DefaultConnectionManager : IConnectionManager
    {
        /// <summary>
        /// Ises the online.
        /// </summary>
        /// <returns><c>true</c>, if online was ised, <c>false</c> otherwise.</returns>
        public bool IsOnline()
        {
            return true;
        }
    }
}
