﻿using System;
namespace Garzon.Core.Managers
{
    public interface IRingtoneManager
    {
        void PlayNotification();
    }
}
