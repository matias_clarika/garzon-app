﻿using System.Collections.Generic;
using Clarika.Xamarin.Base.Helpers;
using MvvmCross.Platform.Platform;

namespace Garzon.Core.Analytics
{
    public class CrashReporting : ILogger
    {
        #region dependency
        readonly IFirebaseCrashReporting _firebaseCrashReporting;
        readonly IMvxTrace _mvxTrace;
        #endregion

        #region constructor
        public CrashReporting(IFirebaseCrashReporting firebaseCrashReporting, IMvxTrace mvxTrace)
        {
            _firebaseCrashReporting = firebaseCrashReporting;
            _mvxTrace = mvxTrace;
        }
        #endregion

        #region Implementation of ILogger
        public void Debug(string tag, string message, Dictionary<string, string> information = null)
        {
            System.Diagnostics.Debug.WriteLine($"TAG: {tag}, Message: {message}, Information: {information?.ToString()}");
            _mvxTrace.Trace(MvxTraceLevel.Diagnostic, tag, message);
        }

        public void Error(string tag, string message, Dictionary<string, string> information = null)
        {
            _mvxTrace.Trace(MvxTraceLevel.Error, tag, message);
            _firebaseCrashReporting.Report($"Error ->  TAG: {tag}, Message: {message}, Information: {information?.ToString()}");
        }

        public void Fatal(string tag, string message, Dictionary<string, string> information = null)
        {
            _mvxTrace.Trace(MvxTraceLevel.Error, tag, message);
            _firebaseCrashReporting.Report($"Fatal ->  TAG: {tag}, Message -> {message}, Information: {information?.ToString()}");
        }

        public void Info(string tag, string message, Dictionary<string, string> information = null)
        {
            _mvxTrace.Trace(MvxTraceLevel.Diagnostic, tag, $"Message: {message}, Information: {information?.ToString()}");
        }
        #endregion

    }
}
