﻿namespace Garzon.Core.Analytics
{
    using System;
    using System.Collections.Generic;
    using Garzon.DataObjects;

    public interface IAnalyticsEvents
    {
        /// <summary>
        /// Sends the click place.
        /// </summary>
        /// <param name="place">Place.</param>
        void SendClickPlace(Place place);

        /// <summary>
        /// Sends the click menu section.
        /// </summary>
        /// <param name="menuSection">Menu section.</param>
        /// <param name="access">Access.</param>
        void SendClickMenuSection(MenuSection menuSection, AnalyticsAccess.MenuSectionAccess access);

        /// <summary>
        /// Sends the click product.
        /// </summary>
        /// <param name="product">Product.</param>
        /// <param name="access">Access.</param>
        void SendClickProduct(Product product, AnalyticsAccess.ProductAccess access);

        /// <summary>
        /// Sends the click add product quantity.
        /// </summary>
        /// <param name="product">Product.</param>
        void SendClickAddProductQuantity(Product product);

        /// <summary>
        /// Sends the click remove product quantity.
        /// </summary>
        /// <param name="product">Product.</param>
        void SendClickRemoveProductQuantity(Product product);

        /// <summary>
        /// Sends the add to my order product.
        /// </summary>
        /// <param name="product">Product.</param>
        void SendAddToMyOrderProduct(Product product);

        /// <summary>
        /// Sends the remove details to my order.
        /// </summary>
        /// <param name="details">Details.</param>
        void SendRemoveDetailsToMyOrder(List<OrderDetail> details);

        /// <summary>
        /// Sends the open pin number.
        /// </summary>
        void SendOpenPinNumber();

        /// <summary>
        /// Sends the open waiter call options.
        /// </summary>
        void SendOpenWaiterCallOptions();
    }

    public abstract class AnalyticsAccess
    {
        public enum MenuSectionAccess { LIST, MY_ORDER_RECOMMENDED, RESUME_RECOMMENDED };
        public enum ProductAccess { LIST, RECOMMENDED};
    }
}
