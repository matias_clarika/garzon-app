﻿using System.Collections.Generic;
using Clarika.Xamarin.Base.Helpers;

namespace Garzon.Core.Analytics
{
    public class UseReporting : IAnalytics
    {
        #region dependency
        readonly IFirebaseAnalytics firebaseAnalytics;
        #endregion

        #region routes

        private const string OPEN_PLACE_MENU_SECTION = "place_menu_section";
        private const string OPEN_MENU_SECTION_PAGE = "page_menu_section";
        private const string OPEN_PRODUCT_DETAIL = "product_detail";
        private const string OPEN_MY_ORDER_DETAIL = "my_order_detail";
        private const string OPEN_LOGIN = "login";
        private const string OPEN_RESUME_CHECK = "login";
        private const string OPEN_PIN_NUMBER = "pin_number";
        private const string OPEN_WEB_VIEW = "web_view";
        private const string OPEN_CALL_WAITER = "call_waiter";
        #endregion

        #region constructor
        public UseReporting(IFirebaseAnalytics firebaseAnalytics)
        {
            this.firebaseAnalytics = firebaseAnalytics;
        }
        #endregion

        #region IAnalytics

        public void SendEvent(string name, Dictionary<string, string> information = null)
        {
            firebaseAnalytics.SendEvent(name, information);
        }



        public void SendView(string view, Dictionary<string, string> information = null)
        {
            firebaseAnalytics.SendView(view, information);
        }

        public void SetCurrentUser(long userId, Dictionary<string, string> attributes = null)
        {
            firebaseAnalytics.SetCurrentUser($"{userId}", attributes);
        }

        public void SendOpenPlace(Dictionary<string, string> information)
        {
            SendView(OPEN_PLACE_MENU_SECTION, information);
        }


        public void SendOpenMenuSection(Dictionary<string, string> information)
        {
            SendView(OPEN_MENU_SECTION_PAGE, information);
        }

        public void SendOpenProductDetail(Dictionary<string, string> information)
        {
            SendView(OPEN_PRODUCT_DETAIL, information);
        }

        public void SendOpenMyOrder(Dictionary<string, string> information = null)
        {
            SendView(OPEN_MY_ORDER_DETAIL, information);
        }

        public void SendOpenLogin(Dictionary<string, string> information = null)
        {
            SendView(OPEN_LOGIN, information);
        }

        public void SendOpenResumeCheck(Dictionary<string, string> information = null)
        {
            SendView(OPEN_RESUME_CHECK, information);
        }

        public void SendOpenPinNumber(Dictionary<string, string> information = null)
        {
            SendView(OPEN_PIN_NUMBER, information);
        }

        public void SendOpenWebView(string url)
        {
            Dictionary<string, string> information = new Dictionary<string, string>
            {
                { "Url", url}
            };

            SendView(OPEN_WEB_VIEW, information);
        }

        public void SendOpenCallWaiter()
        {
            SendView(OPEN_CALL_WAITER); 
        }

        #endregion
    }
}
