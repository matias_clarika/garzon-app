﻿using System;
using System.Collections.Generic;

namespace Garzon.Core.Analytics
{
    public interface IAnalytics
    {
       
        #region base
        /// <summary>
        /// Sends the view.
        /// </summary>
        /// <returns>The view.</returns>
        /// <param name="view">View.</param>
        /// <param name="information">Information.</param>
        void SendView(String view, Dictionary<string, string> information = null);

        /// <summary>
        /// Sends the event.
        /// </summary>
        /// <returns>The event.</returns>
        /// <param name="name">Name.</param>
        /// <param name="information">Information.</param>
        void SendEvent(String name, Dictionary<string, string> information = null);

        /// <summary>
        /// Sets the current user.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="attributes">Attributes.</param>
        void SetCurrentUser(long userId, Dictionary<string, string> attributes = null);
        #endregion

        #region Navigation
        /// <summary>
        /// Sends the open place.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenPlace(Dictionary<string, string> information);

        /// <summary>
        /// Sends the open menu section.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenMenuSection(Dictionary<string, string> information);

        /// <summary>
        /// Sends the open product detail.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenProductDetail(Dictionary<string, string> information);

        /// <summary>
        /// Sends the open my order.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenMyOrder(Dictionary<string, string> information = null);

        /// <summary>
        /// Sends the open login.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenLogin(Dictionary<string, string> information = null);

        /// <summary>
        /// Sends the open resume check.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenResumeCheck(Dictionary<string, string> information = null);

        /// <summary>
        /// Sends the open pin number.
        /// </summary>
        /// <param name="information">Information.</param>
        void SendOpenPinNumber(Dictionary<string, string> information = null);

        /// <summary>
        /// Sends the open web view.
        /// </summary>
        /// <param name="url">URL.</param>
        void SendOpenWebView(string url);

        /// <summary>
        /// Sends the open call waiter.
        /// </summary>
        void SendOpenCallWaiter();
        #endregion

    }
}
