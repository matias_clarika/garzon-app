﻿using System.Threading.Tasks;
using Garzon.Core.ViewModels;
using MvvmCross.Core.ViewModels;

namespace Garzon.Core.Navigation
{
    public interface INavigationCoreController
    {
        /// <summary>
        /// Opens the first page.
        /// </summary>
        /// <returns>The first page.</returns>
        /// <param name="coreApp">Core app.</param>
        Task OpenFirstPage(ICoreApp coreApp);

        /// <summary>
        /// Res the open app.
        /// </summary>
        /// <returns>The open app.</returns>
        Task ReOpenApp();

        /// <summary>
        /// Back the specified viewModel.
        /// </summary>
        /// <returns>The back.</returns>
        /// <param name="viewModel">View model.</param>
        Task Back(BaseViewModel viewModel);



        /// <summary>
        /// Back the specified viewModel and result.
        /// </summary>
        /// <returns>The back.</returns>
        /// <param name="viewModel">View model.</param>
        /// <param name="result">Result.</param>
        /// <typeparam name="TViewModel">The 1st type parameter.</typeparam>
        /// <typeparam name="TResult">The 2nd type parameter.</typeparam>
        Task Back<TViewModel, TResult>(TViewModel viewModel,
                                              TResult result) where TViewModel : IMvxViewModelResult<TResult> where TResult : class;

    }
}
