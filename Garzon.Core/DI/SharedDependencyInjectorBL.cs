﻿using Garzon.BL;
using Garzon.BL.Currency;
using Garzon.BL.Implementation;
using MvvmCross.Platform;

namespace Garzon.Core.DI
{
    public abstract partial class SharedDependencyInjector
    {
        protected virtual void InjectBL()
        {
            Mvx.LazyConstructAndRegisterSingleton<IUserBL, UserBL>();
            Mvx.LazyConstructAndRegisterSingleton<IPlaceBL, PlaceBL>();
            Mvx.LazyConstructAndRegisterSingleton<IMenuBL, MenuBL>();
            Mvx.LazyConstructAndRegisterSingleton<IMenuSectionBL, MenuSectionBL>();

            Mvx.LazyConstructAndRegisterSingleton<IProductRecommendedBL, ProductRecommendedBL>();
            Mvx.LazyConstructAndRegisterSingleton<IProductMenuSectionBL, ProductMenuSectionBL>();

            Mvx.LazyConstructAndRegisterSingleton<IOrderBL, OrderBL>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderDetailBL, OrderDetailBL>();
            Mvx.LazyConstructAndRegisterSingleton<IWaiterBL, WaiterBL>();

            //Others
            Mvx.LazyConstructAndRegisterSingleton<ICurrentCurrency, DollarCurrency>();
        }
    }
}
