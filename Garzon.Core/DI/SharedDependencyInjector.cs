﻿using System;
namespace Garzon.Core.DI
{
    public abstract partial class SharedDependencyInjector : BaseDependencyInjector
    {

        public override void Inject()
        {
            base.Inject();
            InjectDAL();
            InjectBL();
            InjectCore();
        }
    }
}
