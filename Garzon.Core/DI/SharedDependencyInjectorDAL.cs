﻿namespace Garzon.Core.DI
{
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Clarika.Xamarin.Base.Persistence.Manager;
    using Garzon.Config.DataSource;
    using Garzon.Core.Utils;
    using Garzon.DAL;
    using Garzon.DAL.DataSource.Local;
    using Garzon.DAL.DataSource.Local.Database;
    using Garzon.DAL.DataSource.Local.Database.Dao;
    using Garzon.DAL.DataSource.Local.Database.Dao.Implementation;
    using Garzon.DAL.DataSource.Local.Entity.Mapper;
    using Garzon.DAL.DataSource.Local.Implementation;
    using Garzon.DAL.DataSource.Local.LocalStorage;
    using Garzon.DAL.DataSource.Local.LocalStorage.Implementation;
    using Garzon.DAL.DataSource.Remote;
    using Garzon.DAL.DataSource.Remote.Implementation;
    using Garzon.DAL.Repository;
    using Garzon.DAL.Repository.Implementation;
    using MvvmCross.Platform;

    public abstract partial class SharedDependencyInjector
    {
        protected virtual void InjectDAL()
        {
            InjectStrategy();
            InjectDALMappers();
            InjectLocalDataSource();
            InjectRemoteDataSource();
            InjectRepository();
        }


        private void InjectStrategy()
        {
            Mvx.LazyConstructAndRegisterSingleton<IPollyStrategy, PollyStrategy>();
        }

        private void InjectDALMappers()
        {
            Mvx.LazyConstructAndRegisterSingleton<UserEntityDataMapper, UserEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<OrderEntityDataMapper, OrderEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<OrderDetailEntityDataMapper, OrderDetailEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<PlaceEntityDataMapper, PlaceEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<MenuSectionEntityDataMapper, MenuSectionEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<ProductEntityDataMapper, ProductEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<WaiterCallOptionEntityDataMapper, WaiterCallOptionEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<SectorEntityDataMapper, SectorEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<GroupEntityDataMapper, GroupEntityDataMapper>();
            Mvx.LazyConstructAndRegisterSingleton<ProductGroupEntityDataMapper, ProductGroupEntityDataMapper>();
        }

        private void InjectLocalDataSource()
        {
            InjectDatabases();
            InjectSharedPreference();

            Mvx.LazyConstructAndRegisterSingleton<IPlaceLocalDataSource, PlaceLocalDatasource>();
            Mvx.LazyConstructAndRegisterSingleton<IUserLocalDataSource, UserLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IProductLocalDataSource, ProductLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderLocalDataSource, OrderLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderDetailLocalDataSource, OrderDetailLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IWaiterLocalDataSource, WaiterLocalDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<ISectorLocalDataSource, SectorLocalDataSource>();
        }


        private void InjectDatabases()
        {
            //database instance
            Mvx.RegisterSingleton<IDatabase>(() => new GarzonDatabase(Mvx.Resolve<IFileHelper>().GetLocalFilePath("GarzonSQLite.db")));

            //Daos
            Mvx.RegisterType<IUserDao, UserDao>();
            Mvx.RegisterType<IPlaceDao, PlaceDao>();
            Mvx.RegisterType<IMenuSectionDao, MenuSectionDao>();
            Mvx.RegisterType<IProductDao, ProductDao>();
            Mvx.RegisterType<IOrderDetailDao, OrderDetailDao>();
            Mvx.RegisterType<IOrderDao, OrderDao>();
            Mvx.RegisterType<IWaiterCallOptionDao, WaiterCallOptionDao>();
            Mvx.RegisterType<ISectorDao, SectorDao>();
            Mvx.RegisterType<IGroupDao, GroupDao>();
            Mvx.RegisterType<IProductGroupDao, ProductGroupDao>();

        }


        private void InjectSharedPreference()
        {
            Mvx.RegisterSingleton(()=>SharedPreferencesSettings.AppSettings);
            Mvx.RegisterType<ISharedPreferences, SharedPreferences>();
            Mvx.RegisterType<IUserSharedPreferences, UserSharedPreferences>();
        }

        private void InjectRemoteDataSource()
        {
            Mvx.LazyConstructAndRegisterSingleton<IPlaceRemoteDataSource, PlaceRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IProductRemoteDataSource, ProductRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IUserRemoteDataSource, UserRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderRemoteDataSource, OrderRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IWaiterRemoteDataSource, WaiterRemoteDataSource>();
        }

        private void InjectRepository()
        {
            Mvx.LazyConstructAndRegisterSingleton<IDataManagerFactory, BaseDataManagerFactory>();
            //user
            Mvx.LazyConstructAndRegisterSingleton<IUserRepository, UserDataRepository>();

            Mvx.LazyConstructAndRegisterSingleton<IProductRepository, ProductDataRepository>();

            Mvx.LazyConstructAndRegisterSingleton<IOrderRepository, OrderDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderDetailRepository, OrderDetailDataRepository>();

            Mvx.LazyConstructAndRegisterSingleton<IWaiterRepository, WaiterDataRepository>();
        }
    }
}
