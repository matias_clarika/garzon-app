﻿using System;
namespace Garzon.Core.DI
{
    public interface IDependencyInjector
    {
        /// <summary>
        /// Inject this instance.
        /// </summary>
        void Inject();

        /// <summary>
        /// Registers the type.
        /// </summary>
        /// <param name="tInterface">T interface.</param>
        /// <param name="tType">T type.</param>
        void RegisterType(Type tInterface, Type tType);

        /// <summary>
        /// Registers the type.
        /// </summary>
        /// <param name="constructor">Constructor.</param>
        /// <typeparam name="TInterface">The 1st type parameter.</typeparam>
        void RegisterType<TInterface>(Func<TInterface> constructor)
            where TInterface : class;

        /// <summary>
        /// Resolve the specified serviceType.
        /// </summary>
        /// <returns>The resolve.</returns>
        /// <param name="serviceType">Service type.</param>
        object Resolve(Type serviceType);

        /// <summary>
        /// Resolve this instance.
        /// </summary>
        /// <returns>The resolve.</returns>
        /// <typeparam name="TService">The 1st type parameter.</typeparam>
        TService Resolve<TService>() where TService : class;
    }
}
