﻿using System;
using MvvmCross.Platform;

namespace Garzon.Core.DI
{
    public abstract class BaseDependencyInjector : IDependencyInjector
    {
        
        #region implementation of IDependencyInjector

        public virtual void Inject()
        {
            
        }
        public void RegisterType(Type tInterface, Type tType)
        {
            Mvx.RegisterType(tInterface, tType);
        }

        public void RegisterType<TInterface>(Func<TInterface> constructor)
            where TInterface : class
        {
            Mvx.RegisterType(constructor);
        }

        public object Resolve(Type serviceType)
        {
            return Mvx.Resolve(serviceType);
        }

        public TService Resolve<TService>() where TService : class
        {
            return Mvx.Resolve<TService>();
        }

        #endregion

    }
}
