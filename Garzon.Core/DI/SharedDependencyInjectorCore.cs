﻿using System;
using Acr.UserDialogs;
using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.Helpers;
using Clarika.Xamarin.Base.Persistence;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.Config;
using Garzon.Config.DataSource;
using Garzon.Core.Analytics;
using Garzon.Core.EventBus;
using Garzon.Core.Managers;
using Garzon.Core.Payments;
using Garzon.Core.Payments.Implementation;
using Garzon.Core.Utils;
using Garzon.Core.Views.Format;
using Garzon.DAL;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;

namespace Garzon.Core.DI
{
    public abstract partial class SharedDependencyInjector
    {
        protected virtual void InjectCore()
        {
            InjectByEnvironment();
            InjectAppConfig();
            InjectAnalytics();
            InjectNavigation();
            InjectDefaultBehaviours();
            InjectManagers();
            InjectDefaultUtils();
            InjectLibraries();
            InjectFormat();
        }

        private static void InjectAppConfig()
        {
            Mvx.ConstructAndRegisterSingleton<AppDefaultConfig, AppDefaultConfig>();
            Mvx.ConstructAndRegisterSingleton<ICacheSettings, AppDefaultConfig>();
            Mvx.ConstructAndRegisterSingleton<IAppConfig, AppDefaultConfig>();

            Mvx.ConstructAndRegisterSingleton<IOAuthSettings, OAuthSettings>();
        }

        private void InjectAnalytics()
        {
            Mvx.LazyConstructAndRegisterSingleton<IAnalytics, UseReporting>();
            Mvx.LazyConstructAndRegisterSingleton<ILogger, CrashReporting>();
        }

        private void InjectDefaultBehaviours()
        {
            //Mvx.RegisterType<IDinamicBehaviourView, DefaultEmptyViewBehaviour>();
        }

        private void InjectNavigation()
        {
            Mvx.ConstructAndRegisterSingleton<IAppLifecycle, AppFirebaseLifecycle>();
        }

        private void InjectDefaultUtils()
        {
            Mvx.LazyConstructAndRegisterSingleton<IFormatPrice, DefaultFormatPrice>();
            Mvx.LazyConstructAndRegisterSingleton<IDevice, XamarinFormsDevice>();
        }

        private void InjectManagers()
        {
            Mvx.LazyConstructAndRegisterSingleton<IConnectionManager, DefaultConnectionManager>();
        }

        private void InjectLibraries()
        {
            Mvx.RegisterSingleton<IUserDialogs>(() => UserDialogs.Instance);
            Mvx.LazyConstructAndRegisterSingleton<IDialogs, GarzonDialogs>();

            Mvx.LazyConstructAndRegisterSingleton<IMvxMessenger, MvxMessengerHub>();
            Mvx.LazyConstructAndRegisterSingleton<IEventBus, DefaultEventBus>();
        }

        private void InjectByEnvironment()
        {
            string buildEnvironment = Mvx.Resolve<IEnvironment>().BuildEnvironment;

            switch (buildEnvironment)
            {
                case EnvironmentConstants.BUILD_ENVIRONMENT_QA:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, QaServerInfo>();
                    Mvx.ConstructAndRegisterSingleton<IMercadoPagoPayment, QaMercadoPagoPayment>();
                    break;
                case EnvironmentConstants.BUILD_ENVIRONMENT_RELEASE:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, PreProdServerInfo>();
                    Mvx.ConstructAndRegisterSingleton<IMercadoPagoPayment, ProdMercadoPagoPayment>();
                    break;
                case EnvironmentConstants.BUILD_ENVIRONMENT_PROD:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, ProdServerInfo>();
                    Mvx.ConstructAndRegisterSingleton<IMercadoPagoPayment, ProdMercadoPagoPayment>();
                    break;
                default:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, DevServerInfo>();
                    Mvx.ConstructAndRegisterSingleton<IMercadoPagoPayment, DevMercadoPagoPayment>();
                    break;
            }
        }


        private void InjectFormat()
        {
            Mvx.RegisterType<IFormatOrderDetail, BaseFormatOrderDetail>();
        }
    }
}
