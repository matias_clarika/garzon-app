﻿namespace Garzon.Counter.Forms.UI.Droid.FirebaseUtils
{

    using System.Collections.Generic;
    using Android.Content;
    using Clarika.Xamarin.Base.Helpers;
    using Firebase;
    using Firebase.Analytics;
    using Garzon.Counter.Forms.UI.Droid.Utils;

    public class FirebaseAnalyticsDefault : IFirebaseAnalytics
    {
        #region dependency
        readonly FirebaseAnalytics firebaseAnalytics;
        #endregion

        #region constructor
        public FirebaseAnalyticsDefault(Context context)
        {
            FirebaseApp.InitializeApp(context);
            firebaseAnalytics = FirebaseAnalytics.GetInstance(context);
        }

        #endregion

        #region Implementation of IFirebaseAnalytics
        public void SendEvent(string eventName, Dictionary<string, string> optionalInformation = null)
        {
            this.firebaseAnalytics.LogEvent(eventName, AndroidConverter.DictionaryStringToBundle(optionalInformation));
        }

        public void SendView(string view, Dictionary<string, string> optionalInformation = null)
        {
            this.firebaseAnalytics.LogEvent($"screen_{view}", AndroidConverter.DictionaryStringToBundle(optionalInformation));
        }

        public void SetCurrentUser(string id, Dictionary<string, string> attributes = null)
        {
            this.firebaseAnalytics.SetUserId(id);
            if (attributes != null)
            {
                foreach (KeyValuePair<string, string> entry in attributes)
                {
                    this.firebaseAnalytics.SetUserProperty(entry.Key, entry.Value);
                }
            }

        }

        public void ClearCurrentUser()
        {
            this.firebaseAnalytics.SetUserId(null);
        }
        #endregion
    }
}
