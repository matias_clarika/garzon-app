﻿namespace Garzon.Counter.Forms.UI.Droid.FirebaseUtils
{
    using Android.App;
    using Firebase.Messaging;
    using Garzon.Core.EventBus;
    using Garzon.Core.EventBus.Auth;
    using Garzon.Core.Managers;
    using Garzon.Counter.Forms.UI.DI;
    using System;
    using System.Linq;

    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class GarzonFirebaseMessagingService : FirebaseMessagingService
    {
        private static readonly string TAG = typeof(GarzonFirebaseMessagingService).FullName;

        public override void OnMessageReceived(RemoteMessage message)
        {
            try
            {
                DisplayNotification(message);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        private void DisplayNotification(RemoteMessage message)
        {
            if (message.Data != null)
            {
                DependencyManager.GetInstance().Resolve<IRingtoneManager>().PlayNotification();
                var orderEvent = OrderEvent.Builder(this, message.Data.ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
                if (orderEvent.Action != OrderNotificationAction.ORDER_COUNTER_NEW_TAKEAWAY)
                {
                    DependencyManager.GetInstance()
                                         .Resolve<IEventBus>()
                                         .Publish(orderEvent);
                }

            }
        }
    }
}
