﻿using System;
using Android.Media;
using Android.OS;
using Java.Lang;

namespace Garzon.Counter.Forms.UI.Droid.Misc
{
    public class VolumeRunnable : Java.Lang.Object, IRunnable
    {
        private readonly AudioManager mAudioManager;
        private static int DELAY_UNTILL_NEXT_INCREASE = 1 * 1000;//5 seconds between each increment
        readonly Handler mHandler;

        public VolumeRunnable(){
            
        }
        public VolumeRunnable(AudioManager audioManager, Handler handler)
        {
            mAudioManager = audioManager;
            mHandler = handler;
        }

        public IntPtr Handle => (IntPtr)0;

        public void Dispose()
        {
            
        }

        public void Run()
        {
            int currentAlarmVolume = mAudioManager.GetStreamVolume(Stream.Alarm);
            if (currentAlarmVolume != mAudioManager.GetStreamMaxVolume(Stream.Alarm))
            { //if we havent reached the max
                mAudioManager.SetStreamVolume(Stream.Alarm, ++currentAlarmVolume, 0);
            }

            //mHandler.PostDelayed(this, DELAY_UNTILL_NEXT_INCREASE);
        }
    }
}
