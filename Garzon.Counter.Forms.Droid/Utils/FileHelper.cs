﻿using System;
using System.IO;
using Garzon.Core.Utils;

namespace Garzon.Counter.Forms.UI.Droid.Utils
{
    public class FileHelper : IFileHelper
    {
        #region Implementation of IFileHelper
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
        #endregion
    }
}
