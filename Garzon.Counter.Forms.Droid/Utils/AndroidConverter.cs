﻿using System;
using System.Collections.Generic;
using Android.OS;

namespace Garzon.Counter.Forms.UI.Droid.Utils
{
    public abstract class AndroidConverter
    {

        /// <summary>
        /// Dictionaries the string to bundle.
        /// </summary>
        /// <returns>The string to bundle.</returns>
        /// <param name="dictionary">Dictionary.</param>
        public static Bundle DictionaryStringToBundle(Dictionary<string, string> dictionary){
            Bundle bundle = new Bundle();

            if(dictionary!=null){
                foreach (KeyValuePair<string, string> pair in dictionary)
                {
                    bundle.PutString(pair.Key, pair.Value);
                }
            }

            return bundle;
        }
    }
}
