﻿
using System;
using System.IO;
using Android.App;
using Android.Media;
using Android.OS;
using Garzon.Core.Utils;

namespace Garzon.Counter.Forms.UI.Droid
{
    [Activity(Theme = "@android:style/Theme.Translucent.NoTitleBar",
              Label ="",
              NoHistory = true,
              LaunchMode = Android.Content.PM.LaunchMode.SingleTask)]
    public class AlarmInactivityActivity : Activity
    {
        const string TAG = "ExampleFindPhoneApp";
        const string FIELD_ALARM_ON = "alarm_on";

        private AudioManager mAudioManager;
        static int ORIGINAL_VOL;
        Android.Net.Uri mAlarmSound;
        MediaPlayer mMediaPlayer;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AlarmInactivityScreen);
            StartAlarm();
        }

        private void StartAlarm()
        {
            mAudioManager = (AudioManager)GetSystemService(AudioService);

            //set the original volume and max volume variables to the phone's current and max volume levels
            ORIGINAL_VOL = mAudioManager.GetStreamVolume(Android.Media.Stream.Alarm);

            //set the sound for the alarm to the phone's alarm tone
            mAlarmSound = RingtoneManager.GetDefaultUri(RingtoneType.Alarm);

            mMediaPlayer = new MediaPlayer();

            PlayAlarm();
        }

        private void PlayAlarm()
        {
            mAudioManager.SetStreamVolume(Android.Media.Stream.Alarm, 1, 0);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                var attribs = new AudioAttributes.Builder()
                                                 .SetFlags(AudioFlags.None)
                                                 .SetLegacyStreamType(Android.Media.Stream.Alarm)
                                                 .Build();

                mMediaPlayer.SetAudioAttributes(attribs);
            }
            else
            {
                mMediaPlayer.SetAudioStreamType(Android.Media.Stream.Alarm);
            }

            mMediaPlayer.Reset();
            try
            {
                mMediaPlayer.SetDataSource(ApplicationContext, mAlarmSound);
                mMediaPlayer.Prepare();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            mMediaPlayer.Start();

            Timer timer = Timer.StartNew(TimeSpan.FromSeconds(3), UpAudioManagerVolume);

        }

        protected void UpAudioManagerVolume(){
            int currentAlarmVolume = mAudioManager.GetStreamVolume(Android.Media.Stream.Alarm);
            if (currentAlarmVolume != mAudioManager.GetStreamMaxVolume(Android.Media.Stream.Alarm))
            { //if we havent reached the max
                mAudioManager.SetStreamVolume(Android.Media.Stream.Alarm, ++currentAlarmVolume, 0);
            }
        }

        public override void OnUserInteraction()
        {
            base.OnUserInteraction();
            Finish();
        }

        protected override void OnDestroy()
        {
            mAudioManager.SetStreamVolume((Android.Media.Stream.Alarm), ORIGINAL_VOL, 0);
            mMediaPlayer.Release();
            base.OnDestroy();
        }
    }
}
