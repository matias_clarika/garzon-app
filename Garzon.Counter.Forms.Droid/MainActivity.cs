﻿using Acr.UserDialogs;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Garzon.Core;
using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.Droid.Views;
using MvvmCross.Platform;
using FFImageLoading.Forms.Droid;
using Garzon.Core.EventBus;
using System.Threading.Tasks;
using Garzon.Counter.Forms.UI.EventBus;
using System;

namespace Garzon.Counter.Forms.UI.Droid
{
    [Activity(Label = "@string/app_name",
              Icon = "@drawable/ic_launcher",
              Theme = "@style/GarzonTheme",
              MainLauncher = true,
              ScreenOrientation = ScreenOrientation.Portrait,
              ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
              LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : MvxFormsAppCompatActivity
    {
        #region alarm
        protected PowerManager.WakeLock mWakeLock;
        private Handler mAlarmHandler;

        const int DELAY_START_ALARM = 60000;

        private bool mEnableStartActivity = false;
        #endregion

        #region events
        internal IEventBus _eventBus;

        public IEventBus EventBus
        {
            get
            {
                if (_eventBus == null)
                {
                    _eventBus = Mvx.Resolve<IEventBus>();
                }
                return _eventBus;
            }
        }

        IEventToken _alarmToken;

        private async Task Subscribe()
        {
            _alarmToken = await EventBus.SubscribeOnMainThread<AlarmEvent>(OnAlarmEvent);
        }

        private void Unsubscribe()
        {
            EventBus.Unsubscribe<AlarmEvent>(_alarmToken);
        }

        private void OnAlarmEvent(AlarmEvent obj)
        {
            if (mEnableStartActivity != obj.Enable)
            {
                mEnableStartActivity = obj.Enable;
                StartAlarm();
            }
        }
        #endregion
        #region lifecycle
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            InitializeLibraries();
            // No Splash Screen: Uncomment these lines if removing splash screen
            StartupMvx();

            InitializeForms(bundle);
            InitializeWakeLock();
        }

        private void InitializeLibraries()
        {
            CachedImageRenderer.Init(false);
            UserDialogs.Init(() => this);
        }

        private static void StartupMvx()
        {
            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();
        }

        private void InitializeWakeLock()
        {
            PowerManager pm = (PowerManager)GetSystemService(PowerService);
            this.mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim, typeof(MainActivity).FullName);
            this.mWakeLock.Acquire();
        }

        protected override async void OnStart()
        {
            base.OnStart();
            Mvx.Resolve<IAppLifecycle>().Foreground = true;
            await Subscribe();
        }

        protected override void OnResume()
        {
            base.OnResume();
            StartAlarm();
        }

        public override void OnUserInteraction()
        {
            base.OnUserInteraction();
            StartAlarm();
        }

        private void StartAlarm()
        {
            StopAlarm();
            if (mAlarmHandler == null)
            {
                mAlarmHandler = new Handler();
            }
            mAlarmHandler.PostDelayed(StartAlarmActivity, DELAY_START_ALARM);
        }

        private void StopAlarm()
        {
            if (mAlarmHandler != null)
            {
                try
                {
                    mAlarmHandler.RemoveCallbacksAndMessages(null);
                    mAlarmHandler = null;
                }
                catch (Exception)
                {

                }
            }
        }

        private void StartAlarmActivity()
        {
            if (mEnableStartActivity)
            {
                StartActivity(typeof(AlarmInactivityActivity));
            }
            else
            {
                StartAlarm();
            }
        }

        protected override void OnStop()
        {
            Mvx.Resolve<IAppLifecycle>().Foreground = false;
            StopAlarm();
            Unsubscribe();
            base.OnStop();
        }

        protected override void OnDestroy()
        {
            ReleaseWakelock();
            base.OnDestroy();
        }

        private void ReleaseWakelock()
        {
            this.mWakeLock.Release();
            this.mWakeLock = null;
        }

        public override void OnBackPressed()
        {
            //nothing
        }
        #endregion
    }
}
