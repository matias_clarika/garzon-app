﻿namespace Garzon.Counter.Forms.UI.Droid
{
    using Android.Content;
    using MvvmCross.Core.ViewModels;
    using MvvmCross.Forms.Droid.Platform;
    using MvvmCross.Forms.Platform;
    using MvvmCross.Platform.Plugins;
    using System;
    using MvvmCross.Platform;
    using Garzon.Core.Utils;
    using Garzon.Counter.Forms.UI.Droid.Utils;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Counter.Forms.UI.Droid.FirebaseUtils;
    using Garzon.Config;
    using Garzon.Core.Managers;
    using Garzon.Counter.Forms.UI.Droid.Managers;

    public class Setup : MvxFormsAndroidSetup
    {
        readonly Context _applicationContext;

        public Setup(Context applicationContext) : base(applicationContext)
        {
            _applicationContext = applicationContext;
        }

        protected override MvxFormsApplication CreateFormsApplication()
        {
            return new FormsApp();
        }

        protected override IMvxApplication CreateApp()
        {
            return new CoreApp();
        }

        protected override IMvxPluginConfiguration GetPluginConfiguration(Type plugin)
        {
            if (plugin == typeof(MvvmCross.Plugins.Json.PluginLoader))
            {
                return new MvvmCross.Plugins.Json.MvxJsonConfiguration()
                {
                    RegisterAsTextSerializer = false
                };
            }

            return null;
        }

        protected override void InitializeFirstChance()
        {
            Inject();
            base.InitializeFirstChance();
        }

        #region DI
        private void Inject()
        {
            Mvx.ConstructAndRegisterSingleton<IEnvironment, AndroidEnvironment>();
            Mvx.RegisterSingleton<IFileHelper>(new FileHelper());
            Mvx.RegisterSingleton<IFirebaseAnalytics>(() => new FirebaseAnalyticsDefault(_applicationContext));
            Mvx.RegisterSingleton<IFirebaseCrashReporting>(new FirebaseCrashlytics(_applicationContext));
            Mvx.RegisterSingleton<IRingtoneManager>(new AndroidRingtoneManager(_applicationContext));
        }
        #endregion
    }
}
