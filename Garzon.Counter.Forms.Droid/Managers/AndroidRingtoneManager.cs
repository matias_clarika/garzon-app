﻿namespace Garzon.Counter.Forms.UI.Droid.Managers
{
    using System;
    using Android.Content;
    using Android.Media;
    using Android.OS;
    using Garzon.Core.Managers;

    public class AndroidRingtoneManager : IRingtoneManager
    {
        #region dependency
        readonly Context applicationContext;
        #endregion
        public AndroidRingtoneManager(Context applicationContext)
        {
            this.applicationContext = applicationContext;
        }

        #region IRingtoneManager
        public void PlayNotification()
        {
            using (Ringtone r = RingtoneManager.GetRingtone(this.applicationContext,
                                                            RingtoneManager.GetDefaultUri(RingtoneType.Notification)))
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                {
                    var attribs = new AudioAttributes.Builder()
                                                     .SetFlags(AudioFlags.None)
                                                     .SetLegacyStreamType(Android.Media.Stream.Notification)
                                                     .Build();

                    r.AudioAttributes = attribs;
                }
                r.Play();
            }
        }
        #endregion
    }
}
