using MvvmCross.Platform.Plugins;

namespace Garzon.Counter.Forms.UI.Droid.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}