using MvvmCross.Platform.Plugins;

namespace Garzon.Counter.Forms.UI.Droid.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}