﻿using System;
using Garzon.Config;

namespace Garzon.Counter.Forms.UI.Droid
{
    public class AndroidEnvironment: IEnvironment
    {
        string _environment;

        public AndroidEnvironment()
        {
            GetEnvironment();
        }

        public string BuildEnvironment => _environment;

        private void GetEnvironment()
        {
            try
            {
                _environment = EnvironmentConstants.BUILD_ENVIRONMENT_CONFIGURE;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Write(e.Message);
            }
        }
    }
}
