﻿using System;
using System.Reflection;
using Android.Content;
using Garzon.Counter.Forms.UI.Droid.Renderers;
using Garzon.Counter.Forms.UI.Views.Component;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;

namespace Garzon.Counter.Forms.UI.Droid.Renderers
{
    public class CustomNavigationPageRenderer : NavigationPageRenderer
    {
        public CustomNavigationPageRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<NavigationPage> e)
        {
            base.OnElementChanged(e);
            var bar = (Android.Support.V7.Widget.Toolbar)typeof(NavigationPageRenderer)
            .GetField("_toolbar", BindingFlags.NonPublic | BindingFlags.Instance)
            .GetValue(this);
            bar.SetLogo(Resource.Drawable.ic_order_title_toolbar);
        }
    }
}
