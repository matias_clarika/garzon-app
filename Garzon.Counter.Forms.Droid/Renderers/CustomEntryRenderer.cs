﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Views;
using Garzon.Counter.Forms.UI.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(CustomEntryRenderer))]
namespace Garzon.Counter.Forms.UI.Droid.Renderers
{
    //HACK: https://github.com/xamarin/Xamarin.Forms/issues/2638
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (this.Element != null && this.Element.HorizontalTextAlignment == Xamarin.Forms.TextAlignment.Center)
            {
                Control.Gravity = Android.Views.GravityFlags.Center;
            }

        }


    }
}
