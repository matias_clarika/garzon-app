﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Garzon.Counter.Forms.UI.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(CustomPickerRenderer))]
namespace Garzon.Counter.Forms.UI.Droid.Renderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        public CustomPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
        
            if (e.NewElement == null || e.OldElement != null || Control == null)
                return;

            var textSize = 20f;
            var textColor = Android.Graphics.Color.Rgb(66, 66, 66);

            Control.TextSize = textSize;
            Control.SetTextColor(textColor);
            Control.SetHintTextColor(textColor);
            Control.SetTypeface(Typeface.CreateFromAsset(this.Context.Assets, "Roboto-Medium.ttf"), TypefaceStyle.Normal);
            Control.SetBackground(Resources.GetDrawable(Resource.Drawable.spinner_background));
        }
    }
}
