﻿using System;
using Android.Content;
using Garzon.Counter.Forms.UI.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ListView), typeof(CustomListViewRenderer))]
namespace Garzon.Counter.Forms.UI.Droid.Renderers
{
    public class CustomListViewRenderer : ListViewRenderer
    {
        public CustomListViewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            if(Control!=null){
                Control.ScrollbarFadingEnabled = false;
                Control.SmoothScrollbarEnabled = true;

            }
        }
    }
}
