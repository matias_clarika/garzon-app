using MvvmCross.Platform.Plugins;

namespace Garzon.iOS.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}