﻿using System;
using CoreGraphics;
using Garzon.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Switch), typeof(CustomSwitch))]
namespace Garzon.iOS.Renderers
{
    public class CustomSwitch : SwitchRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Switch> e)
        {
            base.OnElementChanged(e);
            CGAffineTransform transform = CGAffineTransform.MakeIdentity();
            transform.Scale(0.75f, 0.75f);
            Control.Transform = transform;
        }
    }
}
