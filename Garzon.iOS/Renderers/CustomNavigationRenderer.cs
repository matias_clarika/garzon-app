﻿using System;
using Garzon.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(CustomNavigationRenderer))]
namespace Garzon.iOS.Renderers
{
    public class CustomNavigationRenderer : NavigationRenderer
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (this.NavigationBar != null)
            {
                this.NavigationBar.TintColor = new UIColor(red: 0.26f, green: 0.26f, blue: 0.26f, alpha: 1.0f);
                this.NavigationBar.BarTintColor = UIColor.White;
            }
        }
    }
}
