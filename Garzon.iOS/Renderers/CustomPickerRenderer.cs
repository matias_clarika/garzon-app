﻿using System;
using System.ComponentModel;
using Garzon.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Picker), typeof(CustomPickerRenderer))]
namespace Garzon.iOS.Renderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);


            if (Control != null)
            {

                Control.Layer.BorderWidth = 0;
                Control.BorderStyle = UITextBorderStyle.None;

                var picker = (UIPickerView)this.Control.InputView;
                Control.Font = UIFont.FromName("Roboto-Medium", 13f);
                Control.TextColor = new UIColor(red: 0.26f, green: 0.26f, blue: 0.26f, alpha: 1.0f);
                Control.AttributedPlaceholder = new Foundation.NSAttributedString(
                    Control.AttributedPlaceholder.Value,
                    font: UIFont.FromName("Roboto-Medium", 13.0f),
                    foregroundColor: new UIColor(red: 0.26f, green: 0.26f, blue: 0.26f, alpha: 1.0f)
                );


                var downarrow = UIImage.FromBundle("ic_arrow_down.png");
                Control.RightViewMode = UITextFieldViewMode.Always;
                Control.RightView = new UIImageView(downarrow);

            }
        }
    }
}

