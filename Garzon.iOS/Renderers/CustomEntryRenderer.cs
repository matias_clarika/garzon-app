﻿using System;
using Garzon.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(CustomEntryRenderer))]
namespace Garzon.iOS.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        #region override
        /// <summary>
        /// Ons the element changed.
        /// </summary>
        /// <param name="e">E.</param>
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            var textField = (UITextField)Control;

            // No auto-correct
            textField.AutocorrectionType = UITextAutocorrectionType.No;
            textField.SpellCheckingType = UITextSpellCheckingType.No;
            textField.AutocapitalizationType = UITextAutocapitalizationType.None;
            textField.BorderStyle = UITextBorderStyle.None;
            textField.TintColor = UIColor.Orange;
        }
        #endregion
    }
}
