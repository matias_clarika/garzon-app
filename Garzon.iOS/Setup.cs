﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.iOS;
using MvvmCross.iOS.Platform;
using MvvmCross.Platform.Platform;
using UIKit;
using MvvmCross.Platform.Plugins;
using System;
using MvvmCross.Platform;
using Garzon.Core.Utils;
using Garzon.iOS.Utils;
using Clarika.Xamarin.Base.Helpers;
using Garzon.iOS.FirebaseUtils;
using Garzon.iOS.FacebookUtils;
using MvvmCross.Forms.Platform;
using Garzon.Core.Social.Facebook;
using Garzon.Forms.UI;
using System.Collections.Generic;
using System.Reflection;
using Garzon.Config;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.Config.DataSource;
using Garzon.Core.Views;
using Garzon.iOS.Views;

namespace Garzon.iOS
{
    public class Setup : MvxFormsIosSetup
    {
        public Setup(IMvxApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
        }

        protected override MvxFormsApplication CreateFormsApplication()
        {
            return new FormsApp();
        }

        protected override IMvxApplication CreateApp()
        {
            return new CoreApp();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxPluginConfiguration GetPluginConfiguration(Type plugin)
        {
            if (plugin == typeof(MvvmCross.Plugins.Json.PluginLoader))
            {
                return new MvvmCross.Plugins.Json.MvxJsonConfiguration()
                {
                    RegisterAsTextSerializer = false
                };
            }

            return null;
        }

        protected override void InitializeFirstChance()
        {
            Mvx.ConstructAndRegisterSingleton<IEnvironment, IosEnvironment>();
            Mvx.RegisterSingleton<IFileHelper>(new FileHelper());

            Mvx.RegisterSingleton<IFirebaseCrashReporting>(new FirebaseCrashReporting());
            Mvx.RegisterSingleton<IFirebaseAnalytics>(new FirebaseAnalyticsDefault());
            Mvx.LazyConstructAndRegisterSingleton<IFacebookManager, FacebookManager>();
            Mvx.LazyConstructAndRegisterSingleton<IDisplay, IosDisplay>();

            base.InitializeFirstChance();
            WhiteListAssembly();
        }

        private void WhiteListAssembly()
        {
            var forceAssembly = typeof(FFImageLoading.Svg.Forms.SvgImageSource).FullName;

        }

    }
}
