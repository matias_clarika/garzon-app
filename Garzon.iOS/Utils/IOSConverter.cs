﻿using System;
using System.Collections.Generic;
using Foundation;
using System.Linq;

namespace Garzon.iOS.Utils
{
    public abstract class IOSConverter
    {
        /// <summary>
        /// Dictionaries to NSD ictionary.
        /// </summary>
        /// <returns>The to NSD ictionary.</returns>
        /// <param name="dictionary">Dictionary.</param>
        public static NSMutableDictionary DictionaryToNSDictionary(Dictionary<string, string> dictionary){
            return null;
        }

       /// <summary>
       /// NSDs the ictionary to dictionary.
       /// </summary>
       /// <returns>The ictionary to dictionary.</returns>
       /// <param name="nsDictionary">Ns dictionary.</param>
        public static Dictionary<string, string> NSDictionaryToDictionary(NSDictionary nsDictionary)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if(nsDictionary!=null){
                foreach(var key in nsDictionary.Keys){
                    result.Add($"{key}", $"{nsDictionary.ValueForKey((Foundation.NSString)key)}");
                }
            }
            return result;

        }
    }
}
