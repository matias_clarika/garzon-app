﻿using System;
using System.IO;
using Garzon.Core.Utils;
using Garzon.iOS.Utils;
using Xamarin.Forms;

namespace Garzon.iOS.Utils
{
    public class FileHelper : IFileHelper
    {
        #region constants
        const string LIBRARY_PATH = "Library";
        const string DATABASES_PATH = "Databases";
        #endregion

        #region Implementation of IFileHelper
        public string GetLocalFilePath(string filename)
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", LIBRARY_PATH, DATABASES_PATH);

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, filename);
        }
        #endregion
    }
}
