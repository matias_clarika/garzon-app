﻿using ButtonCircle.FormsPlugin.iOS;
using Facebook.CoreKit;
using FFImageLoading.Forms.Touch;
using FFImageLoading.Svg.Forms;
using Foundation;
using Garzon.Config;
using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.iOS;
using MvvmCross.Platform;
using UIKit;
using Firebase.Core;
using UserNotifications;
using Firebase.CloudMessaging;
using Garzon.iOS.FirebaseUtils;
using System;
using Garzon.Config.Facebook;

namespace Garzon.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxFormsApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        public override UIWindow Window { get; set; }
        private FirebaseRemoteNotifications _firebaseRemoteNotifications;


        #region lifecycle

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            var setup = new Setup(this, Window);
            setup.Initialize();

            SetUpLibrariesBeforeLoadApplication();

            LoadApplication(setup.FormsApplication);

            SetUpLibrariesAfterLoadApplication();

            _firebaseRemoteNotifications = FirebaseRemoteNotifications.Build(this, this);
            _firebaseRemoteNotifications.Register();

            App.Configure();
            return true;
        }

        public override void OnActivated(UIApplication uiApplication)
        {
            _firebaseRemoteNotifications.Connect();
            base.OnActivated(uiApplication);
            //facebook
            AppEvents.ActivateApp();

        }

        public override void DidEnterBackground(UIApplication uiApplication)
        {
            _firebaseRemoteNotifications.Disconnect();
            base.DidEnterBackground(uiApplication);

        }
        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            //return base.OpenUrl(application, url, sourceApplication, annotation);
            return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
        }
        #endregion

        #region notification
        // To receive notifications in foregroung on iOS 9 and below.
        // To receive notifications in background in any iOS version
        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired 'till the user taps on the notification launching the application.

            // If you disable method swizzling, you'll need to call this method. 
            // This lets FCM track message delivery and analytics, which is performed
            // automatically with method swizzling enabled.
            Console.WriteLine($"User info: {userInfo.ToString()}");
        }

        // You'll need this method if you set "FirebaseAppDelegateProxyEnabled": NO in GoogleService-Info.plist
        //public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
        //{
        //  InstanceId.SharedInstance.SetApnsToken (deviceToken, ApnsTokenType.Sandbox);
        //}

        // To receive notifications in foreground on iOS 10 devices.
        [Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
        public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            _firebaseRemoteNotifications.ProcessToEvent(notification.Request.Content.UserInfo);
        }

        // Receive data message on iOS 10 devices.
        public void ApplicationReceivedRemoteMessage(RemoteMessage remoteMessage)
        {
            _firebaseRemoteNotifications.ProcessToEvent(remoteMessage.AppData);
        }

        // Receive data message on iOS 11 devices.
        [Export("messaging:didReceiveMessage:")]
        public void DidReceiveMessage(Messaging messaging, RemoteMessage remoteMessage)
        {

            _firebaseRemoteNotifications.ProcessToEvent(remoteMessage.AppData);
        }
        #region Workaround for handling notifications in background for iOS 10

        [Export("userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler:")]
        public void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            Console.WriteLine($"background response: {response.ToString()}");
        }

        public void DidRefreshRegistrationToken(Messaging messaging, string fcmToken)
        {
            _firebaseRemoteNotifications.NotifyRefreshToken(fcmToken);
        }
        #endregion
        #endregion



        #region Facebook
        /// <summary>
        /// Configures the facebook.
        /// </summary>
        private void ConfigureFacebook()
        {
            var config = Mvx.Resolve<IFacebookAppConfig>();

            Settings.AppID = config.FacebookAppID;
            Settings.DisplayName = config.FacebookDisplayName;
        }
        #endregion

        #region library setup
        /// <summary>
        /// Sets up libraries before load application.
        /// </summary>
        private void SetUpLibrariesBeforeLoadApplication()
        {

            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();
            Rg.Plugins.Popup.Popup.Init();
            CachedImageRenderer.Init();
            var ignore = typeof(SvgCachedImage);
        }

        /// <summary>
        /// Sets up libraries after load application.
        /// </summary>
        private void SetUpLibrariesAfterLoadApplication()
        {
            ButtonCircleRenderer.Init();
            ConfigureFacebook();
            Window.MakeKeyAndVisible();
        }
        #endregion

    }
}
