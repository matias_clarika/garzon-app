﻿namespace Garzon.iOS.FirebaseUtils
{
    using System;
    using System.Collections.Generic;
    using Clarika.Xamarin.Base.Helpers;
    using Firebase.Analytics;
    using Foundation;
    using System.Linq;

    public class FirebaseAnalyticsDefault : IFirebaseAnalytics
    {
        #region Firebase Events
        readonly string EVENT_NAVIGATE_TO_SCREEN = "screen_";
        #endregion

        public FirebaseAnalyticsDefault()
        {

        }

        #region Implementation of IFirebaseAnalytics
        public void SendEvent(string eventName, Dictionary<string, string> information = null)
        {
            NSDictionary<NSString, NSObject> parameters = information != null ? NSDictionary<NSString, NSObject>
                                                                                .FromObjectsAndKeys(information.Values.ToArray(),
                                                                                                    information.Keys.ToArray()) : new NSDictionary<NSString, NSObject>();
            Analytics.LogEvent(eventName, parameters);
        }

        public void SendView(string view, Dictionary<string, string> information = null)
        {
            NSDictionary<NSString, NSObject> parameters = information != null ? NSDictionary<NSString, NSObject>
                                                                                .FromObjectsAndKeys(information.Values.ToArray(),
                                                                                                    information.Keys.ToArray()) : new NSDictionary<NSString, NSObject>();
            Analytics.LogEvent($"{EVENT_NAVIGATE_TO_SCREEN}{view}", parameters);
        }

        public void SetCurrentUser(string id, Dictionary<string, string> attributes = null)
        {
            Analytics.SetUserID(id);
            if (attributes != null)
            {
                foreach (KeyValuePair<string, string> entry in attributes)
                {
                    Analytics.SetUserProperty(entry.Key, entry.Value);
                }
            }
        }

        public void ClearCurrentUser()
        {
            this.SetCurrentUser(null);
        }

        #endregion

        /// <summary>
        /// Dictionaries to string.
        /// </summary>
        /// <returns>The to string.</returns>
        /// <param name="information">Information.</param>
        private string DictionaryToString(Dictionary<string, string> information = null)
        {
            if (information != null)
            {
                return information.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}
