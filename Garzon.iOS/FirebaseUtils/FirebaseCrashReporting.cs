﻿namespace Garzon.iOS.FirebaseUtils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Clarika.Xamarin.Base.Helpers;

    using Foundation;

    public class FirebaseCrashReporting : IFirebaseCrashReporting
    {
        #region Implementation of IFirebaseCrashReporting
        public void Log(string message)
        {
            var data = new Dictionary<object, object> {
                { "log message", message}
            };

            var nsData = NSDictionary.FromObjectsAndKeys(data.Values.ToArray(), data.Keys.ToArray(), data.Keys.Count);

            //Crashlytics.SharedInstance.SetValue(nsData, "data");
        }

        public void Report(Exception e)
        {
            //Automatically report
            //TODO The Firebase SDK does not currently support using the NSException class in the Xcode simulator. If you use this class, it will result in malformed stack traces in the Firebase console. As a workaround, either use a physical device or test with a different type of exception.
            ReportFatalError(e.Message);
        }

        public void Report(string message)
        {
            //automatically report
            //TODO The Firebase SDK does not currently support using the NSException class in the Xcode simulator. If you use this class, it will result in malformed stack traces in the Firebase console. As a workaround, either use a physical device or test with a different type of exception.
            ReportFatalError(message);
        }

        #endregion

        #region private methods

        private static void ReportFatalError(string message)
        {
            var data = new Dictionary<object, object> {
                { "Fatal message", message}
            };

            var nsData = NSDictionary.FromObjectsAndKeys(data.Values.ToArray(), data.Keys.ToArray(), data.Keys.Count);

            //Crashlytics.SharedInstance.SetValue(nsData, "data");
        }
        #endregion
    }
}
