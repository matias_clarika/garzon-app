﻿using System;
using System.Threading.Tasks;
using Firebase.CloudMessaging;
using Firebase.InstanceID;
using Foundation;
using Garzon.Core.EventBus;
using Garzon.Core.EventBus.Auth;
using Garzon.Forms.UI.DI;
using Garzon.Forms.UI.EventBus.Notifications;
using Garzon.iOS.Utils;
using UIKit;
using UserNotifications;

namespace Garzon.iOS.FirebaseUtils
{
    public class FirebaseRemoteNotifications
    {
        readonly IUNUserNotificationCenterDelegate _notificationCenterdelegate;
        readonly IMessagingDelegate _messagingDelegate;

        private FirebaseRemoteNotifications(IUNUserNotificationCenterDelegate notificationCenterdelegate,
                                           IMessagingDelegate messagingDelegate)
        {
            _notificationCenterdelegate = notificationCenterdelegate;
            _messagingDelegate = messagingDelegate;
        }

        public static FirebaseRemoteNotifications Build(IUNUserNotificationCenterDelegate notificationCenterdelegate,
                                                        IMessagingDelegate messagingDelegate)
        {
            return new FirebaseRemoteNotifications(notificationCenterdelegate, messagingDelegate);
        }

        /// <summary>
        /// Register this instance.
        /// </summary>
        public void Register()
        {

            // Monitor token generation
            InstanceId.Notifications.ObserveTokenRefresh(TokenRefreshNotification);
            // Register your app for remote notifications.
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // iOS 10 or later
                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
                {
                    Console.WriteLine(granted);
                });

                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.Current.Delegate = _notificationCenterdelegate;

                // For iOS 10 data message (sent via FCM)
                Messaging.SharedInstance.RemoteMessageDelegate = _messagingDelegate;
            }
            else
            {
                // iOS 9 or before
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            RegisterForRemoteNotifications();
        }

        private void RegisterForRemoteNotifications()
        {
            NotifyStartRegistration();
            UIApplication.SharedApplication.RegisterForRemoteNotifications();
        }

        public void Connect()
        {
            Messaging.SharedInstance.Connect(async (error) =>
            {
                //System.Diagnostics.Debug.WriteLine("connectFCM\t" + (error != null ? "error occured" : "connect success"));
                Console.WriteLine("connectFCM\t" + (error != null ? "error occured" + error.DebugDescription : "connect success"));
            });
        }

        public void Disconnect()
        {
            Messaging.SharedInstance.Disconnect();
        }

        /// <summary>
        /// Tokens the refresh notification.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void TokenRefreshNotification(object sender, NSNotificationEventArgs e)
        {
            NotifyRefreshToken(InstanceId.SharedInstance.Token);
            NotifySuccessRegistration();
        }


        public async Task NotifyRefreshToken(string token)
        {
            await DependencyManager.GetInstance()
                                     .Resolve<IEventBus>()
                                     .Publish(AuthNotificationMessage
                                              .Builder(this, token));
        }

        public void ProcessToEvent(NSDictionary dictionary)
        {
            DependencyManager.GetInstance()
                                     .Resolve<IEventBus>()
                                     .Publish(OrderEvent
                                              .Builder(this,
                                                       IOSConverter
                                                       .NSDictionaryToDictionary(dictionary)));
        }

        private void NotifyStartRegistration()
        {
            var action = Forms.UI.EventBus.Notifications.Action.START;
            NotifyRegistrationEvent(action);
        }

        private void NotifySuccessRegistration()
        {
            var action = Forms.UI.EventBus.Notifications.Action.SUCCESS;
            NotifyRegistrationEvent(action);
        }

        private void NotifyErrorRegistration()
        {
            var action = Forms.UI.EventBus.Notifications.Action.ERROR;
            NotifyRegistrationEvent(action);
        }
        /// <summary>
        /// Notifies the registration event.
        /// </summary>
        /// <param name="action">Action.</param>
        private void NotifyRegistrationEvent(Forms.UI.EventBus.Notifications.Action action)
        {
            try
            {
                DependencyManager
                    .GetInstance()
                    .Resolve<IEventBus>()
                    .Publish(RegistrationNotificationEvent
                             .Builder(this,
                                      action));
            }
            catch (Exception e)
            {
                //nothing
            }

        }
    }
}


