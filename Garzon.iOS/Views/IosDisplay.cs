﻿using System;
using UIKit;
using Garzon.Core.Views;

namespace Garzon.iOS.Views
{
    public class IosDisplay : IDisplay
    {
        public IosDisplay(){
            int Width;
            int Height;
            if (UIKit.UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                CoreGraphics.CGRect bounds = UIKit.UIScreen.MainScreen.NativeBounds;
                Width = (int)bounds.Width;
                Height = (int)bounds.Height;
            }
            else
            {
                //All older devices are portrait by design so treat the default bounds as such
                CoreGraphics.CGRect bounds = UIKit.UIScreen.MainScreen.Bounds;
                Width = System.Math.Min((int)bounds.Width, (int)bounds.Height);
                Height = System.Math.Max((int)bounds.Width, (int)bounds.Height);
            }

            Width *= (int)UIKit.UIScreen.MainScreen.Scale;
            Height *= (int)UIKit.UIScreen.MainScreen.Scale;

            double baseDPI = 163; //dpi from 1st Gen iPhone devices
            double dpi = baseDPI * UIKit.UIScreen.MainScreen.Scale;
            Xdpi = dpi;
            Ydpi = dpi;
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents the current <see cref="Display" />.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents the current <see cref="Display" />.</returns>
        public override string ToString()
        {
            return string.Format("[Screen: Height={0}, Width={1}, Xdpi={2}, Ydpi={3}]", Height, Width, Xdpi, Ydpi);
        }

        #region IScreen implementation

        /// <summary>
        ///     Gets the screen height in pixels
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        ///     Gets the screen width in pixels
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        ///     Gets the screens X pixel density per inch
        /// </summary>
        public double Xdpi { get; private set; }

        /// <summary>
        ///     Gets the screens Y pixel density per inch
        /// </summary>
        public double Ydpi { get; private set; }

        /// <summary>
        /// Gets the scale value of the display.
        /// </summary>
        public double Scale
        {
            get
            {
                return UIScreen.MainScreen.Scale;
            }
        }

        /// <summary>
        ///     Convert width in inches to runtime pixels
        /// </summary>
        public double WidthRequestInInches(double inches)
        {
            return inches * Xdpi / UIScreen.MainScreen.Scale;
        }

        /// <summary>
        ///     Convert height in inches to runtime pixels
        /// </summary>
        public double HeightRequestInInches(double inches)
        {
            return inches * Ydpi / UIScreen.MainScreen.Scale;
        }

        #endregion
    } 
}
