﻿using Garzon.Core.Social;
using Garzon.Core.Social.Facebook;
using System.Threading.Tasks;
using Facebook.LoginKit;
using UIKit;
using System;
using System.Diagnostics;
using Foundation;
using Xamarin.Forms;
using Facebook.CoreKit;
using Garzon.DataObjects.Social;

namespace Garzon.iOS.FacebookUtils
{
    public class FacebookManager : IFacebookManager
    {
     
        #region Implementation of IFacebookManager
        public Task<SocialUser> Login()
        {
            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;
            while (vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }

            var tcs = new TaskCompletionSource<SocialUser>();
            LoginManager manager = new LoginManager();
            manager.LogOut();
            manager.LoginBehavior = LoginBehavior.SystemAccount;
            manager.LogInWithReadPermissions(FacebookSettings.ReadPermissions, vc, (result, error) =>
            {
                if (error != null || result == null || result.IsCancelled)
                {
                    if (error != null){
                        Debug.WriteLine(error.LocalizedDescription);
                    }
                    tcs.TrySetResult(null);
                }
                else
                {
                    var request = new Facebook.CoreKit.GraphRequest("me", new NSDictionary("fields", "id, first_name, email, last_name"));
                    request.Start((connection, result1, error1) =>
                    {
                        if (error1 != null || result1 == null)
                        {
                            Debug.WriteLine(error1.LocalizedDescription);
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            var id = string.Empty;
                            var first_name = string.Empty;
                            var email = string.Empty;
                            var last_name = string.Empty;

                            try
                            {
                                id = result1.ValueForKey(new NSString("id"))?.ToString();
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.Message);
                            }

                            try
                            {
                                first_name = result1.ValueForKey(new NSString("first_name"))?.ToString();
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.Message);
                            }

                            try
                            {
                                email = result1.ValueForKey(new NSString("email"))?.ToString();
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.Message);
                            }

                            try
                            {
                                last_name = result1.ValueForKey(new NSString("last_name"))?.ToString();
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.Message);
                            }

                            if (tcs != null)
                            {
                                tcs.TrySetResult(SocialUser.Builder()
                                                 .WithId(id)
                                                 .WithToken(result.Token.TokenString)
                                                 .WithFirstName(first_name)
                                                 .WithLastName(last_name)
                                                 .WithEmail(email));
                            }
                        }
                    });
                }
            });
            return tcs.Task;

        }

        public async System.Threading.Tasks.Task LogOut()
        {
            LoginManager manager = new LoginManager();
            manager.LogOut();
        }

        public Task<bool> PostPhoto(ImageSource image)
        {
            throw new NotImplementedException();
        }

        public Task<bool> PostText(string message)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ValidateToken()
        {
            var token = AccessToken.CurrentAccessToken?.TokenString;
            if (string.IsNullOrWhiteSpace(token))
                return false;
            return true;
        }
        #endregion
    }
}
