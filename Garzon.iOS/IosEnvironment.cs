﻿using System;
using Garzon.Config;

namespace Garzon.iOS
{
    public class IosEnvironment : IEnvironment
    {
        readonly string _environment;

        public IosEnvironment()
        {
            _environment = EnvironmentConstants.BUILD_ENVIRONMENT_CONFIGURE;
        }

        public string BuildEnvironment => _environment;
    }
}
