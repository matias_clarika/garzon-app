﻿using System;
namespace Garzon.Config
{
    public interface IEnvironment
    {
        string BuildEnvironment { get; }
    }
}
