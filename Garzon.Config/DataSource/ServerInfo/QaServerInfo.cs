﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.Config.DataSource
{
    public class QaServerInfo : IServerInfo
    {
        string API_ENDPOINT = "https://qa-dot-garzon-dev.appspot.com/api";
        string API_VERSION = "";

        public string GetUrl()
        {
            return API_ENDPOINT;
        }

        public string GetVersion()
        {
            return API_VERSION;
        }
    }
}
