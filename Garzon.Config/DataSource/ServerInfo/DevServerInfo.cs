﻿
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.Config.DataSource
{
    public class DevServerInfo : IServerInfo
    {
        string API_ENDPOINT = "https://garzon-dev.appspot.com/api";
        string API_VERSION = "";

        public string GetUrl()
        {
            return API_ENDPOINT;
        }

        public string GetVersion()
        {
            return API_VERSION;
        }
    }
}
