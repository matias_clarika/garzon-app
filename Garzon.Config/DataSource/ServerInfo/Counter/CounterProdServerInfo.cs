﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.Config.DataSource
{
    public class CounterProdServerInfo : IServerInfo
    {
        string API_ENDPOINT = "http://api-counter.garzonapp.com/api";
        string API_VERSION = "";


        public string GetUrl()
        {
            return API_ENDPOINT;
        }

        public string GetVersion()
        {
            return API_VERSION;
        }
    }
}
