﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.Config.DataSource
{
    public class PollyStrategy : IPollyStrategy
    {
        #region implementation of IPollyStrategy

        public int Retry => 3;

        public TimeSpan RetryTime => TimeSpan.FromSeconds(2);

        public TimeSpan CacheTime => TimeSpan.FromMinutes(10);

        public bool RetryTimeIncremental => true;


        public bool Enable
        {
            get;
            set;
        } = true;
        #endregion
    }
}
