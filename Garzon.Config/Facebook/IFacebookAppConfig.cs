﻿using System;
namespace Garzon.Config.Facebook
{
    public interface IFacebookAppConfig
    {
        string FacebookAppID{
            get;
        }

        string FacebookDisplayName{
            get;
        }

    }
}
