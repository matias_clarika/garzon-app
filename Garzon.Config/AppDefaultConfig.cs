﻿namespace Garzon.Config
{
    using System;
    using Clarika.Xamarin.Base.Persistence;

    public class AppDefaultConfig : IAppConfig, ICacheSettings
    {
        #region IAppConfig
        public int OrderCounterMinutes => 3;
        #endregion

        #region general
        public string LANGUAGE { get { return "en"; } }
        #endregion

        #region product
        public int MIN_QUANTITY_PRODUCT_ADD_TO_ORDER { get { return 1; } }
        public int MAX_QUANTITY_PRODUCT_ADD_TO_ORDER { get { return 20; } }
        #endregion

        #region Implementation of ICacheSettings
        public bool CacheEnabled => true;
        #endregion

        #region order
        public int TimeToRefreshOrderStatus
        {
            get { return 5000; }
        }

        #endregion

        #region appversion
        public int MinAppVersion => 10;
        #endregion

        #region stores
        public string AndroidStoreURL => "https://play.google.com/store/apps/details?id=com.garzon";

        public string IOSStoreURL => "https://itunes.apple.com/ar/app/garzon/id1280403927";
        #endregion

        #region mercadopago
        public string MercadoPagoPaymentSetupURL => "https://auth.mercadopago.com.ar/authorization?client_id=258806643735836&response_type=code&platform_id=mp&redirect_uri=https://v2.garzonapp.com/login-mp?place_id={0}";
        #endregion

    }
}
