﻿using System;
namespace Garzon.Config
{
    public abstract class EnvironmentConstants
    {
        public const string BUILD_ENVIRONMENT= "build_environment";
        public const string BUILD_ENVIRONMENT_DEV = "dev";
        public const string BUILD_ENVIRONMENT_QA = "qa";
        public const string BUILD_ENVIRONMENT_RELEASE = "release";
        public const string BUILD_ENVIRONMENT_PROD = "production";

        public static string BUILD_ENVIRONMENT_CONFIGURE = BUILD_ENVIRONMENT_PROD;
    }
}
