﻿using System;
namespace Garzon.Config
{
    public interface IAppConfig
    {
        int OrderCounterMinutes
        {
            get;
        }

        int MinAppVersion
        {
            get;
        }

        string AndroidStoreURL
        {
            get;
        }

        string IOSStoreURL
        {
            get;
        }

        string MercadoPagoPaymentSetupURL
        {
            get;
        }
    }
}
