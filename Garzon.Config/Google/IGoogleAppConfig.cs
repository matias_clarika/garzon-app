﻿using System;
namespace Garzon.Config.Google
{
    public interface IGoogleAppConfig
    {
        string GoogleAppId{
            get;
        }
    }
}
