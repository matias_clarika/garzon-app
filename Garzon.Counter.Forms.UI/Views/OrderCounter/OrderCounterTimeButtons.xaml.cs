﻿namespace Garzon.Counter.Forms.UI.Views
{
    using Garzon.Counter.Forms.UI.ViewModels;
    using Xamarin.Forms;

    public partial class OrderCounterTimeButtons : ContentView
    {
        public OrderCounterTimeButtons()
        {
            InitializeComponent();
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            var frame = (Frame)sender;

            int? minutes = GetMinutesByFrame(frame);
            if (this.BindingContext is TakeAwayOrderCounterDetailViewModel viewmodel)
            {
                viewmodel.TimeButtonCommand.ExecuteAsync(minutes);
            }
        }

        /// <summary>
        /// Gets the minutes by frame.
        /// </summary>
        /// <returns>The minutes by frame.</returns>
        /// <param name="frame">Frame.</param>
        private int? GetMinutesByFrame(Frame frame)
        {
            int? minutes = null;
            if (frame == this.buttonTime5)
            {
                minutes = 5;
            }
            else if (frame == this.buttonTime10)
            {
                minutes = 10;
            }
            else if (frame == this.buttonTime15)
            {
                minutes = 15;
            }
            else if (frame == this.buttonTime20)
            {
                minutes = 20;
            }
            else if (frame == this.buttonTime30)
            {
                minutes = 30;
            }
            else if (frame == this.buttonTime45)
            {
                minutes = 45;
            }

            return minutes;
        }
    }
}
