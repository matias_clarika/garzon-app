﻿namespace Garzon.Counter.Forms.UI.Views
{
    using Garzon.Counter.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;

    public partial class OrderCounterDetailPage : MvxContentPage<OrderCounterDetailViewModel>
    {
        public OrderCounterDetailPage()
        {
            InitializeComponent();
        }
    }
}
