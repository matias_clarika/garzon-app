﻿namespace Garzon.Counter.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Garzon.Counter.Forms.UI.ViewModels;
    using Garzon.DataObjects;
    using Xamarin.Forms;

    public partial class OrderCounterDetailListView : ContentView
    {
        public OrderCounterDetailListView()
        {
            InitializeComponent();
        }

        private void Item_OnTapped(object sender, EventArgs e)
        {
            if (e is TappedEventArgs tappedEventArgs)
            {
                var orderDetail = tappedEventArgs.Parameter as OrderDetail;
                if (this.BindingContext is OrderCounterDetailViewModel viewModel)
                {
                    if (orderDetail.IsCompound)
                    {
                        viewModel.ItemOrderDetailCompoundClickCommand.ExecuteAsync(orderDetail);
                    }
                }
            }
        }
    }
}
