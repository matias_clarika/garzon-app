﻿namespace Garzon.Counter.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Xamarin.Forms;

    public partial class OrderDetailListView : ContentView
    {
        public OrderDetailListView()
        {
            InitializeComponent();
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            list.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }
    }
}
