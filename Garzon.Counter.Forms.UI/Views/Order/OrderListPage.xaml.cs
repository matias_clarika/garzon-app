﻿namespace Garzon.Counter.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using Garzon.Counter.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;
    using MvvmCross.Forms.Views.Attributes;
    using Xamarin.Forms;

    [MvxTabbedPagePresentation(WrapInNavigationPage = false)]
    public partial class OrderListPage : MvxContentPage<CounterOrderListViewModel>
    {
        public OrderListPage()
        {
            InitializeComponent();
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            list.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }
    }
}
