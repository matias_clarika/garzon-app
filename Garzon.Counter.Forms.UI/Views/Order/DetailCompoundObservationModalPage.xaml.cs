﻿using System;
using Garzon.Counter.Forms.UI.ViewModels;
using Garzon.DataObjects;
using MvvmCross.Forms.Views;
using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms;

namespace Garzon.Counter.Forms.UI.Views
{
    public partial class DetailCompoundObservationModalPage : MvxContentPage<DetailCompoundObservationViewModel>
    {
        public DetailCompoundObservationModalPage()
        {
            InitializeComponent();
        }

        private void Item_OnTapped(object sender, EventArgs e)
        {
            if (e is TappedEventArgs tappedEventArgs)
            {
                var orderDetail = tappedEventArgs.Parameter as OrderDetail;
                if (this.BindingContext is DetailCompoundObservationViewModel viewModel)
                {
                    //viewModel.ItemOrderDetailClickCommand.ExecuteAsync(orderDetail);
                }
            }
        }
    }
}
