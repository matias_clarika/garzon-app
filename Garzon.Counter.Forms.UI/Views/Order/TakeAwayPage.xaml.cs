﻿namespace Garzon.Counter.Forms.UI.Views
{
    using Garzon.Counter.Forms.UI.ViewModels;
    using Garzon.DataObjects;
    using MvvmCross.Forms.Views;
    using MvvmCross.Forms.Views.Attributes;
    using Xamarin.Forms;

    [MvxTabbedPagePresentation(WrapInNavigationPage = false)]
    public partial class TakeAwayPage : MvxContentPage<TakeAwayViewModel>
    {
        public TakeAwayPage()
        {
            InitializeComponent();
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            list.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }

        private async void OnTakeAwayWithdrawTapped(object sender, System.EventArgs e)
        {
            if (e is TappedEventArgs tappedEventArgs)
            {
                var order = tappedEventArgs.Parameter as Order;
                if (this.BindingContext.DataContext is TakeAwayViewModel viewModel)
                {
                    await viewModel.TakeAwayWithdrawCommand.ExecuteAsync(order);
                }
            }
        }

        private async void OnTakeAwayDeliveredTapped(object sender, System.EventArgs e)
        {
            if (e is TappedEventArgs tappedEventArgs)
            {
                var order = tappedEventArgs.Parameter as Order;
                if (this.BindingContext.DataContext is TakeAwayViewModel viewModel)
                {
                    await viewModel.TakeAwayDeliveredCommand.ExecuteAsync(order);
                }
            }
        }
    }
}
