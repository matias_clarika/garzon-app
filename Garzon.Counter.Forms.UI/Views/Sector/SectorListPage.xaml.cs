﻿namespace Garzon.Counter.Forms.UI.Views
{
    using Garzon.Counter.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;

    public partial class SectorListPage : MvxContentPage<SectorListViewModel>
    {
        public SectorListPage()
        {
            InitializeComponent();
        }
    }
}
