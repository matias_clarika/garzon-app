﻿namespace Garzon.Counter.Forms.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Garzon.Counter.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;
    using Xamarin.Forms;

    public partial class PaymentAuthView : MvxContentPage<PaymentAuthViewModel>
    {
        private readonly string ADMIN_URL = "v2.garzonapp.com/admin/";
        private readonly string NOT_REDIRECT = "redirect_uri";

        public PaymentAuthView()
        {
            InitializeComponent();
        }

        async void Handle_Navigating(object sender, WebNavigatingEventArgs e)
        {
            await this.CheckUrlEventAsync(e);
        }

        private async Task CheckUrlEventAsync(WebNavigatingEventArgs e)
        {
            if (e.Url.Contains(ADMIN_URL) && !e.Url.Contains(NOT_REDIRECT))
            {
                try
                {
                    e.Cancel = true;
                    await this.ViewModel.SuccessCommand.ExecuteAsync();
                }
                catch (Exception e1)
                {
                    System.Diagnostics.Debug.WriteLine(e1.Message);
                }
            }
        }
    }
}
