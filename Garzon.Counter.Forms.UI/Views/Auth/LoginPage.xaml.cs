﻿using Garzon.Counter.Forms.UI.ViewModels;
using MvvmCross.Forms.Views;
using MvvmCross.Forms.Views.Attributes;

namespace Garzon.Counter.Forms.UI.Views
{
    [MvxContentPagePresentation(WrapInNavigationPage = false, NoHistory = true)]
    public partial class LoginPage : MvxContentPage<LoginViewModel>
    {
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}
