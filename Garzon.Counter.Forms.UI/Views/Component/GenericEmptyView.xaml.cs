﻿namespace Garzon.Counter.Forms.UI.Views.Component
{
    using System;
    using System.Collections.Generic;
    using Xamarin.Forms;

    public partial class GenericEmptyView : ContentView
    {
        public GenericEmptyView()
        {
            InitializeComponent();
        }
    }
}
