﻿namespace Garzon.Counter.Forms.UI.Views
{
    using Garzon.Counter.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;
    using MvvmCross.Forms.Views.Attributes;

    [MvxTabbedPagePresentation(WrapInNavigationPage = false, Title = "Pendientes")]
    public partial class PendingCounterPage : MvxContentPage<PendingCounterViewModel>
    {
        public PendingCounterPage()
        {
            InitializeComponent();
        }
    }
}
