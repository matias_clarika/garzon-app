﻿namespace Garzon.Counter.Forms.UI.Views
{
    using Garzon.Counter.Forms.UI.ViewModels;
    using MvvmCross.Forms.Views;
    using MvvmCross.Forms.Views.Attributes;
    using Xamarin.Forms;

    [MvxTabbedPagePresentation(TabbedPosition.Root, NoHistory = true)]
    public partial class HomePage : MvxTabbedPage<HomeViewModel>
    {
        private bool firstTime = true;

        public HomePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (this.firstTime)
            {
                //ViewModel.ShowInitialViewModelsCommand.Execute();
                ViewModel.ShowInitialViewModelsCommand.ExecuteAsync(null);
                this.firstTime = false;
            }
        }
    }
}
