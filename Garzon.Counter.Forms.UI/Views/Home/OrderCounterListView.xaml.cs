﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Garzon.Counter.Forms.UI.Views
{
    public partial class OrderCounterListView : ContentView
    {
        public OrderCounterListView()
        {
            InitializeComponent();
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //not selected item
            list.SelectedItem = null;
            //listProducts.SelectedItem = null;
        }
    }
}
