﻿namespace Garzon.Counter.Forms.UI.Views.Format
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Garzon.BL.Currency;
    using Garzon.Core.Views.Format;
    using Garzon.DataObjects;

    public class FormatOrderDetail : BaseFormatOrderDetail, IFormatOrderDetail
    {
        public FormatOrderDetail(ICurrentCurrency currentCurrency) : base(currentCurrency)
        {
        }

        protected override string GetProductDetailLabels(OrderDetail parentDetail, List<OrderDetail> details)
        {
            return String.Join("\n",
                               details.GroupBy((arg) => arg.GroupName)
                               .Select(arg => $"{arg.Key}: {base.GetProductDetailLabels(parentDetail, arg.ToList())}"));
        }
    }
}
