﻿namespace Garzon.Counter.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;

    public class TakeAwayWithdrawVisibilityValueConverter : MvxValueConverter<int?, bool>
    {
        #region MvxValueConverter
        protected override bool Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.HasValue && (value.Value == (int)OrderStatus.OPEN || value.Value == (int)OrderStatus.READY_FOR_WITHDRAW);
        }
        #endregion
    }
}
