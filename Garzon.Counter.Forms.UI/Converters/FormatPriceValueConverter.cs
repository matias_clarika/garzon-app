﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;
using MvvmCross.Platform;
using Garzon.Core.Utils;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class FormatPriceValueConverter : MvxValueConverter<Double, string>
    {

        #region dependency
        readonly IFormatPrice _formatPrice;
        #endregion

        #region constructor
        public FormatPriceValueConverter()
        {
            _formatPrice = Mvx.Resolve<IFormatPrice>();
        }
        #endregion

        #region override of MvxValueConverter
        protected override string Convert(Double value,
                                          Type targetType,
                                          object parameter,
                                          CultureInfo cultureInfo)
        {
            if (value > 0)
            {
                return _formatPrice.FormatPriceWithSymbol(value);
            }
            else
            {
                return "";
            }

        }
        #endregion
    }
}
