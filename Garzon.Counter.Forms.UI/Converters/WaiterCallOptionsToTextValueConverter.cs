﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Garzon.DataObjects;
using MvvmCross.Platform.Converters;
using System.Linq;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class WaiterCallOptionsToTextValueConverter : MvxValueConverter<List<WaiterCallOption>, string>
    {
        #region override of MvxValueConverter
        protected override string Convert(List<WaiterCallOption> value,
                                          Type targetType,
                                          object parameter,
                                          CultureInfo cultureInfo)
        {
            if (value != null
                && value.Any())
            {
                var options = value.Select((option) => option.Name)
                                   .ToArray();
                return String.Join("\n", options);
            }
            else
            {
                return "";
            }

        }
        #endregion
    }
}
