﻿using System;
using System.Globalization;
using Garzon.Config;
using Garzon.Core.Resources;
using MvvmCross.Platform;
using MvvmCross.Platform.Converters;
using Xamarin.Forms;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class TimeClockByDateTimeValueConverter : MvxValueConverter<DateTime, ImageSource>
    {
        #region dependency
        readonly IAppConfig _appConfig;
        #endregion

        #region constructor
        public TimeClockByDateTimeValueConverter()
        {
            _appConfig = Mvx.Resolve<IAppConfig>();
        }
        #endregion

        #region override of MvxValueConverter
        protected override ImageSource Convert(DateTime value,
                                        Type targetType,
                                        object parameter,
                                        CultureInfo culture)
        {
            var resource = ImagesPath.IC_CLOCK;

            if (value != null)
            {
                var diffTime = TimeSpan.FromMinutes(_appConfig.OrderCounterMinutes);
                var now = DateTime.Now.ToLocalTime();
                var seconds = now.ToLocalTime().Subtract(value.ToLocalTime()).TotalSeconds;

                if (seconds > diffTime.TotalSeconds)
                {
                    resource = ImagesPath.IC_CLOCK_ALERT;
                }
            }

            return ImageSource.FromFile(resource);
        }
        #endregion
    }
}
