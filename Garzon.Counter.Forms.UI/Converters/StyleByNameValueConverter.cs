﻿namespace Garzon.Counter.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;
    using Xamarin.Forms;

    public class StyleByNameValueConverter : MvxValueConverter<string, Style>
    {
        protected override Style Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Style)Application.Current.Resources[value];
        }
    }
}
