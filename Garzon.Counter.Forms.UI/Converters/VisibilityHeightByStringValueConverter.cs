﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class VisibilityHeightByStringValueConverter: MvxValueConverter<string, double>
    {
        #region override of MvxValueConverter
        protected override double Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
                if (!String.IsNullOrEmpty(value))
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
        }

        #endregion
    }
}
