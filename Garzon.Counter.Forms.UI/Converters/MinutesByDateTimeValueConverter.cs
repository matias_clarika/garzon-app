﻿using System;
using System.Globalization;
using Garzon.Config;
using MvvmCross.Platform;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class MinutesByDateTimeValueConverter : MvxValueConverter<DateTime, string>
    {
        #region dependency
        readonly IAppConfig _appConfig;
        #endregion

        #region constructor
        public MinutesByDateTimeValueConverter()
        {
            _appConfig = Mvx.Resolve<IAppConfig>();
        }
        #endregion

        #region override of MvxValueConverter
        protected override string Convert(DateTime value,
                                          Type targetType,
                                          object parameter,
                                          CultureInfo cultureInfo)
        {

            if (value != null)
            {
                var diffTime = TimeSpan.FromMinutes(_appConfig.OrderCounterMinutes);
                var now = DateTime.Now.ToLocalTime();
                var minutes = (int)now.ToLocalTime().Subtract(value.ToLocalTime()).TotalMinutes;
                if (minutes == 0)
                {
                    return "0'";
                }
                else
                {
                    return $"{minutes.ToString()}\'";
                }
            }
            else
            {
                return "";
            }
        }
        #endregion
    }
}
