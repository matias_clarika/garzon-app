﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class UpperCaseValueConverter : MvxValueConverter<string, string>
    {
        #region override of MvxValueConverter
        protected override string Convert(string value,
                                          Type targetType,
                                          object parameter,
                                          CultureInfo cultureInfo)
        {
            if (!String.IsNullOrEmpty(value))
            {
                return value.ToUpper();
            }
            else
            {
                return String.Empty;
            }

        }
        #endregion
    }
}
