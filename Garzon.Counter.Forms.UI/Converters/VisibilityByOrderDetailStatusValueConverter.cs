﻿using System;
using System.Globalization;
using Garzon.DataObjects;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class VisibilityByOrderDetailStatusValueConverter: MvxValueConverter<int, bool>
    {
        #region override of MvxValueConverter
        protected override bool Convert(int value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == (int)OrderDetailStatus.OBSERVATION 
                                                  || value == (int)OrderDetailStatus.PENDING 
                                                  || value == (int) OrderDetailStatus.INCOUNTER;
        }

        #endregion
    }
}
