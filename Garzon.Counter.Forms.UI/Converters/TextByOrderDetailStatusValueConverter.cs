﻿using System;
using System.Globalization;
using Garzon.DataObjects;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class TextByOrderDetailStatusValueConverter : MvxValueConverter<int, string>
    {
        #region override of MvxValueConverter
        protected override string Convert(int value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = "";
            switch (value)
            {
                case (int)OrderDetailStatus.OBSERVATION:
                    result = "Observado";
                    break;
                case (int)OrderDetailStatus.PENDING:
                    result = "En cola";
                    break;
                case (int)OrderDetailStatus.INCOUNTER:
                    result = "No cargado";
                    break;

            }
            return result.ToUpper();
        }

        #endregion
    }
}
