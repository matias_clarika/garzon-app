﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class VisibilityByIntValueConverter : MvxValueConverter<int?, bool>
    {
        #region override of MvxValueConverter
        protected override bool Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.HasValue && value.Value > 0;
        }

        #endregion
    }
}
