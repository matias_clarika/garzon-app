﻿namespace Garzon.Counter.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using Garzon.DataObjects;
    using MvvmCross.Platform.Converters;
    using Xamarin.Forms;

    public class TakeAwayWithdrawBackgroundColorByStatusValueConverter : MvxValueConverter<int?, Color>
    {
        #region MvxValueConverter
        protected override Color Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.Value == (int)OrderStatus.OPEN)
            {
                return Color.FromHex("#90a4ae");
            }
            else if (value.Value == (int)OrderStatus.READY_FOR_WITHDRAW)
            {
                return (Color)Application.Current.Resources["accent"];
            }
            else
            {
                return Color.Transparent;
            }
        }
        #endregion
    }
}
