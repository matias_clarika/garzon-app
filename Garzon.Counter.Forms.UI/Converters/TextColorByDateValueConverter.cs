﻿using System;
using Garzon.Config;
using Garzon.Core.Utils;
using MvvmCross.Platform;
using MvvmCross.Platform.Converters;
using Xamarin.Forms;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class TextColorByDateValueConverter : MvxValueConverter<DateTime, Color>
    {
        #region dependency
        readonly IAppConfig _appConfig;
        #endregion

        #region constructor
        public TextColorByDateValueConverter()
        {
            _appConfig = Mvx.Resolve<IAppConfig>();
        }
        #endregion
        protected override Color Convert(DateTime value,
                                            Type targetType,
                                            object parameter,
                                            System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                var diffTime = TimeSpan.FromMinutes(_appConfig.OrderCounterMinutes);
                var now = DateTime.Now.ToLocalTime();
                var seconds = now.ToLocalTime().Subtract(value.ToLocalTime()).TotalSeconds;

                if (seconds > diffTime.TotalSeconds)
                {
                    return GarzonColors.COUNTER_ITEM_ALERT_COLOR;
                }
            }
            if(parameter!=null && (bool)parameter){
                return GarzonColors.COUNTER_ITEM_HIGHLIGHT_DEFAULT;    
            }else{
                return GarzonColors.COUNTER_ITEM_DEFAULT;    
            }


        }
    }
}
