﻿using System;
using Garzon.Core.Resources;
using Garzon.DataObjects;
using MvvmCross.Platform.Converters;
using Xamarin.Forms;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class LabeledColorByOrderDetailStatusValueConverter : MvxValueConverter<int, Color>
    {
        #region MvxValueConverter
        protected override Color Convert(int value,
                                            Type targetType,
                                            object parameter,
                                            System.Globalization.CultureInfo culture)
        {
            var result = Color.Accent;

            switch (value)
            {
                case (int)OrderDetailStatus.OBSERVATION:
                    result = Color.FromHex(Colors.ORDER_DETAIL_STATUS_OBSERVED_HEX);
                    break;
                case (int)OrderDetailStatus.INCOUNTER:
                    result = Color.FromHex(Colors.ORDER_DETAIL_STATUS_OBSERVED_INCOUNTER_HEX);
                    break;
                case (int)OrderDetailStatus.PENDING:
                    result = Color.FromHex(Colors.ORDER_DETAIL_STATUS_OBSERVED_PENDING_HEX);
                    break;
            }

            return result;

        }
        #endregion
    }
}
