﻿namespace Garzon.Counter.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using I18NPortable;
    using MvvmCross.Platform.Converters;

    public class GetStringValueConverter : MvxValueConverter<string, string>
    {
        #region MvxValueConverter
        protected override string Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return I18N.Current[$"{parameter}"];
        }
        #endregion
    }
}
