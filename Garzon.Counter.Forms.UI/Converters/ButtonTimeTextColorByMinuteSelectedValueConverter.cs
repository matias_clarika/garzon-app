﻿namespace Garzon.Counter.Forms.UI.Converters
{
    using System;
    using System.Globalization;
    using MvvmCross.Platform.Converters;
    using Xamarin.Forms;

    public class ButtonTimeTextColorByMinuteSelectedValueConverter : MvxValueConverter<int?, Color>
    {
        protected override Color Convert(int? value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.HasValue && value.Value == int.Parse($"{parameter}"))
            {
                return Color.White;
            }
            else
            {
                return (Color)Application.Current.Resources["primaryDark"];
            }
        }
    }
}
