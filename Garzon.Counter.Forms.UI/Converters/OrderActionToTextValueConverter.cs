﻿using System;
using Garzon.Core.EventBus.Auth;
using MvvmCross.Platform.Converters;

namespace Garzon.Counter.Forms.UI.Converters
{
    public class OrderActionToTextValueConverter : MvxValueConverter<OrderNotificationAction, string>
    {

        #region override of MvxValueConverter
       protected override string Convert(OrderNotificationAction value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch(value){
                case OrderNotificationAction.COUNTER_ORDER:
                    return "Pedido";
                case OrderNotificationAction.COUNTER_WAITER:
                    return "Mozo";
                case OrderNotificationAction.COUNTER_ORDER_PAY:
                    return "Cuenta";
                default:
                    return "";
            }
        }
        #endregion
    }
}
