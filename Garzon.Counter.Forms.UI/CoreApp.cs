﻿using System.Threading.Tasks;
using Garzon.Core;
using Garzon.Core.DI;
using Garzon.Core.ViewModels;
using Garzon.Counter.Forms.UI.DI;
using Garzon.Counter.Forms.UI.Navigation;

namespace Garzon.Counter.Forms.UI
{
    public class CoreApp : MvvmCross.Core.ViewModels.MvxApplication, ICoreApp
    {
        private IDependencyInjector _dependencyInjector;

        public override void Initialize()
        {
            InitializeIoC();
            _dependencyInjector.Resolve<INavigationController>().OpenFirstPage(this);
        }

        private void InitializeIoC()
        {
            _dependencyInjector = DependencyManager.Builder();
            _dependencyInjector.Inject();
        }

        public async Task RegisterFirstPage<TViewModel>() where TViewModel : BaseViewModel
        {
            RegisterNavigationServiceAppStart<TViewModel>();
        }

    }
}
