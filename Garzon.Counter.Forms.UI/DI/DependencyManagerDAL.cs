﻿
using Garzon.DAL.DataSource.Remote;
using Garzon.DAL.DataSource.Remote.Implementation;
using Garzon.DAL.Repository;
using Garzon.DAL.Repository.Implementation;
using MvvmCross.Platform;

namespace Garzon.Counter.Forms.UI.DI
{
    public partial class DependencyManager
    {
        protected override void InjectDAL()
        {
            base.InjectDAL();
            InjectLocalDataSource();
            InjectRemoteDataSource();
            InjectRepository();
        }

        private void InjectLocalDataSource()
        {
        }

        private void InjectRemoteDataSource()
        {
            Mvx.LazyConstructAndRegisterSingleton<ISectorCounterRemoteDataSource, SectorCounterRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderCounterRemoteDataSource, OrderCounterRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderPayCounterRemoteDataSource, OrderPayCounterRemoteDataSource>();
            Mvx.LazyConstructAndRegisterSingleton<IWaiterCounterRemoteDataSource, WaiterCounterRemoteDataSourcce>();
        }

        private void InjectRepository()
        {
            Mvx.LazyConstructAndRegisterSingleton<ISectorRepository, SectorDataRepository>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderCounterRepository, OrderCounterDataRepository>();
        }
    }
}
