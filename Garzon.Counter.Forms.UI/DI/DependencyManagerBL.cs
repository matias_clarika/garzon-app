﻿namespace Garzon.Counter.Forms.UI.DI
{

    using Garzon.BL;
    using Garzon.BL.Implementation;
    using MvvmCross.Platform;

    public partial class DependencyManager
    {
        protected override void InjectBL()
        {
            base.InjectBL();
            Mvx.LazyConstructAndRegisterSingleton<ISectorBL, SectorBL>();
            Mvx.LazyConstructAndRegisterSingleton<IOrderCounterBL, OrderCounterBL>();
        }
    }
}
