﻿namespace Garzon.Counter.Forms.UI.DI
{

    using System;
    using Clarika.Xamarin.Base.Auth;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Garzon.Config;
    using Garzon.Config.DataSource;
    using Garzon.Core.Payments;
    using Garzon.Core.Payments.Implementation;
    using Garzon.Core.Views.Format;
    using Garzon.Counter.Forms.UI.EventBus.Auth;
    using Garzon.Counter.Forms.UI.Navigation;
    using Garzon.Counter.Forms.UI.Views.Format;
    using MvvmCross.Platform;
    public partial class DependencyManager
    {
        protected override void InjectCore()
        {
            base.InjectCore();
            InjectNavigation();
            InjectManagers();
            InjectGlobals();
            InjectByEnvironment();
            InjectFormat();
        }

        private void InjectByEnvironment()
        {
            string buildEnvironment = Mvx.Resolve<IEnvironment>().BuildEnvironment;

            switch (buildEnvironment)
            {
                case EnvironmentConstants.BUILD_ENVIRONMENT_PROD:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, CounterProdServerInfo>();
                    break;
                case EnvironmentConstants.BUILD_ENVIRONMENT_RELEASE:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, PreProdServerInfo>();
                    break;
                case EnvironmentConstants.BUILD_ENVIRONMENT_QA:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, QaServerInfo>();
                    break;
                case EnvironmentConstants.BUILD_ENVIRONMENT_DEV:
                    Mvx.ConstructAndRegisterSingleton<IServerInfo, DevServerInfo>();
                    break;
            }
        }

        private void InjectNavigation()
        {
            Mvx.LazyConstructAndRegisterSingleton<INavigationController, CounterNavigationController>();
        }

        private void InjectManagers()
        {
        }

        private void InjectGlobals()
        {
            Mvx.ConstructAndRegisterSingleton<AuthGlobalEventHandler, AuthGlobalEventHandler>();
        }

        private void InjectFormat()
        {
            Mvx.RegisterType<IFormatOrderDetail, FormatOrderDetail>();
        }

    }
}
