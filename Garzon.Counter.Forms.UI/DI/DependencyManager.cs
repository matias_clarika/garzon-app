﻿using Garzon.Core.DI;
using MvvmCross.Platform;

namespace Garzon.Counter.Forms.UI.DI
{
    public partial class DependencyManager : SharedDependencyInjector, IDependencyInjector
    {
        const bool MockEnabled = true;

        #region builder
        public static IDependencyInjector Builder(){
            Mvx.LazyConstructAndRegisterSingleton<IDependencyInjector, DependencyManager>();
            return Mvx.Resolve<IDependencyInjector>();
        }
        #endregion

        public static IDependencyInjector GetInstance(){
            return Mvx.Resolve<IDependencyInjector>();
        }

    }
}
