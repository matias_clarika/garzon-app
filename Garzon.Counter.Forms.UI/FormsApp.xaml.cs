﻿using System;
using System.Threading.Tasks;
using DLToolkit.Forms.Controls;
using Garzon.BL;
using Garzon.Core.i18n;
using Garzon.Core.Utils;
using MvvmCross.Forms.Platform;
using MvvmCross.Platform;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;

namespace Garzon.Counter.Forms.UI
{
    public partial class FormsApp : MvxFormsApplication
    {
        private Timer timerToSendOnline;

        public FormsApp()
        {
            InitializeComponent();
            Setup();
        }


        private void Setup()
        {
            FlowListView.Init();
            I18nSetup.Setup();
        }

        protected override async void OnStart()
        {
            base.OnStart();
            await OnStartOrOnResumeCalled();
        }

        protected override async void OnResume()
        {
            base.OnResume();
            await OnStartOrOnResumeCalled();
        }

        private async Task OnStartOrOnResumeCalled()
        {
            CrossConnectivity.Current.ConnectivityTypeChanged += Current_ConnectivityTypeChanged;
            await Task.Factory.StartNew(NotifyOnlineModeAndstartTimer);
        }

        private void NotifyOnlineModeAndstartTimer()
        {
            OnOnlineMode();
            StartTimerToSendOnlineMode();
        }

        private void StartTimerToSendOnlineMode()
        {
            StopTimerToSendOnlineMode();
            timerToSendOnline = Timer.StartNew(TimeSpan.FromMinutes(1), OnOnlineMode);
        }

        private void Current_ConnectivityTypeChanged(object sender, ConnectivityTypeChangedEventArgs e)
        {
            if (e.IsConnected)
            {
                NotifyOnlineModeAndstartTimer();
            }
            else
            {
                StopTimerToSendOnlineMode();
            }
        }

        private void StopTimerToSendOnlineMode()
        {
            if (timerToSendOnline != null)
            {
                timerToSendOnline.Stop();
                timerToSendOnline = null;
            }
        }

        private async void OnOnlineMode()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await NotifyOnline(true);
            }
        }

        private async Task<bool> NotifyOnline(bool isOnline)
        {
            try
            {
                var userBL = Mvx.Resolve<IUserBL>();
                if (userBL != null && userBL.FindCurrentUser() != null)
                {
                    return await userBL.IsCounterOnline(isOnline);
                }
                return false;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        protected override async void OnSleep()
        {
            base.OnSleep();
            StopTimerToSendOnlineMode();
            CrossConnectivity.Current.ConnectivityTypeChanged -= Current_ConnectivityTypeChanged;
        }
    }
}
