﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Component;
    using Garzon.Counter.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using System.Linq;
    using Garzon.BL;
    using Garzon.Core.EventBus;
    using Garzon.Counter.Forms.UI.EventBus.Order;
    using System.Collections.Generic;
    using Garzon.Core.Views.Format;
    using Garzon.BL.Abstraction.Exceptions;

    public class OrderCounterDetailViewModel : GarzonCounterBaseViewModel, IMvxViewModel<OrderCounterDetailViewModel.Params>
    {
        #region dependency
        readonly IOrderCounterBL _orderCounterBL;
        readonly IEventBus _eventBus;
        readonly IFormatOrderDetail _formatOrderDetail;
        #endregion

        #region private fields
        private OrderCounter orderCounter;
        public bool _visibilityConfirmOrderCounterButton = true;
        private string _title;
        private string observedButtonText;
        #endregion

        #region bindings
        public OrderCounter OrderCounter
        {
            get => this.orderCounter;
            set => this.SetProperty(ref this.orderCounter, value);
        }

        public string ObservedButtonText
        {
            get => this.observedButtonText;
            set => this.SetProperty(ref this.observedButtonText, value);
        }

        public SelectableObservableCollection<OrderDetail> OrderDetailList { get; } = new SelectableObservableCollection<OrderDetail>();
        public IMvxAsyncCommand<SelectableItem<OrderDetail>> ItemOrderDetailClickCommand => new MvxAsyncCommand<SelectableItem<OrderDetail>>(ItemOrderDetailClick);
        public IMvxAsyncCommand<OrderDetail> ItemOrderDetailCompoundClickCommand => new MvxAsyncCommand<OrderDetail>(ItemOrderDetailCompoundClick);
        public IMvxAsyncCommand SendOrderCounterCommand => new MvxAsyncCommand(TrySendOrderCounter);
        public IMvxAsyncCommand ObservedOrderCounterCommand => new MvxAsyncCommand(TryObservedOrderCounter);

        public bool VisibilityConfirmOrderCounterButton
        {
            get => _visibilityConfirmOrderCounterButton;
            set => SetProperty(ref _visibilityConfirmOrderCounterButton, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
        #endregion
        public OrderCounterDetailViewModel(IOrderCounterBL orderCounterBL,
                                           INavigationController navigationController,
                                           IEventBus eventBus,
                                           IFormatOrderDetail formatOrderDetail,
                                           ILogger logger,
                                           IAnalytics analytics,
                                           AppDefaultConfig appConfig,
                                           IDialogs dialogs) : base(navigationController,
                                                                           logger,
                                                                           analytics,
                                                                           appConfig, dialogs)
        {
            _orderCounterBL = orderCounterBL;
            _eventBus = eventBus;
            _formatOrderDetail = formatOrderDetail;
        }

        protected virtual string GetBaseTitle()
        {
            return Strings["Order"];
        }
        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            ObservedButtonText = this.OrderCounter.IsTakeAway() ? Strings["TakeAwayObservedButtonText"] : Strings["SendObservation"];
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            CheckStateButtons();
        }
        #endregion
        #region implementation of IMvxViewModel
        public void Prepare(Params parameter)
        {
            SetOrderCounter(parameter.OrderCounter);
        }

        #endregion
        private void SetOrderCounter(OrderCounter orderCounter)
        {
            OrderCounter = orderCounter;
            AddOrderDetails(OrderCounter.Details);
            SetTitle(OrderCounter.Details);
        }

        private void SetTitle(List<OrderDetail> details)
        {
            int count = details.Select((detail) => detail.Quantity).Sum();
            string productLabel = count > 1 ? "Items" : "Item";
            Title = $"{GetBaseTitle()} / {count} {productLabel}";
        }

        private async Task ItemOrderDetailClick(SelectableItem<OrderDetail> orderDetail)
        {
            CheckStateButtons();
        }

        private void CheckStateButtons()
        {
            VisibilityConfirmOrderCounterButton = OrderDetailList != null
                                                      && OrderDetailList.SelectedItems != null
                                                      && OrderDetailList.SelectedItems.ToList().Count == 0;
        }

        private async Task ItemOrderDetailCompoundClick(OrderDetail orderDetail)
        {
            if (orderDetail.IsCompound && orderDetail.Details.Count > 1)
            {
                var indexof = this.OrderDetailList.IndexOf(orderDetail);
                orderDetail = await this.navigationController.OpenDetailCompoundObservation(orderDetail);
                var isSelected = orderDetail.IsObserved();
                this.OrderDetailList.Replace(indexof, orderDetail, isSelected);
                CheckStateButtons();
            }

        }

        private void AddOrderDetails(List<OrderDetail> details)
        {
            OrderDetailList.Add(_formatOrderDetail.FormatToList(details));
        }

        private async Task TrySendOrderCounter()
        {
            if (await this.Validate())
            {
                var progress = dialogs.ProgressDialog("Enviando...");
                try
                {
                    await progress.Show();
                    var result = await _orderCounterBL.SendConfirmation(OrderCounter);
                    await progress.Hide();
                    NotifySent(OrderCounter);
                    await navigationController.Back(this);
                }
                catch (OrderCounterWasProcessedBLException e)
                {
                    await AnalyzeAndShowErrorByOrderCounterWasProcessedException(progress, e);
                }
                catch (OrderCounterPaymentRejectedBLException e1)
                {
                    await progress.Hide();
                    await this.ShowPaymentRejectedError(e1);
                }
                catch (Exception e2)
                {
                    await progress.Hide();
                    await this.ShowGenericError(progress, e2);
                }
            }
        }

        protected virtual Task<bool> Validate()
        {
            return Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// Notifies the sent.
        /// </summary>
        /// <param name="orderCounter">Order counter.</param>
        private void NotifySent(OrderCounter orderCounter)
        {
            _eventBus.Publish(OrderCounterSentEvent.Build(this,
                                                          orderCounter));
        }

        private async Task TryObservedOrderCounter()
        {
            var progress = dialogs.ProgressDialog("Enviando...");
            try
            {
                await progress.Show();
                ObservedOrderDetails();
                //clear time delay
                this.OrderCounter.TimeDelay = null;
                var result = await _orderCounterBL.SendObservation(OrderCounter);
                await progress.Hide();
                NotifySent(OrderCounter);
                await navigationController.Back(this);
            }
            catch (OrderCounterWasProcessedBLException e)
            {
                await AnalyzeAndShowErrorByOrderCounterWasProcessedException(progress, e);
            }
            catch (Exception e1)
            {
                System.Diagnostics.Debug.WriteLine(e1.Message);
                await progress.Hide();
                await dialogs.ShowError("Ocurrió un error al enviar la observación intente nuevamente");
            }
        }

        private async Task AnalyzeAndShowErrorByOrderCounterWasProcessedException(IProgressDialog progress, OrderCounterWasProcessedBLException e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            await progress.Hide();
            await dialogs.ShowError(Strings["OrderCounterWasProcessed"]);
            await this.navigationController.Back(this);
        }

        private void ObservedOrderDetails()
        {
            var listObserved = OrderDetailList.SelectedItems.ToList();
            foreach (var observed in listObserved)
            {
                var detailToObservation = OrderCounter.Details
                                                      .FirstOrDefault((filter) => filter.ServerId == observed.ServerId);
                if (detailToObservation != null)
                {
                    detailToObservation.Observed();
                    if (detailToObservation.Details.Any() && detailToObservation.Details.Count == 1)
                    {
                        detailToObservation.Details[0].Observed();
                    }
                }
            }
        }

        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "order_counter_detail";
        }

        private async Task ShowPaymentRejectedError(OrderCounterPaymentRejectedBLException e1)
        {
            System.Diagnostics.Debug.WriteLine(e1.Message);
            await this.dialogs.ShowAlert(Strings["OrderCounterPaymentRejectedWhenConfirmOrderErrorMessage"]);
            await this.navigationController.Back(this);
        }

        private async Task ShowGenericError(IProgressDialog progress, Exception e2)
        {
            System.Diagnostics.Debug.WriteLine(e2.Message);
            await progress.Hide();
            await dialogs.ShowError("Ocurrió un error al confirmar el pedido intente nuevamente");
        }

        public class Params
        {
            public OrderCounter OrderCounter
            {
                get;
                private set;
            }

            private Params(Builder builder)
            {
                this.OrderCounter = builder.OrderCounter;
            }

            public class Builder
            {

                private readonly OrderCounter _orderCounter;
                public OrderCounter OrderCounter
                {
                    get => _orderCounter;
                }

                private Builder(OrderCounter orderCounter)
                {
                    _orderCounter = orderCounter;
                }

                public static Builder ParamsBuilder(OrderCounter orderCounter)
                {
                    return new Builder(orderCounter);
                }

                public Params Build()
                {
                    return new Params(this);
                }

            }
        }
    }
}
