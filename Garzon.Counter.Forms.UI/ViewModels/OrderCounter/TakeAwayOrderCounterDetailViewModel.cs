﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.EventBus;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Format;
    using Garzon.Counter.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;

    public class TakeAwayOrderCounterDetailViewModel : OrderCounterDetailViewModel, IMvxViewModel<OrderCounterDetailViewModel.Params>
    {
        #region private fields
        private int? timeDelay;
        private string numberToWithdraw;
        #endregion
        #region bindings
        public int? TimeDelay
        {
            get => this.timeDelay;
            set
            {
                this.SetProperty(ref this.timeDelay, value);
                this.OrderCounter.TimeDelay = this.timeDelay;
            }
        }

        public string NumberToWithdraw
        {
            get => this.numberToWithdraw;
            set
            {
                this.SetProperty(ref this.numberToWithdraw, value);
                if (this.numberToWithdraw.Length > 0)
                {
                    this.OrderCounter.NumberToWithdraw = int.Parse(this.numberToWithdraw);
                }
                else
                {
                    this.OrderCounter.NumberToWithdraw = null;
                }
            }
        }

        public IMvxAsyncCommand<int?> TimeButtonCommand => new MvxAsyncCommand<int?>(this.OnTimeButtonClick);
        #endregion
        public TakeAwayOrderCounterDetailViewModel(IOrderCounterBL orderCounterBL,
                                                   INavigationController navigationController,
                                                   IEventBus eventBus,
                                                   IFormatOrderDetail formatOrderDetail,
                                                   ILogger logger,
                                                   IAnalytics analytics,
                                                   AppDefaultConfig appConfig,
                                                   IDialogs dialogs) : base(orderCounterBL, navigationController, eventBus, formatOrderDetail, logger, analytics, appConfig, dialogs)
        {
        }

        #region override
        protected override string GetBaseTitle()
        {
            return Strings["TakeAwayTitle"];
        }

        protected override async Task<bool> Validate()
        {
            var result = await base.Validate() && this.OrderCounter.TimeDelay.HasValue;
            if (!result)
            {
                await this.ShowMessage(Strings["TimeDelayIsRequired"]);
            }
            return result;
        }
        #endregion

        #region private

        private Task OnTimeButtonClick(int? minutes)
        {
            return Task.Factory.StartNew(() =>
            {
                if (minutes.HasValue && this.TimeDelay.HasValue && minutes.Value == this.TimeDelay.Value)
                {
                    this.TimeDelay = null;
                }
                else
                {
                    this.TimeDelay = minutes;
                }
            });

        }
        #endregion


    }
}
