﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.BL.Abstraction.Exceptions;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Component;
    using Garzon.Counter.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;

    public class SectorListViewModel : GarzonCounterBaseViewModel
    {
        #region dependency
        protected readonly ISectorBL sectorBL;
        protected readonly IUserBL userBL;
        #endregion
        #region bindings
        public SelectableObservableCollection<SectorCounter> SectorList { get; } = new SelectableObservableCollection<SectorCounter>();

        public IMvxAsyncCommand ConfigureSectorCommand => new MvxAsyncCommand(OnConfigureSector);

        public List<SectorCounter> Sectors { get; private set; }

        private bool isSimpleCounter = true;
        public bool IsSimpleCounter
        {
            get => this.isSimpleCounter;
            set => SetProperty(ref this.isSimpleCounter, value);
        }
        #endregion
        #region fields
        private bool firstTime = true;
        #endregion
        #region constructor
        public SectorListViewModel(INavigationController navigationController,
                                   ILogger logger,
                                   IAnalytics analytics,
                                   AppDefaultConfig appConfig,
                                   IDialogs dialogs,
                                   ISectorBL sectorBL,
                                   IUserBL userBL) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            this.sectorBL = sectorBL;
            this.userBL = userBL;
        }
        #endregion
        #region lifecycle

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await InitializeOnViewAppeared();
            await CheckLoadSectors();
        }

        private async Task InitializeOnViewAppeared()
        {
            if (this.firstTime)
            {
                this.firstTime = false;
                IsSimpleCounter = !(await this.userBL.GetCurrentCounter()).Main;
            }
        }
        #endregion

        #region private

        private async Task CheckLoadSectors()
        {
            if (!this.SectorList.Any())
            {
                await LoadSectors();
            }
        }

        private Task LoadSectors()
        {
            return Task.Factory.StartNew(async () =>
             {
                 var progress = this.dialogs.ProgressDialog(Strings["GettingTheSectorAvailables"]);

                 try
                 {
                     await progress.Show();
                     this.Sectors = await this.sectorBL.FindSectors();
                     AddSectorsToList();
                 }
                 catch (Exception e)
                 {
                     System.Diagnostics.Debug.WriteLine(e.Message);
                     ShowError(e);
                 }
                 finally
                 {
                     await progress.Hide();
                 }
             });
        }

        private Task OnConfigureSector()
        {
            return ConfigureSectors();
        }

        private async Task ConfigureSectors(bool force = false)
        {
            var progress = this.dialogs.ProgressDialog(Strings["SetupSectors"]);
            try
            {
                await progress.Show();
                SetupLinkedSectors();
                await this.sectorBL.ChooseSectors(this.Sectors, force);
                await this.dialogs.ShowAlert(Strings["ChooseSectorSuccessMessage"]);
                await progress.Hide();
                await this.navigationController.Back(this);
            }
            catch (ChooseSectorLinkedException e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                await progress.Hide();
                await QuestionToForceChooseSector();
            }
            catch (Exception e1)
            {
                System.Diagnostics.Debug.WriteLine(e1.Message);
                await progress.Hide();
                ShowError(e1);
            }
        }

        private async Task QuestionToForceChooseSector()
        {
            var confirm = await this.dialogs.ShowConfirmDialog(Strings["QuestionForceChooseSectorTitle"],
                                                               Strings["QuestionForceShooseSectorMessage"],
                                                               Strings["Accept"],
                                                               Strings["Cancel"]);
            if (confirm)
            {
                ConfigureSectors(true);
            }
        }

        private void SetupLinkedSectors()
        {
            var idsSelected = this.SectorList.SelectedItems.Select((arg) => arg.Id);
            foreach (var sector in this.Sectors)
            {
                sector.Linked = idsSelected.Contains(sector.Id);
            }
        }

        private void ShowError(Exception e)
        {
            this.dialogs.ShowError(e.Message);
        }

        private void AddSectorsToList()
        {
            var selectableItems = this.Sectors.Select((arg) => new SelectableItem<SectorCounter>(arg, arg.Linked)).ToList();
            if (selectableItems.Any())
            {
                this.SectorList.AddRange(selectableItems);
            }
        }
        #endregion
        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "sectors";
        }
        #endregion
    }
}
