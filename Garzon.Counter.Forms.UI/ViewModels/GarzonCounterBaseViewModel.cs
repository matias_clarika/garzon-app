﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.ViewModels;
    using Garzon.Counter.Forms.UI.Navigation;
    using Plugin.Connectivity;

    public abstract class GarzonCounterBaseViewModel : BaseViewModel
    {
        #region dependency
        readonly protected INavigationController navigationController;
        #endregion

        public GarzonCounterBaseViewModel(INavigationController navigationController,
                                          ILogger logger,
                                          IAnalytics analytics,
                                          AppDefaultConfig appConfig,
                                          IDialogs dialogs) : base(logger,
                                                                      analytics,
                                                                      appConfig,
                                                                      dialogs)
        {
            this.navigationController = navigationController;
        }

        /// <summary>
        /// Shows the message.
        /// </summary>
        /// <returns>The message.</returns>
        /// <param name="message">Message.</param>
        protected Task ShowMessage(string message)
        {
            return this.dialogs.ShowAlert(message);
        }

        protected void ShowError(string message = null)
        {
            if (String.IsNullOrEmpty(message))
            {
                message = Strings["ErrorConnectionDeviceAfterTime"];
            }
            dialogs.ShowAlert(message);
        }

        /// <summary>
        /// Checks the connection.
        /// </summary>
        /// <returns><c>true</c>, if connection was checked, <c>false</c> otherwise.</returns>
        protected bool CheckConnection()
        {
            return CrossConnectivity.Current.IsConnected;
        }

        /// <summary>
        /// Checks the connection with alert.
        /// </summary>
        /// <returns>The connection with alert.</returns>
        protected async Task<bool> CheckConnectionWithAlert()
        {
            if (!this.CheckConnection())
            {
                await this.dialogs.ShowAlert(Strings["ErrorConnectionDeviceAfterTime"]);
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
