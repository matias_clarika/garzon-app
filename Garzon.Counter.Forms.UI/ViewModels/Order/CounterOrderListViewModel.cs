﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Counter.Forms.UI.Navigation;
using Garzon.DataObjects;
using System.Linq;
using MvvmCross.Core.ViewModels;

namespace Garzon.Counter.Forms.UI.ViewModels
{
    public class CounterOrderListViewModel : GarzonCounterBaseViewModel
    {
        #region dependency
        protected readonly IOrderCounterBL OrderCounterBL;
        #endregion

        #region private fields
        private bool isLoading = false;
        private string title;
        private string emptyViewIconStyleName;
        private string emptyViewLabelText;
        private bool isVisibleEmptyView;
        #endregion
        #region fields
        public MvxObservableCollection<Order> OrderList { get; } = new MvxObservableCollection<Order>();

        public IMvxAsyncCommand<Order> OrderItemClickCommand => new MvxAsyncCommand<Order>(OrderItemClick);

        public bool IsLoading
        {
            get => this.isLoading;
            set => this.SetProperty(ref this.isLoading, value);
        }

        public string Title
        {
            get => this.title;
            set => this.SetProperty(ref this.title, value);
        }

        public string EmptyViewIconStyleName
        {
            get => this.emptyViewIconStyleName;
            set => this.SetProperty(ref this.emptyViewIconStyleName, value);
        }

        public string EmptyViewLabelText
        {
            get => this.emptyViewLabelText;
            set => this.SetProperty(ref this.emptyViewLabelText, value);
        }

        public bool IsVisibleEmptyView
        {
            get => this.isVisibleEmptyView;
            set => this.SetProperty(ref this.isVisibleEmptyView, value);
        }
        #endregion
        public CounterOrderListViewModel(INavigationController navigationController,
                                         ILogger logger,
                                         IAnalytics analytics,
                                         AppDefaultConfig appConfig,
                                         IDialogs dialogs,
                                         IOrderCounterBL orderCounterBL) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            OrderCounterBL = orderCounterBL;
        }

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            this.SetupEmptyView();
            this.SetupTitle();
        }

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            await Load();
            this.SetupTitle();
        }

        public override void ViewDisappearing()
        {
            base.ViewDisappearing();
            this.SetupTitle();
        }
        #endregion

        #region protected

        protected Task Reload()
        {
            this.OrderList.Clear();
            return this.Load();
        }

        protected async Task Load()
        {
            if (!this.IsLoading)
            {
                this.IsVisibleEmptyView = false;
                this.OrderList.Clear();
                this.IsLoading = true;
                try
                {
                    List<Order> result = await GetOrders();
                    this.IsLoading = false;
                    await AddOrders(result);
                }
                catch (Exception)
                {
                    this.IsLoading = false;
                    await ShowGenericGetError();
                }
                this.SetupTitle();
            }
        }


        protected virtual async Task<List<Order>> GetOrders()
        {
            return await OrderCounterBL.GetOrders();
        }

        protected virtual async Task ShowGenericGetError()
        {
            await this.ShowMessage("No se pudieron obtener las mesas activas, por favor verifique su conexión a internet.");
        }

        protected virtual async Task ShowEmptyOrderMessage()
        {
            await this.ShowMessage("No existen mesas activas");
        }

        protected virtual Task OrderItemClick(Order order)
        {
            return navigationController.OpenOrderDetail(order);
        }
        #endregion

        protected virtual void SetupTitle(string baseTitle = null)
        {
            if (string.IsNullOrEmpty(baseTitle))
            {
                baseTitle = Strings["ActiveTables"];
            }

            int pendingCounter = this.GetPendingCounter();
            if (pendingCounter > 0)
            {
                baseTitle = $"{baseTitle}({pendingCounter})";
            }
            this.Title = baseTitle;
        }

        protected virtual void SetupEmptyView()
        {
            this.EmptyViewIconStyleName = "OrderListEmptyIcon";
            this.EmptyViewLabelText = Strings["OrderListEmptyText"];
        }

        protected virtual int GetPendingCounter()
        {
            return this.OrderList.Count;
        }

        private Task AddOrders(List<Order> orders)
        {
            return Task.Factory.StartNew(() =>
            {
                if (orders == null || !orders.Any())
                {
                    this.IsVisibleEmptyView = true;
                }
                else
                {
                    this.IsVisibleEmptyView = false;
                    OrderList.AddRange(orders);
                }
            });

        }

        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "order_counter_list";
        }
    }
}
