﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Core.Views.Component;
    using Garzon.Counter.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;

    public class DetailCompoundObservationViewModel : GarzonCounterBaseViewModel, IMvxViewModel<DetailCompoundObservationViewModel.Params, OrderDetail>
    {
        #region fields
        private OrderDetail parentOrderDetail;
        #endregion
        #region binding
        private string title;
        public string Title
        {
            get => this.title;
            set => SetProperty(ref this.title, value);
        }
        private bool visibilityObservedButton;

        public bool VisibilityObservedButton
        {
            get => this.visibilityObservedButton;
            set => SetProperty(ref this.visibilityObservedButton, value);
        }
        public SelectableObservableCollection<OrderDetail> OrderDetailList { get; } = new SelectableObservableCollection<OrderDetail>();
        public IMvxAsyncCommand<SelectableItem<OrderDetail>> ItemOrderDetailClickCommand => new MvxAsyncCommand<SelectableItem<OrderDetail>>(ItemOrderDetailClick);
        public IMvxAsyncCommand ObservedOrderCounterCommand => new MvxAsyncCommand(ObservedOrderDetail);
        #endregion

        public DetailCompoundObservationViewModel(INavigationController navigationController,
                                                  ILogger logger,
                                                  IAnalytics analytics,
                                                  AppDefaultConfig appConfig,
                                                  IDialogs dialogs) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
        }

        #region IMvxViewModel
        public void Prepare(Params parameter)
        {
            SetParentOrderDetail(parameter.ParentOrderDetail);
        }

        private void SetParentOrderDetail(OrderDetail parentOrderDetail)
        {
            this.parentOrderDetail = parentOrderDetail;
            Title = parentOrderDetail.Product.Name;
            AddItems();
        }
        public class Params
        {
            readonly OrderDetail parentOrderDetail;

            private Params(OrderDetail parentOrderDetail)
            {
                this.parentOrderDetail = parentOrderDetail;
            }

            public static Params Build(OrderDetail parentOrderDetail)
            {
                return new Params(parentOrderDetail);
            }

            public OrderDetail ParentOrderDetail
            {
                get => this.parentOrderDetail;
            }
        }
        #endregion

        #region private

        private void AddItems()
        {
            var list = this.parentOrderDetail.Details.Select(detail => new SelectableItem<OrderDetail>(detail, detail.IsObserved())).ToList();
            this.OrderDetailList.AddRange(list);
        }

        #endregion
        #region actions

        private async Task ItemOrderDetailClick(SelectableItem<OrderDetail> arg)
        {
            VisibilityObservedButton = this.OrderDetailList != null
                                                      && this.OrderDetailList.SelectedItems != null
                                                      && this.OrderDetailList.SelectedItems.ToList().Count > 0;
        }

        private async Task ObservedOrderDetail()
        {
            var observedDetails = this.OrderDetailList.SelectedItems;
            var existObserved = false;
            foreach (var detail in parentOrderDetail.Details)
            {
                var exist = observedDetails.FirstOrDefault(filter => filter.ServerId.Value == detail.ServerId.Value);
                if (exist != null)
                {
                    detail.Observed();
                    existObserved = true;
                }
                else
                {
                    detail.SetInCounter();
                }
            }

            if (existObserved)
            {
                this.parentOrderDetail.Observed();
            }
            else
            {
                this.parentOrderDetail.SetInCounter();
            }

            await this.navigationController.Back(this, this.parentOrderDetail);
        }

        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "counter_detail_compound";
        }
        #endregion


    }
}
