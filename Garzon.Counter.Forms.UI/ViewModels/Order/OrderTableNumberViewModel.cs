﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Counter.Forms.UI.Navigation;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;

namespace Garzon.Counter.Forms.UI.ViewModels
{
    public class OrderTableNumberViewModel : GarzonCounterBaseViewModel, IMvxViewModel<OrderTableNumberViewModel.Params>
    {
        #region dependency
        readonly IOrderCounterBL _orderCounterBL;
        #endregion
        #region private fields
        private string _title;

        private Order order;
        private Sector _selectedSector;
        private string _tableNumber;
        private ObservableCollection<Sector> _sectorList = new ObservableCollection<Sector>();
        private bool isInResto;
        #endregion
        #region binding fields

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public Sector SelectedSector
        {
            get => _selectedSector;
            set => SetProperty(ref _selectedSector, value);
        }

        public string TableNumber
        {
            get => _tableNumber;
            set => SetProperty(ref _tableNumber, value);
        }

        public ObservableCollection<Sector> SectorList
        {
            get => _sectorList;
            set => SetProperty(ref _sectorList, value);
        }

        public bool IsInResto
        {
            get => this.isInResto;
            set => this.SetProperty(ref this.isInResto, value);
        }

        public IMvxAsyncCommand ChangeTableNumberCommand => new MvxAsyncCommand(ChangeTableNumber);
        public IMvxAsyncCommand BlockUsersCommand => new MvxAsyncCommand(BlockUsersQuestion);

        #endregion

        public OrderTableNumberViewModel(INavigationController navigationController,
                                         ILogger logger,
                                         IAnalytics analytics,
                                         AppDefaultConfig appConfig,
                                         IDialogs dialogs,
                                         IOrderCounterBL orderCounterBL) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            _orderCounterBL = orderCounterBL;
        }

        #region IMvxViewModel
        public void Prepare(Params parameter)
        {
            order = parameter.Order;
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            this.IsInResto = order.IsInResto();
            SetupTitle();
            LoadSectors();
        }
        #endregion

        #region private
        /// <summary>
        /// Setups the title.
        /// </summary>
        private void SetupTitle()
        {
            Title = this.IsInResto ? $"{order.Sector} - M {order.TableNumber}" : $"{Strings["TakeAwayTitle"]} - Nº {order.NumberToWithdraw.Value}";
        }
        /// <summary>
        /// Loads the sectors.
        /// </summary>
        private void LoadSectors()
        {
            if (this.IsInResto)
            {
                if (order.Place != null
                && order.Place.Sectors != null
                && order.Place.Sectors.Any())
                {
                    foreach (var sector in order.Place.Sectors)
                    {
                        SectorList.Add(sector);
                    }
                }
                else
                {
                    navigationController.Back(this);
                    dialogs.ShowAlert("Comuniquese con soporte, hay un error al tratar de obtener los sectores de la orden");
                }
            }
        }


        private bool ValidateSector()
        {
            if (SelectedSector == null)
            {
                dialogs.ShowAlert("Debe seleccionar un sector");
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool ValidateTableNumber()
        {
            try
            {
                return !String.IsNullOrEmpty(TableNumber) && int.Parse(TableNumber) > 0;
            }
            catch (Exception e)
            {
                dialogs.ShowAlert("Número de mesa incorrecto.");
                return false;
            }
        }
        #endregion
        #region actions

        private async Task ChangeTableNumber()
        {
            if (ValidateSector() && ValidateTableNumber())
            {
                var progress = dialogs.ProgressDialog("");
                try
                {
                    await progress.Show();
                    var result = await _orderCounterBL.ChangeTableNumberByOrderId(SelectedSector.Id,
                                                                                  int.Parse(TableNumber),
                                                                                  order.ServerId.Value);
                    progress.Hide();
                    Back();
                }
                catch (Exception e)
                {
                    await progress.Hide();
                    dialogs.ShowAlert("No se pudo cambiar la mesa. Puede que exista una orden abierta en la mesa a la cual quiere cambiarse.");
                }
            }
        }

        private Task Back()
        {
            return navigationController.Back(this);
        }

        private async Task BlockUsersQuestion()
        {
            var confirm = await dialogs.ShowConfirmDialog("¿Está seguro que desea bloquear a los usuarios?",
                                                           "La orden se cerrará y los usuarios dentro de la misma serán bloqueados",
                                                           "Aceptar",
                                                           "Cancelar");
            if (confirm)
            {
                await BlockUsers();

            }
        }

        private async Task BlockUsers()
        {
            var progress = dialogs.ProgressDialog("");
            try
            {
                await progress.Show();
                var result = await _orderCounterBL.BlockUsersByOrder(order);
                await progress.Hide();
                Back();
            }
            catch (Exception e)
            {
                await progress.Hide();
                dialogs.ShowAlert("Ocurrió un error al intentar bloquear a los usuarios");
            }
        }

        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "order_table_number";
        }
        #endregion
        #region params
        public class Params
        {
            readonly Order _order;
            public Order Order
            {
                get => _order;
            }
            private Params(Order order)
            {
                _order = order;
            }

            public static Params Builder(Order order)
            {
                return new Params(order);
            }
        }
        #endregion
    }


}
