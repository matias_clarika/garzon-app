﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.BL;
using Garzon.Config;
using Garzon.Core.Analytics;
using Garzon.Core.Utils;
using Garzon.Core.Views.Format;
using Garzon.Counter.Forms.UI.Navigation;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;

namespace Garzon.Counter.Forms.UI.ViewModels
{
    public class OrderDetailViewModel : GarzonCounterBaseViewModel, IMvxViewModel<OrderDetailViewModel.Params>
    {
        #region dependency
        protected readonly IFormatOrderDetail formatOrderDetail;
        protected readonly IOrderCounterBL orderCounterBL;
        #endregion

        #region private fields
        private Order order;
        private String title;
        private double amount = 0;
        private bool isVisibleCloseButton = false;
        private bool firstTime = true;
        #endregion

        #region properties binding
        public String Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }

        public double Amount
        {
            get => this.amount;
            set => SetProperty(ref this.amount, value);
        }

        public bool IsVisibleCloseButton
        {
            get => this.isVisibleCloseButton;
            set => this.SetProperty(ref this.isVisibleCloseButton, value);
        }

        public IMvxAsyncCommand ClosedCommand => new MvxAsyncCommand(CloseOrderQuestion);
        public MvxObservableCollection<OrderDetail> OrderDetailList { get; } = new MvxObservableCollection<OrderDetail>();
        public IMvxAsyncCommand OptionCommand => new MvxAsyncCommand(GoOptionByOrder);
        #endregion
        public OrderDetailViewModel(INavigationController navigationController,
                                    ILogger logger,
                                    IAnalytics analytics,
                                    AppDefaultConfig appConfig,
                                    IDialogs dialogs,
                                    IFormatOrderDetail formatOrderDetail,
                                    IOrderCounterBL orderCounterBL) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            this.formatOrderDetail = formatOrderDetail;
            this.orderCounterBL = orderCounterBL;
        }

        #region IMvxViewModel
        public void Prepare(OrderDetailViewModel.Params parameter)
        {
            SetupOrder(parameter.Order);
        }
        #endregion

        #region lifecycle
        public override void ViewAppeared()
        {
            base.ViewAppeared();
            this.InitializeUI();
        }
        #endregion
        #region actions

        private void InitializeUI()
        {
            if (this.firstTime)
            {
                this.firstTime = false;
                OrderDetailList
                .AddRange(formatOrderDetail
                          .FormatToList(order.Details));
                this.Amount = this.order.Amount.Value;
                this.IsVisibleCloseButton = this.order.IsInResto();
            }
        }
        private Task GoOptionByOrder()
        {
            return navigationController.OpenChangeTableNumber(order);
        }

        private async Task CloseOrderQuestion()
        {
            var confirm = await dialogs.ShowConfirmDialog("¿Esta seguro que quiere cerrar la orden?",
                                                           "La orden no podra recuperarse una vez cerrada.",
                                                           "Aceptar",
                                                           "Cancelar");
            if (confirm)
            {
                await ClosedOrder();

            }
        }

        private async Task ClosedOrder()
        {
            var progress = dialogs.ProgressDialog("");
            try
            {
                await progress.Show();
                var result = await orderCounterBL.ClosedOrder(order);
                await progress.Hide();
                Back();
            }
            catch (Exception e)
            {
                await progress.Hide();
                dialogs.ShowAlert("Ocurrió un error al intentar eliminar la orden, intenté nuevamente.");
            }
        }

        #endregion

        #region private

        private void SetupOrder(Order order)
        {

            this.order = order;
            SetupTitle(order);
        }

        private void SetupTitle(Order order)
        {
            Title = order.IsInResto() ? $"{order.Sector} - M {order.TableNumber}" : $"{Strings["TakeAwayTitle"]} - Nº {order.NumberToWithdraw.Value}";
        }

        private Task Back()
        {
            return navigationController.Back(this);
        }

        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "order_detail";
        }
        #endregion
        #region Params
        public class Params
        {
            private readonly Order _order;

            private Params(Order order)
            {
                _order = order;
            }

            public Order Order
            {
                get => _order;
            }

            public static Params Builder(Order order)
            {
                return new Params(order);
            }
        }
        #endregion
    }
}
