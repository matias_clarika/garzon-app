﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.EventBus;
    using Garzon.Core.EventBus.Auth;
    using Garzon.Core.Utils;
    using Garzon.Counter.Forms.UI.Navigation;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using MvvmCross.Platform;

    public class TakeAwayViewModel : CounterOrderListViewModel
    {
        #region dependencies
        protected readonly IUserBL userBL;
        protected readonly IEventBus eventBus;
        #endregion
        #region private fields
        private bool isMain = true;
        private IEventToken takeAwayToken;
        private bool inPage = false;
        private string pendingCount = string.Empty;
        #endregion
        #region bindings
        public string PendingCount
        {
            get => this.pendingCount;
            set => this.SetProperty(ref this.pendingCount, value);
        }

        public IMvxAsyncCommand<Order> TakeAwayWithdrawCommand => new MvxAsyncCommand<Order>(this.OnTakeAwayWithdraw);
        public IMvxAsyncCommand<Order> TakeAwayDeliveredCommand => new MvxAsyncCommand<Order>(this.OnTakeAwayDelivered);
        #endregion
        public TakeAwayViewModel(INavigationController navigationController,
                                 ILogger logger,
                                 IAnalytics analytics,
                                 AppDefaultConfig appConfig,
                                 IDialogs dialogs,
                                 IOrderCounterBL orderCounterBL,
                                 IUserBL userBL,
                                 IEventBus eventBus) : base(navigationController, logger, analytics, appConfig, dialogs, orderCounterBL)
        {
            this.userBL = userBL;
            this.eventBus = eventBus;
        }

        #region lifecycle
        public override async Task Initialize()
        {
            isMain = (await this.userBL.GetCurrentCounter()).Main;
            await base.Initialize();
            if (isMain)
            {
                await this.Subscribe();
                this.PreLoad();
            }
        }

        private async Task PreLoad()
        {
            await this.Load();
            this.SetupCounter();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            this.inPage = true;
            this.PendingCount = string.Empty;
        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            this.inPage = false;
            this.SetupCounter();
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            base.ViewDestroy(viewFinishing);
            this.Unsubscribe();
        }
        #endregion

        #region override

        protected override Task<List<Order>> GetOrders()
        {
            return !isMain ? Task.Factory.StartNew(() => default(List<Order>)) : this.OrderCounterBL.GetOrdersTakeAway();
        }

        protected override void SetupTitle(string baseTitle = null)
        {
            this.Title = Strings["TakeAwayTitle"];
        }

        protected override void SetupEmptyView()
        {
            this.EmptyViewIconStyleName = "TakeAwayListEmptyIcon";
            if (isMain)
            {
                this.EmptyViewLabelText = Strings["TakeAwayListEmptyText"];
            }
            else
            {
                this.EmptyViewLabelText = Strings["TakeAwayListEmptyTextSecondaryCounter"];
            }

        }

        protected override int GetPendingCounter()
        {
            return this.OrderList.Count((arg) => !arg.IsClosed());
        }

        protected override Task OrderItemClick(Order order)
        {
            if (!order.Details.Any())
            {
                return this.dialogs.ShowAlert(Strings["OrderDetailTakeAwayWithoutDetailMessage"]);
            }
            else
            {
                return base.OrderItemClick(order);
            }
        }
        #endregion
        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "take away";
        }
        #endregion

        #region events
        protected async Task Subscribe()
        {
            this.takeAwayToken = await this.eventBus.SubscribeOnThreadPoolThread<OrderEvent>(OnNewTakeAway);
        }

        protected void Unsubscribe()
        {
            this.eventBus.Unsubscribe<OrderEvent>(this.takeAwayToken);
        }

        private void OnNewTakeAway(OrderEvent obj)
        {
            if (obj.Action == OrderNotificationAction.ORDER_COUNTER_TAKE_AWAY_CONFIRM)
            {
                this.UpCount();
            }
        }
        #endregion
        #region private


        private void SetupCounter(int more = 0)
        {
            if (!this.inPage)
            {
                var count = this.OrderList.Where((arg) => !arg.IsClosed()).ToList().Count;

                if (more > 0)
                {
                    count = count + more;
                }

                if (count > 0)
                {
                    this.PendingCount = $"{count}";
                }
            }
            else if (more == 0)
            {
                this.PendingCount = string.Empty;
            }
            else
            {
                var count = 0;
                if (!string.IsNullOrEmpty(this.PendingCount))
                {
                    count = int.Parse(this.PendingCount);
                }
                count = count + more;
                this.PendingCount = $"{count}";
            }
        }

        private void UpCount()
        {
            this.SetupCounter(1);
        }

        private async Task OnTakeAwayWithdraw(Order order)
        {
            if (await this.CheckConnectionWithAlert())
            {
                var progress = this.dialogs.ProgressDialog(string.Empty);
                try
                {
                    await progress.Show();
                    bool result = await this.OrderCounterBL.MarkWithdraw(order);
                    await progress.Hide();
                    if (result)
                    {
                        await this.Reload();
                    }

                }
                catch (Exception e)
                {
                    await progress.Hide();
                }
            }
        }

        private async Task OnTakeAwayDelivered(Order order)
        {
            if (await this.CheckConnectionWithAlert() && !order.IsClosed())
            {
                var progress = this.dialogs.ProgressDialog(string.Empty);
                try
                {
                    await progress.Show();
                    bool result = await this.OrderCounterBL.MarkDelivered(order);
                    await progress.Hide();
                    if (result)
                    {
                        await this.Reload();
                    }
                }
                catch (Exception)
                {
                    await progress.Hide();
                }
            }
        }
        #endregion
    }
}
