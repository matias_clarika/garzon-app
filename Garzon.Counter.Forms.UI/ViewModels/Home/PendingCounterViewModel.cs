﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.EventBus;
    using Garzon.Core.EventBus.Auth;
    using Garzon.Core.Utils;
    using Garzon.Counter.Forms.UI.Navigation;
    using Xamarin.Forms;
    using System.Linq;
    using Garzon.Counter.Forms.UI.EventBus;
    using MvvmCross.Core.ViewModels;
    using Plugin.Connectivity;
    using Garzon.Core.Managers;

    public partial class PendingCounterViewModel : GarzonCounterBaseViewModel
    {
        #region dependency
        protected readonly IOrderCounterBL orderCounterBL;
        protected readonly IEventBus eventBus;
        protected readonly ISectorBL sectorBL;
        protected readonly IUserBL userBL;
        #endregion

        #region fields
        IEventToken _orderCounterToken;
        IProgressDialog _progressDialog;

        private string pendingCount;


        public string PendingCount
        {
            get => this.pendingCount;
            set => this.SetProperty(ref this.pendingCount, value);
        }

        private bool inPage = false;
        #endregion

        public PendingCounterViewModel(IOrderCounterBL orderCounterBL,
                             IEventBus eventBus,
                             INavigationController navigationController,
                             IDialogs dialogs,
                             AppDefaultConfig appConfig,
                             ILogger logger,
                             IAnalytics analytics,
                             ISectorBL sectorBL,
                             IUserBL userBL) : base(navigationController,
                                                                 logger,
                                                                 analytics,
                                                                 appConfig,
                                                                 dialogs)
        {
            this.orderCounterBL = orderCounterBL;
            this.eventBus = eventBus;
            this.sectorBL = sectorBL;
            this.userBL = userBL;
        }

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            await Subscribe();
        }

        public override async void ViewAppeared()
        {
            base.ViewAppeared();
            this.inPage = true;
            this.PendingCount = string.Empty;
            if (!await CheckForceLogout())
            {
                ClearAllList();
                await CheckSectorOrRefreshData();
            }
        }

        private async Task<bool> CheckForceLogout()
        {
            var forceLogout = await this.userBL.ForceLogout();
            if (forceLogout)
            {
                await this.navigationController.ReOpenApp();
            }
            return forceLogout;
        }

        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            this.inPage = false;
            this.SetupCounter();
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            base.ViewDestroy(viewFinishing);
            StopAllTimers();
            Unsubscribe();
        }
        #endregion

        #region events
        protected async Task Subscribe()
        {
            _orderCounterToken = await eventBus.SubscribeOnThreadPoolThread<OrderEvent>(OnOrderCounter);
        }

        protected void Unsubscribe()
        {
            eventBus.Unsubscribe<OrderEvent>(_orderCounterToken);
        }
        #endregion

        private void ClearAllList()
        {
            ClearOrderCounterList();
            ClearWaiterCounterList();
            ClearOrderPayCounterList();
        }

        private async Task CheckSectorOrRefreshData()
        {
            if (await CheckSectors())
            {
                await RefreshData();
            }
            else
            {
                await this.dialogs.ShowAlert(Strings["CounterWithoutSectorMessage"]);
                await GoSectors();
            }
        }

        private async Task<bool> CheckSectors()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    var sectors = await this.sectorBL.FindSectors();
                    return sectors.Exists((obj) => obj.Linked);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return true;
                }
            }
            return true;
        }

        private async Task GoSectors()
        {
            if (await this.CheckConnectionWithAlert())
            {
                await this.navigationController.OpenSectors();
            }
        }

        private void OnOrderCounter(OrderEvent orderCounterEvent)
        {
            AddByObject(orderCounterEvent);
        }

        private void SetupCounter(int more = 0)
        {
            if (!this.inPage)
            {
                var count = this.OrderCounterList.Count;

                if (more > 0)
                {
                    count = count + more;
                }

                if (count > 0)
                {
                    this.PendingCount = $"{count}";
                }
            }
            else if (more == 0)
            {
                this.PendingCount = string.Empty;
            }
            else
            {
                var count = 0;
                if (!string.IsNullOrEmpty(this.PendingCount))
                {
                    count = int.Parse(this.PendingCount);
                }
                count = count + more;
                this.PendingCount = $"{count}";
            }
        }

        private void UpCount()
        {
            this.SetupCounter(1);
        }

        private void AddByObject(OrderEvent orderCounterEvent)
        {
            switch (orderCounterEvent.Action)
            {
                case OrderNotificationAction.COUNTER_ORDER_PAY:
                    AddOrderPayCounter(orderCounterEvent.ToOrderPayCounter());
                    break;
                case OrderNotificationAction.COUNTER_WAITER:
                    AddWaiterCounter(orderCounterEvent.ToWaiterCounter());
                    break;
                case OrderNotificationAction.COUNTER_ORDER:
                    AddOrderCounter(orderCounterEvent.ToOrderCounter());
                    break;
                case OrderNotificationAction.ORDER_COUNTER_NEED_REFRESH:
                    RefreshData();
                    break;
            }
        }

        private async Task RefreshData()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await OrderCounterPullToRefresh();
                await WaiterCounterPullToRefresh();
                OrderPayCounterPullToRefresh();
            }
        }

        #region timers
        private void StopAllTimers()
        {
            var timers = new List<Timer>
            {
                _timerOrderCounterAutoRefresh,
                _timerOrderPayCounterAutoRefresh,
                _timerWaiterCounterAutoRefresh
            };
            foreach (var timer in timers)
            {
                StopTimer(timer);
            }
        }
        protected void StopTimer(Timer timer)
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }
        }
        #endregion
        /// <summary>
        /// Shows the progress dialog.
        /// </summary>
        /// <param name="message">Message.</param>
        protected void ShowProgressDialog(string message)
        {
            _progressDialog = dialogs.ProgressDialog(message);
            _progressDialog.Show();
        }
        /// <summary>
        /// Hides the progress dialog.
        /// </summary>
        protected void HideProgressDialog()
        {
            if (_progressDialog != null)
            {
                _progressDialog.Hide();
                _progressDialog = null;
            }
        }
        /// <summary>
        /// Shows the loading.
        /// </summary>
        /// <param name="loading">If set to <c>true</c> loading.</param>
        protected async Task ShowLoading(bool loading)
        {
            await Task.Delay(16);
            Device.BeginInvokeOnMainThread(() =>
            {
                loading = true;
            });
        }
        /// <summary>
        /// Hides the loading.
        /// </summary>
        /// <param name="loading">If set to <c>true</c> loading.</param>
        protected async Task HideLoading(bool loading)
        {
            await Task.Delay(16);
            Device.BeginInvokeOnMainThread(() =>
            {
                loading = false;
            });
        }

        protected void CheckAlarm()
        {
            bool enable = OrderCounterList.Any() || OrderPayCounterList.Any() || WaiterCounterList.Any();
            eventBus.Publish(AlarmEvent.Builder(this, enable));
        }

        #region actions

        private Task GoOrders()
        {
            return Task.Factory.StartNew(async () =>
            {
                //HACK: https://github.com/MvvmCross/MvvmCross/issues/1215
                await Task.Delay(250);
                if (await CheckConnection())
                {
                    await this.navigationController.OpenOrderList();
                }
            });
        }

        private async Task<bool> CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                await this.dialogs.ShowAlert(Strings["ErrorConnectionDeviceAfterTime"]);
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "home";
        }

        #endregion
    }
}
