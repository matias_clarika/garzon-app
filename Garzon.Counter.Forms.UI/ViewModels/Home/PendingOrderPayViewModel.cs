﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;
using System.Linq;
using Garzon.Core.Utils;

namespace Garzon.Counter.Forms.UI.ViewModels
{
    public partial class PendingCounterViewModel : GarzonCounterBaseViewModel
    {
        #region constants
        private static string TAG = typeof(HomeViewModel).GetType().FullName;
        #endregion
        #region bindings
        public Boolean _isOrderPayCounterLoading;

        public Boolean IsOrderPayCounterLoading
        {
            get => _isOrderPayCounterLoading;
            set => SetProperty(ref _isOrderPayCounterLoading, value);
        }

        public MvxObservableCollection<OrderPayCounter> OrderPayCounterList { get; } = new MvxObservableCollection<OrderPayCounter>();
        public IMvxAsyncCommand OrderPayCounterPullToRefreshCommand => new MvxAsyncCommand(OrderPayCounterPullToRefresh);
        public IMvxAsyncCommand<OrderPayCounter> OrderPayCounterClickCommand => new MvxAsyncCommand<OrderPayCounter>(OrderPayCounterItemClick);

        #endregion

        Timer _timerOrderPayCounterAutoRefresh;

        #region actions
        private async Task OrderPayCounterItemClick(OrderPayCounter orderPayCounter)
        {
            ShowProgressDialog("Cerrando cuenta...");
            try
            {
                await orderCounterBL.ClosedOrder(orderPayCounter);
                RemoveOrderPayCounter(orderPayCounter);
                CheckAlarm();
                OrderPayCounterCheckNeedRefresh();
                HideProgressDialog();
            }
            catch (Exception e)
            {
                HideProgressDialog();
                ShowError();
            }
        }


        private async Task OrderPayCounterCheckNeedRefresh()
        {
            if (!OrderPayCounterList.Any())
            {
                OrderPayCounterPullToRefresh();
            }
        }

        private async Task OrderPayCounterPullToRefresh()
        {
            IsOrderPayCounterLoading = true;
            try
            {
                StopOrderPayCounterTimer();
                var result = await orderCounterBL.FindOrderPayCounter();
                ClearOrderPayCounterList();
                AddOrderPayCounters(result);
            }
            catch (Exception e)
            {
                logger.Error(TAG, e.Message);
                ShowError(Strings["ErrorWhenLoadOrderPayCounter"]);
            }
            finally
            {
                IsOrderPayCounterLoading = false;
                ExecuteOrderPayCounterTimer();
            }
        }
        #endregion

        #region private/protected

        private void RemoveOrderPayCounter(OrderPayCounter orderPayCounter)
        {
            try
            {
                OrderPayCounterList.Remove(orderPayCounter);
            }
            catch (Exception e)
            {

            }
        }

        protected void AddOrderPayCounter(OrderPayCounter orderPayCounter)
        {
            if (!orderPayCounter.IsValid())
            {
                OrderPayCounterPullToRefresh();
            }
            else if (!OrderPayCounterList.Any()
                       || !OrderPayCounterList
                               .ToList()
                               .Exists((filter) => filter.Id == orderPayCounter.Id))
            {
                OrderPayCounterList.Add(orderPayCounter);
                ExecuteIfNotExistOrderPayCounterTimer();
            }
            CheckAlarm();
        }

        private void AddOrderPayCounters(List<OrderPayCounter> orderPayCounters)
        {
            if (orderPayCounters != null
                && orderPayCounters.Any())
            {
                OrderPayCounterList.AddRange(orderPayCounters);
            }
            CheckAlarm();

        }

        private void ClearOrderPayCounterList()
        {
            OrderPayCounterList.Clear();
        }
        #endregion

        #region autorefresh
        private void StopOrderPayCounterTimer()
        {
            StopTimer(_timerOrderPayCounterAutoRefresh);
        }


        protected void ExecuteIfNotExistOrderPayCounterTimer()
        {
            if (_timerOrderPayCounterAutoRefresh == null)
            {
                ExecuteOrderPayCounterTimer();
            }
        }


        protected void StopAndExecuteOrderPayCounterTimer()
        {
            StopOrderPayCounterTimer();
            ExecuteOrderPayCounterTimer();
        }

        protected void ExecuteOrderPayCounterTimer()
        {
            _timerOrderPayCounterAutoRefresh = Timer.StartNew(TimeSpan.FromMinutes(1),
                                                              RefreshOrderPayCounterListView);
        }

        private void RefreshOrderPayCounterListView()
        {
            if (OrderPayCounterList.Any())
            {
                List<OrderPayCounter> copyList = OrderPayCounterList.ToList().GetRange(0, OrderPayCounterList.Count);
                OrderPayCounterList.Clear();
                AddOrderPayCounters(copyList);
            }
        }
        #endregion
    }
}
