﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Managers;
    using Garzon.Core.Utils;
    using Garzon.Counter.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;
    using Plugin.Connectivity;

    public class HomeViewModel : GarzonCounterBaseViewModel
    {
        protected readonly IUserBL userBL;
        protected readonly IRingtoneManager ringtoneManager;

        private Timer connectionTimer;

        public IMvxAsyncCommand ShowInitialViewModelsCommand => new MvxAsyncCommand(this.ShowInitialViewModels);
        public IMvxAsyncCommand SectorsCommand => new MvxAsyncCommand(GoSectors);
        public IMvxAsyncCommand PaymentCommand => new MvxAsyncCommand(SetupPayment);

        public IMvxAsyncCommand SignOutCommand => new MvxAsyncCommand(SignOut);

        public HomeViewModel(INavigationController navigationController,
                             ILogger logger,
                             IAnalytics analytics,
                             AppDefaultConfig appConfig,
                             IDialogs dialogs,
                             IUserBL userBL,
                            IRingtoneManager ringtoneManager) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            this.userBL = userBL;
            this.ringtoneManager = ringtoneManager;
        }

        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "home";
        }
        #endregion

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            SubscribeInternetChanged();
        }
        #endregion
        #region private
        private async Task ShowInitialViewModels()
        {
            var tasks = new List<Task>
            {
                this.navigationController.OpenPendingList(),
                this.navigationController.OpenOrderList(),
                this.navigationController.OpenTakeAway()
            };
            await Task.WhenAll(tasks);
        }

        private void SubscribeInternetChanged()
        {
            CrossConnectivity.Current.ConnectivityTypeChanged += async (sender, args) =>
            {
                if (!args.IsConnected)
                {
                    StartTimerAlertConnection();
                }
                else
                {
                    StopTimerAlertConnection();
                }
            };
        }

        #region timers
        private void StopTimerAlertConnection()
        {
            if (this.connectionTimer != null)
            {
                this.connectionTimer.Stop();
                this.connectionTimer = null;
            }
        }

        private void StartTimerAlertConnection()
        {
            if (this.connectionTimer == null)
            {
                this.connectionTimer = Timer.StartNew(TimeSpan.FromSeconds(30), AlertConnection);
            }
        }

        private async void AlertConnection()
        {
            StopTimerAlertConnection();
            this.ringtoneManager.PlayNotification();
            await this.dialogs.ShowError(Strings["ErrorConnectionDeviceAfterTime"]);
            if (!CrossConnectivity.Current.IsConnected)
            {
                StartTimerAlertConnection();
            }
        }

        #endregion

        private async Task GoSectors()
        {
            if (await this.CheckConnectionWithAlert())
            {
                await this.navigationController.OpenSectors();
            }
        }

        private async Task SignOut()
        {
            var confirm = await this.dialogs.ShowConfirmDialog(Strings["SignOutTitle"],
                                                               Strings["SignOutMessage"],
                                                               Strings["Accept"],
                                                               Strings["Cancel"]);
            if (confirm)
            {
                try
                {
                    var result = await this.userBL.Logout();
                    if (result)
                    {
                        await this.navigationController.ReOpenApp();
                    }
                    else
                    {
                        ShowError();
                    }

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    ShowError();
                }
            }

        }

        private Task SetupPayment()
        {
            return this.navigationController.OpenPaymentSetup();
        }

        #endregion
    }
}
