﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;
using System.Linq;
using Garzon.Core.Utils;
using Garzon.Counter.Forms.UI.EventBus.Order;
using Garzon.Core.EventBus;

namespace Garzon.Counter.Forms.UI.ViewModels
{
    public partial class PendingCounterViewModel : GarzonCounterBaseViewModel
    {
        #region bindings
        public bool _isOrderCounterLoading;

        public bool IsOrderCounterLoading
        {
            get => _isOrderCounterLoading;
            set => SetProperty(ref _isOrderCounterLoading, value);
        }

        public MvxObservableCollection<OrderCounter> OrderCounterList { get; } = new MvxObservableCollection<OrderCounter>();
        public IMvxAsyncCommand OrderCounterPullToRefreshCommand => new MvxAsyncCommand(OrderCounterPullToRefresh);
        public IMvxAsyncCommand<OrderCounter> OrderCounterItemClickCommand => new MvxAsyncCommand<OrderCounter>(OrderCounterItemClick);
        #endregion

        Timer _timerOrderCounterAutoRefresh;

        #region events

        #endregion
        #region actions

        private async Task OrderCounterPullToRefresh()
        {
            IsOrderCounterLoading = true;
            try
            {
                StopOrderCounterTimer();
                var result = await orderCounterBL.FindOrderCounter();
                ClearOrderCounterList();
                AddOrderCounters(result);
            }
            catch (Exception e)
            {
                ShowError();
            }
            finally
            {
                IsOrderCounterLoading = false;
                ExecuteOrderCounterTimer();
            }
        }

        private async Task OrderCounterItemClick(OrderCounter orderCounter)
        {
            await navigationController.OpenOrderCounterDetail(orderCounter);
        }
        #endregion

        #region events

        private void OnOrderCounterSent(OrderCounterSentEvent orderCounterSentEvent)
        {
            if (orderCounterSentEvent != null
               && orderCounterSentEvent.OrderCounterSent != null)
            {
                RemoveOrderCounter(orderCounterSentEvent.OrderCounterSent);
            }
        }
        #endregion
        #region private/protected

        private void RemoveOrderCounter(OrderCounter orderCounter)
        {
            try
            {
                OrderCounterList.Remove(orderCounter);
            }
            catch (Exception e)
            {

            }
            this.SetupCounter();
        }

        protected void AddOrderCounter(OrderCounter orderCounter)
        {
            if (!orderCounter.IsValid())
            {
                OrderCounterPullToRefresh();
            }
            else if (!OrderCounterList.Any()
                    || !OrderCounterList
                            .ToList()
                            .Exists((filter) => filter.Id == orderCounter.Id))
            {
                OrderCounterList.Add(orderCounter);
                ExecuteIfNotExistOrderCounterTimer();
            }
            CheckAlarm();
            this.UpCount();
        }

        private void AddOrderCounters(List<OrderCounter> orderCounters)
        {
            if (orderCounters != null
               && orderCounters.Any())
            {
                foreach (var orderCounter in orderCounters)
                {
                    OrderCounterList.Add(orderCounter);
                }
            }
            CheckAlarm();
            this.SetupCounter();

        }
        private void ClearOrderCounterList()
        {
            OrderCounterList.Clear();
            this.SetupCounter();
        }

        #endregion

        #region autorefresh
        private void StopOrderCounterTimer()
        {
            StopTimer(_timerOrderCounterAutoRefresh);
        }

        protected void ExecuteIfNotExistOrderCounterTimer()
        {
            if (_timerOrderCounterAutoRefresh == null)
            {
                ExecuteOrderCounterTimer();
            }
        }

        protected void StopAndExecuteOrderCounterTimer()
        {
            StopOrderCounterTimer();
            ExecuteOrderCounterTimer();
        }

        protected void ExecuteOrderCounterTimer()
        {
            _timerOrderCounterAutoRefresh = Timer.StartNew(TimeSpan.FromMinutes(1),
                                                        RefreshOrderCounterListView);
        }

        private async void RefreshOrderCounterListView()
        {
            if (OrderCounterList.Any())
            {
                InvalidateOrderCounterListView();
            }
        }

        private async void InvalidateOrderCounterListView()
        {
            List<OrderCounter> copyList = OrderCounterList.ToList().GetRange(0, OrderCounterList.Count);
            OrderCounterList.Clear();
            AddOrderCounters(copyList);

        }
        #endregion
    }

    public class InvalidateOrderCounterEvent : Event
    {
        private InvalidateOrderCounterEvent(object sender) : base(sender)
        {
        }

        public static InvalidateOrderCounterEvent Builder(object sender)
        {
            return new InvalidateOrderCounterEvent(sender);
        }
    }
}
