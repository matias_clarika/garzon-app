﻿using System;
using System.Threading.Tasks;
using Garzon.DataObjects;
using MvvmCross.Core.ViewModels;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using Garzon.Core.Utils;

namespace Garzon.Counter.Forms.UI.ViewModels
{
    public partial class PendingCounterViewModel : GarzonCounterBaseViewModel
    {
        #region bindings
        public Boolean _isWaiterCounterLoading;

        public Boolean IsWaiterCounterLoading
        {
            get => _isWaiterCounterLoading;
            set => SetProperty(ref _isWaiterCounterLoading, value);
        }


        public MvxObservableCollection<WaiterCounter> WaiterCounterList { get; } = new MvxObservableCollection<WaiterCounter>();
        public IMvxAsyncCommand WaiterCounterPullToRefreshCommand => new MvxAsyncCommand(WaiterCounterPullToRefresh);
        public IMvxAsyncCommand<WaiterCounter> WaiterCounterClickCommand => new MvxAsyncCommand<WaiterCounter>(WaiterCounterItemClick);

        #endregion
        Timer _timerWaiterCounterAutoRefresh;
        #region actions
        private async Task WaiterCounterItemClick(WaiterCounter waiterCounter)
        {
            ShowProgressDialog("Enviando respuesta...");
            try
            {
                await orderCounterBL.MarkWaiterCallReceived(waiterCounter.Id);
                RemoveWaiterCounter(waiterCounter);
                CheckAlarm();
                WaiterCounterCheckNeedRefresh();
                HideProgressDialog();
            }
            catch (Exception e)
            {
                HideProgressDialog();
                ShowError();
            }
        }

        private async Task WaiterCounterCheckNeedRefresh()
        {
            if (!WaiterCounterList.Any())
            {
                WaiterCounterPullToRefresh();
            }
        }

        private async Task WaiterCounterPullToRefresh()
        {
            IsWaiterCounterLoading = true;
            try
            {
                StopWaiterCounterTimer();
                var result = await orderCounterBL.FindWaiterCounter();
                ClearWaiterCounterList();
                AddWaiterCounters(result);
            }
            catch (Exception e)
            {
                ShowError("Ocurrió un error al actualizar las llamadas al mozo. Revise su conexión a internet, si el error persiste comuniquese con soporte.");
            }
            finally
            {
                IsWaiterCounterLoading = false;
                ExecuteWaiterCounterTimer();
            }
        }
        #endregion

        #region private/protected


        private void RemoveWaiterCounter(WaiterCounter waiterCounter)
        {
            try
            {
                WaiterCounterList.Remove(waiterCounter);
            }
            catch (Exception e)
            {

            }
        }

        protected void AddWaiterCounter(WaiterCounter waiterCounter)
        {
            if (!waiterCounter.IsValid())
            {
                WaiterCounterPullToRefresh();
            }
            else if (!WaiterCounterList.Any()
                   || !WaiterCounterList.ToList().Exists((filter) => filter.Id == waiterCounter.Id))
            {
                WaiterCounterList.Add(waiterCounter);
                ExecuteIfNotExistWaiterCounterTimer();
            }
            CheckAlarm();

        }

        private void AddWaiterCounters(List<WaiterCounter> waiterCounters)
        {
            if (waiterCounters != null
                && waiterCounters.Any())
            {
                WaiterCounterList.AddRange(waiterCounters);
            }
            CheckAlarm();

        }
        private void ClearWaiterCounterList()
        {
            WaiterCounterList.Clear();
        }
        #endregion

        #region autorefresh
        private void StopWaiterCounterTimer()
        {
            StopTimer(_timerWaiterCounterAutoRefresh);
        }

        protected void ExecuteIfNotExistWaiterCounterTimer()
        {
            if (_timerWaiterCounterAutoRefresh == null)
            {
                ExecuteWaiterCounterTimer();
            }
        }


        protected void StopAndExecuteWaiterCounterTimer()
        {
            StopWaiterCounterTimer();
            ExecuteWaiterCounterTimer();
        }

        protected void ExecuteWaiterCounterTimer()
        {
            _timerWaiterCounterAutoRefresh = Timer.StartNew(TimeSpan.FromMinutes(1),
                                                            RefreshWaiterCounterListView);

        }

        private void RefreshWaiterCounterListView()
        {
            if (WaiterCounterList.Any())
            {
                List<WaiterCounter> copyList = WaiterCounterList.ToList().GetRange(0, WaiterCounterList.Count);
                WaiterCounterList.Clear();
                AddWaiterCounters(copyList);
            }
        }
        #endregion
    }
}
