﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Counter.Forms.UI.Navigation;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Garzon.DataObjects;
    using MvvmCross.Core.ViewModels;
    using Plugin.Connectivity;
    using Xamarin.Forms;

    public class LoginViewModel : GarzonCounterBaseViewModel
    {
        #region constants
        private static string TAG = typeof(LoginViewModel).FullName;
        #endregion
        #region dependency
        protected readonly IUserBL userBL;
        #endregion
        #region fields to binding
        private string userName;
        private string password;

        public string UserName
        {
            get => this.userName;
            set => SetProperty(ref this.userName, value);
        }

        public string Password
        {
            get => this.password;
            set => SetProperty(ref this.password, value);
        }

        public IMvxAsyncCommand SignInCommand => new MvxAsyncCommand(TrySignIn);
        #endregion
        public LoginViewModel(INavigationController navigationController,
                              ILogger logger,
                              IAnalytics analytics,
                              AppDefaultConfig appConfig,
                              IDialogs dialogs,
                              IUserBL userBL) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            this.userBL = userBL;
        }

        #region lifecycle
        public override void ViewAppeared()
        {
            base.ViewAppeared();
        }
        #endregion
        private Task TrySignIn()
        {
            return Task.Factory.StartNew(() =>
            {
                if (Validate())
                {
                    SignIn();
                }
            });

        }

        private async Task SignIn()
        {
            var progress = dialogs.ProgressDialog(String.Empty);

            try
            {
                await progress.Show();
                var counter = await this.userBL.SignInCounter(UserName, Password);
                SendCurrentUser(counter.GetUser());
                await progress.Hide();
                await base.navigationController.ReOpenApp();
            }
            catch (UserAuthException e)
            {
                logger.Error(TAG, e.Message);
                await progress.Hide();
                ShowError(Strings["UserOrPasswordIncorrect"]);
            }
            catch (Exception e)
            {
                await progress.Hide();
                ShowGenericError();
            }
        }

        private void SendCurrentUser(User user)
        {
            if (user != null)
            {
                Dictionary<string, string> attributes = new Dictionary<string, string>
            {
                { AnalyticsAttributes.USER_NAME, $"{user.FirstName} {user.LastName}" }
            };
                this.analytics.SetCurrentUser(user.Id, attributes);
            }
        }

        private void ShowGenericError()
        {
            if (DoIHaveInternet())
            {
                ShowError(Strings["GenericError"]);
            }
            else
            {
                ShowError(Strings["ErrorConnectionToAlert"]);
            }
        }

        private void ShowError(string message)
        {
            dialogs.ShowError(message);
        }

        private bool Validate()
        {
            if (!String.IsNullOrEmpty(UserName)
                && !String.IsNullOrEmpty(Password))
            {
                return true;
            }
            else
            {
                dialogs.ShowAlert(Strings["UserAndPasswordIsRequired"]);
                return false;
            }
        }

        public bool DoIHaveInternet()
        {
            if (!CrossConnectivity.IsSupported)
                return true;

            return CrossConnectivity.Current.IsConnected;
        }

        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "login";
        }
    }
}
