﻿namespace Garzon.Counter.Forms.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL;
    using Garzon.Config;
    using Garzon.Core.Analytics;
    using Garzon.Core.Utils;
    using Garzon.Counter.Forms.UI.Navigation;
    using MvvmCross.Core.ViewModels;
    using Xamarin.Forms;

    public class PaymentAuthViewModel : GarzonCounterBaseViewModel
    {
        #region dependency
        protected readonly IUserBL UserBL;
        #endregion
        private UrlWebViewSource htmlSource;

        public PaymentAuthViewModel(INavigationController navigationController, ILogger logger, IAnalytics analytics, AppDefaultConfig appConfig, IDialogs dialogs, IUserBL userBL) : base(navigationController, logger, analytics, appConfig, dialogs)
        {
            this.UserBL = userBL;
        }

        public UrlWebViewSource HtmlSource
        {
            get => this.htmlSource;
            set => SetProperty(ref this.htmlSource, value);
        }

        public IMvxAsyncCommand SuccessCommand => new MvxAsyncCommand(OnSuccess);

        #region lifecycle
        public override async Task Initialize()
        {
            await base.Initialize();
            await SetupUrlByCurrentCounterAsync();
        }
        #endregion
        #region abstract
        protected override Dictionary<string, string> GetViewInformation()
        {
            return null;
        }

        protected override string GetViewName()
        {
            return "payment_auth";
        }
        #endregion

        #region private methods
        private async Task SetupUrlByCurrentCounterAsync()
        {
            var currentCounter = await this.UserBL.GetCurrentCounter();
            if (!currentCounter.PlaceId.HasValue || currentCounter.PlaceId.Value < 0)
            {
                await this.dialogs.ShowAlert(Strings["SetupPaymentAccountErrorMessage"]);
                await this.navigationController.Back(this);
            }
            else
            {
                this.HtmlSource = new UrlWebViewSource
                {
                    Url = string.Format(this.appConfig.MercadoPagoPaymentSetupURL, currentCounter.PlaceId.Value)
                };
            }

        }

        private async Task OnSuccess()
        {
            await this.dialogs.ShowAlert(Strings["SetupPaymentAccountSuccessMessage"]);
            await this.navigationController.Back(this);
        }
        #endregion
    }
}
