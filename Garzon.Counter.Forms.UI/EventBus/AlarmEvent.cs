﻿using System;
using Garzon.Core.EventBus;

namespace Garzon.Counter.Forms.UI.EventBus
{
    public class AlarmEvent : Event
    {
        readonly bool _enable;
        public bool Enable
        {
            get => _enable;
        }

        private AlarmEvent(object sender, bool enable) : base(sender)
        {
            _enable = enable;
        }

        public static AlarmEvent Builder(object sender, bool enable){
            return new AlarmEvent(sender, enable);
        }
    }
}
