﻿using System;
using System.Threading.Tasks;
using Garzon.BL;
using Garzon.Core.EventBus;
using Garzon.Core.EventBus.Auth;

namespace Garzon.Counter.Forms.UI.EventBus.Auth
{
    public class AuthGlobalEventHandler
    {
        #region dependency
        readonly IEventBus _eventBus;
        readonly IUserBL _userBL;
        #endregion
        #region tokens
        Task<IEventToken> _token;
        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Garzon.Counter.Forms.UI.EventBus.Auth.AuthGlobalEventHandler"/> class.
        /// </summary>
        /// <param name="eventBus">Event bus.</param>
        /// <param name="userBL">User bl.</param>
        public AuthGlobalEventHandler(IEventBus eventBus,
                                      IUserBL userBL)
        {
            _eventBus = eventBus;
            _userBL = userBL;
            _token = _eventBus.SubscribeOnThreadPoolThread<AuthNotificationMessage>(OnTokenRefreshed);
        }

        #region methods
        void OnTokenRefreshed(AuthNotificationMessage authNotificationMessage)
        {
            try
            {
                if (authNotificationMessage != null
                    && !String.IsNullOrEmpty(authNotificationMessage.RegistrationToken))
                {
                    _userBL.SetDeviceToken(authNotificationMessage.RegistrationToken);
                }
            }
            catch (Exception e)
            {
                
            }
        }
        #endregion
    }
}
