﻿using System;
using Garzon.BL;
using Garzon.Core.EventBus;

namespace Garzon.Counter.Forms.UI.EventBus.Order
{
    public class CounterOrderGlobalEventHandler
    {
        #region dependency
        readonly IEventBus _eventBus;
        readonly IOrderBL _orderBL;
        #endregion

        public CounterOrderGlobalEventHandler(IEventBus eventBus,
                                      IOrderBL orderBL)
        {
            _eventBus = eventBus;
            _orderBL = orderBL;
        }
    }
}
