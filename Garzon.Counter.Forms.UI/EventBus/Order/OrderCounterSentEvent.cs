﻿using System;
using Garzon.Core.EventBus;
using Garzon.DataObjects;

namespace Garzon.Counter.Forms.UI.EventBus.Order
{
    public class OrderCounterSentEvent : Event
    {
        readonly OrderCounter _orderCounterSent;

        private OrderCounterSentEvent(object sender,
                                OrderCounter orderCounter) : base(sender)
        {
            _orderCounterSent = orderCounter;
        }

        public OrderCounter OrderCounterSent
        {
            get=>_orderCounterSent;
        }
        /// <summary>
        /// Build the specified sender and orderCounter.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="sender">Sender.</param>
        /// <param name="orderCounter">Order counter.</param>
        public static OrderCounterSentEvent Build(object sender,
                                             OrderCounter orderCounter){
            return new OrderCounterSentEvent(sender, orderCounter);
        }
    }
}
