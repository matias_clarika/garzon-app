﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Garzon.Core.Navigation;
using Garzon.DataObjects;

namespace Garzon.Counter.Forms.UI.Navigation
{
    public interface INavigationController : INavigationCoreController
    {
        /// <summary>
        /// Opens the order counter detail.
        /// </summary>
        /// <returns>The order counter detail.</returns>
        /// <param name="orderCounter">Order counter.</param>
        Task OpenOrderCounterDetail(OrderCounter orderCounter);

        /// <summary>
        /// Opens the order list.
        /// </summary>
        /// <returns>The order list.</returns>
        Task OpenOrderList();

        /// <summary>
        /// Opens the change table number.
        /// </summary>
        /// <returns>The change table number.</returns>
        /// <param name="order">Order.</param>
        Task OpenChangeTableNumber(Order order);


        /// <summary>
        /// Opens the order detail.
        /// </summary>
        /// <returns>The order detail.</returns>
        /// <param name="order">Order.</param>
        Task OpenOrderDetail(Order order);

        /// <summary>
        /// Opens the home.
        /// </summary>
        /// <returns>The home.</returns>
        Task OpenHome();

        /// <summary>
        /// Opens the pending list.
        /// </summary>
        /// <returns>The pending list.</returns>
        Task OpenPendingList();

        /// <summary>
        /// Opens the login.
        /// </summary>
        /// <returns>The login.</returns>
        Task OpenLogin();

        /// <summary>
        /// Opens the detail compound observation.
        /// </summary>
        /// <returns>The detail compound observation.</returns>
        /// <param name="orderDetail">Order detail.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task<OrderDetail> OpenDetailCompoundObservation(OrderDetail orderDetail,
                                                        CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Opens the sectors.
        /// </summary>
        /// <returns>The sectors.</returns>
        Task OpenSectors();

        /// <summary>
        /// Opens the take away.
        /// </summary>
        /// <returns>The take away.</returns>
        Task OpenTakeAway();

        /// <summary>
        /// Opens the payment setup.
        /// </summary>
        /// <returns>The payment setup.</returns>
        Task OpenPaymentSetup();
    }
}
