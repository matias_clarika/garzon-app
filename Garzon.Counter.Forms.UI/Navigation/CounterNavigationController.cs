﻿namespace Garzon.Counter.Forms.UI.Navigation
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Garzon.BL;
    using Garzon.Core;
    using Garzon.Core.Analytics;
    using Garzon.Core.ViewModels;
    using Garzon.Counter.Forms.UI.ViewModels;
    using Garzon.DataObjects;
    using MvvmCross.Core.Navigation;
    using MvvmCross.Core.ViewModels;

    public class CounterNavigationController : INavigationController
    {
        #region dependency
        protected readonly IMvxNavigationService navigationService;
        protected readonly IAnalytics analytics;
        protected readonly IOAuthSettings oAuthSettings;
        protected readonly IUserBL userBL;
        #endregion

        #region constructor
        public CounterNavigationController(IAnalytics analytics,
                                           IMvxNavigationService navigationService,
                                           IOAuthSettings oAuthSettings,
                                           IUserBL userBL)
        {
            this.navigationService = navigationService;
            this.analytics = analytics;
            this.oAuthSettings = oAuthSettings;
            this.userBL = userBL;
        }
        #endregion

        #region Implementation of INavigationController
        public async Task OpenFirstPage(ICoreApp coreApp)
        {
            if (String.IsNullOrEmpty(this.oAuthSettings.GetToken()))
            {
                await coreApp.RegisterFirstPage<LoginViewModel>();
            }
            else
            {
                await coreApp.RegisterFirstPage<HomeViewModel>();
                var currentUser = await this.userBL.FindCurrentUser();
                SendCurrentUser(currentUser);
            }

        }

        public Task OpenOrderCounterDetail(OrderCounter orderCounter)
        {
            var p = OrderCounterDetailViewModel.Params.Builder.ParamsBuilder(orderCounter).Build();
            if (orderCounter.IsTakeAway())
            {
                return navigationService.Navigate<TakeAwayOrderCounterDetailViewModel, OrderCounterDetailViewModel.Params>(p);
            }
            else
            {
                return navigationService.Navigate<OrderCounterDetailViewModel, OrderCounterDetailViewModel.Params>(p);
            }
        }

        public Task ReOpenApp()
        {
            var token = this.oAuthSettings.GetToken();

            if (String.IsNullOrEmpty(token))
            {
                return navigationService.Navigate<LoginViewModel>();
            }
            else
            {
                return OpenHome();
            }
        }

        public Task Back(BaseViewModel viewModel)
        {
            return navigationService.Close(viewModel);
        }

        public Task OpenOrderList()
        {
            return navigationService.Navigate<CounterOrderListViewModel>();
        }

        public Task OpenChangeTableNumber(Order order)
        {
            return navigationService
                        .Navigate<OrderTableNumberViewModel,
                                    OrderTableNumberViewModel.Params>(OrderTableNumberViewModel.Params.Builder(order));
        }

        public Task OpenOrderDetail(Order order)
        {
            return navigationService.Navigate<OrderDetailViewModel, OrderDetailViewModel.Params>(OrderDetailViewModel.Params.Builder(order));
        }

        public Task OpenHome()
        {
            return navigationService.Navigate<HomeViewModel>();
        }

        public Task OpenPendingList()
        {
            return navigationService.Navigate<PendingCounterViewModel>();
        }

        public Task OpenLogin()
        {
            return navigationService.Navigate<LoginViewModel>();
        }

        public Task<OrderDetail> OpenDetailCompoundObservation(OrderDetail orderDetail, CancellationToken cancellationToken = default(CancellationToken))
        {
            return navigationService.Navigate<DetailCompoundObservationViewModel, DetailCompoundObservationViewModel.Params, OrderDetail>(DetailCompoundObservationViewModel.Params.Build(orderDetail), null, cancellationToken);
        }

        public Task Back<TViewModel, TResult>(TViewModel viewModel, TResult result)
            where TViewModel : IMvxViewModelResult<TResult>
            where TResult : class
        {
            return this.navigationService.Close(viewModel, result);
        }

        public Task OpenSectors()
        {
            return this.navigationService.Navigate<SectorListViewModel>();
        }

        public Task OpenTakeAway()
        {
            return this.navigationService.Navigate<TakeAwayViewModel>();
        }

        public Task OpenPaymentSetup()
        {
            return this.navigationService.Navigate<PaymentAuthViewModel>();
        }
        #endregion

        private void SendCurrentUser(User user)
        {
            if (user != null)
            {
                Dictionary<string, string> attributes = new Dictionary<string, string>
            {
                { AnalyticsAttributes.USER_NAME, $"{user.FirstName} {user.LastName}" }
            };
                this.analytics.SetCurrentUser(user.Id, attributes);
            }
        }
    }
}
