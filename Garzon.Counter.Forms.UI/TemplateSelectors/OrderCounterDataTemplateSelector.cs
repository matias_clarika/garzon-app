﻿namespace Garzon.Counter.Forms.UI.TemplateSelectors
{
    using System;
    using Garzon.DataObjects;
    using Xamarin.Forms;

    public class OrderCounterDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CustomTemplate { get; set; }
        public DataTemplate TakeAwayTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {

            var orderCounter = (OrderCounter)item;

            return orderCounter.IsTakeAway() ? TakeAwayTemplate : CustomTemplate;
        }
    }
}
