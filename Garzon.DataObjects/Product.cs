﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.DataObjects;
using Newtonsoft.Json;

namespace Garzon.DataObjects
{
    public class Product : BaseDataObject
    {
        public Product()
        {
            Type = (int)ProductType.SIMPLE;
        }
        public string Name
        {
            get;
            set;
        }

        public string ShortDescription
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Icon
        {
            get;
            set;
        }

        public bool IsRecommended
        {
            get;
            set;
        }

        public int? SequenceRecommended
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public string Calories
        {
            get;
            set;
        }

        public string Label
        {
            get;
            set;
        }

        [JsonIgnore]
        public List<ProductAdditional> Additional
        {
            get;
            set;
        }

        public bool? FastOrder
        {
            get;
            set;
        }

        public long? MenuSectionId
        {
            get;
            set;
        }

        public long? GroupId
        {
            get;
            set;
        }

        public long? PlaceId
        {
            get;
            set;
        }

        public int? Sequence
        {
            get;
            set;
        }

        public int? Type
        {
            get;
            set;
        }

        public List<Group> Groups
        {
            get;
            set;
        }

        #region hours
        public string HourMondayFrom
        {
            get;
            set;
        }
        public string HourMondayTo
        {
            get;
            set;
        }
        public bool HourMondayMore24Hours
        {
            get;
            set;
        }
        public string HourTuesdayFrom
        {
            get;
            set;
        }
        public string HourTuesdayTo
        {
            get;
            set;
        }
        public bool HourTuesdayMore24Hours
        {
            get;
            set;
        }
        public string HourWednesdayFrom
        {
            get;
            set;
        }
        public string HourWednesdayTo
        {
            get;
            set;
        }
        public bool HourWednesdayMore24Hours
        {
            get;
            set;
        }
        public string HourThursdayFrom
        {
            get;
            set;
        }
        public string HourThursdayTo
        {
            get;
            set;
        }
        public bool HourThursdayMore24Hours
        {
            get;
            set;
        }
        public string HourFridayFrom
        {
            get;
            set;
        }
        public string HourFridayTo
        {
            get;
            set;
        }
        public bool HourFridayMore24Hours
        {
            get;
            set;
        }
        public string HourSaturdayFrom
        {
            get;
            set;
        }
        public string HourSaturdayTo
        {
            get;
            set;
        }
        public bool HourSaturdayMore24Hours
        {
            get;
            set;
        }
        public string HourSundayFrom
        {
            get;
            set;
        }
        public string HourSundayTo
        {
            get;
            set;
        }
        public bool HourSundayMore24Hours
        {
            get;
            set;
        }

        public bool IsCompoundPromoEqual()
        {
            return this.Type == (int)ProductType.COMPOUND_PROMO_EQUAL;
        }

        public Product Clone()
        {
            var json = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject<Product>(json);
        }

        public bool IsAnyCompound()
        {
            return this.Type == (int)ProductType.COMPOUND || this.Type == (int)ProductType.COMPOUND_PROMO || this.Type == (int)ProductType.COMPOUND_PROMO_EQUAL;
        }

        public bool IsCompoundPromo()
        {
            return this.Type == (int)ProductType.COMPOUND_PROMO || this.Type == (int)ProductType.COMPOUND_PROMO_EQUAL;
        }

        #endregion
    }

    public enum ProductType
    {
        SIMPLE = 1, COMPOUND = 2, COMPOUND_PROMO = 3, COMPOUND_PROMO_EQUAL = 4
    }
}
