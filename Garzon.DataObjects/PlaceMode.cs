﻿namespace Garzon.DataObjects
{
    using System;
    using System.Collections.Generic;
    using Clarika.Xamarin.Base.DataObjects;

    public class PlaceMode : BaseDataObject
    {
        public string Name
        {
            get;
            set;
        }

        public List<PaymentType> PaymentTypes
        {
            get;
            set;
        }

        public enum ServerPlaceModeIds
        {
            RESTO = 1, TAKE_AWAY = 2, DELIVERY = 3, TAKE_AWAY_EXIST_ORDER = 4
        }

        /// <summary>
        /// Builds the take away exist.
        /// </summary>
        /// <returns>The take away exist.</returns>
        public static PlaceMode BuildTakeAwayExist()
        {
            return new PlaceMode
            {
                Id = (int)ServerPlaceModeIds.TAKE_AWAY_EXIST_ORDER,
                Name = "Ya tengo un pedido"
            };
        }
    }
}
