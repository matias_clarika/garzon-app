﻿using System;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class Sector : BaseDataObject
    {
        public string Name
        {
            get;
            set;
        }
    }
}
