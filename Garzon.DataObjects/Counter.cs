﻿namespace Garzon.DataObjects
{
    using System;
    using Clarika.Xamarin.Base.DataObjects;

    public class Counter : BaseDataObject
    {
        public string Name
        {
            get;
            set;
        }

        public bool Main
        {
            get;
            set;
        }

        public string Token
        {
            get;
            set;
        }

        public long? PlaceId
        {
            get;
            set;
        }

        /// <summary>
        /// get the user by counter.
        /// </summary>
        /// <returns>The user.</returns>
        public User GetUser()
        {
            return new User
            {
                FirstName = this.Name,
                Id = this.Id,
                Token = this.Token
            };
        }

        /// <summary>
        /// Build the specified user and isMain.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="user">User.</param>
        /// <param name="isMain">If set to <c>true</c> is main.</param>
        public static Counter Build(User user, bool isMain, long placeId)
        {
            return new Counter
            {
                Id = user.Id,
                Token = user.Token,
                Name = user.FirstName,
                Main = isMain,
                PlaceId = placeId
            };
        }
    }
}
