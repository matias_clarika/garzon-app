﻿using System;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class WaiterCallOption : BaseDataObject
    {
        #region fields

        public string Name
        {
            get;
            set;
        }

        public Boolean? IsSelected
        {
            get;
            set;
        }

        public long PlaceId
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }
        #endregion
    }
}
