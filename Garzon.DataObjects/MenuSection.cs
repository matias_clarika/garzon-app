﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.DataObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Garzon.DataObjects
{
    public class MenuSection : BaseDataObject
    {
        public string Name
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Icon
        {
            get;
            set;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("link_type")]
        public DeepLink DeepLink
        {
            get;
            set;
        }

        public bool? FastCharge
        {
            get;
            set;
        } = false;

        public long? MenuId
        {
            get;
            set;
        }

        public long? Parent
        {
            get;
            set;
        }

        public List<MenuSection> Children
        {
            get;
            set;
        }

        public List<Product> Products
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }

        public bool AvailableBySchedule
        {
            get;
            set;
        }
        #region daily menu
        [JsonIgnore]
        public bool IsDailyMenu
        {
            get { return ProductId > 0; }
        }

        [JsonIgnore]
        public long? ProductId
        {
            get;
            set;
        }

        [JsonIgnore]
        public string DailyMenuDescription
        {
            get;
            set;
        }

        [JsonIgnore]
        public string DailyMenuComment
        {
            get;
            set;
        }
        #endregion

        #region utils
        public void AddTempDailyProduct(Product product,
                                        string comment = null)
        {
            DailyMenuDescription = $"- {product.Name}";
            DailyMenuComment = comment;
        }
        #endregion

    }

    public enum DeepLink
    {
        GoToProductImage = 1,
        GoToProduct = 2,
        GoToMenuSectionImage = 3,
        GoToMenuSectionIcon = 4,
        GoToMenu = 5
    }
}
