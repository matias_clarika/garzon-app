﻿using System;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class CheckVersion : BaseDataObject
    {
        public int Version{
            get;
            set;
        }
    }
}
