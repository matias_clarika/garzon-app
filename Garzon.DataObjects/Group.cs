﻿namespace Garzon.DataObjects
{
    using System;
    using System.Collections.Generic;
    using Clarika.Xamarin.Base.DataObjects;

    public class Group : BaseDataObject
    {
        public string Name
        {
            get;
            set;
        }

        public bool SendLater
        {
            get;
            set;
        }

        public List<Product> Products
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }

        public long? ProductId
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public bool? Optional
        {
            get;
            set;
        }

        public bool? MultipleSelection
        {
            get;
            set;
        }

        public string LabelEs
        {
            get;
            set;
        }

        public string LabelEn
        {
            get;
            set;
        }

        /// <summary>
        /// Ises the optional.
        /// </summary>
        /// <returns><c>true</c>, if optional was ised, <c>false</c> otherwise.</returns>
        public bool IsOptional()
        {
            return this.Optional.HasValue && this.Optional.Value;
        }

        /// <summary>
        /// Finds the label by lenguage.
        /// </summary>
        /// <returns>The label by lenguage.</returns>
        /// <param name="twoLetterISOLanguageName">Two letter ISOL anguage name.</param>
        public string FindLabelByLenguage(string twoLetterISOLanguageName)
        {
            switch (twoLetterISOLanguageName)
            {
                case "es":
                    return this.LabelEs;
                default:
                    return this.LabelEn;
            }
        }
    }
}
