﻿
namespace Garzon.DataObjects.Social
{
    public class SocialUser
    {
        private SocialUser()
        {

        }

        #region builder
        public static SocialUser Builder()
        {
            return new SocialUser();
        }

        public SocialUser WithId(string id)
        {
            _id = id;
            return this;
        }

        public SocialUser WithToken(string token){
            _token = token;
            return this;
        }

        public SocialUser WithFirstName(string firstName)
        {
            _firstName = firstName;
            return this;
        }

        public SocialUser WithLastName(string lastName)
        {
            _lastName = lastName;
            return this;
        }

        public SocialUser WithEmail(string email)
        {
            _email = email;
            return this;
        }

        public SocialUser WithPicture(string picture)
        {
            _picture = picture;
            return this;
        }

        #endregion
        #region fields
        private string _id;

        public string Id { get => _id; }

        private string _token;

        public string Token { get => _token; }

        private string _firstName;

        public string FirstName { get => _firstName; }

        private string _lastName;

        public string LastName { get => _lastName; }

        private string _email;

        public string Email { get => _email; }

        private string _picture;

        public string Picture { get => _picture; }
        #endregion
    }
}
