﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class WaiterCounter : BaseDataObject
    {
        public List<WaiterCallOption> WaiterCallOptions
        {
            get;
            set;
        }

        public long PlaceId
        {
            get;
            set;
        }

        public string Sector
        {
            get;
            set;
        }

        public int TableNumber
        {
            get;
            set;
        }

        public long UserId
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public bool IsValid()
        {
            return PlaceId > 0 && TableNumber > 0 && Date != null;
        }
    }
}
