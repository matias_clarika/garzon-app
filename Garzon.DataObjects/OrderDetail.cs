﻿namespace Garzon.DataObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Clarika.Xamarin.Base.DataObjects;
    using Newtonsoft.Json;

    public class OrderDetail : IBaseDataObject
    {
        #region constructor
        public OrderDetail()
        {
            Quantity = 0;
            Details = new List<OrderDetail>();
        }
        #endregion
        #region fields

        [JsonProperty("id")]
        public long? ServerId
        {
            get;
            set;
        }


        public string Comment
        {
            get;
            set;
        }

        private string childrenDetail;
        [JsonIgnore]
        public string ChildrenDetail
        {
            get;
            set;
        }

        private int quantity;
        public int Quantity
        {
            get { return this.quantity; }
            set
            {
                this.quantity = value;
                UpdateChildrenQuantity();
            }
        }

        private Product _product;
        public Product Product
        {
            get => _product;
            set
            {
                //clear amount to recalculate
                _amount = null;
                _product = value;
                if (_product != null)
                {
                    ProductId = _product.Id;
                    Price = _product.Price;
                    if (_product.GroupId.HasValue)
                    {
                        GroupId = _product.GroupId.Value;
                    }
                }
            }
        }

        public long ProductId
        {
            get;
            set;
        }

        public double? Price
        {
            get;
            set;
        }

        private double? _amount;

        [JsonIgnore]
        public double? Amount
        {
            get
            {
                if (_amount == null)
                {
                    CalculateAmount();
                }
                return _amount;
            }
            set { _amount = value; }
        }

        public long? UserId
        {
            get;
            set;
        }

        public long? OrderId
        {
            get;
            set;
        }

        public int? Status
        {
            get;
            set;
        }

        [JsonIgnore]
        public int? ParentProductId
        {
            get;
            set;
        }

        public bool IsSendLater()
        {
            return this.SendLater.HasValue && this.SendLater.Value;
        }

        [JsonIgnore]
        public long Id { get; set; }
        #region daily menu

        [JsonIgnore]
        public long? ParentId
        {
            get;
            set;
        }

        public List<OrderDetail> Details
        {
            get;
            set;
        }

        public int? MenuSectionId
        {
            get;
            set;
        }

        [JsonIgnore]
        public string MenuSectionName
        {
            get;
            set;
        }

        public long? GroupId
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "group", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string GroupName
        {
            get;
            set;
        }

        [JsonIgnore]
        public string GroupImage
        {
            get;
            set;
        }

        public bool? SendLater
        {
            get;
            set;
        }

        [JsonIgnore]
        public bool IsCompound
        {
            get { return this.Details != null && this.Details.Any(); }
        }

        #endregion
        #endregion

        public double CalculateAmount()
        {
            if (Product != null)
            {
                Amount = Price * Quantity;
                if (this.IsCompound)
                {
                    Amount = CompoundAmount();
                }
            }
            else
            {
                Amount = 0;
            }
            return Amount.Value;
        }

        public bool Validate()
        {
            return Quantity > 0 && Product != null;
        }

        public void Observed()
        {
            Status = (int)OrderDetailStatus.OBSERVATION;
        }

        public bool IsObserved()
        {
            return Status > 0 && Status.Value == (int)OrderDetailStatus.OBSERVATION;
        }

        public bool IsWaiting()
        {
            return Status > 0 && Status.Value == (int)OrderDetailStatus.INPROGRESS;
        }

        public bool InKitchen()
        {
            return Status > 0 && Status.Value == (int)OrderDetailStatus.KITCHEN;
        }


        public bool InCounter()
        {
            return Status > 0 && Status.Value == (int)OrderDetailStatus.INCOUNTER;
        }

        public void SetInKitchen()
        {
            Status = (int)OrderDetailStatus.KITCHEN;
        }

        public void SetPending()
        {
            Status = this.IsPendingRequest ? (int)OrderDetailStatus.PENDING_REQUEST_TO_SEND : (int)OrderDetailStatus.PENDING;
        }

        public void SetInCounter()
        {
            Status = (int)OrderDetailStatus.INCOUNTER;
        }

        public void ClearDetailsAndComment()
        {
            this.Details = new List<OrderDetail>();
            this.Comment = string.Empty;
        }

        public void SetPendingRequest()
        {
            Status = (int)OrderDetailStatus.PENDING_REQUEST;
        }

        public double CompoundAmount()
        {
            double amount = Amount.Value;
            if (this.Details != null)
            {
                foreach (var detail in this.Details)
                {
                    detail.CalculateAmount();
                    amount += detail.Amount.Value;
                }
            }
            return amount;
        }

        /// <summary>
        /// Validates the daily menu.
        /// </summary>
        /// <returns><c>true</c>, if daily menu was validated, <c>false</c> otherwise.</returns>
        /// <param name="minProducts">Minimum products.</param>
        public bool ValidateCompound(int minProducts)
        {
            return Details.Count == minProducts;
        }

        /// <summary>
        /// Gets the label to children detail.
        /// </summary>
        /// <returns>The label to children detail.</returns>
        /// <param name="currencySymbol">Currency symbol.</param>
        /// <param name="pendingLabel">Pending label.</param>
        /// <param name="firstUnit">First unit.</param>
        /// <param name="secondUnit">Second unit.</param>
        /// <param name="parentProduct">Parent product.</param>
        public string GetLabelToChildrenDetail(string currencySymbol = "", string pendingLabel = "", string firstUnit = "", string secondUnit = "", Product parentProduct = null)
        {
            if (Product != null)
            {
                string price = Amount.Value > 0 ? $" (+{currencySymbol}{Amount.Value})" : "";
                var pendingText = "";
                if (this.Status == (int)OrderDetailStatus.PENDING_REQUEST)
                {
                    pendingText = $"  ({pendingLabel})";
                }
                else if (parentProduct != null && parentProduct.IsCompoundPromo() && this.Status == (int)OrderDetailStatus.INCOUNTER)
                {
                    var unit = !(this.SendLater.HasValue && this.SendLater.Value) ? firstUnit : secondUnit;
                    pendingText = $"  ({unit})";
                }
                return $"{Product.Name}{price}{pendingText}";
            }
            else
            {
                return "";
            }
        }
        [JsonIgnore]
        public bool IsPendingRequest
        {
            get
            {
                return this.Status == (int)OrderDetailStatus.PENDING_REQUEST
                           || this.Status == (int)OrderDetailStatus.PENDING_REQUEST_TO_SEND;
            }
        }

        public bool IsPending()
        {
            return this.Status == (int)OrderDetailStatus.PENDING
                       || this.Status == (int)OrderDetailStatus.PENDING_REQUEST_TO_SEND;
        }


        public bool IsSimple()
        {
            return this.Product != null && !this.Product.IsAnyCompound();
        }

        public void UpdateChildrenQuantity()
        {
            if (this.IsCompound)
            {
                foreach (var detail in this.Details)
                {
                    detail.Quantity = this.Quantity;
                }
            }
        }

        public bool IsGroupObserved()
        {
            if (this.IsCompound)
            {
                return this.Details.Exists((children) => children.IsObserved());
            }
            return false;
        }

        public bool IsGroupPendingRequest()
        {
            if (this.IsCompound)
            {
                return this.Details.Exists((children) => children.IsPendingRequest);
            }
            return false;
        }
    }

    public enum OrderDetailStatus
    {
        INPROGRESS = 1,
        KITCHEN = 2,
        CLOSED = 3,
        CANCELLED = 4,
        OBSERVATION = 5,
        PENDING = 6,
        INCOUNTER = 7,
        PENDING_REQUEST = 8,
        PENDING_REQUEST_TO_SEND = 81
    }
}
