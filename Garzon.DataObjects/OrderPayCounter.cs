﻿using System;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class OrderPayCounter : BaseDataObject
    {
        public long UserId
        {
            get;
            set;
        }

        public string Sector
        {
            get;
            set;
        }

        public int TableNumber
        {
            get;
            set;
        }

        public double Amount
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public long OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// Convert to order
        /// </summary>
        /// <returns>The order.</returns>
        public Order ToOrder()
        {
            return new Order
            {
                ServerId = OrderId
            };
        }

        public bool IsValid()
        {
            return TableNumber > 0 && Amount > 0 && Date != null && OrderId > 0;
        }
    }
}
