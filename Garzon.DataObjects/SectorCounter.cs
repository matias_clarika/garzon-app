﻿using System;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class SectorCounter : Sector
    {
        public bool Linked{
            get;
            set;
        }

        public bool Main{
            get;
            set;
        }
    }
}
