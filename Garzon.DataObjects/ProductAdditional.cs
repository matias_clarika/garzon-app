﻿using System;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class ProductAdditional : BaseDataObject
    {

        public string Name {
            get;
            set;
        }
    }
}
