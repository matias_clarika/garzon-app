﻿namespace Garzon.DataObjects
{
    using System;
    using Clarika.Xamarin.Base.DataObjects;

    public class PaymentType : BaseDataObject
    {
        public string Name
        {
            get;
            set;
        }
    }
}
