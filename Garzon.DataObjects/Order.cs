﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clarika.Xamarin.Base.DataObjects;
using Newtonsoft.Json;

namespace Garzon.DataObjects
{
    public class Order : IBaseDataObject
    {
        #region fields

        [JsonProperty("id")]
        public long? ServerId
        {
            get;
            set;
        }

        public List<OrderDetail> Details
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? SectorId
        {
            get;
            set;
        }

        public string Sector
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? TableNumber
        {
            get;
            set;
        }

        public Place Place
        {
            get;
            set;
        }

        public long? UserId
        {
            get;
            set;
        }

        public long PlaceId
        {
            get;
            set;
        }

        public int? Pin
        {
            get;
            set;
        }

        public bool? InKitchen
        {
            get;
            set;
        }

        [JsonIgnore]
        public long Id { get; set; }

        public bool Await
        {
            get;
            set;
        }

        public long? OrderCounterId
        {
            get;
            set;
        }

        public long? EntityId
        {
            get;
            set;
        }

        public int? Status
        {
            get;
            set;
        }

        [JsonIgnore]
        public bool PendingToSync
        {
            get;
            set;
        }

        public double? Amount
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public int? QuantityDiners
        {
            get;
            set;
        }

        public string RedirectUrl
        {
            get;
            set;
        }

        public int Mode
        {
            get;
            set;
        }

        public int? NumberToWithdraw
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeDelay
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Created
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Payment Payment
        {
            get;
            set;
        }
        #endregion

        public bool Validate()
        {
            return TableNumber > 0
                && PlaceId > 0
                && Details != null
                && Details.Count > 0;
        }

        public bool IsClosed()
        {
            return Status.HasValue && Status.Value == (int)OrderStatus.CLOSED;
        }

        public bool ValidToCoreInfo()
        {
            return TableNumber > 0
               && PlaceId > 0
                && TableNumber > 0
                && SectorId > 0;
        }

        public bool IsInResto()
        {
            return this.Mode == (int)PlaceMode.ServerPlaceModeIds.RESTO;
        }

        public bool InCounter()
        {
            return Details.Exists(s => s.InCounter());
        }

        public bool IsObserved()
        {
            return Details.Exists(s => s.IsObserved());
        }

        public bool AnyInKitchen()
        {
            return Details.Exists(s => s.InKitchen()) || !Details.Any();
        }

        public bool ReadyToWithdraw()
        {
            return this.Status.HasValue && this.Status == (int)OrderStatus.READY_FOR_WITHDRAW;
        }
    }

    public enum OrderStatus
    {
        OPEN = 1,
        CLOSED = 2,
        READY_FOR_WITHDRAW = 4
    }
}
