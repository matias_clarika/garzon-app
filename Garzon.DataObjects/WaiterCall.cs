﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.DataObjects;
using Newtonsoft.Json;

namespace Garzon.DataObjects
{
    public class WaiterCall : IBaseDataObject
    {
        [JsonIgnore]
        public long Id { get; set; }

        public long SectorId
        {
            get;
            set;
        }
        public int TableNumber
        {
            get;
            set;
        }

        public int PlaceId
        {
            get;
            set;
        }

        public List<int> WaiterCallOptions
        {
            get;
            set;
        }
        #region utils
        /// <summary>
        /// Sets the options.
        /// </summary>
        /// <param name="options">Options.</param>
        public void SetOptions(List<WaiterCallOption> options)
        {
            WaiterCallOptions = new List<int>();
            foreach (var option in options)
            {
                WaiterCallOptions.Add((int)option.Id);
            }
        }
        #endregion
    }
}
