﻿using Clarika.Xamarin.Base.DataObjects;
using Newtonsoft.Json;

namespace Garzon.DataObjects
{
    public class OrderCounterReceived : BaseDataObject
    {
        public bool Received
        {
            get=>true;
            set {/*nothing*/ var a = value; }
        }

        [JsonIgnore]
        public ReceivedAction ReceivedAction
        {
            get;
            set;
        }
    }


    public enum ReceivedAction{
        ORDER = 1, WAITER = 2, CHECK_ORDER = 3
    }
}
