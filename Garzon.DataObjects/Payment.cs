﻿namespace Garzon.DataObjects
{
    using System;
    using Clarika.Xamarin.Base.DataObjects;
    using Newtonsoft.Json;

    public class Payment : IBaseDataObject
    {
        public string Token
        {
            get;
            set;
        }

        public string PaymentMethodId
        {
            get;
            set;
        }

        public bool Capture
        {
            get;
            set;
        }

        [JsonIgnore]
        public long Id { get; set; }
    }
}
