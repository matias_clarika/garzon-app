﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.DataObjects;
using Newtonsoft.Json;

namespace Garzon.DataObjects
{
    public class OrderCounter : BaseDataObject
    {
        #region fields

        public long OrderId
        {
            get;
            set;
        }

        public List<OrderDetail> Details
        {
            get;
            set;
        }

        public int Action
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public long TableNumber
        {
            get;
            set;
        }

        public string Sector
        {
            get;
            set;
        }

        public bool IsValid()
        {
            return Details != null
                          && Details.Count > 0
                          && TableNumber > 0;
        }

        public int? QuantityDiners
        {
            get;
            set;
        }

        public int Mode
        {
            get;
            set;
        }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? TimeDelay
        {
            get;
            set;
        }

        public int? NumberToWithdraw
        {
            get;
            set;
        }
        #endregion

        public bool IsTakeAway()
        {
            return this.Mode == (int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY;
        }
    }
}
