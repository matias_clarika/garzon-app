﻿namespace Garzon.DataObjects
{
    using System.Collections.Generic;
    using Clarika.Xamarin.Base.DataObjects;
    using Newtonsoft.Json;

    public class Place : BaseDataObject
    {
        #region fields
        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public List<WaiterCallOption> WaiterCallOptions
        {
            get;
            set;
        }

        public List<Sector> Sectors
        {
            get;
            set;
        }

        public int? Distance
        {
            get;
            set;
        }

        public List<PlaceMode> Modes
        {
            get;
            set;
        }

        public string Logo
        {
            get;
            set;
        }
        #region utils

        public long? CurrentMenuId
        {
            get;
            set;
        }

        #endregion

        #endregion

    }
}
