﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.DataObjects;

namespace Garzon.DataObjects
{
    public class Menu : BaseDataObject
    {
        public List<Product> RecommendedProducts
        {
            get;
            set;
        }

        public List<MenuSection> MenuSections
        {
            get;
            set;
        }
        
    }
}
