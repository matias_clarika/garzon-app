﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Garzon.Core.Social.Facebook;
using Garzon.DataObjects.Social;
using Org.Json;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Forms;


namespace Garzon.Droid.FacebookUtils
{
    public class FacebookManager : Java.Lang.Object, IFacebookManager, IFacebookCallback, GraphRequest.IGraphJSONObjectCallback, GraphRequest.ICallback
    {
        public ICallbackManager CallbackManager;

        TaskCompletionSource<SocialUser> tcs;

        TaskCompletionSource<bool> tcss;

        TaskCompletionSource<bool> tcsText;

        public FacebookManager()
        {
            CallbackManager = CallbackManagerFactory.Create();
            LoginManager.Instance.RegisterCallback(CallbackManager, this);
        }




        bool HasPublishPermission()
        {
            var accessToken = AccessToken.CurrentAccessToken;
            return accessToken != null && accessToken.Permissions.Contains("publish_actions");
        }

        bool _isPermissionsLogin;
        TaskCompletionSource<bool> _pTask;

        Task<bool> RequestPublishPermissions(string[] permissions)
        {
            _isPermissionsLogin = true;
            if (_pTask != null)
            {
                _pTask.TrySetResult(false);
                _pTask = null;
            }

            _pTask = new TaskCompletionSource<bool>();
            LoginManager.Instance.SetLoginBehavior(LoginBehavior.NativeWithFallback);
            LoginManager.Instance.LogInWithPublishPermissions(Xamarin.Forms.Forms.Context as Activity, FacebookSettings.PublishPermissions);
            return _pTask.Task;
        }

        #region Implementation of IFacebookManager
        public Task<SocialUser> Login()
        {
            if (tcs != null)
            {
                tcs.TrySetResult(null);
                tcs = null;
            }
            tcs = new TaskCompletionSource<SocialUser>();
            LoginManager.Instance.SetLoginBehavior(LoginBehavior.NativeWithFallback);
            LoginManager.Instance.LogInWithReadPermissions(Xamarin.Forms.Forms.Context as Activity, FacebookSettings.ReadPermissions);
            return tcs.Task;
        }

        public async Task LogOut()
        {
            await Task.Run(() => LoginManager.Instance.LogOut());

        }


        public Task<bool> PostPhoto(ImageSource image)
        {
            throw new NotImplementedException();
        }

        public Task<bool> PostText(string message)
        {
            if (tcsText != null)
            {
                tcsText.TrySetResult(false);
                tcsText = null;
            }

            tcsText = new TaskCompletionSource<bool>();

            var token = AccessToken.CurrentAccessToken?.Token;
            if (string.IsNullOrWhiteSpace(token))
            {
                tcsText.TrySetResult(false);
            }
            else
            {

                bool hasRights;
                if (!HasPublishPermission())
                {
                    hasRights = RequestPublishPermissions(new[] { "publish_actions" }).Result;
                }
                else
                {
                    hasRights = true;
                }

                if (hasRights)
                {
                    //TODO Login here
                    var bundle = new Bundle();
                    bundle.PutString("message", message);
                    GraphRequest request = new GraphRequest(AccessToken.CurrentAccessToken, "me/feed", bundle, HttpMethod.Post, this);
                    request.ExecuteAsync();
                }
                else
                {
                    tcsText.TrySetResult(hasRights);
                }
            }
            return tcsText.Task;
        }

        public async Task<bool> ValidateToken()
        {
            return await Task.Run(() =>
            {
                var token = AccessToken.CurrentAccessToken?.Token;
                if (string.IsNullOrWhiteSpace(token))
                    return false;
                return true;
            });

        }

        #endregion

        #region Implementation of IFacebookCallback
        public void OnCancel()
        {
            if (tcs != null)
            {
                tcs.TrySetResult(null);
                tcs = null;
            }

            if (tcss != null)
            {
                tcss.TrySetResult(false);
                tcss = null;
            }

            if (_pTask != null)
            {
                _pTask.TrySetResult(false);
                _pTask = null;
            }

        }

        public void OnError(FacebookException error)
        {

            if (tcs != null)
            {
                tcs.TrySetResult(null);
                tcs = null;
            }

            if (tcss != null)
            {
                tcss.TrySetResult(false);
                tcss = null;
            }

            if (_pTask != null)
            {
                _pTask.TrySetResult(false);
                _pTask = null;
            }


        }
        public void OnSuccess(Java.Lang.Object result)
        {
            var n = result as LoginResult;
            if (n != null)
            {
                if (_isPermissionsLogin && _pTask != null)
                {
                    _isPermissionsLogin = false;
                    _pTask.TrySetResult(true);
                    _pTask = null;
                    return;
                }

                var request = GraphRequest.NewMeRequest(n.AccessToken, this);
                var bundle = new Android.OS.Bundle();
                bundle.PutString("fields", "id, first_name, email, last_name");
                request.Parameters = bundle;
                request.ExecuteAsync();
            }
        }


        #endregion

        #region Implementation of GraphRequest.IGraphJSONObjectCallback
        public void OnCompleted(JSONObject p0, GraphResponse p1)
        {
            var id = string.Empty;
            var first_name = string.Empty;
            var email = string.Empty;
            var last_name = string.Empty;
            var pictureUrl = string.Empty;
            if (p0 != null)
            {
                if (p0.Has("id"))
                    id = p0.GetString("id");

                if (p0.Has("first_name"))
                    first_name = p0.GetString("first_name");

                if (p0.Has("email"))
                    email = p0.GetString("email");

                if (p0.Has("last_name"))
                    last_name = p0.GetString("last_name");

                if (p0.Has("picture"))
                {
                    var p2 = p0.GetJSONObject("picture");
                    if (p2.Has("data"))
                    {
                        var p3 = p2.GetJSONObject("data");
                        if (p3.Has("url"))
                        {
                            pictureUrl = p3.GetString("url");
                        }
                    }
                }
            }
            if (tcs != null)
            {
                tcs.TrySetResult(SocialUser.Builder()
                                 .WithId(id)
                                 .WithToken(AccessToken.CurrentAccessToken.Token)
                                 .WithFirstName(first_name)
                                 .WithLastName(last_name)
                                 .WithEmail(email)
                                 .WithPicture(pictureUrl));
            }

        }
        #endregion

        #region Implementation of GraphRequest.ICallback
        public void OnCompleted(GraphResponse response)
        {
            if (tcsText != null)
            {
                tcsText.TrySetResult((response.Error == null) ? true : false);
                tcsText = null;
            }
        }

        #endregion
    }
}
