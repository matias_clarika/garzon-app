﻿using Android.Runtime;
using Garzon.Core.ViewModels;
using MvvmCross.Core.Views;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views.Attributes;

namespace Garzon.Droid.Views
{
    [Register(nameof(OverrideAttributeView))]
    [MvxFragmentPresentation(AddToBackStack = true)]
    public class OverrideAttributeView : MvxFragment<OverrideAttributeViewModel>, IMvxOverridePresentationAttribute
    {

        #region IMvxOverridePresentationAttribute
        public MvxBasePresentationAttribute PresentationAttribute()
        {
            return new MvxFragmentPresentationAttribute() { AddToBackStack = true };
        }
        #endregion
    }
}
