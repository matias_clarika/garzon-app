﻿namespace Garzon.Droid.FirebaseUtils
{
    using System;
    using Android.App;
    using Android.Content;
    using Clarika.Xamarin.Base.Helpers;

    public class FirebaseCrashlytics : IFirebaseCrashReporting
    {

        public FirebaseCrashlytics(Context context)
        {
            Fabric.Fabric.With(context, new Crashlytics.Crashlytics());
            Crashlytics.Crashlytics.HandleManagedExceptions();
        }

        #region Implementation of IFirebaseCrashReporting
        public void Log(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Report(Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
        }

        public void Report(string message)
        {
            Report(new Exception(message));
        }
        #endregion
    }
}
