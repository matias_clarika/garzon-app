﻿using Android.App;
using Firebase.Messaging;
using Garzon.Core.EventBus;
using System.Linq;
using Garzon.Forms.UI.DI;
using Garzon.Core.EventBus.Auth;
using System;
using Garzon.Forms.UI.EventBus.OrderNotifications;
using Garzon.Droid.Helpers;
namespace Garzon.Droid.FirebaseUtils
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class GarzonFirebaseMessagingService : FirebaseMessagingService
    {
        private static readonly string TAG = typeof(GarzonFirebaseMessagingService).FullName.ToString();

        public override void OnMessageReceived(RemoteMessage message)
        {
            if (message.Data != null)
            {
                var orderEvent = OrderEvent.Builder(this, message.Data.ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
                try
                {
                    SendEvent(orderEvent);
                }
                catch (Exception e)
                {
                    SendPendingEvent(orderEvent);
                }

            }
        }
        /// <summary>
        /// Sends the pending event.
        /// </summary>
        /// <param name="orderEvent">Order event.</param>
        private static void SendPendingEvent(OrderEvent orderEvent)
        {
            OrderNotification orderNotification = OrderNotification.Builder(orderEvent);
            if (orderNotification.IsPendingRouter())
            {
                Settings.AppSettings.AddOrUpdateValue(orderNotification.GetKey(), orderNotification.ToString());
            }
        }
        /// <summary>
        /// Sends the event.
        /// </summary>
        /// <param name="orderEvent">Order event.</param>
        private void SendEvent(OrderEvent orderEvent)
        {
            DependencyManager.GetInstance().Resolve<IEventBus>().Publish(orderEvent);
        }
    }
}
