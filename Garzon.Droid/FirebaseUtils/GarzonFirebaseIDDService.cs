﻿
using Android.App;
using Firebase.Iid;
using Garzon.Core.EventBus;
using System.Threading.Tasks;
using Garzon.Forms.UI.DI;
using Garzon.Core.EventBus.Auth;

namespace Garzon.Droid.FirebaseUtils
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class GarzonFirebaseIDDService : FirebaseInstanceIdService
    {
        private static readonly string TAG = typeof(GarzonFirebaseIDDService).FullName;

        /**
         * Called if InstanceID token is updated. This may occur if the security of
         * the previous token had been compromised. Note that this is called when the InstanceID token
         * is initially generated so this is where you would retrieve the token.
         */
        public override void OnTokenRefresh()
        {
            // Get updated InstanceID token.
            var refreshedToken = FirebaseInstanceId.Instance.Token;

            NotifyRefreshedToken(refreshedToken);
        }

        /// <summary>
        /// Notifies the refreshed token.
        /// </summary>
        /// <returns>The refreshed token.</returns>
        /// <param name="refreshedToken">Refreshed token.</param>
        private async Task NotifyRefreshedToken(string refreshedToken){
            await Task.Delay(1000);
            DependencyManager.GetInstance()
                                     .Resolve<IEventBus>()
                                     .Publish<AuthNotificationMessage>(AuthNotificationMessage
                                        .Builder(this, refreshedToken));
        }
    }
}
