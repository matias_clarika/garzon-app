﻿using Android.Gms.Common.Apis;
using MvvmCross.Platform;
using MvvmCross.Platform.Droid.Platform;
using System;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Auth.Api;
using Android.Util;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Garzon.DataObjects.Social;
using Garzon.Core.Social.Google;
using Garzon.Config;
using Garzon.Config.Google;

namespace Garzon.Droid.GoogleUtils
{
    public class SignInGoogleManager : IGoogleManager
    {
        #region dependency
        readonly AppDefaultConfig _appConfig;
        #endregion
        #region fields
        const string TAG = "SignInGoogleManager";
        const int RC_SIGN_IN = 9001;

        GoogleApiClient _googleApiClient;

        TaskCompletionSource<SocialUser> tcs;

        MainActivity _mainActivity;
        #endregion

        #region constructor
        public SignInGoogleManager()
        {
            _appConfig = Mvx.Resolve<AppDefaultConfig>();
        }
        #endregion

        #region Implementation of IGoogleManager

        private void InitializeGoogleApi()
        {
            if (_googleApiClient == null)
            {
                try
                {
                    var config = Mvx.Resolve<IGoogleAppConfig>();
                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                                                                     .RequestServerAuthCode(config.GoogleAppId)
                                                                     .RequestProfile()
                                                                     .RequestEmail()
                                                                     .Build();

                    var activity = Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity;

                    _mainActivity = (MainActivity)activity;
                    _googleApiClient = new GoogleApiClient.Builder(_mainActivity)
                                                          .EnableAutoManage(_mainActivity, _mainActivity)
                                                          .AddApi(Auth.GOOGLE_SIGN_IN_API, gso)
                                                          .Build();
                }
                catch (Exception e)
                {
                    Log.Error(TAG, "InitializeGoogleApiClient: " + e);
                }
            }
        }

        public Task<SocialUser> SignIn()
        {
            if (tcs != null)
            {
                tcs.TrySetResult(null);
                tcs = null;
            }
            tcs = new TaskCompletionSource<SocialUser>();

            InitializeGoogleApi();
            var signInIntent = Auth.GoogleSignInApi.GetSignInIntent(_googleApiClient);

            if (_mainActivity != null)
            {
                _mainActivity.StartActivityForResult(signInIntent, RC_SIGN_IN);
            }

            return tcs.Task;
        }

        public void CancelSignIn()
        {
            _googleApiClient.Disconnect();
        }
        #endregion

        #region utils
        public void OnActivityResult(int requestCode,
                                     Result resultCode,
                                     Intent data)
        {
            if (requestCode == RC_SIGN_IN)
            {
                var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                var googleUser = HandleSignInResult(result);
                if (tcs != null)
                {
                    tcs.TrySetResult(googleUser);
                }
            }
        }

        public SocialUser HandleSignInResult(GoogleSignInResult result)
        {
            if (result != null && result.IsSuccess && result.SignInAccount != null)
            {
                // Signed in successfully, show authenticated UI.
                var acct = result.SignInAccount;
                return SocialUser.Builder()
                                 .WithId(acct.Id)
                                 .WithFirstName(acct.GivenName)
                                 .WithLastName(acct.FamilyName)
                                 .WithToken(acct.ServerAuthCode)
                                 .WithEmail(acct.Email)
                                 .WithPicture(acct.PhotoUrl != null ? acct.PhotoUrl.ToString() : "");
            }
            else
            {
                return null;
            }
        }

        #endregion

    }
}
