﻿using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Forms.Droid.Platform;
using MvvmCross.Forms.Platform;
using Clarika.Xamarin.Base.Helpers;
using Garzon.Droid.FirebaseUtils;
using Garzon.Droid.FacebookUtils;
using MvvmCross.Platform.Plugins;
using System;
using MvvmCross.Platform;
using Garzon.Core.Utils;
using Garzon.Droid.Utils;
using Garzon.Droid.GoogleUtils;
using Garzon.Core.Social.Facebook;
using Garzon.Core.Social.Google;
using Garzon.Forms.UI;
using Garzon.Config;
using Garzon.Core.Views;
using Garzon.Droid.Views;

namespace Garzon.Droid
{
    public class Setup : MvxFormsAndroidSetup
    {
        readonly Context _applicationContext;

        public Setup(Context applicationContext) : base(applicationContext)
        {
            _applicationContext = applicationContext;
        }

        protected override MvxFormsApplication CreateFormsApplication()
        {
            return new FormsApp();
        }

        protected override IMvxApplication CreateApp()
        {
            return new CoreApp();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxPluginConfiguration GetPluginConfiguration(Type plugin)
        {
            if (plugin == typeof(MvvmCross.Plugins.Json.PluginLoader))
            {
                return new MvvmCross.Plugins.Json.MvxJsonConfiguration()
                {
                    RegisterAsTextSerializer = false
                };
            }

            return null;
        }

        protected override void InitializeFirstChance()
        {
            Inject();
            base.InitializeFirstChance();
        }

        #region DI
        private void Inject()
        {
            Mvx.ConstructAndRegisterSingleton<IEnvironment, AndroidEnvironment>();
            Mvx.RegisterSingleton<IFileHelper>(new FileHelper());
            Mvx.RegisterSingleton<IFirebaseAnalytics>(() => new FirebaseAnalyticsDefault(_applicationContext));
            Mvx.RegisterSingleton<IFirebaseCrashReporting>(new FirebaseCrashlytics(_applicationContext));
            Mvx.LazyConstructAndRegisterSingleton<IFacebookManager, FacebookManager>();
            Mvx.LazyConstructAndRegisterSingleton<IGoogleManager, SignInGoogleManager>();
            Mvx.LazyConstructAndRegisterSingleton<IDisplay, AndroidDisplay>();
        }
       
        #endregion
    }
}
