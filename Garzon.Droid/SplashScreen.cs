﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;

namespace Garzon.Droid
{
    [Activity(
        Label = "@string/app_name"
        , MainLauncher = true
        , Icon = "@drawable/ic_launcher"
        , Theme = "@style/Theme.Splash"
        , NoHistory = true
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {

        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void TriggerFirstNavigate()
        {
            var lifecycle = Mvx.Resolve<Core.IAppLifecycle>();

            StartActivity(typeof(MainActivity));
            if (lifecycle == null || !lifecycle.ExistInstance)
            {
                base.TriggerFirstNavigate();
            }

        }
    }
}
