﻿using Acr.UserDialogs;using Android.App;using Android.Content;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.OS;
using Garzon.Core;
using Garzon.Core.Social.Facebook;
using Garzon.Core.Social.Google;
using Garzon.Droid.FacebookUtils;
using Garzon.Droid.GoogleUtils;
using MvvmCross.Forms.Droid.Views;
using MvvmCross.Platform;
using Xamarin.Facebook;namespace Garzon.Droid{    [Activity(Label = "@string/app_name",              Icon = "@drawable/ic_launcher",              Theme = "@style/GarzonTheme",              ScreenOrientation = ScreenOrientation.Portrait,              ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,              WindowSoftInputMode = Android.Views.SoftInput.AdjustResize,              LaunchMode = LaunchMode.SingleInstance)]    public class MainActivity : MvxFormsAppCompatActivity, GoogleApiClient.IOnConnectionFailedListener    {
        public static ICallbackManager CallbackManager { get; private set; }



        #region lifecycle         protected override void OnCreate(Bundle bundle)        {            TabLayoutResource = Resource.Layout.Tabbar;            ToolbarResource = Resource.Layout.Toolbar;            base.OnCreate(bundle);
            Rg.Plugins.Popup.Popup.Init(this, bundle);            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(false);            UserDialogs.Init(() => Mvx.Resolve<MvvmCross.Platform.Droid.Platform.IMvxAndroidCurrentTopActivity>().Activity);            if (CallbackManager == null)            {                CallbackManager = CallbackManagerFactory.Create();            }            Mvx.Resolve<IAppLifecycle>().ExistInstance = true;            Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;        }
        protected override void OnStart()
        {
            base.OnStart();            Mvx.Resolve<IAppLifecycle>().Foreground = true;
        }        protected override void OnStop()
        {            Mvx.Resolve<IAppLifecycle>().Foreground = false;
            base.OnStop();
        }        protected override void OnDestroy()
        {            Mvx.Resolve<IAppLifecycle>().ExistInstance = false;
            base.OnDestroy();
        }

        #endregion        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)        {            base.OnActivityResult(requestCode, resultCode, data);            if (CallbackManager != null)                CallbackManager.OnActivityResult(requestCode, (int)resultCode, data);

            TryFacebookResult(requestCode, resultCode, data);
            TryGoogleResult(requestCode, resultCode, data);
        }

        private static void TryGoogleResult(int requestCode, Result resultCode, Intent data)
        {
            var googleManager = Mvx.Resolve<IGoogleManager>();            if (googleManager != null)
            {                (googleManager as SignInGoogleManager).OnActivityResult(requestCode, resultCode, data);            }
        }

        private static void TryFacebookResult(int requestCode, Result resultCode, Intent data)
        {
            var facebookManager = Mvx.Resolve<IFacebookManager>();
            if (facebookManager != null)
            {
                (facebookManager as FacebookManager).CallbackManager.OnActivityResult(requestCode, (int)resultCode, data);
            }
        }

        #region Implementation of GoogleApiClient.IOnConnectionFailedListener
        public void OnConnectionFailed(ConnectionResult result)
        {

        }



        #endregion        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)        {            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);        }
    }}