﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource.Remote;
using Garzon.DataObjects;
using System.Linq;
using System;

namespace Garzon.DAL.Repository.Implementation
{
    public class OrderCounterDataRepository : IOrderCounterRepository
    {
        #region dependency
        readonly IOrderCounterRemoteDataSource _remoteDataSource;
        readonly IOrderRemoteDataSource _orderRemoteDataSource;
        readonly IOrderPayCounterRemoteDataSource _orderPayCounterRemoteDataSource;
        readonly IWaiterCounterRemoteDataSource _waiterCounterRemoteDataSource;
        #endregion
        public OrderCounterDataRepository(IOrderCounterRemoteDataSource remoteDataSource,
                                          IOrderRemoteDataSource orderRemoteDataSource,
                                          IOrderPayCounterRemoteDataSource orderPayCounterRemoteDataSource,
                                          IWaiterCounterRemoteDataSource waiterCounterRemoteDataSource)
        {
            _remoteDataSource = remoteDataSource;
            _orderRemoteDataSource = orderRemoteDataSource;
            _orderPayCounterRemoteDataSource = orderPayCounterRemoteDataSource;
            _waiterCounterRemoteDataSource = waiterCounterRemoteDataSource;
        }

        #region implementation of IOrderCounterRepository
        public async Task<List<OrderCounter>> Find(IPaggination paggination = null)
        {
            var queryFilter = BaseQueryFilter
                                .Builder()
                                .WithPaggination(paggination);

            return (await _remoteDataSource.Get(queryFilter)).ToList();
        }

        public Task<bool> MarkReceived(OrderCounterReceived orderCounterReceived)
        {
            return _remoteDataSource.MarkReceived(orderCounterReceived);
        }

        public Task<bool> SendObservation(OrderCounter orderCounter)
        {
            return _orderRemoteDataSource
                    .ObservedOrder(ConvertOrderCounterToOrder(orderCounter));
        }

        public Task<bool> SendConfirmation(OrderCounter orderCounter)
        {
            foreach (var detail in orderCounter.Details)
            {
                detail.Status = (int)OrderDetailStatus.KITCHEN;
            }
            return _orderRemoteDataSource.SendOrderConfirmation(ConvertOrderCounterToOrder(orderCounter));
        }

        public Task<bool> ClosedOrder(Order order)
        {
            return _orderRemoteDataSource.ClosedOrder(order);
        }

        public async Task<List<WaiterCounter>> FindWaiterCalled(IPaggination paggination)
        {
            var queryFilter = BaseQueryFilter
                                .Builder()
                                .WithPaggination(paggination);
            return (await _waiterCounterRemoteDataSource.Get(queryFilter)).ToList();
        }

        public async Task<List<OrderPayCounter>> FindOrderPayRequest(IPaggination paggination)
        {
            var queryFilter = BaseQueryFilter
                                .Builder()
                                .WithPaggination(paggination);
            return (await _orderPayCounterRemoteDataSource.Get(queryFilter)).ToList();
        }

        public async Task<List<Order>> FindOrders(int mode)
        {
            var queryFilter = BaseQueryFilter.Builder();
            queryFilter.WithParam("mode", mode);

            return (await _orderRemoteDataSource.Get(queryFilter)).ToList();
        }

        public Task<bool> ChangeTableNumberByOrderId(long sectorId, int tableNumber, long orderId)
        {
            return _remoteDataSource.ChangeTableNumber(sectorId, tableNumber, orderId);
        }

        public Task<bool> BlockUsersByOrderId(long orderId)
        {
            return _remoteDataSource.BlockUsersByOrderId(orderId);
        }


        public Task<bool> MarkWithdraw(Order order)
        {
            return _orderRemoteDataSource.MarkWithdraw(order);
        }

        public Task<bool> MarkDelivered(Order order)
        {
            return _orderRemoteDataSource.MarkDelivered(order);
        }
        #endregion

        private Order ConvertOrderCounterToOrder(OrderCounter ordercounter)
        {
            return new Order
            {
                ServerId = ordercounter.OrderId,
                Details = ordercounter.Details,
                OrderCounterId = ordercounter.Id,
                NumberToWithdraw = ordercounter.NumberToWithdraw,
                TimeDelay = ordercounter.TimeDelay,
                Mode = ordercounter.Mode
            };
        }

        public async Task<List<Order>> FindActiveOrders(int mode)
        {
            var queryFilter = BaseQueryFilter.Builder();
            queryFilter.WithParam("mode", mode);

            return (await _orderRemoteDataSource.GetActives(queryFilter)).ToList();
        }
    }
}
