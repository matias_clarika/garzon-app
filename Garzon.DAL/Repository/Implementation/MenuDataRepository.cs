﻿using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local;
using Garzon.DAL.DataSource.Remote;
using Garzon.DataObjects;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Garzon.DAL.Repository.Implementation
{
    public class MenuDataRepository : IMenuRepository
    {
        #region dependency
        readonly IMenuRemoteDataSource _remoteDataSource;
        readonly IMenuSectionLocalDataSource _menuSectionLocalDatasource;
        readonly IProductLocalDataSource _productLocalDataSource;
        #endregion
        public MenuDataRepository(IMenuRemoteDataSource remoteDataSource,
                                  IMenuSectionLocalDataSource menuSectionLocalDatasource,
                                  IProductLocalDataSource productLocalDataSource)
        {
            _remoteDataSource = remoteDataSource;
            _menuSectionLocalDatasource = menuSectionLocalDatasource;
            _productLocalDataSource = productLocalDataSource;
        }

        #region Implementation of IMenuRepository

        public async Task<Menu> FindMenuByPlace(long placeId)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
            Menu menu = await _remoteDataSource.FindMenuByPlace(placeId);
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
            await SaveMenuWithChildrens(menu);
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
            return menu;
        }
        #endregion

        #region cache utils
        private async Task SaveMenuWithChildrens(Menu menu)
        {
            if (menu.MenuSections != null)
            {
                //clear cache
                await _menuSectionLocalDatasource.DeleteAll();
                await _productLocalDataSource.DeleteAll();
                //update with new menu sections
                await _menuSectionLocalDatasource.InsertOrUpdate(menu.MenuSections);
                await SaveRecommendedProducts(menu);


            }
        }
        #endregion

        /// <summary>
        /// Saves the recommended products.
        /// </summary>
        /// <returns>The recommended products.</returns>
        /// <param name="menu">Menu.</param>
        private async Task SaveRecommendedProducts(Menu menu)
        {
            if (menu.RecommendedProducts != null)
            {
                await _productLocalDataSource.InsertOrUpdateRecommended(menu.RecommendedProducts);
            }
        }
    }
}
