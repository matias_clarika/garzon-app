﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local;
using Garzon.DAL.DataSource.Remote;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;

namespace Garzon.DAL.Repository.Implementation
{
    public class UserDataRepository : IUserRepository
    {
        #region dependencies
        /// <summary>
        /// The local data source.
        /// </summary>
        readonly IUserLocalDataSource _localDataSource;
        /// <summary>
        /// The remote data source.
        /// </summary>
        readonly IUserRemoteDataSource _remoteDataSource;
        #endregion

        #region params
        public const string USER_LOGGED = "is_logged";
        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Garzon.DAL.Repository.Implementation.UserDataRepository"/> class.
        /// </summary>
        /// <param name="localRepository">Local repository.</param>
        /// <param name="remoteDataSource">Remote data source.</param>
        public UserDataRepository(IUserLocalDataSource localRepository,
                                  IUserRemoteDataSource remoteDataSource)
        {
            _localDataSource = localRepository;
            _remoteDataSource = remoteDataSource;
        }

        public UserDataRepository()
        {
        }
        #region Implementation of IUserRepository
        public async Task<User> SignInFacebook(SocialUser socialUser)
        {
            User user = await _remoteDataSource.SignInSocial(SocialProviders.FACEBOOK,
                                                             socialUser);
            return user;
        }

        public async Task<User> SignInGoogle(SocialUser socialUser)
        {
            User user = await _remoteDataSource.SignInSocial(SocialProviders.GOOGLE,
                                                             socialUser);
            return user;
        }

        public Task<User> GetLoggedUser()
        {
            return _localDataSource.GetCurrentUserLogged();
        }

        public async Task<bool> LogoutCurrentUser()
        {
            try
            {
                var resultLocal = await _localDataSource.LogoutCurrentUser();
                var remoteResult = await _remoteDataSource.LogoutCurrentUser();
                return resultLocal && remoteResult;
            }
            catch
            {
                return false;
            }

        }

        public async Task SetDeviceToken(string deviceToken)
        {
            if (!string.IsNullOrEmpty(deviceToken))
            {
                await _localDataSource.SetDeviceToken(deviceToken);
                await _remoteDataSource.SetDeviceToken(deviceToken);
            }
        }

        public async Task<User> SignInLocalUser(User user)
        {
            await ProcessSignIn(user);
            return user;
        }

        public async Task<Counter> SignInCounter(string userName, string password)
        {
            var counter = await _remoteDataSource.SignInCounter(userName, password);
            await ProcessSignInCounter(counter);
            return counter;
        }


        public Task<bool> GetCurrentCounterIsMain()
        {
            return this._localDataSource.GetCurrentCounterIsMain();
        }

        public Task<long> GetCurrentCounterPlaceId()
        {
            return this._localDataSource.GetCurrentCounterPlaceId();
        }

        #endregion

        #region utils
        /// <summary>
        /// Processes the sign in counter.
        /// </summary>
        /// <returns>The sign in counter.</returns>
        /// <param name="counter">Counter.</param>
        private async Task ProcessSignInCounter(Counter counter)
        {
            await this._localDataSource.SetCurrentCounterIsMain(counter.Main);
            await this._localDataSource.SetCurrentCounterPlaceId(counter.PlaceId.Value);
            await ProcessSignIn(counter.GetUser());
        }
        /// <summary>
        /// Processes the sign in.
        /// </summary>
        /// <returns>The sign in.</returns>
        /// <param name="user">User.</param>
        private async Task ProcessSignIn(User user)
        {
            await _localDataSource.SetCurrentUserLogged(user);

            var deviceToken = await _localDataSource.GetDeviceToken();
            if (!string.IsNullOrEmpty(deviceToken))
            {
                await _remoteDataSource.SetDeviceToken(deviceToken);
            }
        }

        public Task<bool> IsCounterOnline(long counterId, bool isOnline)
        {
            return this._remoteDataSource.IsCounterOnline(counterId, isOnline);
        }

        public Task<bool> GetForceLogout()
        {
            return this._localDataSource.GetForceLogout();
        }

        public Task SetForceLogout(bool forceLogout)
        {
            return this._localDataSource.SetForceLogout(forceLogout);
        }
        #endregion
    }
}
