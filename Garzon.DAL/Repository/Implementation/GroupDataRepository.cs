﻿namespace Garzon.DAL.Repository.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource;
    using Garzon.DAL.DataSource.Local;
    using Garzon.DataObjects;

    public class GroupDataRepository : IGroupRepository
    {
        #region dependency
        readonly IGroupLocalDataSource groupLocalDataSource;
        #endregion
        public GroupDataRepository(IGroupLocalDataSource groupLocalDataSource)
        {
            this.groupLocalDataSource = groupLocalDataSource;
        }

        #region IGroupRepository
        public async Task<List<Group>> FindListByProductId(long productId, IPaggination paggination)
        {
            BaseQueryFilter queryFilter = BaseQueryFilter.Builder()
                                                         .WithPaggination(paggination)
                                                         .WithParam(GroupParams.PRODUCT_ID, productId);
            return (await this.groupLocalDataSource.Get(queryFilter)).ToList();
        }


        public Task<Group> FindById(long id, long productId)
        {
            return this.groupLocalDataSource.GetByIdAndProductId(id, productId);
        }
        #endregion

    }
}
