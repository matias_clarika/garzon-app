﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local;

namespace Garzon.DAL.Repository.Implementation
{
    public class TutorialDataRepository : ITutorialRepository
    {
        readonly ITutorialLocalDataSource tutorialLocalDataSource;

        public TutorialDataRepository(ITutorialLocalDataSource tutorialLocalDataSource){
            this.tutorialLocalDataSource = tutorialLocalDataSource;
        }

        #region ITutorialRepository
        public Task<bool> GetHowToCallCounterTutorial()
        {
            return this.tutorialLocalDataSource.GetHowToCallCounterTutorial();
        }

        public Task SetHowToCallCounterTutorial(bool value)
        {
            return this.tutorialLocalDataSource.SetHowToCallCounterTutorial(value);
        }


        public Task<bool> GetPinNumberAndCheckOrderTutorial()
        {
            return this.tutorialLocalDataSource.GetPinNumberAndCheckOrderTutorial();
        }

        public Task SetPinNumberAndCheckOrderTutorial(bool value)
        {
            return this.tutorialLocalDataSource.SetPinNumberAndCheckOrderTutorial(value);
        }
        #endregion
    }
}
