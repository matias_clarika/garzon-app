﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository.Implementation
{
    public class OrderDetailDataRepository : IOrderDetailRepository
    {
        #region dependency
        readonly IOrderDetailLocalDataSource orderDetailLocalDataSource;
        readonly IGroupLocalDataSource groupLocalDataSource;
        readonly IMenuSectionLocalDataSource menuSectionLocalDataSource;
        #endregion
        #region constructor
        public OrderDetailDataRepository(IOrderDetailLocalDataSource orderDetailLocalDataSource,
                                         IGroupLocalDataSource groupLocalDataSource,
                                         IMenuSectionLocalDataSource menuSectionLocalDataSource)
        {
            this.orderDetailLocalDataSource = orderDetailLocalDataSource;
            this.groupLocalDataSource = groupLocalDataSource;
            this.menuSectionLocalDataSource = menuSectionLocalDataSource;
        }
        #endregion

        #region Implementation of IOrderDetailRepository
        public Task<List<OrderDetail>> FindByOrderId(long orderId)
        {
            return this.orderDetailLocalDataSource.GetListByOrderId(orderId);
        }

        public Task<List<OrderDetail>> FindPending()
        {
            return this.orderDetailLocalDataSource.GetPendingList();
        }


        public Task<List<OrderDetail>> FindConfirmed()
        {
            return this.orderDetailLocalDataSource.GetConfirmedList();
        }

        public async Task<List<OrderDetail>> FindPendingByUserId(long userId)
        {
            var result = await this.orderDetailLocalDataSource.GetPendingListByUserId(userId);

            await MergeCompoundData(result);
            return result;
        }

        public Task<OrderDetail> InsertOrUpdate(OrderDetail orderDetail)
        {
            return this.orderDetailLocalDataSource.InsertOrUpdate(orderDetail);
        }

        public async Task<List<OrderDetail>> UpdatePendingToUser(long userId)
        {
            List<OrderDetail> pending = await FindPending();
            foreach (var detail in pending)
            {
                detail.UserId = userId;
            }
            await this.orderDetailLocalDataSource.InsertOrUpdate(pending);
            return pending;
        }

        public Task<List<OrderDetail>> FindConfirmedByUserId(long userId)
        {
            return this.orderDetailLocalDataSource.GetConfirmedListByUserId(userId);
        }

        public Task<bool> Delete(IEnumerable<OrderDetail> orderDetails)
        {
            return this.orderDetailLocalDataSource.Delete(orderDetails);
        }

        public Task<OrderDetail> FindPendingByProductId(long productId, User ownerUser = null)
        {
            return this.orderDetailLocalDataSource.GetPendingByProductId(productId, ownerUser);
        }

        public Task<List<OrderDetail>> FindWaitingOrderDetails(long userId)
        {
            return this.orderDetailLocalDataSource.GetWaitingOrderDetailList(userId);
        }

        public async Task<List<OrderDetail>> FindPendingRequestListByUserId(long userId)
        {
            var pendingRequestList = await this.orderDetailLocalDataSource.GetPendingRequestListByUserId(userId);
            await MergeCompoundData(pendingRequestList);
            return pendingRequestList;
        }


        public Task<OrderDetail> FindById(long id)
        {
            return this.orderDetailLocalDataSource.GetById(id);
        }

        public Task<bool> RemovePendingOrderDetailByProductId(long productId)
        {
            return this.orderDetailLocalDataSource.RemovePendingOrderDetailByProductId(productId);
        }

        public async Task DeletePendingOrderDetails()
        {
            await this.orderDetailLocalDataSource.DeletePendingOrderDetails();
        }

        public Task<bool> DeleteByParentId(long parentId)
        {
            return this.orderDetailLocalDataSource.DeleteByParentId(parentId);
        }


        #endregion

        #region private


        private async Task MergeCompoundData(List<OrderDetail> orderDetailList)
        {
            foreach (var orderDetail in orderDetailList)
            {
                await FindGroupByOrderDetail(orderDetail);
                await FindParentMenuSectionByOrderDetail(orderDetail);
            }
        }


        private async Task FindParentMenuSectionByOrderDetail(OrderDetail orderDetail)
        {
            if (orderDetail.ParentId.HasValue)
            {
                var parentOrderDetail = await this.orderDetailLocalDataSource.GetById(orderDetail.ParentId.Value);
                if (parentOrderDetail != null && parentOrderDetail.MenuSectionId.HasValue)
                {
                    var menuSection = await this.menuSectionLocalDataSource.GetById(parentOrderDetail.MenuSectionId.Value);
                    orderDetail.MenuSectionName = menuSection.Name;
                }
            }

        }

        private async Task FindGroupByOrderDetail(OrderDetail orderDetail)
        {
            if (orderDetail.GroupId.HasValue)
            {
                var group = await this.groupLocalDataSource.GetByIdAndProductId(orderDetail.GroupId.Value, orderDetail.ParentProductId.HasValue ? orderDetail.ParentProductId.Value : orderDetail.ProductId);
                orderDetail.GroupName = group.Name;
                orderDetail.SendLater = group.SendLater;
                orderDetail.GroupImage = group.Image;
            }
        }


        #endregion
    }
}
