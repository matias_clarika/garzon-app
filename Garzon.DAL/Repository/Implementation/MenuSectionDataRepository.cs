﻿namespace Garzon.DAL.Repository.Implementation
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DAL.DataSource.Local;
    using Garzon.DataObjects;
    using System.Linq;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource;

    public class MenuSectionDataRepository : IMenuSectionRepository
    {
        #region dependency
        readonly IMenuSectionLocalDataSource localDataSource;
        readonly IProductRepository productRepository;
        #endregion

        #region constructor
        public MenuSectionDataRepository(IMenuSectionLocalDataSource localDataSource, IProductRepository productRepository)
        {
            this.localDataSource = localDataSource;
            this.productRepository = productRepository;
        }
        #endregion
        #region Implementation of IMenuSectionRepository
        public async Task<List<MenuSection>> Find(long menuSectionId = -1, IPaggination paggination = null)
        {
            BaseQueryFilter queryFilter = BaseQueryFilter.Builder()
                                                         .WithPaggination(paggination);

            if (menuSectionId > 0)
            {
                queryFilter.WithParam(MenuParams.MENU_SECTION_ID, menuSectionId.ToString());
            }
            var menuSections = (await localDataSource.Get(queryFilter)).ToList();
            await CheckAvailableBySchedule(menuSections);
            return menuSections;
        }

        public Task<MenuSection> FindById(long menuSectionId)
        {
            return localDataSource.GetById(menuSectionId);
        }

        public Task<MenuSection> FindByProductId(long productId)
        {
            return localDataSource.FindByProductId(productId);
        }
        #endregion
        #region private

        private async Task CheckAvailableBySchedule(List<MenuSection> menuSections)
        {
            var menuSectionsPossibleHidden = menuSections.Where((arg) => arg.AvailableBySchedule).ToList();
            if (menuSectionsPossibleHidden.Any())
            {
                foreach (var menuSection in menuSectionsPossibleHidden)
                {
                    var exist = (await this.productRepository.FindBySectionMenu(menuSection.Id)).Any();
                    if (!exist)
                    {
                        menuSections.Remove(menuSection);
                    }
                }
            }
        }

        #endregion
    }
}
