﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource;
using Garzon.DAL.DataSource.Local;
using Garzon.DataObjects;
using System.Linq;
using Garzon.DAL.DataSource.Remote;

namespace Garzon.DAL.Repository.Implementation
{
    public class WaiterDataRepository : IWaiterRepository
    {
        #region dependency
        readonly IWaiterRemoteDataSource _remoteDataSource;
        readonly IWaiterLocalDataSource _localDataSource;
        #endregion

        public WaiterDataRepository(IWaiterRemoteDataSource remoteDataSource,
                                    IWaiterLocalDataSource localDataSource)
        {
            _remoteDataSource = remoteDataSource;
            _localDataSource = localDataSource;
        }

        #region Implementation of IWaiterRepository
        public async Task<List<WaiterCallOption>> FindByPlaceId(long placeId)
        {
            BaseQueryFilter queryFilter = BaseQueryFilter.Builder()
                                                         .WithParam(WaiterParams.PLACE_ID, placeId.ToString());
            
            return (await _localDataSource.Get(queryFilter)).ToList();
        }

        public Task<bool> CallWaiter(long placeId,
                                     long sectorId,
                                     int tableNumber,
                                     List<WaiterCallOption> options)
        {
            return _remoteDataSource.CallWaiter(placeId,
                                                sectorId,
                                                tableNumber,
                                                options);
        }
        #endregion
    }
}
