﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Remote;

namespace Garzon.DAL.Repository.Implementation
{
    public class CheckVersionDataRepository : ICheckVersionRepository
    {
        readonly ICheckVersionRemoteDataSource checkVersionRemoteDataSource;
        public CheckVersionDataRepository(ICheckVersionRemoteDataSource checkVersionRemoteDataSource)
        {
            this.checkVersionRemoteDataSource = checkVersionRemoteDataSource;
        }

        #region ICheckVersionRepository
        public Task<bool> ValidateAppVersion()
        {
            return checkVersionRemoteDataSource.ValidateAppVersion();
        }
        #endregion
    }
}
