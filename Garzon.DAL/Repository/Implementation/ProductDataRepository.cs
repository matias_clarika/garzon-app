﻿namespace Garzon.DAL.Repository
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DAL.DataSource.Local;
    using Garzon.DAL.DataSource.Remote;
    using Garzon.DataObjects;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource;

    public class ProductDataRepository : IProductRepository
    {
        #region dependency
        readonly IProductRemoteDataSource _remoteDataSource;
        readonly IProductLocalDataSource _localDataSource;
        #endregion

        #region constructor
        public ProductDataRepository(IProductRemoteDataSource remoteDataSource,
                                     IProductLocalDataSource localDataSource)
        {
            _remoteDataSource = remoteDataSource;
            _localDataSource = localDataSource;
        }

        #endregion

        #region Implementation of IProductRepository
        public async Task<List<Product>> FindBySectionMenu(long menuSectionId, IPaggination paggination = null)
        {
            BaseQueryFilter queryFilter = BaseQueryFilter.Builder()
                                                         .WithPaggination(paggination)
                                                         .WithParam(ProductParams.MENU_SECTION, menuSectionId);

            return (await this._localDataSource.Get(queryFilter)).ToList();
        }

        public async Task<List<Product>> FindRecommended(long placeId, IPaggination paggination = null)
        {
            return (await _localDataSource.FindRecommended(placeId, paggination)).ToList();
        }


        public Task<Product> FindById(long productId)
        {
            return _localDataSource.GetById(productId);
        }

        public async Task<List<Product>> FindListByGroupId(long groupId, IPaggination paggination = null)
        {
            BaseQueryFilter queryFilter = BaseQueryFilter.Builder()
                                                         .WithPaggination(paggination)
                                                         .WithParam(ProductParams.GROUP, groupId);
            return (await _localDataSource.Get(queryFilter)).ToList();
        }
        #endregion
    }
}
