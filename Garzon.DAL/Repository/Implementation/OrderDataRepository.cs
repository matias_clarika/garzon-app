﻿using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource;
using Garzon.DAL.DataSource.Local;
using Garzon.DAL.DataSource.Remote;
using Garzon.DataObjects;
using System.Linq;
using System;
using Newtonsoft.Json;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using System.Collections.Generic;
using Clarika.Xamarin.Base.Helpers;

namespace Garzon.DAL.Repository.Implementation
{
    public class OrderDataRepository : IOrderRepository
    {
        #region dependency
        readonly IOrderLocalDataSource _localDataSource;
        readonly IOrderRemoteDataSource _remoteDataSource;
        readonly IOrderDetailLocalDataSource _orderDetailLocalDataSource;
        #endregion

        #region fields
        JsonSerializerSettings _serializerSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            ContractResolver = new UnderscorePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };
        #endregion
        public OrderDataRepository(IOrderLocalDataSource localDataSource,
                                   IOrderRemoteDataSource remoteDataSource,
                                   IOrderDetailLocalDataSource orderDetailLocalDataSource)
        {
            _localDataSource = localDataSource;
            _remoteDataSource = remoteDataSource;
            _orderDetailLocalDataSource = orderDetailLocalDataSource;
        }

        #region Implementation of IOrderRepository
        public async Task<Order> SendToKitchen(Order order)
        {
            ClearStatusToOrderDetails(order.Details);
            order = await _remoteDataSource.SendToKitchen(order);
            await _orderDetailLocalDataSource.DeleteAll();
            await SaveOrder(order, false);
            return order;
        }

        public async Task<Order> GetOrderById(long id)
        {
            Order order = await GetRemoteOrderById(id);
            if (order != null)
            {
                await SaveOrder(order);
            }
            return order;
        }

        public async Task<Order> GetCurrentOrder()
        {
            Order order = await _localDataSource.GetCurrentOrder();

            return order;
        }

        public Task<Order> CheckOrder(Order order, bool force)
        {
            return _remoteDataSource.CheckOrder(order, force);
        }

        public async Task<Order> SyncOrder(string data)
        {
            try
            {
                Order order = await GetSyncOrder(data);
                if (order.IsInResto())
                {
                    await MarkCurrentOrderPendingToSync();

                    if (order.Validate())
                    {
                        await SaveOrder(order);
                        return order;
                    }
                    else if (order.ValidToCoreInfo())
                    {
                        await SaveOrder(order);
                    }
                }

                return order;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Marks the current order pending to sync.
        /// </summary>
        /// <returns>The current order pending to sync.</returns>
        /// <param name="isPending">If set to <c>true</c> is pending.</param>
        private async Task MarkCurrentOrderPendingToSync(bool isPending = true)
        {
            var order = await GetCurrentOrder();
            order.PendingToSync = isPending;
            await SaveOrder(order);
        }

        public async Task<bool> ClosedOrder(Order order)
        {
            try
            {
                await _localDataSource.DeleteAll();
                await _orderDetailLocalDataSource.DeleteInServer();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task<bool> ClearCurrentOrder()
        {
            try
            {
                await _localDataSource.DeleteAll();
                await _orderDetailLocalDataSource.DeleteAll();
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }
        #endregion

        #region private methods
        /// <summary>
        /// Gets the remote order by identifier.
        /// </summary>
        /// <returns>The remote order by identifier.</returns>
        /// <param name="id">Identifier.</param>
        private async Task<Order> GetRemoteOrderById(long id)
        {
            BaseQueryFilter query = BaseQueryFilter.Builder()
                                                               .WithParam(OrderParams.ID, id);
            Order order = (await _remoteDataSource.Get(query)).FirstOrDefault();
            return order;
        }

        /// <summary>
        /// Gets the sync order.
        /// </summary>
        /// <returns>The sync order.</returns>
        /// <param name="data">Data.</param>
        public async Task<Order> GetSyncOrder(string data)
        {//TODO mejorar manera en la cual se descarga, lo ideal seria retornar una exception indicandole que se debe correr un proceso para descargar la orden.
            try
            {
                Order order = JsonConvert.DeserializeObject<Order>(data, _serializerSettings);
                if (order.EntityId.HasValue && order.EntityId.Value > 0)
                {
                    return await PendingToDownload(new EntityPendingToDownload { EntityId = order.EntityId.Value });
                }
                else
                {
                    return order;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                var entityPendingToDownload = JsonConvert
                                                .DeserializeObject<EntityPendingToDownload>(data, _serializerSettings);
                return await PendingToDownload(entityPendingToDownload);
            }
        }

        private async Task<Order> PendingToDownload(EntityPendingToDownload entityPendingToDownload)
        {
            return await GetRemoteOrderById(entityPendingToDownload.EntityId);
        }

        /// <summary>
        /// Saves the order details.
        /// </summary>
        /// <returns>The order details.</returns>
        /// <param name="order">Order.</param>
        private async Task SaveOrderDetails(Order order)
        {
            if (order.Details != null && order.Details.Count > 0)
            {
                await _orderDetailLocalDataSource.DeleteInServer(order.Details);
                foreach (var detail in order.Details)
                {
                    var exist = await _orderDetailLocalDataSource.GetByServerId(detail.ServerId.Value);
                    if (exist == null || (!exist.Status.Equals(detail.Status) && !exist.IsPending()))
                    {
                        await _orderDetailLocalDataSource.InsertOrUpdate(detail);
                    }
                }
            }
        }

        private async Task SaveOrder(Order order, bool saveAllModes = true)
        {
            await _localDataSource.DeleteAll();
            if (saveAllModes || order.IsInResto())
            {
                await _localDataSource.Insert(order);

                if (order.ServerId > 0)
                {
                    await SaveOrderDetails(order);
                }
            }

        }

        /// <summary>
        /// Clears the status to order details.
        /// </summary>
        /// <param name="details">Details.</param>
        private void ClearStatusToOrderDetails(List<OrderDetail> details)
        {
            if (details != null)
            {
                foreach (var orderDetail in details)
                {
                    orderDetail.Status = null;
                    ClearStatusToOrderDetails(orderDetail.Details);
                }
            }
        }

        public Task<bool> SendTakeAwayExistOrder(long placeId, int orderNumber)
        {
            return this._remoteDataSource.SendTakeAwayExistOrder(placeId, orderNumber);
        }

        public async Task<List<Order>> GetMyOrderHistory(int offset, int limit)
        {
            IPaggination paggination = new Paggination(offset, limit);
            var queryFilter = BaseQueryFilter.Builder().WithPaggination(paggination);
            var result = await this._remoteDataSource.Get(queryFilter);
            return result.ToList();
        }
        #endregion


    }

    public class EntityPendingToDownload
    {
        public long EntityId
        {
            get;
            set;
        }

        public int? Mode
        {
            get;
            set;
        }

        public int? NumberToWithdraw
        {
            get;
            set;
        }
    }

}
