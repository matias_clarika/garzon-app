﻿namespace Garzon.DAL.Repository.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource.Local;
    using Garzon.DAL.DataSource.Remote;
    using Garzon.DataObjects;

    public class SectorDataRepository : ISectorRepository
    {
        #region dependency
        protected readonly ISectorCounterRemoteDataSource sectorCounterRemoteDataSource;
        #endregion
        #region constructor
        public SectorDataRepository(ISectorCounterRemoteDataSource sectorCounterRemoteDataSource)
        {
            this.sectorCounterRemoteDataSource = sectorCounterRemoteDataSource;
        }

        #endregion
        #region ISectorRepository
        public async Task<List<SectorCounter>> FindSectors()
        {
            IPaggination paggination = new Paggination(0, 100);
            var queryFilter = BaseQueryFilter.Builder().WithPaggination(paggination);
            var response = await this.sectorCounterRemoteDataSource.Get(queryFilter);
            return response.ToList();
        }

        public Task<bool> ChooseSectors(List<SectorCounter> sectors, bool force)
        {
            return this.sectorCounterRemoteDataSource.ChooseSectors(sectors, force);
        }

        #endregion
    }
}
