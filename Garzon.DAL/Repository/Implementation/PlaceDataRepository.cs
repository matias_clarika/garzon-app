﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DAL.DataSource.Local;
using Garzon.DAL.DataSource.Remote;
using Garzon.DataObjects;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Clarika.Xamarin.Base.Persistence.Manager;
using System.Linq;
using System.Globalization;

namespace Garzon.DAL.Repository.Implementation
{
    public class PlaceDataRepository : IPlaceRepository
    {
        #region dependency
        readonly IPlaceRemoteDataSource _remoteDataSource;
        readonly IPlaceLocalDataSource _localDataSource;
        #endregion

        #region constructor
        public PlaceDataRepository(IPlaceRemoteDataSource remoteDataSource,
                                   IPlaceLocalDataSource localDataSource)
        {
            _remoteDataSource = remoteDataSource;
            _localDataSource = localDataSource;
        }
        #endregion

        #region Implementation of IRestaurantRepository

        public async Task<List<Place>> Find(IPaggination paggination = null, double? latitude = null, double? longitude = null)
        {

            BaseQueryFilter queryFilter = BaseQueryFilter.Builder().WithPaggination(paggination);
            if (latitude.HasValue && longitude.HasValue)
            {
                queryFilter.WithParam(PlaceParams.LATITUDE, $"{latitude.Value.ToString(CultureInfo.InvariantCulture)}");
                queryFilter.WithParam(PlaceParams.LONGITUDE, $"{longitude.Value.ToString(CultureInfo.InvariantCulture)}");
            }
            var result = (await _remoteDataSource.Get(queryFilter)).ToList();
            await _localDataSource.DeleteAll();
            await _localDataSource.InsertOrUpdate(result);
            return result;
        }
        #endregion

    }
}
