﻿using System.Threading.Tasks;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;

namespace Garzon.DAL
{
    public interface IUserRepository
    {
        /// <summary>
        /// Signs the in facebook.
        /// </summary>
        /// <returns>The in facebook.</returns>
        /// <param name="socialUser">Social user.</param>
        Task<User> SignInFacebook(SocialUser socialUser);
        /// <summary>
        /// Signs the in google.
        /// </summary>
        /// <returns>The in google.</returns>
        /// <param name="socialUser">Social user.</param>
        Task<User> SignInGoogle(SocialUser socialUser);
        Task<long> GetCurrentCounterPlaceId();

        /// <summary>
        /// Signs the in local user.
        /// </summary>
        /// <returns>The in local user.</returns>
        /// <param name="user">User.</param>
        Task<User> SignInLocalUser(User user);

        /// <summary>
        /// Gets the logged user.
        /// </summary>
        /// <returns>The logged user.</returns>
        Task<User> GetLoggedUser();
        /// <summary>
        /// Logouts the current user.
        /// </summary>
        /// <returns>The current user.</returns>
        Task<bool> LogoutCurrentUser();

        /// <summary>
        /// Sets the device token.
        /// </summary>
        /// <returns>The device token.</returns>
        /// <param name="deviceToken">Device token.</param>
        Task SetDeviceToken(string deviceToken);

        /// <summary>
        /// Signs the in counter.
        /// </summary>
        /// <returns>The in counter.</returns>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        Task<Counter> SignInCounter(string userName, 
                                    string password);
        
        /// <summary>
        /// Ises the counter online.
        /// </summary>
        /// <returns>The counter online.</returns>
        /// <param name="counterId">Counter identifier.</param>
        /// <param name="isOnline">If set to <c>true</c> is online.</param>
        Task<bool> IsCounterOnline(long counterId, bool isOnline);

        /// <summary>
        /// Gets the current counter is main.
        /// </summary>
        /// <returns>The current counter is main.</returns>
        Task<bool> GetCurrentCounterIsMain();

        /// <summary>
        /// Gets the force logout.
        /// </summary>
        /// <returns>The force logout.</returns>
        Task<bool> GetForceLogout();

        /// <summary>
        /// Sets the force logout.
        /// </summary>
        /// <returns>The force logout.</returns>
        /// <param name="forceLogout">If set to <c>true</c> force logout.</param>
        Task SetForceLogout(bool forceLogout);
    }
}
