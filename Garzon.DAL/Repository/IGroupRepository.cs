﻿namespace Garzon.DAL.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DataObjects;

    public interface IGroupRepository
    {
        /// <summary>
        /// Finds the list by product identifier.
        /// </summary>
        /// <returns>The list by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Group>> FindListByProductId(long productId, IPaggination paggination);
        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="productId">Product identifier.</param>
        Task<Group> FindById(long id, long productId);
    }
}
