﻿using System;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IMenuRepository
    {
        /// <summary>
        /// Finds the menu by place.
        /// </summary>
        /// <returns>The menu by place.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<Menu> FindMenuByPlace(long placeId);
    }
}
