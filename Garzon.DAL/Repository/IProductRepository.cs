﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IProductRepository
    {
        /// <summary>
        /// Finds the recommended.
        /// </summary>
        /// <returns>The recommended.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Product>> FindRecommended(long placeId, IPaggination paggination = null);

        /// <summary>
        /// Finds the by section menu.
        /// </summary>
        /// <returns>The by section menu.</returns>
        /// <param name="menuSectionId">Menu section identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Product>> FindBySectionMenu(long menuSectionId, IPaggination paggination = null);
        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<Product> FindById(long productId);

        /// <summary>
        /// Finds the list by group identifier.
        /// </summary>
        /// <returns>The list by group identifier.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Product>> FindListByGroupId(long groupId, IPaggination paggination = null);

    }
}
