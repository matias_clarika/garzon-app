﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IOrderDetailRepository
    {

        /// <summary>
        /// Finds the by order identifier.
        /// </summary>
        /// <returns>The by order identifier.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<List<OrderDetail>> FindByOrderId(long orderId);

        /// <summary>
        /// Finds the pending by user.
        /// </summary>
        /// <returns>The pending by user.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> FindPendingByUserId(long userId);

        /// <summary>
        /// Finds the pending.
        /// </summary>
        /// <returns>The pending.</returns>
        Task<List<OrderDetail>> FindPending();

        /// <summary>
        /// Finds the confirmed.
        /// </summary>
        /// <returns>The confirmed.</returns>
        Task<List<OrderDetail>> FindConfirmed();

        /// <summary>
        /// Finds the confirmed by user identifier.
        /// </summary>
        /// <returns>The confirmed by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> FindConfirmedByUserId(long userId);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="orderDetail">Order detail.</param>
        Task<OrderDetail> InsertOrUpdate(OrderDetail orderDetail);

        /// <summary>
        /// Pendings to user.
        /// </summary>
        /// <returns>The to user.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> UpdatePendingToUser(long userId);

        /// <summary>
        /// Delete the specified orderDetails.
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="orderDetails">Order details.</param>
        Task<bool> Delete(IEnumerable<OrderDetail> orderDetails);

        /// <summary>
        /// Finds the pending by product identifier.
        /// </summary>
        /// <returns>The pending by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        /// <param name="ownerUser">Owner user.</param>
        Task<OrderDetail> FindPendingByProductId(long productId,
                                                 User ownerUser = null);
        /// <summary>
        /// Finds the waiting order details.
        /// </summary>
        /// <returns>The waiting order details.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> FindWaitingOrderDetails(long userId);

        /// <summary>
        /// Finds the pending request list by user identifier.
        /// </summary>
        /// <returns>The pending request list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> FindPendingRequestListByUserId(long userId);

        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<OrderDetail> FindById(long id);

        /// <summary>
        /// Removes the pending order detail by product identifier.
        /// </summary>
        /// <returns>The pending order detail by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<bool> RemovePendingOrderDetailByProductId(long productId);

        /// <summary>
        /// Deletes the pending order details.
        /// </summary>
        /// <returns>The pending order details.</returns>
        Task DeletePendingOrderDetails();

        /// <summary>
        /// Deletes the by parent identifier.
        /// </summary>
        /// <returns>The by parent identifier.</returns>
        /// <param name="parentId">Parent identifier.</param>
        Task<bool> DeleteByParentId(long parentId);
    }
}
