﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IMenuSectionRepository
    {
        /// <summary>
        /// Find the specified menuSectionId and paggination.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="menuSectionId">Menu section identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<MenuSection>> Find(long menuSectionId = -1, IPaggination paggination = null);
        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="menuSectionId">Menu section identifier.</param>
        Task<MenuSection> FindById(long menuSectionId);
        Task<MenuSection> FindByProductId(long productId);
    }
}
