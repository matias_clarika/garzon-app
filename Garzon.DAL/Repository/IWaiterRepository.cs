﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IWaiterRepository
    {
        /// <summary>
        /// Finds the by place identifier.
        /// </summary>
        /// <returns>The by place identifier.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<List<WaiterCallOption>> FindByPlaceId(long placeId);

        /// <summary>
        /// Calls the waiter.
        /// </summary>
        /// <returns>The waiter.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="tableNumber">Table number.</param>
        /// <param name="options">Options.</param>
        Task<bool> CallWaiter(long placeId,
                              long sectorId,
                              int tableNumber,
                              List<WaiterCallOption> options);
    }
}
