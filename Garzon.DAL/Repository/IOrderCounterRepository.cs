﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IOrderCounterRepository
    {

        /// <summary>
        /// Find the specified paggination.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<OrderCounter>> Find(IPaggination paggination = null);

        /// <summary>
        /// Marks the received.
        /// </summary>
        /// <returns>The received.</returns>
        /// <param name="orderCounterReceived">Order counter received.</param>
        Task<bool> MarkReceived(OrderCounterReceived orderCounterReceived);

        /// <summary>
        /// Sends the observation.
        /// </summary>
        /// <returns>The observation.</returns>
        /// <param name="orderCounter">Order counter.</param>
        Task<bool> SendObservation(OrderCounter orderCounter);

        /// <summary>
        /// Sends the confirmation.
        /// </summary>
        /// <returns>The confirmation.</returns>
        /// <param name="orderCounter">Order counter.</param>
        Task<bool> SendConfirmation(OrderCounter orderCounter);

        /// <summary>
        /// Closeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        Task<bool> ClosedOrder(Order order);

        /// <summary>
        /// Finds the waiter called.
        /// </summary>
        /// <returns>The waiter called.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<WaiterCounter>> FindWaiterCalled(IPaggination paggination);

        /// <summary>
        /// Finds the order pay request.
        /// </summary>
        /// <returns>The order pay request.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<OrderPayCounter>> FindOrderPayRequest(IPaggination paggination);

        /// <summary>
        /// Finds the orders.
        /// </summary>
        /// <returns>The orders.</returns>
        Task<List<Order>> FindOrders(int mode);

        /// <summary>
        /// Changes the table number by order identifier.
        /// </summary>
        /// <returns>The table number by order identifier.</returns>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="tableNumber">Table number.</param>
        /// <param name="orderId">Order identifier.</param>
        Task<bool> ChangeTableNumberByOrderId(long sectorId, int tableNumber, long orderId);

        /// <summary>
        /// Blocks the users by order identifier.
        /// </summary>
        /// <returns>The users by order identifier.</returns>
        /// <param name="serverId">Server identifier.</param>
        Task<bool> BlockUsersByOrderId(long serverId);
        Task<bool> MarkWithdraw(Order order);
        Task<bool> MarkDelivered(Order order);
        Task<List<Order>> FindActiveOrders(int rESTO);
    }
}
