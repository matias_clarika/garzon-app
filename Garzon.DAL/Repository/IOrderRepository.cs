﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository
{
    public interface IOrderRepository
    {
        /// <summary>
        /// Sends to kitchen.
        /// </summary>
        /// <returns>The to kitchen.</returns>
        /// <param name="order">Order.</param>
        Task<Order> SendToKitchen(Order order);

        /// <summary>
        /// Gets the order by identifier.
        /// </summary>
        /// <returns>The order by identifier.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<Order> GetOrderById(long orderId);

        /// <summary>
        /// Gets the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task<Order> GetCurrentOrder();

        /// <summary>
        /// Checks the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        /// <param name="force">If set to <c>true</c> force.</param>
        Task<Order> CheckOrder(Order order, bool force);

        /// <summary>
        /// Syncs the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="data">Data.</param>
        Task<Order> SyncOrder(string data);
        /// <summary>
        /// Closeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        Task<bool> ClosedOrder(Order order);
        /// <summary>
        /// Clears the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task<bool> ClearCurrentOrder();

        /// <summary>
        /// Gets the sync order.
        /// </summary>
        /// <returns>The sync order.</returns>
        /// <param name="data">Data.</param>
        Task<Order> GetSyncOrder(string data);

        /// <summary>
        /// Sends the take away exist order.
        /// </summary>
        /// <returns>The take away exist order.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="orderNumber">Order number.</param>
        Task<bool> SendTakeAwayExistOrder(long placeId, int orderNumber);

        /// <summary>
        /// Gets my order history.
        /// </summary>
        /// <returns>The my order history.</returns>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<Order>> GetMyOrderHistory(int offset, int limit);
    }
}
