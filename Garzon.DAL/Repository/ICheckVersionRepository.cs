﻿using System;
using System.Threading.Tasks;

namespace Garzon.DAL.Repository
{
    public interface ICheckVersionRepository
    {
        /// <summary>
        /// Validates the app version.
        /// </summary>
        /// <returns>The app version.</returns>
        Task<bool> ValidateAppVersion();
    }
}
