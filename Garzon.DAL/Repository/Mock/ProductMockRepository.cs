﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository.Mock
{
    public class ProductMockRepository : IProductRepository
    {

        private static long _productId = 1;
        #region implementation of IProductRepository
        public async Task<List<Product>> FindRecommended(long placeId, IPaggination paggination = null)
        {
            if(paggination==null){
                paggination = new Paggination(0, 10);
            }
            await Task.Delay(TimeSpan.FromMilliseconds(new Random().Next(500, 3000)));
            try
            {
                return GetProducts().GetRange(paggination.Offset, paggination.Limit);
            }
            catch
            {
                return new List<Product>();
            }
        }

        public async Task<List<Product>> FindBySectionMenu(long menuSectionId, IPaggination paggination = null)
        {
            return await FindRecommended(menuSectionId, paggination);
        }

        #endregion

        #region fake
        public static Product CreateProduct(string name, string image, double price, bool isRecommended, string description)
        {
            List<ProductAdditional> additional = new List<ProductAdditional>
            {
                new ProductAdditional { Name = "Salsa Mixta" },
                new ProductAdditional { Name = "Salsa Blanca" },
                new ProductAdditional { Name = "Salsa Pomodoro" }
            };

            Product newProduct = new Product
            {
                Id = _productId,
                Name = name,
                Description = description,
                Image = image,
                Price = price,
                IsRecommended = isRecommended,
                Calories = "650 Kcal",
                Additional = additional
            };
            _productId++;
            return newProduct;
        }

        public static List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110.99, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110.43, false, ""));

            //repeat
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110.99, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));
            products.Add(CreateProduct("Tallarines c/Salsa", "http://2.bp.blogspot.com/-ZeMZOODwIhM/VILvp9O6pWI/AAAAAAAA5OQ/hwOOHCSNiuE/s1600/carne%2B1.jpg", 110, false, "Incluye bebida y postre"));
            products.Add(CreateProduct("Ravioles Verdes c/ Pesto", "http://www.toquecriollo.com/wp-content/uploads/2015/02/toquecriollo_platodefondo_raviolesalpesto.jpg", 130, false, "Incluye bebida"));
            products.Add(CreateProduct("Lasagna de Carne y verdura", "https://i.ytimg.com/vi/WA9RKr_9Rs4/maxresdefault.jpg", 110, false, "No incluye bebida"));
            products.Add(CreateProduct("Ñoquis de Papa c/ salsa", "http://www.pragasrl.com.ar/contenidos/noticia_134.jpg", 110, false, ""));

            return products;
        }

        public Task<Product> FindById(long productId)
        {
            throw new NotImplementedException();
        }

        public Task<List<Product>> FindListByGroupId(long groupId, IPaggination paggination = null)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
