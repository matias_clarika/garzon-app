﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository.Mock
{
    public class MenuSectionMockRepository : IMenuSectionRepository
    {
        #region fields

        private long fakeGeneratedId = 0;
        #endregion
        public MenuSectionMockRepository()
        {
        }

        #region implementation of IMenuSectionRepository
        public async Task<List<MenuSection>> Find(long placeId, long menuSectionId, IPaggination paggination = null)
        {
            if(paggination==null){
                paggination = new Paggination(0, 10);
            }
            await Task.Delay(TimeSpan.FromMilliseconds(new Random().Next(500, 3000)));
            try
            {
                if(menuSectionId>0){
                    return GetIconMenuSections();
                }else{
                    return GetMenuSections().GetRange(paggination.Offset, paggination.Limit);    
                }

            }
            catch
            {
                return new List<MenuSection>();
            }
        }
        #endregion

        #region fake

        private List<MenuSection> GetMenuSections()
        {
            List<MenuSection> menuSections = new List<MenuSection>();

            menuSections.Add(CreateMenuSection("Entradas", "https://s3-media1.fl.yelpcdn.com/bphoto/7OEKArjyM34F9QE8AXURUw/o.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg", DeepLink.GoToMenuSectionImage));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg", DeepLink.GoToMenu));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg", DeepLink.GoToMenuSectionIcon));
            menuSections.Add(CreateMenuSection("Tablas", "http://www.patioburgues.com.ar/images/fotos/14.jpg"));
            menuSections.Add(CreateMenuSection("Hamburguesas", "http://www.patioburgues.com.ar/images/fotos/22.jpg"));
            menuSections.Add(CreateMenuSection("Sandwiches", "http://www.patioburgues.com.ar/images/fotos/21.jpg"));
            //repeat
            menuSections.Add(CreateMenuSection("Entradas", "http://img.recetas-de-cocina.net.s3.amazonaws.com/wp-content/uploads/2008/01/pimientos-rellenos-arroz.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg"));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg"));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg", DeepLink.GoToMenuSectionIcon));

            menuSections.Add(CreateMenuSection("Entradas", "http://img.recetas-de-cocina.net.s3.amazonaws.com/wp-content/uploads/2008/01/pimientos-rellenos-arroz.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg"));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg"));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg", DeepLink.GoToMenuSectionIcon));

            menuSections.Add(CreateMenuSection("Entradas", "http://img.recetas-de-cocina.net.s3.amazonaws.com/wp-content/uploads/2008/01/pimientos-rellenos-arroz.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg"));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg"));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg"));

            menuSections.Add(CreateMenuSection("Entradas", "http://img.recetas-de-cocina.net.s3.amazonaws.com/wp-content/uploads/2008/01/pimientos-rellenos-arroz.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg"));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg"));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg"));


            menuSections.Add(CreateMenuSection("Entradas", "http://img.recetas-de-cocina.net.s3.amazonaws.com/wp-content/uploads/2008/01/pimientos-rellenos-arroz.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg"));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg"));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg"));

            menuSections.Add(CreateMenuSection("Entradas", "http://img.recetas-de-cocina.net.s3.amazonaws.com/wp-content/uploads/2008/01/pimientos-rellenos-arroz.jpg"));
            menuSections.Add(CreateMenuSection("Carnes", "http://info7.blob.core.windows.net.optimalcdn.com/images/2016/11/08/614671_comida-19.jpg"));
            menuSections.Add(CreateMenuSection("Pastas", "http://www.comidakraft.com/-/media/assets/recipe_images/pasta-alfredo-de-pollo-145760-580x250.jpg"));
            menuSections.Add(CreateMenuSection("Ensaladas", "https://mejorconsalud.com/wp-content/uploads/2014/02/ensalada.jpg"));
            menuSections.Add(CreateMenuSection("Bebidas", "http://planetafeliz.cl/wp-content/uploads/2013/10/bebidas-azucaradas.jpg"));
            return menuSections;

        }

        private List<MenuSection> GetIconMenuSections()
        {
            List<MenuSection> menuSections = new List<MenuSection>
            {
                CreateMenuSectionIcon("Gaseosas", "https://d30y9cdsu7xlg0.cloudfront.net/png/189675-200.png"),
                CreateMenuSectionIcon("Aguas / Aguas saborizadas", "https://cdn3.iconfinder.com/data/icons/drinks-beverages/91/Drinks__Beverages_27-512.png"),
                CreateMenuSectionIcon("Jugos", "http://downloadicons.net/sites/default/files/drinks-icons-63272.png"),
                CreateMenuSectionIcon("Cervezas", "https://d30y9cdsu7xlg0.cloudfront.net/png/189675-200.png"),
                CreateMenuSectionIcon("Vinos", "http://www.iconninja.com/files/123/282/462/beverage-alcohol-bottle-drink-beer-icon.svg"),
                CreateMenuSectionIcon("Tragos", "https://cdn0.iconfinder.com/data/icons/food-and-drinks-vol-3/48/144-512.png")
            };
            return menuSections;

        }

        private MenuSection CreateMenuSection(string name, string image, DeepLink linkType = DeepLink.GoToProductImage)
        {
            fakeGeneratedId++;
            return new MenuSection
            {
                Id = fakeGeneratedId,
                Name = name,
                Image = image,
                DeepLink = linkType,
                FastCharge = false
            };
        }

        private MenuSection CreateMenuSectionIcon(string name, string icon, DeepLink linkType = DeepLink.GoToProduct){
            fakeGeneratedId++;
            return new MenuSection
            {
                Id = fakeGeneratedId,
                Name = name,
                Icon = icon,
                DeepLink = linkType,
                FastCharge = true
            };
        }

        public Task<List<MenuSection>> Find(long menuSectionId = -1, IPaggination paggination = null)
        {
            throw new NotImplementedException();
        }

        public Task<MenuSection> FindById(long menuSectionId)
        {
            throw new NotImplementedException();
        }

        public Task<MenuSection> FindByProductId(long productId)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
