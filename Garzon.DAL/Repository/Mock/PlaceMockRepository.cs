﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.Repository.Mock
{
    public class PlaceMockRepository : IPlaceRepository
    {
        
        #region Implementation of IPlaceRepository

        public async Task<List<Place>> Find(IPaggination paggination = null, double? latitude = null, double? longitude = null)
        {
            if (paggination == null)
            {
                paggination = new Paggination(0, 10);
            }

            await Task.Delay(TimeSpan.FromMilliseconds(new Random().Next(500, 3000)));
            try
            {
                return GetFakePlaces().GetRange(paggination.Offset, paggination.Limit);
            }
            catch
            {
                return new List<Place>();
            }
        }

        #endregion

        #region fake data

        private Place CreateFakePlace(long id,
                                             string name,
                                             string description,
                                             string image)
        {
            return new Place()
            {
                Id = id,
                Name = name,
                Description = description,
                Image = image,
                Address = "L. Tejeda 4500 esq. Reynafé. Cerro de las Rosas"
            };
        }
        private List<Place> GetFakePlaces()
        {
            List<Place> fakes = new List<Place>();
            fakes.Add(CreateFakePlace(1, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(2, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));

            //repeat
            fakes.Add(CreateFakePlace(3, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(4, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(5, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(6, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(7, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(8, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(1, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(2, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(3, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(4, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(5, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(6, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(7, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(8, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(1, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(2, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(3, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(4, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(5, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(6, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            fakes.Add(CreateFakePlace(7, "Patio Burges", "Hamburgesa / Sandwiches / Tablas", "http://www.patioburgues.com.ar/images/slider/patio-burgues.png"));
            fakes.Add(CreateFakePlace(8, "It Italy", "Pastas / Carnes / Cafes", "http://api.fidelitytools.net/obtenerImgProd.aspx?id=66763&idSitio=Mjgx"));
            return fakes;
        }
        #endregion
    }
}
