﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IProductLocalDataSource : IProductDataSource
    {
        /// <summary>
        /// Finds the recommended.
        /// </summary>
        /// <returns>The recommended.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<IEnumerable<Product>> FindRecommended(long placeId, IPaggination paggination = null);
        /// <summary>
        /// Inserts the or update recommended.
        /// </summary>
        /// <returns>The or update recommended.</returns>
        /// <param name="recommendedProducts">Recommended products.</param>
        Task InsertOrUpdateRecommended(List<Product> recommendedProducts);
        /// <summary>
        /// Inserts the or update by menu section.
        /// </summary>
        /// <returns>The or update by menu section.</returns>
        /// <param name="products">Products.</param>
        /// <param name="menuSection">Menu section.</param>
        Task<IEnumerable<Product>> InsertOrUpdateByMenuSection(List<Product> products,
                                         MenuSection menuSection);
    }
}
