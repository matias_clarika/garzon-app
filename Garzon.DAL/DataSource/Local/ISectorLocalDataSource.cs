﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface ISectorLocalDataSource : ISectorDataSource
    {
        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="dataObjects">Data objects.</param>
        /// <param name="placeId">Place identifier.</param>
        Task<IEnumerable<Sector>> InsertOrUpdate(List<Sector> dataObjects,
                                                 long placeId);
    }
}
