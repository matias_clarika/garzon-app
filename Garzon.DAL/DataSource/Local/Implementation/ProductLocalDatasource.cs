﻿namespace Garzon.DAL.DataSource.Local
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource.Local.Database.Dao;
    using Garzon.DAL.DataSource.Local.Entity.Mapper;
    using Garzon.DataObjects;
    using System.Linq;
    using Clarika.Xamarin.Base.Helpers;

    public class ProductLocalDataSource : IProductLocalDataSource
    {
        #region dependency
        readonly IProductDao productDao;
        readonly IProductGroupDao productGroupDao;
        readonly IGroupLocalDataSource groupLocalDataSource;
        readonly ProductEntityDataMapper productMapper;
        readonly ProductGroupEntityDataMapper productGroupMapper;
        #endregion

        #region constants
        private const int DEFAULT_LIMIT = 999;
        #endregion
        #region constructor
        public ProductLocalDataSource(IProductDao productDao,
                                      IProductGroupDao productGroupDao,
                                      IGroupLocalDataSource groupLocalDataSource,
                                      ProductEntityDataMapper productMapper,
                                      ProductGroupEntityDataMapper productGroupMapper)
        {
            this.productDao = productDao;
            this.productGroupDao = productGroupDao;
            this.groupLocalDataSource = groupLocalDataSource;
            this.productMapper = productMapper;
            this.productGroupMapper = productGroupMapper;
        }
        #endregion

        #region Implementation of IProductLocalDataSource

        public async Task<IEnumerable<Product>> Get(BaseQueryFilter queryFilter = null)
        {
            //default values
            int offset = 0;
            int limit = DEFAULT_LIMIT;

            IEnumerable<Product> result = default(IEnumerable<Product>);

            if (queryFilter != null)
            {
                offset = queryFilter.Offset;
                limit = queryFilter.Limit;

                if (queryFilter.ContainsKey(ProductParams.MENU_SECTION))
                {
                    var menuSectionId = queryFilter.GetIntParam(ProductParams.MENU_SECTION);
                    var products = await this.productDao.FindByMenuSectionId(menuSectionId, offset, limit);
                    result = this.productMapper.Transform(products);
                }
                else if (queryFilter.ContainsKey(ProductParams.GROUP))
                {
                    var groupId = queryFilter.GetIntParam(ProductParams.GROUP);
                    var products = await this.productGroupDao.FindByGroupId(groupId, offset, limit);
                    result = this.productGroupMapper.Transform(products);
                }

            }
            return result;
        }

        public async Task<Product> Insert(Product entity)
        {
            if (entity.GroupId != null && entity.GroupId > 0)
            {
                await productGroupDao.Insert(this.productGroupMapper.Mapping(entity));
            }
            else
            {
                await productDao.Insert(this.productMapper.Mapping(entity));
            }

            return entity;
        }

        public async Task<Product> Update(Product entity)
        {
            await productDao.Update(productMapper.Mapping(entity));
            return entity;
        }

        public async Task<Boolean> DeleteAll()
        {
            var resultProductDao = await this.productDao.DeleteAll() > 0;
            var resultProductGroupDao = await this.productGroupDao.DeleteAll() > 0;
            var resultGroupLocalDataSource = await this.groupLocalDataSource.DeleteAll();
            return resultProductDao && resultProductGroupDao && resultGroupLocalDataSource;
        }

        public async Task<IEnumerable<Product>> InsertOrUpdate(List<Product> dataObjects)
        {

            if (dataObjects != null)
            {
                await InsertOrUpdateMainProducts(dataObjects);
                await InsertOrUpdateProductChildrends(dataObjects);
            }
            return dataObjects;

        }

        public async Task<IEnumerable<Product>> FindRecommended(long placeId, IPaggination paggination = null)
        {
            return productMapper.Transform(await productDao.FindRecommended((int)placeId));
        }

        public async Task<Product> GetById(long id)
        {
            return productMapper.Transform(await productDao.FindById((int)id));
        }


        public async Task InsertOrUpdateRecommended(List<Product> recommendedProducts)
        {
            if (recommendedProducts != null)
            {
                List<Product> list = new List<Product>();
                foreach (var recommendedProduct in recommendedProducts)
                {
                    var product = await GetById(recommendedProduct.Id);
                    if (product == null)
                    {
                        product = recommendedProduct;
                    }
                    product.IsRecommended = true;
                    product.SequenceRecommended = recommendedProduct.Sequence;
                    list.Add(product);
                }

                await this.InsertOrUpdate(list);
            }
        }


        public Task<IEnumerable<Product>> InsertOrUpdateByMenuSection(List<Product> products,
                                                MenuSection menuSection)
        {
            foreach (var product in products)
            {
                product.MenuSectionId = menuSection.Id;
                product.FastOrder = menuSection.FastCharge ?? false;
            }
            return this.InsertOrUpdate(products);
        }

        #region NotImplemented



        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        #endregion
        #endregion

        #region private
        private async Task InsertOrUpdateMainProducts(List<Product> dataObjects)
        {
            if (dataObjects != null && dataObjects.Any())
            {
                if (dataObjects.Exists((obj) => obj.IsRecommended))
                {
                    await this.productDao.InsertOrReplaceRecommended(this.productMapper.Mapping(dataObjects));
                }
                else
                {
                    await this.productDao.InsertOrReplace(this.productMapper.Mapping(dataObjects));
                }

            }
        }

        private async Task InsertOrUpdateProductChildrends(List<Product> dataObjects)
        {
            if (dataObjects.Any())
            {
                List<Group> groupsToInsertOrReplace = new List<Group>();
                List<Product> productsToInsertOrReplace = new List<Product>();

                foreach (var dataObject in dataObjects)
                {
                    if (dataObject.Groups != null)
                    {
                        foreach (var group in dataObject.Groups)
                        {
                            group.ProductId = dataObject.Id;

                            foreach (var product in group.Products)
                            {
                                product.GroupId = group.Id;
                            }
                            productsToInsertOrReplace.AddRange(group.Products);
                        }
                        groupsToInsertOrReplace.AddRange(dataObject.Groups);
                    }
                }
                if (groupsToInsertOrReplace.Any())
                {
                    await groupLocalDataSource.InsertOrUpdate(groupsToInsertOrReplace);
                }
                if (productsToInsertOrReplace.Any())
                {
                    await productGroupDao.InsertOrReplace(this.productGroupMapper.Mapping(productsToInsertOrReplace));
                }

            }
        }

        private static List<Product> FilterProductsByType(List<Product> dataObjects, ProductType type)
        {
            return dataObjects.Where((filter) => filter.Type.Value == (int)type).ToList();
        }

        #endregion
    }
}
