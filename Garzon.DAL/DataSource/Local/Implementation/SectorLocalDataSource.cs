﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource.Local.Database.Dao;
using Garzon.DAL.DataSource.Local.Entity.Mapper;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Implementation
{
    public class SectorLocalDataSource : ISectorLocalDataSource
    {
        #region dependency
        readonly ISectorDao _sectorDao;
        readonly SectorEntityDataMapper _mapper;
        #endregion
        public SectorLocalDataSource(ISectorDao sectorDao,
                                     SectorEntityDataMapper mapper){
            _sectorDao = sectorDao;
            _mapper = mapper;
        }

        #region ISectorLocalDataSource

        public async Task<IEnumerable<Sector>> InsertOrUpdate(List<Sector> dataObjects)
        {
            if(dataObjects!=null){
                await _sectorDao.InsertOrReplace(_mapper.Mapping(dataObjects));    
            }
            return dataObjects;
        }


        public async Task<IEnumerable<Sector>> InsertOrUpdate(List<Sector> dataObjects, long placeId)
        {
            _mapper.PlaceId = (int)placeId;
            var result = await InsertOrUpdate(dataObjects);
            _mapper.PlaceId = -1;
            return result;
        }

        public async Task<bool> DeleteAll()
        {
            return await _sectorDao.DeleteAll() == 1;
        }


        public async Task<IEnumerable<Sector>> Get(BaseQueryFilter queryFilter = null)
        {
            int placeId =  queryFilter.GetIntParam(SectorParams.PLACE_ID);
            if (placeId > 0)
            {
                return _mapper.Transform(await _sectorDao.FindByPlaceId(placeId));
            }
            else
            {
                return null;
            }
        }
        #region not implemented
       

        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }
        public Task<Sector> GetById(long id)
        {
            throw new NotImplementedException();
        }

        public Task<Sector> Insert(Sector entity)
        {
            throw new NotImplementedException();
        }


        public Task<Sector> Update(Sector entity)
        {
            throw new NotImplementedException();
        }

        #endregion
        #endregion
    }
}
