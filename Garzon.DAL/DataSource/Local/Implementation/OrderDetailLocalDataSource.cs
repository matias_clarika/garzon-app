﻿namespace Garzon.DAL.DataSource.Local
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource.Local.Database.Dao;
    using Garzon.DAL.DataSource.Local.Entity;
    using Garzon.DAL.DataSource.Local.Entity.Mapper;
    using Garzon.DataObjects;
    using System.Linq;

    public class OrderDetailLocalDataSource : IOrderDetailLocalDataSource
    {
        #region dependency
        readonly IOrderDetailDao orderDetailDao;
        readonly OrderDetailEntityDataMapper orderDetailMapper;
        #endregion

        #region constructor
        public OrderDetailLocalDataSource(IOrderDetailDao orderDetailDao, OrderDetailEntityDataMapper orderDetailMapper)
        {
            this.orderDetailDao = orderDetailDao;
            this.orderDetailMapper = orderDetailMapper;
        }
        #endregion

        #region Implementation of IOrderDetailLocalDataSource

        public async Task<List<OrderDetail>> GetListByOrderId(long orderId)
        {
            List<OrderDetailEntity> orderDetailEntities = await orderDetailDao.FindListByOrderId((int)orderId);
            return orderDetailMapper.Transform(orderDetailEntities);
        }

        public async Task<OrderDetail> Insert(OrderDetail entity)
        {
            OrderDetailEntity orderDetailEntity = orderDetailMapper.Mapping(entity);
            int isAdded = await orderDetailDao.Insert(orderDetailEntity);
            entity.Id = (int)orderDetailEntity.Id;

            return entity;
        }

        public async Task<OrderDetail> Update(OrderDetail entity)
        {
            OrderDetailEntity orderDetailEntity = orderDetailMapper.Mapping(entity);
            int isAdded = await orderDetailDao.Update(orderDetailEntity);
            entity.Id = (int)orderDetailEntity.Id;

            return entity;
        }

        public async Task<List<OrderDetail>> GetPendingListByUserId(long userId)
        {
            var result = await orderDetailDao.FindPendingAndObservationListByUserId((int)userId);
            if (result != null)
            {
                return orderDetailMapper.Transform(result);
            }
            else
            {
                return null;
            }
        }

        public async Task<List<OrderDetail>> GetPendingList()
        {
            var result = await orderDetailDao.FindPendingAndObservationList();
            if (result != null)
            {
                return orderDetailMapper.Transform(result);
            }
            else
            {
                return null;
            }
        }

        public async Task<List<OrderDetail>> GetConfirmedList()
        {
            var result = await orderDetailDao.FindConfirmedList();
            if (result != null)
            {
                return orderDetailMapper.Transform(result);
            }
            else
            {
                return null;
            }
        }

        public async Task<IEnumerable<OrderDetail>> InsertOrUpdate(List<OrderDetail> dataObjects)
        {
            foreach (var detail in dataObjects)
            {
                await InsertOrUpdate(detail);
            }
            return dataObjects;
        }

        public async Task<OrderDetail> InsertOrUpdate(OrderDetail orderDetail)
        {
            var orderDetailEntity = await orderDetailDao.InsertOrUpdate(orderDetailMapper.Mapping(orderDetail));
            if (orderDetailEntity.Id.HasValue)
            {
                await this.DeleteByParentId(orderDetailEntity.Id.Value);
            }
            var resultOrderDetail = orderDetailMapper.Transform(orderDetailEntity);
            if (orderDetail.IsCompound)
            {
                List<OrderDetail> resultChildrenOrderDetail = new List<OrderDetail>();
                foreach (var childrenDetail in orderDetail.Details)
                {
                    childrenDetail.ParentId = orderDetailEntity.Id.Value;
                    childrenDetail.ParentProductId = orderDetailEntity.ProductId;
                    childrenDetail.UserId = orderDetail.UserId;
                    var childrenDetailEntity = await orderDetailDao.InsertOrUpdate(orderDetailMapper.Mapping(childrenDetail));
                    resultChildrenOrderDetail.Add(orderDetailMapper.Transform(childrenDetailEntity));
                }
                resultOrderDetail.Details = resultChildrenOrderDetail;
            }
            return resultOrderDetail;

        }

        public async Task<bool> DeletePending()
        {
            try
            {
                return await orderDetailDao.DeletePending() > 0;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<bool> DeleteInServer(List<OrderDetail> details = null)
        {
            try
            {
                if (CheckStatusToRefreshDetailsInOrder(details))
                {
                    await DeleteOrderDetailsInServer(details);
                    await DeleteOrderDetailNotExistInServer(details);
                    return true;
                }
                else if (details == null || !details.Any() || CheckIfOrderIsCompleteAndNotOnlyObservation(details))
                {
                    return await orderDetailDao.DeleteInServer() > 0;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        private async Task DeleteOrderDetailNotExistInServer(List<OrderDetail> details)
        {
            if (details != null && details.Any())
            {
                var childrenLists = details.Where((arg) => arg.IsCompound).Select((arg) => arg.Details).ToList();
                foreach (var childrenlist in childrenLists)
                {
                    details.AddRange(childrenlist);
                }

                await this.orderDetailDao.DeleteByNotExistServerId(details.Select((arg) => (int)arg.ServerId.Value).AsEnumerable());
            }

        }

        private async Task DeleteOrderDetailsInServer(List<OrderDetail> details)
        {
            foreach (var detail in details)
            {
                await DeleteOrderDetailInServer(detail);
                if (detail.IsCompound)
                {
                    await DeleteOrderDetailsInServer(detail.Details);
                }
            }
        }

        private async Task DeleteOrderDetailInServer(OrderDetail detail)
        {
            var existServerDetail = await this.GetByServerId(detail.ServerId.Value);

            if (existServerDetail != null
                && !existServerDetail.Status.Value.Equals(detail.Status.Value)
                && !existServerDetail.IsPending())
            {
                await this.orderDetailDao.DeleteByServerId((int)existServerDetail.ServerId.Value);
            }
        }

        private static bool CheckStatusToRefreshDetailsInOrder(List<OrderDetail> details)
        {
            return details != null
                && details.Any()
                          && details.Exists((obj) => obj.IsObserved() || obj.IsGroupObserved() || obj.IsCompound)
                          && CheckIfOrderIsCompleteAndNotOnlyObservation(details);
        }

        private static bool CheckIfOrderIsCompleteAndNotOnlyObservation(List<OrderDetail> details)
        {
            return details.Count() > details.Count((arg) => arg.IsObserved() || arg.IsGroupObserved());
        }

        public async Task<bool> DeleteInServerWithoutPending()
        {
            try
            {
                return await orderDetailDao.DeleteInServerWithoutPending() > 0;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<bool> DeleteAll()
        {
            return await orderDetailDao.DeleteAll() > 0;
        }

        public async Task<List<OrderDetail>> GetConfirmedListByUserId(long userId)
        {
            var result = await orderDetailDao.FindConfirmedListByUserId((int)userId);
            if (result != null)
            {
                return orderDetailMapper.Transform(result);
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> Delete(IEnumerable<OrderDetail> orderDetails)
        {
            if (orderDetails != null)
            {
                await DeleteCompoundPendingDetails(orderDetails);
                await DeleteCompoundLocalDetails(orderDetails.ToList());
                return await DeleteSimpleAndLocalOrderDetails(orderDetails);
            }
            else
            {
                return false;
            }
        }

        private async Task<bool> DeleteCompoundLocalDetails(List<OrderDetail> details)
        {
            var result = details.Where((filter) => filter.IsCompound && !filter.ServerId.HasValue).Select((x) => x.Details).ToList();
            if (result.Any())
            {
                foreach (var list in result)
                {
                    var ids = list.Select(x => (int)x.Id).AsEnumerable();
                    await orderDetailDao.Delete(ids);
                }
                return true;
            }
            return true;
        }

        private async Task DeleteCompoundPendingDetails(IEnumerable<OrderDetail> orderDetails)
        {
            var compoundPendingDetails = orderDetails.Where((filter) => filter.Details.Any() && filter.ServerId.HasValue && filter.Details.Where(detail => detail.SendLater.HasValue && detail.SendLater.Value).Count() == filter.Details.Count);
            foreach (var parentPendingDetail in compoundPendingDetails)
            {
                parentPendingDetail.SetInCounter();
                foreach (var pendingOrderDetail in parentPendingDetail.Details)
                {
                    pendingOrderDetail.SetPendingRequest();
                    pendingOrderDetail.Comment = string.Empty;
                }
            }
            await InsertOrUpdate(compoundPendingDetails.ToList());
        }

        private async Task<bool> DeleteSimpleAndLocalOrderDetails(IEnumerable<OrderDetail> orderDetails)
        {
            IEnumerable<int> ids = orderDetails.Where((filter) => (!filter.Details.Any() || !filter.ServerId.HasValue) || filter.IsObserved()).Select(x => (int)x.Id).AsEnumerable();
            if (ids.Any())
            {
                return await orderDetailDao.Delete(ids) > 0;
            }
            else
            {
                return true;
            }

        }

        public async Task<OrderDetail> GetPendingByProductId(long productId,
                                                       User ownerUser = null)
        {
            OrderDetailEntity orderDetailEntity;
            if (ownerUser != null)
            {
                orderDetailEntity = await orderDetailDao.FindPendingByProductAndUser((int)productId,
                                                                                      (int)ownerUser.Id);
            }
            else
            {
                orderDetailEntity = await orderDetailDao.FindPendingByProduct((int)productId);
            }
            if (orderDetailEntity != null)
            {
                return orderDetailMapper.Transform(orderDetailEntity);
            }
            else
            {
                return null;
            }
        }

        public async Task<List<OrderDetail>> GetWaitingOrderDetailList(long userId)
        {
            var result = await orderDetailDao.FindWaitingListByUserId((int)userId);
            return orderDetailMapper.Transform(result);
        }

        public async Task<List<OrderDetail>> GetPendingRequestListByUserId(long userId)
        {
            var result = await orderDetailDao.FindPendingRequestListByUserId((int)userId);
            return orderDetailMapper.Transform(result);
        }


        public async Task<bool> RemovePendingOrderDetailByProductId(long productId)
        {
            try
            {
                var entityPendingList = await this.orderDetailDao.FindListPendingByProductId((int)productId);
                var pendingList = orderDetailMapper.Transform(entityPendingList);
                await Delete(pendingList);
                return true;
            }
            catch
            {
                return false;
            }

        }


        public async Task DeletePendingOrderDetails()
        {
            await this.orderDetailDao.DeletePendingOrderDetails();
        }

        public async Task<OrderDetail> GetById(long id)
        {
            var entity = await this.orderDetailDao.FindById((int)id);
            return this.orderDetailMapper.Transform(entity);
        }

        public async Task<OrderDetail> GetByServerId(long serverId)
        {
            var entity = await this.orderDetailDao.FindByServerId((int)serverId);
            return this.orderDetailMapper.Transform(entity);
        }

        public async Task<bool> DeleteByParentId(long parentId)
        {
            try
            {
                return (await this.orderDetailDao.DeleteByParentId((int)parentId)) > 0;
            }
            catch
            {
                return false;
            }
        }

        #region NotImplemented
        public Task<IEnumerable<OrderDetail>> Get(BaseQueryFilter genericParams)
        {
            throw new NotImplementedException();
        }
        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        #endregion
        #endregion
    }
}
