﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local.LocalStorage;

namespace Garzon.DAL.DataSource.Local.Implementation
{
    public class TutorialLocalDataSource : ITutorialLocalDataSource
    {
        readonly ITutorialSharedPreferences tutorialSharedPreferences;
        public TutorialLocalDataSource(ITutorialSharedPreferences tutorialSharedPreferences)
        {
            this.tutorialSharedPreferences = tutorialSharedPreferences;
        }

        #region ITutorialLocalDataSource
        public Task<bool> GetHowToCallCounterTutorial()
        {
            return this.tutorialSharedPreferences.GetHowToCallCounterTutorial();
        }

        public Task SetHowToCallCounterTutorial(bool value)
        {
            return this.tutorialSharedPreferences.SetHowToCallCounterTutorial(value);
        }


        public Task<bool> GetPinNumberAndCheckOrderTutorial()
        {
            return this.tutorialSharedPreferences.GetPinNumberAndCheckOrderTutorial();
        }

        public Task SetPinNumberAndCheckOrderTutorial(bool value)
        {
            return this.tutorialSharedPreferences.SetPinNumberAndCheckOrderTutorial(value);
        }
        #endregion ITutorialLocalDataSource
    }
}
