﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource.Local.Database.Dao;
using Garzon.DAL.DataSource.Local.Entity.Mapper;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Implementation
{
    public class WaiterLocalDataSource : IWaiterLocalDataSource
    {
        #region dependency
        readonly IWaiterCallOptionDao _waiterCallOptionDao;
        readonly WaiterCallOptionEntityDataMapper _mapper;
        #endregion
        public WaiterLocalDataSource(IWaiterCallOptionDao waiterCallOptionDao,
                                              WaiterCallOptionEntityDataMapper mapper)
        {
            _waiterCallOptionDao = waiterCallOptionDao;
            _mapper = mapper;
        }

        #region Implementation of IWaiterCallOptionLocalDataSource

        public async Task<bool> DeleteAll()
        {
            return await _waiterCallOptionDao.DeleteAll() == 1;

        }

        public async Task<WaiterCallOption> Insert(WaiterCallOption entity)
        {
            await _waiterCallOptionDao.Insert(_mapper.Mapping(entity));
            return entity;
        }

        public async Task<IEnumerable<WaiterCallOption>> InsertOrUpdate(List<WaiterCallOption> dataObjects)
        {
            await _waiterCallOptionDao.InsertOrReplace(_mapper.Mapping(dataObjects));
            return dataObjects;
        }

        public async Task<WaiterCallOption> Update(WaiterCallOption entity)
        {
            await _waiterCallOptionDao.Update(_mapper.Mapping(entity));
            return entity;
        }

        public async Task<IEnumerable<WaiterCallOption>> Get(BaseQueryFilter queryFilter = null)
        {
            int placeId = queryFilter.GetIntParam(WaiterParams.PLACE_ID);
            if (placeId > 0)
            {
                return _mapper.Transform(await _waiterCallOptionDao.FindByPlace(placeId));
            }
            else
            {
                throw new Exception($"{WaiterParams.PLACE_ID} is required");
            }
        }

        public async Task<List<WaiterCallOption>> InsertOrUpdate(List<WaiterCallOption> waiterCallOptions, long placeId)
        {
            if (waiterCallOptions != null)
            {
                foreach (var waiterCallOption in waiterCallOptions)
                {
                    waiterCallOption.PlaceId = placeId;
                }
            }
            await InsertOrUpdate(waiterCallOptions);
            return waiterCallOptions;
        }


        public Task<bool> DeleteByPlaceId(long placeId)
        {
            return _waiterCallOptionDao.DeleteByPlaceId((int)placeId);
        }


        #region Not Implemented
        public Task<WaiterCallOption> GetById(long id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        #endregion
        #endregion
    }
}
