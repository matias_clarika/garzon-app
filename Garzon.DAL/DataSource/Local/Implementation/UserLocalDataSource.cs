﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource.Local.Database.Dao;
using Garzon.DAL.DataSource.Local.Entity;
using Garzon.DAL.DataSource.Local.Entity.Mapper;
using Garzon.DAL.DataSource.Local.LocalStorage;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public class UserLocalDataSource : IUserLocalDataSource
    {
        #region dependency
        readonly IUserDao _userDao;
        readonly UserEntityDataMapper _mapper;
        readonly IUserSharedPreferences _userSharedPreferences;
        #endregion
        public UserLocalDataSource(IUserDao userDao,
                                   IUserSharedPreferences userSharedPreferences,
                                   UserEntityDataMapper mapper)
        {
            _userDao = userDao;
            _userSharedPreferences = userSharedPreferences;
            _mapper = mapper;
        }

        public UserLocalDataSource()
        {
        }

        #region Implementation of IUserDataSource

        public async Task<User> Insert(User entity)
        {
            await _userDao.Insert(_mapper.Mapping(entity));
            return entity;
        }


        public async Task<User> Update(User entity)
        {
            await _userDao.Insert(_mapper.Mapping(entity));
            return entity;
        }

        public async Task<bool> LogoutCurrentUser()
        {
            UserEntity userEntity = await _userDao.FindUserLogged();
            await this.SetCurrentCounterIsMain(false);
            if (userEntity != null)
            {
                userEntity.IsLogged = false;
                await _userDao.Insert(userEntity);
                return true;
            }
            else
            {
                return false;
            }

        }

        public async Task<User> SetCurrentUserLogged(User user)
        {
            UserEntity userEntity = _mapper.Mapping(user);
            userEntity.IsLogged = true;
            await _userDao.Insert(userEntity);
            return user;
        }


        public async Task<User> GetCurrentUserLogged()
        {
            UserEntity userEntity = await _userDao.FindUserLogged();
            if (userEntity != null)
            {
                return _mapper.Transform(userEntity);
            }
            else
            {
                return null;
            }
        }

        public async Task SetDeviceToken(string deviceToken)
        {
            _userSharedPreferences.DeviceToken = deviceToken;
        }

        public async Task<string> GetDeviceToken()
        {
            return _userSharedPreferences.DeviceToken;
        }


        public Task SetCurrentCounterIsMain(bool isMain)
        {
            return Task.Factory.StartNew(() =>
            {
                _userSharedPreferences.CurrentCounterIsMain = isMain;
            });
        }

        public Task<bool> GetCurrentCounterIsMain()
        {
            return Task.Factory.StartNew<bool>(() =>  this._userSharedPreferences.CurrentCounterIsMain);
        }


        public Task<bool> GetForceLogout()
        {
            return Task.Factory.StartNew<bool>(() => this._userSharedPreferences.ForceLogout);
        }

        public Task SetForceLogout(bool forceLogout)
        {
            return Task.Factory.StartNew(() =>
            {
                _userSharedPreferences.ForceLogout = forceLogout;
            });
        }

        public Task SetCurrentCounterPlaceId(long placeId)
        {
            return Task.Factory.StartNew(() =>
            {
                _userSharedPreferences.CurrentCounterPlaceId = placeId;
            });
        }

        public Task<long> GetCurrentCounterPlaceId()
        {
            return Task.Factory.StartNew<long>(() => this._userSharedPreferences.CurrentCounterPlaceId);
        }

        #region NotImplemented
        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> Get(BaseQueryFilter genericParams = null)
        {
            throw new NotImplementedException();
        }

        public Task<User> GetById(long id)
        {
            throw new NotImplementedException();
        }



        public Task<IEnumerable<User>> InsertOrUpdate(List<User> dataObjects)
        {
            throw new NotImplementedException();
        }
        #endregion
        #endregion
    }
}
