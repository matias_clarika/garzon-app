﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource.Local.Database.Dao;
using Garzon.DAL.DataSource.Local.Entity.Mapper;
using Garzon.DataObjects;
using System.Linq;

namespace Garzon.DAL.DataSource.Local
{
    public class PlaceLocalDatasource : IPlaceLocalDataSource
    {
        #region dependency
        readonly IPlaceDao _placeDao;
        readonly PlaceEntityDataMapper _mapper;
        readonly IWaiterLocalDataSource _waiterLocalDataSource;
        readonly ISectorLocalDataSource _sectorLocalDataSource;
        #endregion

        #region constants
        const int DEFAULT_LIMIT = 20;
        #endregion
        #region constructor
        public PlaceLocalDatasource(IPlaceDao placeDao,
                                    PlaceEntityDataMapper mapper,
                                    IWaiterLocalDataSource waiterLocalDataSource,
                                    ISectorLocalDataSource sectorLocalDataSource)
        {
            _placeDao = placeDao;
            _mapper = mapper;
            _waiterLocalDataSource = waiterLocalDataSource;
            _sectorLocalDataSource = sectorLocalDataSource;
        }
        #endregion

        #region Implementation of IPlaceLocalDataSource

        public async Task<IEnumerable<Place>> Get(BaseQueryFilter queryFilter = null)
        {
            //default values
            int offset = 0;
            int limit = DEFAULT_LIMIT;
            if (queryFilter != null)
            {
                offset = queryFilter.Offset;
                limit = queryFilter.Limit;
            }
            List<Place> places = _mapper.Transform(await _placeDao.Find(offset, limit));
            await FindSectorsToPlaces(places);
            return places;
        }

        public async Task<IEnumerable<Place>> InsertOrUpdate(List<Place> places)
        {
            await _placeDao.InsertOrReplace(_mapper.Mapping(places));
            await InsertRelationshipsToPlaces(places);
            return places;
        }

        public async Task<Place> GetById(long id)
        {
            var placeEntity = await _placeDao.FindById((int)id);
            if (placeEntity != null)
            {
                var place = _mapper.Transform(placeEntity);
                await FindSectorToPlace(place);
                return place;
            }
            else
            {
                return null;
            }

        }

        public async Task<Place> Insert(Place entity)
        {
            await _placeDao.Insert(_mapper.Mapping(entity));
            await InsertRelationshipsToPlace(entity);
            return entity;
        }

        public async Task<bool> DeleteAll()
        {
            try
            {
                await _placeDao.DeleteAll();
                await _waiterLocalDataSource.DeleteAll();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region NotImplemented
        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        public Task<Place> Update(Place entity)
        {
            throw new NotImplementedException();
        }

        #endregion
        #endregion

        #region others
        private async Task InsertRelationshipsToPlaces(List<Place> places)
        {
            if (places != null)
            {
                foreach (var place in places)
                {
                    await InsertRelationshipsToPlace(place);
                }
            }
        }

        private async Task InsertRelationshipsToPlace(Place place)
        {
            await _waiterLocalDataSource.InsertOrUpdate(place.WaiterCallOptions, place.Id);
            await _sectorLocalDataSource.InsertOrUpdate(place.Sectors, place.Id);
        }

        private async Task FindSectorsToPlaces(List<Place> places)
        {
            if (places != null)
            {
                foreach (var place in places)
                {
                    await FindSectorToPlace(place);
                }
            }
        }

        private async Task FindSectorToPlace(Place place)
        {
            var query = BaseQueryFilter.Builder().WithParam(SectorParams.PLACE_ID, place.Id);
            place.Sectors = (await _sectorLocalDataSource.Get(query)).ToList();
        }
        #endregion
    }
}
