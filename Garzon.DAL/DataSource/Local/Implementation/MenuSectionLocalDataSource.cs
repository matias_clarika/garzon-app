﻿namespace Garzon.DAL.DataSource.Local
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource.Local.Database.Dao;
    using Garzon.DAL.DataSource.Local.Entity;
    using Garzon.DAL.DataSource.Local.Entity.Mapper;
    using Garzon.DataObjects;

    public class MenuSectionLocalDataSource : IMenuSectionLocalDataSource
    {

        #region dependency
        readonly IMenuSectionDao _menuSectionDao;
        readonly MenuSectionEntityDataMapper _mapper;
        readonly IProductLocalDataSource _productLocalDataSource;
        #endregion


        public MenuSectionLocalDataSource(IMenuSectionDao menuSectionDao,
                                          MenuSectionEntityDataMapper mapper,
                                          IProductLocalDataSource productLocalDataSource = null)
        {
            _menuSectionDao = menuSectionDao;
            _mapper = mapper;
            _productLocalDataSource = productLocalDataSource;
        }

        #region Implementation of IMenuSectionDataSource
        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<MenuSection>> Get(BaseQueryFilter queryFilter = null)
        {

            int parentId = queryFilter.GetIntParam(MenuParams.MENU_SECTION_ID);
            List<MenuSectionEntity> result;
            if (parentId > 0)
            {
                result = await _menuSectionDao.FindByParent(parentId);
            }
            else
            {
                result = await _menuSectionDao.Find();
            }
            return _mapper.Transform(result);
        }

        public async Task<MenuSection> Insert(MenuSection entity)
        {
            await _menuSectionDao.Insert(_mapper.Mapping(entity));
            return entity;
        }

        public async Task<IEnumerable<MenuSection>> InsertOrUpdate(List<MenuSection> dataObjects)
        {
            if (dataObjects != null)
            {
                dataObjects = await InsertOrUpdateWithCascade(dataObjects);
                await _menuSectionDao.InsertOrReplace(_mapper.Mapping(dataObjects));

            }
            return dataObjects;
        }

        public async Task<Boolean> DeleteAll()
        {
            return await _menuSectionDao.DeleteAll() == 1;
        }

        public async Task<MenuSection> GetById(long id)
        {
            return _mapper.Transform(await _menuSectionDao.FindById((int)id));
        }


        public async Task<IEnumerable<MenuSection>> FindListByProductId(long productId)
        {
            return _mapper.Transform(await _menuSectionDao.FindListByProductId((int)productId));
        }

        #region NotImplemented

        public Task<MenuSection> Update(MenuSection entity)
        {
            throw new NotImplementedException();
        }
        #endregion
        #endregion


        #region utils
        /// <summary>
        /// Inserts the or update with cascade.
        /// </summary>
        /// <returns>The or update with cascade.</returns>
        /// <param name="menuSections">Menu sections.</param>
        private async Task<List<MenuSection>> InsertOrUpdateWithCascade(List<MenuSection> menuSections)
        {
            menuSections = PlaneMenuSectionList(menuSections);
            foreach (var menuSection in menuSections)
            {
                if (menuSection.Products != null
                    && menuSection.Products.Any())
                {
                    if (menuSection.AvailableBySchedule)
                    {
                        await _productLocalDataSource.InsertOrUpdateByMenuSection(menuSection.Products, menuSection);
                    }
                    else
                    {
                        _productLocalDataSource.InsertOrUpdateByMenuSection(menuSection.Products, menuSection);
                    }

                }
            }

            return menuSections;
        }
        /// <summary>
        /// Planes the menu section list.
        /// </summary>
        /// <returns>The menu section list.</returns>
        /// <param name="menuSections">Menu sections.</param>
        private List<MenuSection> PlaneMenuSectionList(List<MenuSection> menuSections)
        {
            List<MenuSection> result = new List<MenuSection>(menuSections);
            foreach (var menuSection in menuSections)
            {
                if (menuSection.Children != null && menuSection.Children.Count > 0)
                {
                    result.AddRange(PlaneMenuSectionList(menuSection.Children));
                }
            }
            return result;
        }

        public async Task<MenuSection> FindByProductId(long productId)
        {
            var menuSection = _mapper.Transform(await _menuSectionDao.FindByProductId((int)productId));
            menuSection.Children = _mapper.Transform(await _menuSectionDao.FindByParent((int)menuSection.Id));
            return menuSection;
        }

        #endregion
    }
}
