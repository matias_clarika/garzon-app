﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DAL.DataSource.Local.Database.Dao;
using Garzon.DAL.DataSource.Local.Entity;
using Garzon.DAL.DataSource.Local.Entity.Mapper;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public class OrderLocalDataSource : IOrderLocalDataSource
    {
        #region dependency
        readonly IOrderDao _orderDao;
        readonly IPlaceLocalDataSource _placeLocalDatasource;
        readonly OrderEntityDataMapper _mapper;
        #endregion
        public OrderLocalDataSource(IOrderDao orderDao,
                                    IPlaceLocalDataSource placeLocalDatasource,
                                    OrderEntityDataMapper mapper)
        {
            _orderDao = orderDao;
            _mapper = mapper;
            _placeLocalDatasource = placeLocalDatasource;
        }

        #region Implementation of IOrderLocalDataSource

        public async Task<bool> Delete(long id)
        {
            OrderEntity orderEntity = await _orderDao.FindById((int)id);
            return await _orderDao.Delete(orderEntity) == 1;
        }

        public async Task<Order> GetById(long id)
        {
            return _mapper.Transform(await _orderDao.FindById((int)id));
        }

        public async Task<Order> Insert(Order entity)
        {

            OrderEntity orderEntity = _mapper.Mapping(entity);
            await _orderDao.Insert(orderEntity);
            return entity;

        }

        public async Task<Order> Update(Order entity)
        {
            OrderEntity orderEntity = _mapper.Mapping(entity);
            await _orderDao.Update(orderEntity);
            return entity;
        }


        public async Task<Order> GetCurrentOrder()
        {
            OrderEntity orderEntity = await _orderDao.FindCurrentOrder();
            if (orderEntity != null)
            {
                Order order = _mapper.Transform(orderEntity);
                if (order.Place == null)
                {
                    order.Place = await _placeLocalDatasource.GetById(order.PlaceId);
                }
                return order;
            }
            else
            {
                return null;
            }
        }


        public async Task<bool> DeleteAll()
        {
            try
            {
                return await _orderDao.DeleteAll() > 0;
            }
            catch
            {
                return false;
            }
        }

        #region NotImplemented

        public Task<IEnumerable<Order>> InsertOrUpdate(List<Order> dataObjects)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Order>> Get(BaseQueryFilter genericParams = null)
        {
            throw new NotImplementedException();
        }

        #endregion
        #endregion
    }
}
