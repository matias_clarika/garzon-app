﻿namespace Garzon.DAL.DataSource.Local.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource.Local.Database.Dao;
    using Garzon.DAL.DataSource.Local.Entity.Mapper;
    using Garzon.DataObjects;

    public class GroupLocalDataSource : IGroupLocalDataSource
    {
        #region dependency
        readonly IGroupDao groupDao;
        readonly GroupEntityDataMapper groupEntityDataMapper;
        #endregion
        public GroupLocalDataSource(IGroupDao groupDao,
                                    GroupEntityDataMapper groupEntityDataMapper)
        {
            this.groupDao = groupDao;
            this.groupEntityDataMapper = groupEntityDataMapper;
        }
        #region IGroupLocalDataSource
        public async Task<bool> DeleteAll()
        {
            return (await this.groupDao.DeleteAll()) > 0;
        }

        public async Task<IEnumerable<Group>> Get(BaseQueryFilter queryFilter = null)
        {
            if (queryFilter != null)
            {
                var productId = queryFilter.GetIntParam(GroupParams.PRODUCT_ID);
                var resultDao = await this.groupDao.FindListByProductId(productId);
                return this.groupEntityDataMapper.Transform(resultDao);
            }
            else
            {
                return null;
            }
        }

        public async Task<Group> GetByIdAndProductId(long id, long productId)
        {
            var entity = await this.groupDao.FindByServerId((int)id, (int)productId);
            return this.groupEntityDataMapper.Transform(entity);
        }

        public async Task<IEnumerable<Group>> InsertOrUpdate(List<Group> dataObjects)
        {
            await this.groupDao.InsertOrReplace(this.groupEntityDataMapper.Mapping(dataObjects));
            return dataObjects;
        }

        #region not implemented
        public Task<Group> Update(Group entity)
        {
            throw new NotImplementedException();
        }


        public Task<Group> Insert(Group entity)
        {
            throw new NotImplementedException();
        }

        public Task<Group> GetById(long id)
        {
            throw new NotImplementedException();
        }
        public Task<bool> Delete(long id)
        {
            throw new NotImplementedException();
        }
        #endregion
        #endregion
    }
}
