﻿
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IUserLocalDataSource : IUserDataSource
    {
        /// <summary>
        /// Sets the current user logged.
        /// </summary>
        /// <returns>The current user logged.</returns>
        /// <param name="user">User.</param>
        Task<User> SetCurrentUserLogged(User user);
        /// <summary>
        /// Gets the current user logged.
        /// </summary>
        /// <returns>The current user logged.</returns>
        Task<User> GetCurrentUserLogged();

        /// <summary>
        /// Sets the current counter is main.
        /// </summary>
        /// <returns>The current counter is main.</returns>
        /// <param name="isMain">If set to <c>true</c> is main.</param>
        Task SetCurrentCounterIsMain(bool isMain);
        /// <summary>
        /// Gets the current counter is main.
        /// </summary>
        /// <returns>The current counter is main.</returns>
        Task<bool> GetCurrentCounterIsMain();

        /// <summary>
        /// Gets the force logout.
        /// </summary>
        /// <returns>The force logout.</returns>
        Task<bool> GetForceLogout();

        /// <summary>
        /// Sets the force logout.
        /// </summary>
        /// <returns>The force logout.</returns>
        /// <param name="forceLogout">If set to <c>true</c> force logout.</param>
        Task SetForceLogout(bool forceLogout);

        /// <summary>
        /// Sets the current counter place identifier.
        /// </summary>
        /// <returns>The current counter place identifier.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task SetCurrentCounterPlaceId(long placeId);

        /// <summary>
        /// Gets the current counter place identifier.
        /// </summary>
        /// <returns>The current counter place identifier.</returns>
        Task<long> GetCurrentCounterPlaceId();
    }
}
