﻿
namespace Garzon.DAL.DataSource.Local.LocalStorage
{
    public interface IUserSharedPreferences
    {
        /// <summary>
        /// Gets or sets the device token.
        /// </summary>
        /// <value>The device token.</value>
        string DeviceToken { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="T:Garzon.DAL.DataSource.Local.LocalStorage.IUserSharedPreferences"/> current counter is main.
        /// </summary>
        /// <value><c>true</c> if current counter is main; otherwise, <c>false</c>.</value>
        bool CurrentCounterIsMain { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="T:Garzon.DAL.DataSource.Local.LocalStorage.IUserSharedPreferences"/> force logout.
        /// </summary>
        /// <value><c>true</c> if force logout; otherwise, <c>false</c>.</value>
        bool ForceLogout { get; set; }

        /// <summary>
        /// Gets or sets the current counter place identifier.
        /// </summary>
        /// <value>The current counter place identifier.</value>
        long CurrentCounterPlaceId { get; set; }
    }
}
