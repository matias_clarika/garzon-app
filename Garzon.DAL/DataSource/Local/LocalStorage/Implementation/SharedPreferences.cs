﻿using System;
using Plugin.Settings.Abstractions;

namespace Garzon.DAL.DataSource.Local.LocalStorage.Implementation
{
    public class SharedPreferences : ISharedPreferences
    {
        readonly ISettings _settings;

        public SharedPreferences(ISettings settings)
        {
            _settings = settings;
        }

        #region Implementation of IGenericSharedPreferences
        public void Put(string key, string value)
        {
            _settings.AddOrUpdateValue(key, value);
        }

        public string Get(string key,
                          string defaultValue = null)
        {
            return _settings.GetValueOrDefault(key, defaultValue);
        }

        public void Put(string key, bool value)
        {
            _settings.AddOrUpdateValue(key, value);
        }

        public bool Get(string key, bool defaultValue = false)
        {
            return _settings.GetValueOrDefault(key, defaultValue);
        }

        #endregion
    }
}
