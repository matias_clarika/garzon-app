﻿using System;
using System.Threading.Tasks;

namespace Garzon.DAL.DataSource.Local.LocalStorage.Implementation
{
    public class TutorialSharedPreferences : ITutorialSharedPreferences
    {
        #region constants
        private const string HOW_TO_CALL_COUNTER_TUTORIAL = "how_to_call_counter_tutorial";
        private const string PIN_NUMBER_AND_CHECKOUT_TUTORIAL = "pin_number_and_checkout_tutorial";
        #endregion

        readonly ISharedPreferences sharedPreferences;

        public TutorialSharedPreferences(ISharedPreferences sharedPreferences)
        {
            this.sharedPreferences = sharedPreferences;
        }

        #region ITutorialSharedPreferences
        public Task<bool> GetHowToCallCounterTutorial()
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                return this.sharedPreferences.Get(HOW_TO_CALL_COUNTER_TUTORIAL, true);
            });

        }

        public Task SetHowToCallCounterTutorial(bool value)
        {
            return Task.Factory.StartNew(() =>
            {
                this.sharedPreferences.Put(HOW_TO_CALL_COUNTER_TUTORIAL, value);
            });
        }


        public Task<bool> GetPinNumberAndCheckOrderTutorial()
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                return this.sharedPreferences.Get(PIN_NUMBER_AND_CHECKOUT_TUTORIAL, true);
            });
        }

        public Task SetPinNumberAndCheckOrderTutorial(bool value)
        {
            return Task.Factory.StartNew(() =>
            {
                this.sharedPreferences.Put(PIN_NUMBER_AND_CHECKOUT_TUTORIAL, value);
            });
        }
        #endregion
    }
}
