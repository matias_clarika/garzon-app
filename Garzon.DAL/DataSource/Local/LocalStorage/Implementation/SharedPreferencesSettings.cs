﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Garzon.DAL.DataSource.Local.LocalStorage.Implementation
{
    public class SharedPreferencesSettings
    {
        public static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }
    }
}
