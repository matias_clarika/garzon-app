﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Garzon.DAL.DataSource.Local.LocalStorage.Implementation
{

    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public class UserSharedPreferences : IUserSharedPreferences
    {
        #region constants
        private const string DeviceTokenKey = "device_token_key";
        private const string CounterIdKey = "counter_id_key";
        private const string CounterIsMainKey = "counter_is_main_key";
        private const string CounterPlaceId = "counter_place_id_key";
        private const string ForceLogoutKey = "force_logout_key";
        #endregion

        readonly ISharedPreferences _sharedPreferences;
        public UserSharedPreferences(ISharedPreferences sharedPreferences)
        {
            _sharedPreferences = sharedPreferences;
        }


        /// <summary>
        /// get the token registration id to send push notification
        /// </summary>
        /// <value>The device token.</value>
        public string DeviceToken
        {
            get
            {
                return _sharedPreferences.Get(DeviceTokenKey, null);
            }
            set
            {
                _sharedPreferences.Put(DeviceTokenKey, value);
            }
        }

        public bool CurrentCounterIsMain
        {
            get
            {
                return _sharedPreferences.Get(CounterIsMainKey, false);
            }
            set
            {
                _sharedPreferences.Put(CounterIsMainKey, value);
            }
        }

        public bool ForceLogout
        {
            get
            {
                return _sharedPreferences.Get(ForceLogoutKey, true);
            }
            set
            {
                _sharedPreferences.Put(ForceLogoutKey, value);
            }
        }

        public long CurrentCounterPlaceId
        {
            get
            {
                var stringValue = _sharedPreferences.Get(CounterPlaceId, "-1");
                return long.Parse(stringValue);
            }
            set
            {
                _sharedPreferences.Put(CounterPlaceId, $"{value}");
            }
        }
    }

}
