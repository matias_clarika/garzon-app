﻿using System;
using System.Threading.Tasks;

namespace Garzon.DAL.DataSource.Local.LocalStorage
{
    public interface ITutorialSharedPreferences
    {
        /// <summary>
        /// Gets the how to call counter tutorial.
        /// </summary>
        /// <returns>The how to call counter tutorial.</returns>
        Task<bool> GetHowToCallCounterTutorial();
        /// <summary>
        /// Sets the how to call counter tutorial.
        /// </summary>
        /// <returns>The how to call counter tutorial.</returns>
        /// <param name="value">If set to <c>true</c> value.</param>
        Task SetHowToCallCounterTutorial(bool value);
        /// <summary>
        /// Gets the pin number and check order tutorial.
        /// </summary>
        /// <returns>The pin number and check order tutorial.</returns>
        Task<bool> GetPinNumberAndCheckOrderTutorial();
        /// <summary>
        /// Sets the pin number and check order tutorial.
        /// </summary>
        /// <returns>The pin number and check order tutorial.</returns>
        /// <param name="value">If set to <c>true</c> value.</param>
        Task SetPinNumberAndCheckOrderTutorial(bool value);
    }
}
