﻿namespace Garzon.DAL.DataSource.Local.LocalStorage
{
    using System;

    public interface ISharedPreferences
    {
        /// <summary>
        /// Put the specified key and value.
        /// </summary>
        /// <returns>The put.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        void Put(string key, string value);

        /// <summary>
        /// Get the specified key and defaultValue.
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        string Get(string key, string defaultValue = null);

        /// <summary>
        /// Put the specified key and value.
        /// </summary>
        /// <returns>The put.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">If set to <c>true</c> value.</param>
        void Put(string key, bool value);

        /// <summary>
        /// Get the specified key and defaultValue.
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">If set to <c>true</c> default value.</param>
        bool Get(string key, bool defaultValue = default(bool));
    }
}
