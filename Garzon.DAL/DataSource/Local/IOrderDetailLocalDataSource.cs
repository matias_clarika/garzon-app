﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IOrderDetailLocalDataSource : IOrderDetailDataSource
    {
        /// <summary>
        /// Gets the list by order identifier.
        /// </summary>
        /// <returns>The list by order identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<List<OrderDetail>> GetListByOrderId(long id);

        /// <summary>
        /// Gets the pending list by user identifier.
        /// </summary>
        /// <returns>The pending list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> GetPendingListByUserId(long userId);

        /// <summary>
        /// Gets the pending list.
        /// </summary>
        /// <returns>The pending list.</returns>
        Task<List<OrderDetail>> GetPendingList();

        /// <summary>
        /// Gets the confirmed list.
        /// </summary>
        /// <returns>The confirmed list.</returns>
        Task<List<OrderDetail>> GetConfirmedList();

        /// <summary>
        /// Gets the confirmed list by user identifier.
        /// </summary>
        /// <returns>The confirmed list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> GetConfirmedListByUserId(long userId);

        /// <summary>
        /// Deletes the pending.
        /// </summary>
        /// <returns>The pending.</returns>
        Task<bool> DeletePending();

        /// <summary>
        /// Deletes the in server.
        /// </summary>
        /// <returns>The in server.</returns>
        /// <param name="detailsInServer">Details in server.</param>
        Task<bool> DeleteInServer(List<OrderDetail> detailsInServer = null);

        /// <summary>
        /// Deletes the in server without pending.
        /// </summary>
        /// <returns>The in server without pending.</returns>
        Task<bool> DeleteInServerWithoutPending();

        /// <summary>
        /// Delete the specified orderDetails.
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="orderDetails">Order details.</param>
        Task<bool> Delete(IEnumerable<OrderDetail> orderDetails);
        /// <summary>
        /// Gets the pending by product identifier.
        /// </summary>
        /// <returns>The pending by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        /// <param name="ownerUser">Owner user.</param>
        Task<OrderDetail> GetPendingByProductId(long productId,
                                                User ownerUser = null);

        /// <summary>
        /// Gets the waiting order detail list.
        /// </summary>
        /// <returns>The waiting order detail list.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> GetWaitingOrderDetailList(long userId);

        /// <summary>
        /// Gets the pending request list by user identifier.
        /// </summary>
        /// <returns>The pending request list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> GetPendingRequestListByUserId(long userId);

        /// <summary>
        /// Removes the pending order detail by product identifier.
        /// </summary>
        /// <returns>The pending order detail by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<bool> RemovePendingOrderDetailByProductId(long productId);

        /// <summary>
        /// Deletes the pending order details.
        /// </summary>
        /// <returns>The pending order details.</returns>
        Task DeletePendingOrderDetails();

        /// <summary>
        /// Gets the by server identifier.
        /// </summary>
        /// <returns>The by server identifier.</returns>
        /// <param name="serverId">Server identifier.</param>
        Task<OrderDetail> GetByServerId(long serverId);

        /// <summary>
        /// Deletes the by parent identifier.
        /// </summary>
        /// <returns>The by parent identifier.</returns>
        /// <param name="parentId">Parent identifier.</param>
        Task<bool> DeleteByParentId(long parentId);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="orderDetail">Order detail.</param>
        Task<OrderDetail> InsertOrUpdate(OrderDetail orderDetail);
    }
}
