﻿
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class OrderEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int? Id
        {
            get;
            set;
        }

        [Indexed]
        public long UserId
        {
            get;
            set;
        }

        public int SectorId
        {
            get;
            set;
        }

        public int TableNumber
        {
            get;
            set;
        }

        public int Pin
        {
            get;
            set;
        }

        public int? Status{
            get;
            set;
        }

        public int? QuantityDiners
        {
            get;
            set;
        }

        public int Mode
        {
            get;
            set;
        }

        #region place

        public int PlaceId
        {
            get;
            set;
        }

        #endregion

        #region sync
        public bool PendingToSync
        {
            get;
            set;
        }
        #endregion
        #endregion
    }
}
