﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class OrderCounterEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int? Id
        {
            get;
            set;
        }
        #endregion
    }
}
