﻿
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class MenuSectionEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Icon
        {
            get;
            set;
        }

        [Indexed]
        public int MenuId
        {
            get;
            set;
        }

        public int Parent
        {
            get;
            set;
        }

        public int DeepLink
        {
            get;
            set;
        }

        public bool FastCharge
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public bool AvailableBySchedule
        {
            get;
            set;
        }
        #endregion
    }
}
