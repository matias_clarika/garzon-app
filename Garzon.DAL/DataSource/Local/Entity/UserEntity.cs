﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class UserEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int Id
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Token
        {
            get;
            set;
        }

        public bool IsLogged
        {
            get;
            set;
        }

        #endregion
    }
}
