﻿namespace Garzon.DAL.DataSource.Local.Entity
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using SQLite;

    public class GroupEntity : IBaseEntity
    {
        [PrimaryKey]
        public int? Id
        {
            get;
            set;
        }

        public string Name{
            get;
            set;
        }

        [Indexed]
        public int ProductId{
            get;
            set;
        }

        public int Sequence{
            get;
            set;
        }

        public bool SendLater{
            get;
            set;
        }

        public string Image{
            get;
            set;
        }

        [Indexed]
        public int ServerId{
            get;
            set;
        }

        public bool Optional{
            get;
            set;
        }

        public bool MultipleSelection{
            get;
            set;
        }

        public string LabelEs{
            get;
            set;
        }

        public string LabelEn{
            get;
            set;
        }
    }
}
