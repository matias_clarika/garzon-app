﻿using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class MenuSectionEntityDataMapper : IEntityDataMapper<MenuSectionEntity, MenuSection>
    {

        #region Implementation of IEntityDataMapper
        public MenuSectionEntity Mapping(MenuSection dataObject)
        {
            var menuSectionEntity = new MenuSectionEntity
            {
                Id = (int)dataObject.Id,
                Name = dataObject.Name,
                Image = dataObject.Image,
                Icon = dataObject.Icon,
                MenuId = dataObject.MenuId != null ? (int)dataObject.MenuId.Value : -1,
                Parent = dataObject.Parent != null ? (int)dataObject.Parent.Value : -1,
                DeepLink = (int)dataObject.DeepLink,
                FastCharge = dataObject.FastCharge ?? false,
                Sequence = dataObject.Sequence,
                AvailableBySchedule = dataObject.AvailableBySchedule
            };

            if (dataObject.ProductId != null)
            {
                menuSectionEntity.ProductId = (int)dataObject.ProductId.Value;
            }

            return menuSectionEntity;
        }

        public List<MenuSectionEntity> Mapping(List<MenuSection> dataObjects)
        {
            List<MenuSectionEntity> entities = new List<MenuSectionEntity>();
            foreach (var menuSection in dataObjects)
            {
                entities.Add(Mapping(menuSection));
            }
            return entities;
        }

        public MenuSection Transform(MenuSectionEntity entity)
        {
            var menuSection = new MenuSection
            {
                Id = entity.Id,
                Name = entity.Name,
                Image = entity.Image,
                Icon = entity.Icon,
                MenuId = entity.MenuId,
                Parent = entity.Parent,
                DeepLink = (DeepLink)entity.DeepLink,
                FastCharge = entity.FastCharge,
                Sequence = entity.Sequence,
                AvailableBySchedule = entity.AvailableBySchedule
            };

            if (entity.ProductId > 0)
            {
                menuSection.ProductId = entity.ProductId;
            }

            return menuSection;
        }

        public List<MenuSection> Transform(List<MenuSectionEntity> entities)
        {
            List<MenuSection> menuSections = new List<MenuSection>();
            foreach (var menuSection in entities)
            {
                menuSections.Add(Transform(menuSection));
            }
            return menuSections;
        }
        #endregion
    }
}
