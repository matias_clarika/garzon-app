﻿using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class WaiterCallOptionEntityDataMapper : IEntityDataMapper<WaiterCallOptionEntity, WaiterCallOption>
    {
        #region Implementation of IEntityDataMapper
        public WaiterCallOptionEntity Mapping(WaiterCallOption dataObject)
        {
            return new WaiterCallOptionEntity
            {
                Id = (int)dataObject.Id,
                Name = dataObject.Name,
                PlaceId = (int)dataObject.PlaceId,
                Sequence = dataObject.Sequence
            };
        }

        public List<WaiterCallOptionEntity> Mapping(List<WaiterCallOption> dataObjects)
        {
            List<WaiterCallOptionEntity> entities = new List<WaiterCallOptionEntity>();

            foreach(var dataObject in dataObjects){
                entities.Add(Mapping(dataObject));    
            }

            return entities;
        }

        public WaiterCallOption Transform(WaiterCallOptionEntity entity)
        {
            return new WaiterCallOption
            {
                Id = entity.Id,
                Name = entity.Name,
                PlaceId = entity.PlaceId,
                Sequence = entity.Sequence
            };
        }

        public List<WaiterCallOption> Transform(List<WaiterCallOptionEntity> entities)
        {
            List<WaiterCallOption> dataObjects = new List<WaiterCallOption>();

            foreach (var entity in entities)
            {
                dataObjects.Add(Transform(entity));
            }

            return dataObjects;
        }
        #endregion
    }
}
