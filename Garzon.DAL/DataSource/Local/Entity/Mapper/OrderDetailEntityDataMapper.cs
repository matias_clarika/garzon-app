﻿using System;
using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class OrderDetailEntityDataMapper : BaseDataMapper<OrderDetailEntity, OrderDetail>, IEntityDataMapper<OrderDetailEntity, OrderDetail>
    {
        #region Implementation of EntityDataMapper
        public override OrderDetailEntity Mapping(OrderDetail dataObject)
        {
            OrderDetailEntity orderDetailEntity = new OrderDetailEntity
            {
                Comment = dataObject.Comment,
                Quantity = dataObject.Quantity,
                Amount = dataObject.Amount.Value,
                ProductId = (int)dataObject.Product.Id,
                ProductName = dataObject.Product.Name,
                ProductDescription = dataObject.Product.Description,
                ProductImage = dataObject.Product.Image,
                ProductCalories = dataObject.Product.Calories,
                ProductPrice = dataObject.Price.Value,
                ProductFastOrder = dataObject.Product.FastOrder ?? false,
                ProductMenuSectionId = dataObject.MenuSectionId.HasValue
                                                 && dataObject.MenuSectionId.Value > 0
                                                 ? dataObject.MenuSectionId.Value
                                                    : dataObject.Product.MenuSectionId.HasValue
                                                    ? (int)dataObject.Product.MenuSectionId.Value : 0,
                ProductType = dataObject.Product.Type.Value,
                ProductGroupId = (int?)(dataObject.GroupId ?? 0),
                ProductGroupSendLater = dataObject.SendLater.HasValue && dataObject.SendLater.Value
            };

            if (dataObject.OrderId > 0)
            {
                orderDetailEntity.OrderId = (int)dataObject.OrderId.Value;
            }

            if (dataObject.UserId > 0)
            {
                orderDetailEntity.UserId = (int)dataObject.UserId.Value;
            }
            if (dataObject.ServerId.HasValue && dataObject.ServerId.Value > 0)
            {
                orderDetailEntity.ServerId = (int)dataObject.ServerId.Value;
            }
            if (dataObject.Id > 0)
            {
                orderDetailEntity.Id = (int)dataObject.Id;
            }
            if (dataObject.Status > 0)
            {
                orderDetailEntity.Status = dataObject.Status.Value;
            }

            if (dataObject.ParentId > 0)
            {
                orderDetailEntity.ParentId = (int)dataObject.ParentId.Value;
            }

            if (dataObject.MenuSectionId.HasValue)
            {
                orderDetailEntity.ProductMenuSectionId = dataObject.MenuSectionId.Value;
            }

            if (dataObject.ParentProductId.HasValue)
            {
                orderDetailEntity.ParentProductId = dataObject.ParentProductId.Value;
            }
            return orderDetailEntity;
        }


        public override OrderDetail Transform(OrderDetailEntity entity)
        {
            if (entity != null)
            {
                OrderDetail orderDetail = new OrderDetail
                {
                    Comment = entity.Comment,
                    Amount = entity.Amount,
                    Quantity = entity.Quantity,
                    Product = new Product
                    {
                        Id = entity.ProductId,
                        Name = entity.ProductName,
                        Description = entity.ProductDescription,
                        Image = entity.ProductImage,
                        Calories = entity.ProductCalories,
                        Price = entity.ProductPrice,
                        FastOrder = entity.ProductFastOrder,
                        Type = entity.ProductType
                    },
                    Status = entity.Status,
                    SendLater = entity.ProductGroupSendLater
                };

                if (entity.ProductGroupId.HasValue && entity.ProductGroupId.Value > 0)
                {
                    orderDetail.Product.GroupId = entity.ProductGroupId.Value;
                    orderDetail.GroupId = entity.ProductGroupId.Value;
                }

                if (entity.ProductMenuSectionId.HasValue && entity.ProductMenuSectionId.Value > 0)
                {
                    orderDetail.Product.MenuSectionId = entity.ProductMenuSectionId;
                    orderDetail.MenuSectionId = entity.ProductMenuSectionId;
                }

                if (entity.UserId > 0)
                {
                    orderDetail.UserId = entity.UserId.Value;
                }

                if (entity.OrderId > 0)
                {
                    orderDetail.OrderId = entity.OrderId;
                }

                if (entity.Id > 0)
                {
                    orderDetail.Id = (int)entity.Id.Value;
                }
                if (entity.ServerId.HasValue && entity.ServerId.Value > 0)
                {
                    orderDetail.ServerId = entity.ServerId.Value;
                }

                if (entity.ParentId > 0)
                {
                    orderDetail.ParentId = entity.ParentId.Value;
                }

                if (entity.ParentProductId.HasValue)
                {
                    orderDetail.ParentProductId = entity.ParentProductId.Value;
                }

                return orderDetail;
            }
            else
            {
                return null;
            }


        }

        #endregion
    }
}
