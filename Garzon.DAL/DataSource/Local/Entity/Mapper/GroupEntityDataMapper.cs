﻿namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    using System;
    using System.Collections.Generic;
    using Garzon.DataObjects;

    public class GroupEntityDataMapper : BaseDataMapper<GroupEntity, Group>
    {
        public override GroupEntity Mapping(Group dataObject)
        {
            if (dataObject == null)
            {
                return null;
            }
            return new GroupEntity
            {
                Name = dataObject.Name,
                Sequence = dataObject.Sequence,
                ServerId = (int)dataObject.Id,
                ProductId = (int)dataObject.ProductId.Value,
                SendLater = dataObject.SendLater,
                Image = dataObject.Image,
                Optional = dataObject.Optional.HasValue && dataObject.Optional.Value,
                MultipleSelection = dataObject.MultipleSelection.HasValue && dataObject.MultipleSelection.Value,
                LabelEn = dataObject.LabelEn,
                LabelEs = dataObject.LabelEs
            };
        }

        public override Group Transform(GroupEntity entity)
        {
            if (entity == null)
            {
                return null;
            }
            return new Group
            {
                Name = entity.Name,
                Sequence = entity.Sequence,
                Id = entity.ServerId,
                ProductId = entity.ProductId,
                SendLater = entity.SendLater,
                Image = entity.Image,
                Optional = entity.Optional,
                MultipleSelection = entity.MultipleSelection,
                LabelEn = entity.LabelEn,
                LabelEs = entity.LabelEs
            };
        }
    }
}
