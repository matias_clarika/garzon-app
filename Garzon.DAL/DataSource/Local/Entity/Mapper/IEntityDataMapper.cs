﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public interface IEntityDataMapper<E, D>
    {
        /// <summary>
        /// Mapping the specified dataObject.
        /// </summary>
        /// <returns>The mapping.</returns>
        /// <param name="dataObject">Data object.</param>
        E Mapping(D dataObject);

        /// <summary>
        /// Mapping the specified dataObjects.
        /// </summary>
        /// <returns>The mapping.</returns>
        /// <param name="dataObjects">Data objects.</param>
        List<E> Mapping(List<D> dataObjects);

        /// <summary>
        /// Transform the specified entity.
        /// </summary>
        /// <returns>The transform.</returns>
        /// <param name="entity">Entity.</param>
        D Transform(E entity);

        /// <summary>
        /// Transform the specified entities.
        /// </summary>
        /// <returns>The transform.</returns>
        /// <param name="entities">Entities.</param>
        List<D> Transform(List<E> entities);
    }
}
