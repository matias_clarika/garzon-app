﻿using System;
using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class ProductEntityDataMapper : IEntityDataMapper<ProductEntity, Product>
    {

        #region Implementation of IEntityDataMapper
        public ProductEntity Mapping(Product dataObject)
        {
            if (dataObject != null)
            {
                var productEntity = new ProductEntity
                {
                    Id = (int)dataObject.Id,
                    Name = dataObject.Name,
                    ShortDescription = dataObject.ShortDescription,
                    Description = dataObject.Description,
                    Image = dataObject.Image,
                    Icon = dataObject.Icon,
                    Price = dataObject.Price,
                    Label = dataObject.Label,
                    FastOrder = dataObject.FastOrder ?? false,
                    Recommended = dataObject.IsRecommended,
                    Sequence = dataObject.Sequence ?? 999,
                    MenuSectionId = dataObject.MenuSectionId.HasValue ? (int)dataObject.MenuSectionId.Value : -1,
                    PlaceId = (int)dataObject.PlaceId,
                    Type = dataObject.Type.Value
                };

                MappingHour(dataObject, productEntity);
                productEntity.SequenceRecommended = dataObject.SequenceRecommended ?? 999;
                return productEntity;
            }
            else
            {
                return null;
            }

        }

        public List<ProductEntity> Mapping(List<Product> dataObjects)
        {
            List<ProductEntity> entities = new List<ProductEntity>();
            foreach (var product in dataObjects)
            {
                if (entities.Find((ProductEntity obj) => obj.Id == product.Id) == null)
                {
                    entities.Add(Mapping(product));
                }
            }
            return entities;
        }

        public Product Transform(ProductEntity entity)
        {
            if (entity != null)
            {
                var product = new Product
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    ShortDescription = entity.ShortDescription,
                    Description = entity.Description,
                    Image = entity.Image,
                    Icon = entity.Icon,
                    Price = entity.Price,
                    FastOrder = entity.FastOrder,
                    Label = entity.Label,
                    IsRecommended = entity.Recommended,
                    PlaceId = entity.PlaceId,
                    Sequence = entity.Sequence,
                    Type = entity.Type
                };

                TransformHour(entity, product);
                if (entity.MenuSectionId > 0)
                {
                    product.MenuSectionId = entity.MenuSectionId;
                }

                return product;
            }
            else
            {
                return null;
            }

        }

        public List<Product> Transform(List<ProductEntity> entities)
        {
            List<Product> products = new List<Product>();
            foreach (var entity in entities)
            {
                products.Add(Transform(entity));
            }
            return products;
        }

        /// <summary>
        /// Mappings the hour.
        /// </summary>
        /// <param name="product">Product.</param>
        /// <param name="productEntity">Product entity.</param>
        private void MappingHour(Product product, ProductEntity productEntity)
        {
            productEntity.HourMondayFrom = product.HourMondayFrom;
            productEntity.HourMondayTo = product.HourMondayTo;
            productEntity.HourMondayMore24Hours = product.HourMondayMore24Hours;

            productEntity.HourTuesdayFrom = product.HourTuesdayFrom;
            productEntity.HourTuesdayTo = product.HourTuesdayTo;
            productEntity.HourTuesdayMore24Hours = product.HourTuesdayMore24Hours;

            productEntity.HourWednesdayFrom = product.HourWednesdayFrom;
            productEntity.HourWednesdayTo = product.HourWednesdayTo;
            productEntity.HourWednesdayMore24Hours = product.HourWednesdayMore24Hours;

            productEntity.HourThursdayFrom = product.HourThursdayFrom;
            productEntity.HourThursdayTo = product.HourThursdayTo;
            productEntity.HourThursdayMore24Hours = product.HourThursdayMore24Hours;

            productEntity.HourFridayFrom = product.HourFridayFrom;
            productEntity.HourFridayTo = product.HourFridayTo;
            productEntity.HourFridayMore24Hours = product.HourFridayMore24Hours;

            productEntity.HourSaturdayFrom = product.HourSaturdayFrom;
            productEntity.HourSaturdayTo = product.HourSaturdayTo;
            productEntity.HourSaturdayMore24Hours = product.HourSaturdayMore24Hours;

            productEntity.HourSundayFrom = product.HourSundayFrom;
            productEntity.HourSundayTo = product.HourSundayTo;
            productEntity.HourSundayMore24Hours = product.HourSundayMore24Hours;
        }

        /// <summary>
        /// Transforms the hour.
        /// </summary>
        /// <param name="productEntity">Product entity.</param>
        /// <param name="product">Product.</param>
        private void TransformHour(ProductEntity productEntity, Product product)
        {
            product.HourMondayFrom = productEntity.HourMondayFrom;
            product.HourMondayTo = productEntity.HourMondayTo;
            product.HourMondayMore24Hours = productEntity.HourMondayMore24Hours;

            product.HourTuesdayFrom = productEntity.HourTuesdayFrom;
            product.HourTuesdayTo = productEntity.HourTuesdayTo;
            product.HourTuesdayMore24Hours = productEntity.HourTuesdayMore24Hours;

            product.HourWednesdayFrom = productEntity.HourWednesdayFrom;
            product.HourWednesdayTo = productEntity.HourWednesdayTo;
            product.HourWednesdayMore24Hours = productEntity.HourWednesdayMore24Hours;

            product.HourThursdayFrom = productEntity.HourThursdayFrom;
            product.HourThursdayTo = productEntity.HourThursdayTo;
            product.HourThursdayMore24Hours = productEntity.HourThursdayMore24Hours;

            product.HourFridayFrom = productEntity.HourFridayFrom;
            product.HourFridayTo = productEntity.HourFridayTo;
            product.HourFridayMore24Hours = productEntity.HourFridayMore24Hours;

            product.HourSaturdayFrom = productEntity.HourSaturdayFrom;
            product.HourSaturdayTo = productEntity.HourSaturdayTo;
            product.HourSaturdayMore24Hours = productEntity.HourSaturdayMore24Hours;

            product.HourSundayFrom = productEntity.HourSundayFrom;
            product.HourSundayTo = productEntity.HourSundayTo;
            product.HourSundayMore24Hours = productEntity.HourSundayMore24Hours;
        }

        #endregion
    }
}
