﻿using System;
using System.Collections.Generic;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public abstract class BaseDataMapper<E, D> : IEntityDataMapper<E, D>
    {
        public List<E> Mapping(List<D> dataObjects)
        {
            List<E> entities = new List<E>();
            if (dataObjects != null)
            {
                foreach (D dataObject in dataObjects)
                {

                    var entity = Mapping(dataObject);
                    if (entity != null)
                    {
                        entities.Add(entity);
                    }
                }
            }

            return entities;
        }

        public List<D> Transform(List<E> entities)
        {
            List<D> dataObjects = new List<D>();
            if (entities != null)
            {
                foreach (E entity in entities)
                {
                    var dataObject = Transform(entity);
                    if (dataObject != null)
                    {
                        dataObjects.Add(dataObject);
                    }

                }
            }

            return dataObjects;
        }

        public abstract D Transform(E entity);

        public abstract E Mapping(D dataObject);

    }
}
