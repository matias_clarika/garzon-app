﻿namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    using System;
    using System.Collections.Generic;
    using Garzon.DataObjects;

    public class ProductGroupEntityDataMapper : BaseDataMapper<ProductGroupEntity, Product>, IEntityDataMapper<ProductGroupEntity, Product>
    {

        #region IEntityDataMapper
        public override Product Transform(ProductGroupEntity entity)
        {
            return new Product
            {
                Id = entity.ServerId.HasValue ? entity.ServerId.Value : 0,
                Name = entity.Name,
                ShortDescription = entity.ShortDescription,
                Description = entity.Description,
                Price = entity.Price,
                IsRecommended = false,
                Sequence = entity.Sequence,
                GroupId = entity.GroupId,
                PlaceId = entity.PlaceId
            };
        }

        public override ProductGroupEntity Mapping(Product dataObject)
        {
            if (dataObject != null)
            {
                var productGroup = new ProductGroupEntity
                {
                    ServerId = (int)dataObject.Id,
                    Name = dataObject.Name,
                    ShortDescription = dataObject.ShortDescription,
                    Description = dataObject.Description,
                    Price = dataObject.Price,
                    Sequence = dataObject.Sequence ?? 999,
                    GroupId = dataObject.GroupId != null ? (int)dataObject.GroupId.Value : -1,
                    PlaceId = (int)dataObject.PlaceId
                };
                if (dataObject.IsRecommended)
                {
                    productGroup.SequenceRecommended = dataObject.SequenceRecommended ?? 999;
                }
                return productGroup;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
