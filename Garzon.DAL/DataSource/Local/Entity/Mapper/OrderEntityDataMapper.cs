﻿using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class OrderEntityDataMapper : IEntityDataMapper<OrderEntity, Order>
    {

        #region Implementation of EntityDataMapper
        public OrderEntity Mapping(Order dataObject)
        {
            if (dataObject != null)
            {
                OrderEntity orderEntity = new OrderEntity
                {
                    Id = (int)dataObject.ServerId,
                    PlaceId = (int)dataObject.PlaceId,
                    UserId = dataObject.UserId.Value,
                    PendingToSync = dataObject.PendingToSync,
                    Status = dataObject.Status.HasValue ? dataObject.Status.Value : default(int),
                    Mode = dataObject.Mode
                };

                if (dataObject.Id > 0)
                {
                    orderEntity.Id = (int)dataObject.Id;
                }

                if (dataObject.QuantityDiners.HasValue)
                {
                    orderEntity.QuantityDiners = dataObject.QuantityDiners.Value;
                }

                if (dataObject.SectorId.HasValue)
                {
                    orderEntity.SectorId = (int)dataObject.SectorId.Value;
                }

                if (dataObject.TableNumber.HasValue)
                {
                    orderEntity.TableNumber = (int)dataObject.TableNumber.Value;
                }

                if (dataObject.Pin.HasValue)
                {
                    orderEntity.Pin = dataObject.Pin.Value;
                }

                return orderEntity;
            }
            else
            {
                return null;
            }
        }

        public List<OrderEntity> Mapping(List<Order> dataObjects)
        {
            List<OrderEntity> entities = new List<OrderEntity>();

            foreach (var order in dataObjects)
            {
                entities.Add(Mapping(order));
            }

            return entities;
        }

        public Order Transform(OrderEntity entity)
        {
            if (entity != null)
            {
                Order order = new Order
                {
                    PlaceId = entity.PlaceId,
                    SectorId = entity.SectorId,
                    TableNumber = entity.TableNumber,
                    UserId = entity.UserId,
                    Pin = entity.Pin,
                    PendingToSync = entity.PendingToSync,
                    Status = entity.Status.HasValue ? entity.Status.Value : default(int),
                    Mode = entity.Mode
                };

                if (entity.Id > 0)
                {
                    order.ServerId = entity.Id.Value;
                }

                if (order.QuantityDiners.HasValue)
                {
                    order.QuantityDiners = entity.QuantityDiners.Value;
                }

                return order;
            }
            else
            {
                return null;
            }
        }

        public List<Order> Transform(List<OrderEntity> entities)
        {
            List<Order> orders = new List<Order>();
            foreach (var entity in entities)
            {
                orders.Add(Transform(entity));
            }
            return orders;
        }
        #endregion
    }
}
