﻿using System;
using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class PlaceEntityDataMapper : IEntityDataMapper<PlaceEntity, Place>
    {
        #region Implementation of IEntityDataMapper
        public PlaceEntity Mapping(Place dataObject)
        {
            var entity = new PlaceEntity
            {
                Id = (int)dataObject.Id,
                Name = dataObject.Name,
                Description = dataObject.Description,
                Address = dataObject.Address,
                Image = dataObject.Image
            };
            if (dataObject.Distance.HasValue)
            {
                entity.Distance = dataObject.Distance.Value;
            }
            return entity;
        }

        public List<PlaceEntity> Mapping(List<Place> dataObjects)
        {
            List<PlaceEntity> entityPlaces = new List<PlaceEntity>();
            if (dataObjects != null)
            {
                foreach (var entity in dataObjects)
                {
                    entityPlaces.Add(Mapping(entity));
                }
            }
            return entityPlaces;
        }

        public Place Transform(PlaceEntity entity)
        {
            var dataObject = new Place
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Address = entity.Address,
                Image = entity.Image
            };

            if (entity.Distance.HasValue)
            {
                dataObject.Distance = entity.Distance.Value;
            }
            return dataObject;
        }

        public List<Place> Transform(List<PlaceEntity> entities)
        {
            List<Place> places = new List<Place>();
            if (entities != null)
            {
                foreach (var entity in entities)
                {
                    places.Add(Transform(entity));
                }
            }

            return places;
        }
        #endregion
    }
}
