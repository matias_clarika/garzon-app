﻿using System;
using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class UserEntityDataMapper : IEntityDataMapper<UserEntity, User>
    {
        #region Implementation of IEntityDataMapper
        public UserEntity Mapping(User dataObject)
        {
            return new UserEntity
            {
                Id = (int)dataObject.Id,
                FirstName = dataObject.FirstName,
                LastName = dataObject.LastName,
                Email = dataObject.Email,
                Token = dataObject.Token
            };
        }

        public List<UserEntity> Mapping(List<User> dataObjects)
        {
            List<UserEntity> entities = new List<UserEntity>();
            foreach (var dataObject in dataObjects)
            {
                entities.Add(Mapping(dataObject));
            }
            return entities;
        }

        public User Transform(UserEntity entity)
        {
            return new User
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Email = entity.Email,
                Token = entity.Token
            };
        }

        public List<User> Transform(List<UserEntity> entities)
        {
            List<User> dataObjects = new List<User>();
            foreach (var entity in entities)
            {
                dataObjects.Add(Transform(entity));
            }
            return dataObjects;
        }
        #endregion
    }
}
