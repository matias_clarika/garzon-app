﻿using System.Collections.Generic;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local.Entity.Mapper
{
    public class SectorEntityDataMapper : IEntityDataMapper<SectorEntity, Sector>
    {
        public int PlaceId
        {
            private get;
            set;
        }

        #region IEntityDataMapper
        public SectorEntity Mapping(Sector dataObject)
        {
            return new SectorEntity
            {
                Id = (int)dataObject.Id,
                Name = dataObject.Name.ToUpper(),
                PlaceId = PlaceId
            };
        }

        public List<SectorEntity> Mapping(List<Sector> dataObjects)
        {
            List<SectorEntity> entities = new List<SectorEntity>();
            foreach (var dataObject in dataObjects)
            {
                entities.Add(Mapping(dataObject));
            }
            return entities;
        }

        public Sector Transform(SectorEntity entity)
        {
            return new Sector
            {
                Id = entity.Id,
                Name = entity.Name.ToUpper()
            };
        }

        public List<Sector> Transform(List<SectorEntity> entities)
        {
            List<Sector> sectors = new List<Sector>();
            foreach (var entity in entities)
            {
                sectors.Add(Transform(entity));
            }
            return sectors;
        }
        #endregion
    }
}
