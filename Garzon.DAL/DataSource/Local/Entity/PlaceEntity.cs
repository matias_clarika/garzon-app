﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class PlaceEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public int? Distance
        {
            get;
            set;
        }

        #endregion
    }
}
