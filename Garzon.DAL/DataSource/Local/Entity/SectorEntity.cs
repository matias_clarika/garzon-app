﻿using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class SectorEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        [Indexed]
        public int PlaceId
        {
            get;
            set;
        }
        #endregion
    }
}
