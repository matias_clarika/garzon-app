﻿using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class OrderDetailEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey, AutoIncrement]
        public int? Id
        {
            get;
            set;
        }

        #region sync
        [Unique]
        public int? ServerId
        {
            get;
            set;
        }

        #endregion

        public int OrderId
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }

        public int Quantity
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public double Amount
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        #region product

        public int ProductId
        {
            get;
            set;
        }


        public string ProductName
        {
            get;
            set;
        }

        public string ProductDescription
        {
            get;
            set;
        }

        public string ProductImage
        {
            get;
            set;
        }

        public double ProductPrice
        {
            get;
            set;
        }

        public string ProductCalories
        {
            get;
            set;
        }

        public bool ProductFastOrder
        {
            get;
            set;
        }

        public int ProductType
        {
            get;
            set;
        }

        public int? ProductMenuSectionId
        {
            get;
            set;
        }

        public int? ProductGroupId
        {
            get;
            set;
        }

        public bool ProductGroupSendLater
        {
            get;
            set;
        }

        public int? ParentProductId
        {
            get;
            set;
        }
        #endregion

        #region user

        public int? UserId
        {
            get;
            set;
        }
        #endregion

        #region daily menu
        public int? ParentId
        {
            get;
            set;
        }

        #endregion
        #endregion
    }
}
