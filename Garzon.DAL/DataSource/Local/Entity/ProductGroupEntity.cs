﻿namespace Garzon.DAL.DataSource.Local.Entity
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using SQLite;

    public class ProductGroupEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey, AutoIncrement]
        public int? Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string ShortDescription
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public bool Recommended
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public int? PlaceId
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }

        public int? SequenceRecommended
        {
            get;
            set;
        }

        [Indexed(Name = "ProductGroupId", Order = 2, Unique = true)]
        public int? GroupId
        {
            get;
            set;
        }

        [Indexed(Name = "ProductGroupId", Order = 1, Unique = true)]
        public int? ServerId
        {
            get;
            set;
        }

        #endregion
    }
}
