﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using SQLite;

namespace Garzon.DAL.DataSource.Local.Entity
{
    public class WaiterCallOptionEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int PlaceId
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }
        #endregion
    }
}
