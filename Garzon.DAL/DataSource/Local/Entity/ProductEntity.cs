﻿namespace Garzon.DAL.DataSource.Local.Entity
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using SQLite;

    public class ProductEntity : IBaseEntity
    {
        #region fields
        [PrimaryKey]
        public virtual int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string ShortDescription
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Icon
        {
            get;
            set;
        }

        public bool Recommended
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public string Calories
        {
            get;
            set;
        }


        public string Label
        {
            get;
            set;
        }

        public bool FastOrder
        {
            get;
            set;
        }

        [Indexed]
        public int MenuSectionId
        {
            get;
            set;
        }

        public int? PlaceId
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }

        public int? SequenceRecommended
        {
            get;
            set;
        }

        public int Type{
            get;
            set;
        }

        #region hours
        public string HourMondayFrom
        {
            get;
            set;
        }
        public string HourMondayTo
        {
            get;
            set;
        }
        public bool HourMondayMore24Hours
        {
            get;
            set;
        }
        public string HourTuesdayFrom
        {
            get;
            set;
        }
        public string HourTuesdayTo
        {
            get;
            set;
        }
        public bool HourTuesdayMore24Hours
        {
            get;
            set;
        }
        public string HourWednesdayFrom
        {
            get;
            set;
        }
        public string HourWednesdayTo
        {
            get;
            set;
        }
        public bool HourWednesdayMore24Hours
        {
            get;
            set;
        }
        public string HourThursdayFrom
        {
            get;
            set;
        }
        public string HourThursdayTo
        {
            get;
            set;
        }
        public bool HourThursdayMore24Hours
        {
            get;
            set;
        }
        public string HourFridayFrom
        {
            get;
            set;
        }
        public string HourFridayTo
        {
            get;
            set;
        }
        public bool HourFridayMore24Hours
        {
            get;
            set;
        }
        public string HourSaturdayFrom
        {
            get;
            set;
        }
        public string HourSaturdayTo
        {
            get;
            set;
        }
        public bool HourSaturdayMore24Hours
        {
            get;
            set;
        }
        public string HourSundayFrom
        {
            get;
            set;
        }
        public string HourSundayTo
        {
            get;
            set;
        }
        public bool HourSundayMore24Hours
        {
            get;
            set;
        }

        #endregion
        #endregion
    }
}
