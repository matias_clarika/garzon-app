﻿using System;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IOrderLocalDataSource : IOrderDataSource
    {
        /// <summary>
        /// Gets the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task<Order> GetCurrentOrder();
    }
}
