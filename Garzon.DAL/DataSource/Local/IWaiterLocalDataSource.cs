﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IWaiterLocalDataSource : IWaiterDataSource
    {
        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="waiterCallOptions">Waiter call options.</param>
        /// <param name="placeId">Place identifier.</param>
        Task<List<WaiterCallOption>> InsertOrUpdate(List<WaiterCallOption> waiterCallOptions, long placeId);
        /// <summary>
        /// Deletes the by place identifier.
        /// </summary>
        /// <returns>The by place identifier.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<bool> DeleteByPlaceId(long placeId);
    }
}
