﻿namespace Garzon.DAL.DataSource.Local.Database
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using Garzon.DAL.DataSource.Local.Entity;
    using SQLite;

    public class GarzonDatabase : IDatabase
    {
        #region fields
        readonly SQLiteAsyncConnection _database;
        #endregion

        #region constructor
        public GarzonDatabase(string dbPath)
        {
            _database = CreateDatabase(dbPath);
        }
        #endregion

        #region Implementation of IDatabase
        public SQLiteAsyncConnection GetDatabase()
        {
            return _database;
        }
        #endregion

        private SQLiteAsyncConnection CreateDatabase(string dbPath, bool retry = true)
        {
            try
            {
                SQLiteAsyncConnection database = new SQLiteAsyncConnection(dbPath);
                database.CreateTableAsync<UserEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<PlaceEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<MenuSectionEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<ProductEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<OrderEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<OrderDetailEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<WaiterCallOptionEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<SectorEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<GroupEntity>(CreateFlags.AllImplicit).Wait();
                database.CreateTableAsync<ProductGroupEntity>(CreateFlags.AllImplicit).Wait();
                return database;
            }
            catch (Exception e)
            {
                if (retry)
                {
                    SQLiteAsyncConnection database = new SQLiteAsyncConnection(dbPath);
                    database.DropTableAsync<UserEntity>();
                    database.DropTableAsync<PlaceEntity>();
                    database.DropTableAsync<MenuSectionEntity>();
                    database.DropTableAsync<ProductEntity>();
                    database.DropTableAsync<OrderEntity>();
                    database.DropTableAsync<OrderDetailEntity>();
                    database.DropTableAsync<WaiterCallOptionEntity>();
                    database.DropTableAsync<SectorEntity>();
                    database.DropTableAsync<GroupEntity>();
                    database.DropTableAsync<ProductGroupEntity>();
                    return this.CreateDatabase(dbPath, false);
                }
                throw e;
            }
        }
    }
}

