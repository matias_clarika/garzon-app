﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IPlaceDao : IGenericDao<PlaceEntity>
    {
        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<PlaceEntity> FindById(int id);
        /// <summary>
        /// Find the specified offset and limit.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<PlaceEntity>> Find(int offset = 0, int limit = 100);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();
    }
}
