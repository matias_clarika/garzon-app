﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IWaiterCallOptionDao : IGenericDao<WaiterCallOptionEntity>
    {
        /// <summary>
        /// Finds the by place.
        /// </summary>
        /// <returns>The by place.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<List<WaiterCallOptionEntity>> FindByPlace(int placeId);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();
        Task<bool> DeleteByPlaceId(int placeId);
    }
}
