﻿using System;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IUserDao : IGenericDao<UserEntity>
    {
        Task<UserEntity> FindUserLogged();
    }
}
