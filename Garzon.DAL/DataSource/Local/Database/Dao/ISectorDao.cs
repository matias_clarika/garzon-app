﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface ISectorDao : IGenericDao<SectorEntity>
    {
        /// <summary>
        /// Finds the by place identifier.
        /// </summary>
        /// <returns>The by place identifier.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<List<SectorEntity>> FindByPlaceId(int placeId);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();
    }
}
