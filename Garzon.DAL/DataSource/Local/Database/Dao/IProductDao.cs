﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IProductDao : IGenericDao<ProductEntity>
    {
        /// <summary>
        /// Find the specified menuSectionId, offset and limit.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="menuSectionId">Menu section identifier.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<ProductEntity>> FindByMenuSectionId(int menuSectionId, int offset = 0, int limit = 999);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();

        /// <summary>
        /// Finds the recommended.
        /// </summary>
        /// <returns>The recommended.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<ProductEntity>> FindRecommended(int placeId, int offset = 0, int limit = 10);

        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<ProductEntity> FindById(int id);

        /// <summary>
        /// Inserts the or replace.
        /// </summary>
        /// <returns>The or replace.</returns>
        /// <param name="productEntity">Product entity.</param>
        Task<ProductEntity> InsertOrReplace(ProductEntity productEntity);

        /// <summary>
        /// Inserts the or replace recommended.
        /// </summary>
        /// <returns>The or replace recommended.</returns>
        /// <param name="recommendedProducts">Recommended products.</param>
        Task<int> InsertOrReplaceRecommended(List<ProductEntity> recommendedProducts);
    }
}
