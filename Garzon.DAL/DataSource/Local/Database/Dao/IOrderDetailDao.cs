﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IOrderDetailDao : IGenericDao<OrderDetailEntity>
    {
        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<OrderDetailEntity> FindById(int id);

        /// <summary>
        /// Finds the by server identifier.
        /// </summary>
        /// <returns>The by server identifier.</returns>
        /// <param name="serverId">Server identifier.</param>
        Task<OrderDetailEntity> FindByServerId(int serverId);

        /// <summary>
        /// Finds the list by order identifier.
        /// </summary>
        /// <returns>The list by order identifier.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<List<OrderDetailEntity>> FindListByOrderId(int orderId);

        /// <summary>
        /// Finds the pending list by user identifier.
        /// </summary>
        /// <returns>The pending list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetailEntity>> FindPendingAndObservationListByUserId(int userId);

        /// <summary>
        /// Finds the pending list.
        /// </summary>
        /// <returns>The pending list.</returns>
        Task<List<OrderDetailEntity>> FindPendingAndObservationList();

        /// <summary>
        /// Finds the confirmed list.
        /// </summary>
        /// <returns>The confirmed list.</returns>
        Task<List<OrderDetailEntity>> FindConfirmedList();

        /// <summary>
        /// Finds the confirmed list by user identifier.
        /// </summary>
        /// <returns>The confirmed list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetailEntity>> FindConfirmedListByUserId(int userId);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();

        /// <summary>
        /// Deletes the pending.
        /// </summary>
        /// <returns>The pending.</returns>
        Task<int> DeletePending();

        /// <summary>
        /// Deletes the in server.
        /// </summary>
        /// <returns>The in server.</returns>
        Task<int> DeleteInServer();

        /// <summary>
        /// Deletes the in server without pending.
        /// </summary>
        /// <returns>The in server without pending.</returns>
        Task<int> DeleteInServerWithoutPending();

        /// <summary>
        /// Delete the specified ids.
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="ids">Identifiers.</param>
        Task<int> Delete(IEnumerable<int> ids);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="orderDetailEntity">Order detail entity.</param>
        Task<OrderDetailEntity> InsertOrUpdate(OrderDetailEntity orderDetailEntity);
        /// <summary>
        /// Finds the pending by product and user.
        /// </summary>
        /// <returns>The pending by product and user.</returns>
        /// <param name="productId">Product identifier.</param>
        /// <param name="id">Identifier.</param>
        Task<OrderDetailEntity> FindPendingByProductAndUser(int productId,
                                                            int id);

        /// <summary>
        /// Finds the pending by product.
        /// </summary>
        /// <returns>The pending by product.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<OrderDetailEntity> FindPendingByProduct(int productId);

        /// <summary>
        /// Finds the waiting list by user identifier.
        /// </summary>
        /// <returns>The waiting list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetailEntity>> FindWaitingListByUserId(int userId);

        /// <summary>
        /// Finds the pending request list by user identifier.
        /// </summary>
        /// <returns>The pending request list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetailEntity>> FindPendingRequestListByUserId(int userId);

        /// <summary>
        /// Finds the list pending by product identifier.
        /// </summary>
        /// <returns>The list pending by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<List<OrderDetailEntity>> FindListPendingByProductId(int productId);

        /// <summary>
        /// Deletes the pending order details.
        /// </summary>
        /// <returns>The pending order details.</returns>
        Task DeletePendingOrderDetails();
        Task DeleteByNotExistServerId(IEnumerable<int> enumerable);

        /// <summary>
        /// Deletes the by parent identifier.
        /// </summary>
        /// <returns>The by parent identifier.</returns>
        /// <param name="parentId">Parent identifier.</param>
        Task<int> DeleteByParentId(int parentId);

        /// <summary>
        /// Deletes the by server identifier.
        /// </summary>
        /// <returns>The by server identifier.</returns>
        /// <param name="serverId">Server identifier.</param>
        Task<int> DeleteByServerId(int serverId);
    }
}
