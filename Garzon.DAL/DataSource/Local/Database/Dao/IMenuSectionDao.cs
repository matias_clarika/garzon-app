﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IMenuSectionDao : IGenericDao<MenuSectionEntity>
    {
        /// <summary>
        /// Finds all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<List<MenuSectionEntity>> Find();

        /// <summary>
        /// Finds the by parent.
        /// </summary>
        /// <returns>The by parent.</returns>
        /// <param name="parentId">Parent identifier.</param>
        Task<List<MenuSectionEntity>> FindByParent(int parentId);
        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();

        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<MenuSectionEntity> FindById(int id);
        /// <summary>
        /// Finds the list by product identifier.
        /// </summary>
        /// <returns>The list by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<List<MenuSectionEntity>> FindListByProductId(int productId);

        /// <summary>
        /// Finds the by product identifier.
        /// </summary>
        /// <returns>The by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<MenuSectionEntity> FindByProductId(int productId);

        /// <summary>
        /// Inserts the or replace.
        /// </summary>
        /// <returns>The or replace.</returns>
        /// <param name="menuSectionEntity">Menu section entity.</param>
        Task<MenuSectionEntity> InsertOrReplace(MenuSectionEntity menuSectionEntity);
    }
}
