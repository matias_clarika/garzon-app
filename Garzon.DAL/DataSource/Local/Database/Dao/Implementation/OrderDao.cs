﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local.Entity;
using System.Linq;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public class OrderDao : GenericDao<OrderEntity>, IOrderDao
    {
        #region constants
        const string TABLE_NAME = "[OrderEntity]";
        const string USER_COLUMN = "[UserId]";
        const string ORDER_BY = "ORDER BY [Id] DESC";
        #endregion

        #region constructor
        public OrderDao(IDatabase database) : base(database)
        {
        }

        #endregion

        #region Implementation of IOrderDao

        public Task<OrderEntity> FindById(int id)
        {
            return _database.GetDatabase().Table<OrderEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<OrderEntity> FindCurrentOrder()
        {
            try
            {
                var query = $"SELECT * FROM {TABLE_NAME} {ORDER_BY} LIMIT 1 ";
                var result = await _database.GetDatabase().QueryAsync<OrderEntity>(query);
                return result.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public async Task<int> DeleteAll()
        {
            return await Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }

        #endregion


    }
}
