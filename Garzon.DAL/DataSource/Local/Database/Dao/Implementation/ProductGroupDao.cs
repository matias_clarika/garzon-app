﻿
namespace Garzon.DAL.DataSource.Local.Database.Dao.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
    using Garzon.DAL.DataSource.Local.Entity;

    public class ProductGroupDao : GenericDao<ProductGroupEntity>, IProductGroupDao
    {
        #region dependency
        readonly ILogger logger;
        #endregion

        #region constants
        private readonly string TAG = typeof(ProductGroupDao).FullName;
        private const string TABLE_NAME = "[ProductGroupEntity]";
        private const string ID_COLUMN = "[Id]";
        private const string GROUP_COLUMN = "[GroupId]";
        private const string ORDER_BY = "ORDER BY [Sequence] ASC";
        #endregion

        public ProductGroupDao(IDatabase database, ILogger logger) : base(database)
        {
            this.logger = logger;
        }

        #region IProductGroupDao

        public Task<List<ProductGroupEntity>> FindByGroupId(int groupId,
                                                                   int offset = 0,
                                                                   int limit = 20)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {GROUP_COLUMN} = {groupId} {ORDER_BY}  LIMIT {limit} OFFSET {offset} ";
            return _database.GetDatabase().QueryAsync<ProductGroupEntity>(query);
        }

        public async Task<int> DeleteAll()
        {
            return await Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }

        public override async Task<int> InsertOrReplace(List<ProductGroupEntity> entities)
        {
            int result = 0;
            foreach (var entity in entities)
            {
                try
                {
                    result += await _database.GetDatabase().InsertOrReplaceAsync(entity);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
            return result;
        }
        #endregion
    }
}
