﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao.Implementation
{
    public class SectorDao : GenericDao<SectorEntity>, ISectorDao
    {
        #region constants
        private readonly string TAG = typeof(SectorDao).FullName;
        private const string TABLE_NAME = "[SectorEntity]";
        private const string ID_COLUMN = "[Id]";
        private const string PLACE_COLUMN = "[PlaceId]";
        private const string ORDER_BY = "ORDER BY [Name] ASC";
        #endregion

        #region dependency
        readonly ILogger _logger;
        #endregion
        public SectorDao(IDatabase database,
                         ILogger logger) : base(database)
        {
            _logger = logger;
        }

        #region ISectorDao
        public Task<int> DeleteAll()
        {
            return Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    _logger.Error(TAG, $"DeleteAll -> {e.Message}");
                    return 0;
                }
            });
        }

        public Task<List<SectorEntity>> FindByPlaceId(int placeId)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {PLACE_COLUMN} = {placeId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<SectorEntity>(query);
        }
        #endregion
    }
}
