﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local.Entity;
using System.Linq;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;

namespace Garzon.DAL.DataSource.Local.Database.Dao.Implementation
{
    public class UserDao : GenericDao<UserEntity>, IUserDao
    {
        #region constants
        private const string TABLE_NAME = "[UserEntity]";
        private const string ORDER_BY = "ORDER BY [FirstName]";
        private const string IS_LOGGED = "IsLogged";
        #endregion
        public UserDao(IDatabase database) : base(database)
        {
        }

        #region Implementation of IUserDao
        public async Task<UserEntity> FindUserLogged()
        {
            try
            {
                var query = $"SELECT * FROM {TABLE_NAME} WHERE {IS_LOGGED} = 1";
                return (await _database.GetDatabase().QueryAsync<UserEntity>(query)).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        #endregion
    }
}
