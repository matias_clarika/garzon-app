﻿namespace Garzon.DAL.DataSource.Local.Database.Dao.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
    using Garzon.DAL.DataSource.Local.Entity;

    public class GroupDao : GenericDao<GroupEntity>, IGroupDao
    {
        #region constants
        const string TABLE_NAME = "[GroupEntity]";
        const string ID_COLUMN = "[Id]";
        const string PRODUCT_COLUMN = "[ProductId]";
        const string SERVER_ID_COLUMN = "[ServerId]";
        const string ORDER_BY = "ORDER BY [Sequence] DESC";
        #endregion

        public GroupDao(IDatabase database) : base(database)
        {
        }

        #region IGroupDao
        public async Task<int> DeleteAll()
        {
            return await Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }

        public async Task<GroupEntity> FindByServerId(int serverId, int productId)
        {
            try
            {
                string query = $"SELECT * FROM {TABLE_NAME} where {SERVER_ID_COLUMN} = {serverId} and {PRODUCT_COLUMN} = {productId}";
                return (await _database.GetDatabase().QueryAsync<GroupEntity>(query)).FirstOrDefault();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
        }

        public Task<List<GroupEntity>> FindListByProductId(int productId, int offset = 0, int limit = 20)
        {
            string query = $"SELECT * FROM {TABLE_NAME} WHERE {PRODUCT_COLUMN} = {productId} {ORDER_BY} LIMIT {limit} OFFSET {offset} ";
            return _database.GetDatabase().QueryAsync<GroupEntity>(query);
        }
        #endregion
    }
}
