﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Local.Entity;
using System.Linq;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public class PlaceDao : GenericDao<PlaceEntity>, IPlaceDao
    {
        #region constants
        const string TABLE_NAME = "[PlaceEntity]";
        const string ORDER_BY = "ORDER BY [Name] ASC";
        const string ID_COLUMN = "[Id]";
        #endregion

        #region constructor
        public PlaceDao(IDatabase database) : base(database)
        {
        }
        #endregion

        #region Implementation of IPlaceDao


        public async Task<PlaceEntity> FindById(int id)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {ID_COLUMN} = {id} LIMIT 1";
            return (await _database.GetDatabase().QueryAsync<PlaceEntity>(query)).FirstOrDefault();
        }

        public Task<List<PlaceEntity>> Find(int offset = 0, int limit = 100)
        {
            string query = $"SELECT * FROM {TABLE_NAME} {ORDER_BY} LIMIT {limit} OFFSET {offset} ";
            return _database.GetDatabase().QueryAsync<PlaceEntity>(query);
        }

        public Task<int> DeleteAll()
        {
            return Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }
        #endregion
    }
}
