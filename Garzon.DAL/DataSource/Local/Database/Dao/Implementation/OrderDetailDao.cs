﻿namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
    using Garzon.DAL.DataSource.Local.Entity;
    using System.Linq;
    using Garzon.DataObjects;

    public class OrderDetailDao : GenericDao<OrderDetailEntity>, IOrderDetailDao
    {
        #region constants
        const string TABLE_NAME = "[OrderDetailEntity]";
        const string ID_COLUMN = "[Id]";
        const string ORDER_BY = "ORDER BY [Status] ASC, [Id] ASC";
        const string ORDER_BY_PENDING = "ORDER BY [Id] ASC";
        const string SERVER_ID_COLUMN = "[ServerId]";
        const string ORDER_ID_COLUMN = "[OrderId]";
        const string USER_ID_COLUMN = "[UserId]";
        const string STATUS_COLUMN = "[Status]";
        const string PARENT_ID_COLUMN = "[ParentId]";
        const string PRODUCT_ID_COLUMN = "[ProductId]";
        const string COMMENT_COLUMN = "[Comment]";

        string PENDING_AND_OBSERVATION_CONDITION = $"({SERVER_ID_COLUMN} <= 0 or {SERVER_ID_COLUMN} {IS_NULL}) or ({STATUS_COLUMN} = {(int)OrderDetailStatus.PENDING} or {STATUS_COLUMN} = {(int)OrderDetailStatus.PENDING_REQUEST_TO_SEND} or  {STATUS_COLUMN} = {(int)OrderDetailStatus.OBSERVATION})";
        string PENDING_CONDITION = $"({SERVER_ID_COLUMN} <= 0 or {SERVER_ID_COLUMN} {IS_NULL}) or {STATUS_COLUMN} = {(int)OrderDetailStatus.PENDING}";
        string IN_SERVER_CONDITION = $"{SERVER_ID_COLUMN} > 0";
        string NOT_PARENT = $"({PARENT_ID_COLUMN} IS NULL or {PARENT_ID_COLUMN} <= 0)";
        string COMMENT_EMPTY = $"({COMMENT_COLUMN} LIKE '' or {COMMENT_COLUMN} IS NULL)";
        string WAITING_CONDITION = $"{SERVER_ID_COLUMN} > 0 and {STATUS_COLUMN} = {(int)OrderDetailStatus.INPROGRESS}";
        string PENDING_REQUEST_CONDITION = $"{SERVER_ID_COLUMN} > 0 and {STATUS_COLUMN} = {(int)OrderDetailStatus.PENDING_REQUEST}";
        #endregion
        #region constructor
        public OrderDetailDao(IDatabase database) : base(database)
        {
        }
        #endregion

        #region Implementation of IOrderDetailDao
        public Task<List<OrderDetailEntity>> FindListByOrderId(int orderId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {ORDER_ID_COLUMN} = {orderId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

        public Task<List<OrderDetailEntity>> FindPendingAndObservationList()
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {PENDING_AND_OBSERVATION_CONDITION} {ORDER_BY_PENDING}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }


        public Task<List<OrderDetailEntity>> FindConfirmedList()
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {IN_SERVER_CONDITION} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

        public Task<List<OrderDetailEntity>> FindPendingAndObservationListByUserId(int userId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {PENDING_AND_OBSERVATION_CONDITION} and {USER_ID_COLUMN} = {userId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

        public async Task<int> DeletePending()
        {
            try
            {
                var query = $"DELETE FROM {TABLE_NAME} WHERE {PENDING_AND_OBSERVATION_CONDITION}";
                await _database.GetDatabase().ExecuteAsync(query);
                return 1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return 0;
            }

        }

        public async Task<int> DeleteInServer()
        {
            try
            {
                var query = $"DELETE FROM {TABLE_NAME} WHERE {IN_SERVER_CONDITION}";
                await _database.GetDatabase().ExecuteAsync(query);
                return 1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return 0;
            }
        }

        public async Task<int> DeleteInServerWithoutPending()
        {
            try
            {
                var query = $"DELETE FROM {TABLE_NAME} WHERE {IN_SERVER_CONDITION} and NOT ({STATUS_COLUMN} = {(int)OrderDetailStatus.PENDING} or {STATUS_COLUMN} = {(int)OrderDetailStatus.PENDING_REQUEST_TO_SEND})";
                await _database.GetDatabase().ExecuteAsync(query);
                return 1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return 0;
            }
        }

        public Task<int> DeleteAll()
        {
            return Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }

        public Task<List<OrderDetailEntity>> FindConfirmedListByUserId(int userId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {IN_SERVER_CONDITION} and {USER_ID_COLUMN} = {userId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

        public async Task<int> Delete(IEnumerable<int> ids)
        {
            return await Task.Factory.StartNew<int>(() =>
            {
                try
                {
                    const string DELIMITER = ",";
                    string idsString = string.Join(DELIMITER, ids.Select(x => x.ToString()).ToArray());

                    string query = $"DELETE FROM {TABLE_NAME} WHERE ({ID_COLUMN} IN ({idsString})) OR ({PARENT_ID_COLUMN} IN ({idsString}))";
                    return _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });

        }

        public async Task<OrderDetailEntity> InsertOrUpdate(OrderDetailEntity orderDetailEntity)
        {
            var id = await _database.GetDatabase().InsertOrReplaceAsync(orderDetailEntity);

            return orderDetailEntity;
        }

        public async Task<OrderDetailEntity> FindPendingByProductAndUser(int productId, int userId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE ({PENDING_CONDITION}) and {PRODUCT_ID_COLUMN} = {productId} and {COMMENT_EMPTY} and {NOT_PARENT} and {USER_ID_COLUMN} = {userId} {ORDER_BY}";
            return (await _database.GetDatabase().QueryAsync<OrderDetailEntity>(query)).FirstOrDefault();
        }

        public async Task<OrderDetailEntity> FindPendingByProduct(int productId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE ({PENDING_CONDITION}) and {PRODUCT_ID_COLUMN} = {productId} and {COMMENT_EMPTY} and {NOT_PARENT} {ORDER_BY}";
            var result = (await _database.GetDatabase().QueryAsync<OrderDetailEntity>(query)).FirstOrDefault();
            return result;
        }

        public Task<List<OrderDetailEntity>> FindWaitingListByUserId(int userId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {WAITING_CONDITION} and {USER_ID_COLUMN} = {userId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }


        public async Task<OrderDetailEntity> FindById(int id)
        {
            try
            {
                string query = $"SELECT * FROM {TABLE_NAME} where {ID_COLUMN} = {id}";
                return (await _database.GetDatabase().QueryAsync<OrderDetailEntity>(query)).FirstOrDefault();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<OrderDetailEntity> FindByServerId(int serverId)
        {
            try
            {
                string query = $"SELECT * FROM {TABLE_NAME} where {SERVER_ID_COLUMN} = {serverId}";
                return (await _database.GetDatabase().QueryAsync<OrderDetailEntity>(query)).FirstOrDefault();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
        }

        public Task<List<OrderDetailEntity>> FindPendingRequestListByUserId(int userId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {PENDING_REQUEST_CONDITION} and {USER_ID_COLUMN} = {userId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

        public Task<List<OrderDetailEntity>> FindListPendingByProductId(int productId)
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE ({PENDING_CONDITION}) and {PRODUCT_ID_COLUMN} = {productId}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

        public async Task DeletePendingOrderDetails()
        {
            await Task.Factory.StartNew(async () =>
            {
                try
                {
                    var pendingList = await this.FindPendingWithoutParentListList();
                    if (pendingList.Any())
                    {
                        foreach (var pendingDetail in pendingList)
                        {
                            await DeleteByParentId(pendingDetail.Id.Value);
                        }
                        await this.Delete(pendingList.Select((detail) => detail.Id.Value).AsEnumerable());
                    }

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            });
        }


        public async Task<int> DeleteByParentId(int parentId)
        {
            return await Task.Factory.StartNew<int>(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME} WHERE {PARENT_ID_COLUMN} = {parentId}";
                    return _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }

        public async Task<int> DeleteByServerId(int serverId)
        {
            try
            {
                var query = $"DELETE FROM {TABLE_NAME} WHERE {SERVER_ID_COLUMN} = {serverId}";
                await _database.GetDatabase().ExecuteAsync(query);
                return 1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return 0;
            }
        }



        public async Task DeleteByNotExistServerId(IEnumerable<int> serverIds)
        {
            await Task.Factory.StartNew(async () =>
            {
                try
                {
                    const string DELIMITER = ",";
                    string idsString = string.Join(DELIMITER, serverIds.Select(x => x.ToString()).ToArray());

                    string query = $"DELETE FROM {TABLE_NAME} WHERE ({SERVER_ID_COLUMN} NOT IN ({idsString})) and ({SERVER_ID_COLUMN} IS NOT NULL and {SERVER_ID_COLUMN} > 0)";
                    _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            });
        }
        #endregion

        private Task<List<OrderDetailEntity>> FindPendingWithoutParentListList()
        {
            var query = $"SELECT * FROM {TABLE_NAME} WHERE {NOT_PARENT} and ({PENDING_CONDITION}) {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<OrderDetailEntity>(query);
        }

    }
}
