﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao.Implementation
{
    public class WaiterCallOptionDao : GenericDao<WaiterCallOptionEntity>, IWaiterCallOptionDao
    {
        #region constants
        private const string TABLE_NAME = "[WaiterCallOptionEntity]";
        private const string ORDER_BY = "ORDER BY [Sequence], [Name] ASC";
        private const string PLACE_COLUMN = "[PlaceId]";
        #endregion

        #region constructor
        public WaiterCallOptionDao(IDatabase database) : base(database)
        {
        }
        #endregion

        #region Implementation of IWaiterCallOptionDao

        public Task<List<WaiterCallOptionEntity>> FindByPlace(int placeId)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {PLACE_COLUMN} = {placeId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<WaiterCallOptionEntity>(query);
        }

        public async Task<int> DeleteAll()
        {
            return await Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });
        }

        public async Task<bool> DeleteByPlaceId(int placeId)
        {
            return await Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME} WHERE {PLACE_COLUMN} = {placeId}";
                    _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return false;
                }
            });
        }

        #endregion

    }
}
