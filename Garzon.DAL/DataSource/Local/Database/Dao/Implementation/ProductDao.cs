﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;
using System.Linq;
namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public class ProductDao : GenericDao<ProductEntity>, IProductDao
    {
        #region dependency
        readonly ILogger _logger;
        #endregion

        #region constants
        private readonly string TAG = typeof(ProductDao).FullName;
        private const string TABLE_NAME = "[ProductEntity]";
        private const string ID_COLUMN = "[Id]";
        private const string NAME_COLUMN = "[Name]";
        private const string DESCRIPTION_COLUMN = "[Description]";
        private const string SHORT_DESCRIPTION_COLUMN = "[ShortDescription]";
        private const string IMAGE_COLUMN = "[Image]";
        private const string ICON_COLUMN = "[Icon]";

        private const string MENU_SECTION_COLUMN = "[MenuSectionId]";
        private const string PLACE_COLUMN = "[PlaceId]";
        private const string RECOMMENDED_COLUMN = "[Recommended]";
        private const string PRICE_COLUMN = "[Price]";
        private const string LABEL_COLUMN = "[Label]";
        private const string FAST_ORDER_COLUMN = "[FastOrder]";
        private const string SEQUENCE_COLUMN = "[Sequence]";
        private const string SEQUENCE_RECOMMENDED_COLUMN = "[SequenceRecommended]";
        private const string TYPE_COLUMN = "[Type]";
        private const string HourMondayFrom = "[HourMondayFrom]";
        private const string HourMondayTo = "[HourMondayTo]";
        private const string HourMondayMore24Hours = "[HourMondayMore24Hours]";
        private const string HourTuesdayFrom = "[HourTuesdayFrom]";
        private const string HourTuesdayTo = "[HourTuesdayTo]";
        private const string HourTuesdayMore24Hours = "[HourTuesdayMore24Hours]";
        private const string HourWednesdayFrom = "[HourWednesdayFrom]";
        private const string HourWednesdayTo = "[HourWednesdayTo]";
        private const string HourWednesdayMore24Hours = "[HourWednesdayMore24Hours]";
        private const string HourThursdayFrom = "[HourThursdayFrom]";
        private const string HourThursdayTo = "[HourThursdayTo]";
        private const string HourThursdayMore24Hours = "[HourThursdayMore24Hours]";
        private const string HourFridayFrom = "[HourFridayFrom]";
        private const string HourFridayTo = "[HourFridayTo]";
        private const string HourFridayMore24Hours = "[HourFridayMore24Hours]";
        private const string HourSaturdayFrom = "[HourSaturdayFrom]";
        private const string HourSaturdayTo = "[HourSaturdayTo]";
        private const string HourSaturdayMore24Hours = "[HourSaturdayMore24Hours]";
        private const string HourSundayFrom = "[HourSundayFrom]";
        private const string HourSundayTo = "[HourSundayTo]";
        private const string HourSundayMore24Hours = "[HourSundayMore24Hours]";

        private const string ORDER_BY = "ORDER BY [Sequence] ASC";
        private const string ORDER_BY_RECOMMENDED = "ORDER BY [SequenceRecommended] ASC";
        private const string HOUR = "[Hour";
        private const string FROM = "From]";
        private const string TO = "To]";
        private const string MORE_24_HOURS = "More24Hours]";
        private const string IS_TRUE = " = 1 ";
        private const string IS_FALSE = " = 0 ";
        private const string IS_NOT_NULL = " IS NOT NULL ";
        #endregion

        #region constructor
        public ProductDao(IDatabase database, ILogger logger) : base(database)
        {
            _logger = logger;
        }
        #endregion

        #region Implementation of IProductDao
        public async Task<List<ProductEntity>> FindByMenuSectionId(int menuSectionId,
                                              int offset = 0,
                                              int limit = 999)
        {
            try
            {
                string query = $"SELECT * FROM {TABLE_NAME} where {MENU_SECTION_COLUMN} = {menuSectionId} and ({HourCondition()}) {ORDER_BY}  LIMIT {limit} OFFSET {offset} ";
                var result = await _database.GetDatabase().QueryAsync<ProductEntity>(query);
                return result;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return default(List<ProductEntity>);
            }
        }

        public Task<int> DeleteAll()
        {
            return Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    _logger.Error(TAG, $"DeleteAll -> {e.Message}");
                    return 0;
                }
            });
        }

        public Task<List<ProductEntity>> FindRecommended(int placeId, int offset = 0, int limit = 10)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {RECOMMENDED_COLUMN} = 1 and {HourCondition()} {ORDER_BY_RECOMMENDED} LIMIT {limit} OFFSET {offset} ";
            return _database.GetDatabase().QueryAsync<ProductEntity>(query);
        }

        public async Task<ProductEntity> FindById(int id)
        {
            try
            {
                string query = $"SELECT * FROM {TABLE_NAME} where {ID_COLUMN} = {id}";
                return (await _database.GetDatabase().QueryAsync<ProductEntity>(query)).FirstOrDefault();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
        }

        private string HourCondition()
        {
            DateTime now = DateTime.UtcNow;
            var today = GetStringByDayOfWeek(now.DayOfWeek);
            var hourToday = now.ToString("HH:mm:ss");

            var yesterday = GetStringByDayOfWeek(DateTime.Today.AddDays(-1).DayOfWeek);

            var HOUR_TODAY_FROM = $"{HOUR}{today}{FROM}";
            var HOUR_TODAY_TO = $"{HOUR}{today}{TO}";
            var HOUR_TODAY_MORE_24_HOURS = $"{HOUR}{today}{MORE_24_HOURS}";
            var HOUR_YESTERDAY_MORE_24_HOURS = $"{HOUR}{yesterday}{MORE_24_HOURS}";
            var HOUR_YESTERDAY_TO = $"{HOUR}{yesterday}{TO}";

            var firstCondition = $"{HOUR_TODAY_FROM} {IS_NULL}";
            var secondCondition = $"({HOUR_TODAY_TO} {IS_NOT_NULL} and time({HOUR_TODAY_FROM}) <= time('{hourToday}') and time({HOUR_TODAY_TO}) >= time('{hourToday}'))";
            var thirdCondition = $"({HOUR_TODAY_MORE_24_HOURS} {IS_TRUE} and time({HOUR_TODAY_FROM}) <= time('{hourToday}') and time({HOUR_TODAY_TO}) <= time('{hourToday}'))";
            var yesterdayCondition = $"({HOUR_YESTERDAY_MORE_24_HOURS} {IS_TRUE} and time({HOUR_YESTERDAY_TO}) >= time('{hourToday}'))";
            var query = $"({firstCondition} or {secondCondition} or {thirdCondition} or {yesterdayCondition})";

            return query;
        }

        private string GetStringByDayOfWeek(DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Monday:
                    return "Monday";
                case DayOfWeek.Tuesday:
                    return "Tuesday";
                case DayOfWeek.Wednesday:
                    return "Wednesday";
                case DayOfWeek.Thursday:
                    return "Thursday";
                case DayOfWeek.Friday:
                    return "Friday";
                case DayOfWeek.Saturday:
                    return "Saturday";
                case DayOfWeek.Sunday:
                    return "Sunday";
                default:
                    return "";
            }
        }

        public async Task<ProductEntity> InsertOrReplace(ProductEntity productEntity)
        {
            await _database.GetDatabase().InsertOrReplaceAsync(productEntity);
            return productEntity;
        }

        public override async Task<int> InsertOrReplace(List<ProductEntity> entities)
        {

            if (entities.Any())
            {
                try
                {
                    var cmd = $"INSERT OR REPLACE INTO {TABLE_NAME} ({ID_COLUMN},{NAME_COLUMN},{DESCRIPTION_COLUMN}, {SHORT_DESCRIPTION_COLUMN}, {IMAGE_COLUMN}," +
                        $" {ICON_COLUMN}, {MENU_SECTION_COLUMN}, {PLACE_COLUMN}, {PRICE_COLUMN}, {LABEL_COLUMN}, {FAST_ORDER_COLUMN}, {SEQUENCE_COLUMN}, {TYPE_COLUMN}, " +
                        $" {HourMondayFrom}, {HourMondayTo}, {HourMondayMore24Hours}, {HourTuesdayFrom}," +
                        $" {HourTuesdayTo}, {HourTuesdayMore24Hours}, {HourWednesdayFrom}, {HourWednesdayTo}, {HourWednesdayMore24Hours}," +
                        $" {HourThursdayFrom}, {HourThursdayTo}, {HourThursdayMore24Hours}, {HourFridayFrom}, {HourFridayTo}, {HourTuesdayMore24Hours}," +
                        $" {HourSaturdayFrom}, {HourSaturdayTo}, {HourSaturdayMore24Hours}, {HourSundayFrom}, {HourSundayTo}, {HourSundayMore24Hours})";
                    cmd += " VALUES ";
                    bool first = true;
                    foreach (var entity in entities)
                    {
                        if (!first)
                        {
                            cmd += ", ";
                        }
                        cmd += $"({entity.Id}, '{entity.Name}', '{entity.Description}', '{entity.ShortDescription}', '{entity.Image}', '{entity.Icon}'," +
                            $" {entity.MenuSectionId}, {entity.PlaceId}, {entity.Price}, '{entity.Label}', {(entity.FastOrder ? 1 : 0)}, {entity.Sequence}, {entity.Type}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourMondayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourMondayFrom)}, {(entity.HourMondayMore24Hours ? 1 : 0)}," +
                            $"{CheckStringIsEmptyReturnNull(entity.HourTuesdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourTuesdayTo)}, {(entity.HourTuesdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourWednesdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourWednesdayTo)}, {(entity.HourWednesdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourThursdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourThursdayTo)}, {(entity.HourThursdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourFridayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourFridayTo)}, {(entity.HourFridayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourSaturdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourSaturdayTo)}, {(entity.HourSaturdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourSundayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourSundayTo)}, {(entity.HourSundayMore24Hours ? 1 : 0)})";
                        first = false;
                    }
                    cmd += ";";
                    return await _database.GetDatabase().ExecuteAsync(cmd);
                }
                catch (Exception e)
                {
                    return 0;
                }

            }
            return 0;

        }

        public async Task<int> InsertOrReplaceRecommended(List<ProductEntity> recommendedProducts)
        {
            if (recommendedProducts.Any())
            {
                try
                {
                    var cmd = $"INSERT OR REPLACE INTO {TABLE_NAME} ({ID_COLUMN},{NAME_COLUMN},{DESCRIPTION_COLUMN}, {SHORT_DESCRIPTION_COLUMN}, {IMAGE_COLUMN}," +
                        $" {ICON_COLUMN}, {MENU_SECTION_COLUMN}, {PLACE_COLUMN}, {RECOMMENDED_COLUMN}, {PRICE_COLUMN}, {LABEL_COLUMN}, {FAST_ORDER_COLUMN}, {SEQUENCE_COLUMN}," +
                        $" {SEQUENCE_RECOMMENDED_COLUMN}, {TYPE_COLUMN}," +
                        $" {HourMondayFrom}, {HourMondayTo}, {HourMondayMore24Hours}, {HourTuesdayFrom}," +
                        $" {HourTuesdayTo}, {HourTuesdayMore24Hours}, {HourWednesdayFrom}, {HourWednesdayTo}, {HourWednesdayMore24Hours}," +
                        $" {HourThursdayFrom}, {HourThursdayTo}, {HourThursdayMore24Hours}, {HourFridayFrom}, {HourFridayTo}, {HourTuesdayMore24Hours}," +
                        $" {HourSaturdayFrom}, {HourSaturdayTo}, {HourSaturdayMore24Hours}, {HourSundayFrom}, {HourSundayTo}, {HourSundayMore24Hours})";
                    cmd += " VALUES ";
                    bool first = true;
                    foreach (var entity in recommendedProducts)
                    {
                        if (!first)
                        {
                            cmd += ", ";
                        }
                        cmd += $"({entity.Id}, '{entity.Name}', '{entity.Description}', '{entity.ShortDescription}', '{entity.Image}', '{entity.Icon}', " +
                            $"{entity.MenuSectionId}, {entity.PlaceId}, {(entity.Recommended ? 1 : 0)}, {entity.Price}, '{entity.Label}', " +
                            $"{(entity.FastOrder ? 1 : 0)}, {entity.Sequence}, {entity.SequenceRecommended.Value}, {entity.Type}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourMondayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourMondayFrom)}, {(entity.HourMondayMore24Hours ? 1 : 0)}," +
                            $"{CheckStringIsEmptyReturnNull(entity.HourTuesdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourTuesdayTo)}, {(entity.HourTuesdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourWednesdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourWednesdayTo)}, {(entity.HourWednesdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourThursdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourThursdayTo)}, {(entity.HourThursdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourFridayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourFridayTo)}, {(entity.HourFridayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourSaturdayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourSaturdayTo)}, {(entity.HourSaturdayMore24Hours ? 1 : 0)}, " +
                            $"{CheckStringIsEmptyReturnNull(entity.HourSundayFrom)}, {CheckStringIsEmptyReturnNull(entity.HourSundayTo)}, {(entity.HourSundayMore24Hours ? 1 : 0)})";
                        first = false;
                    }
                    cmd += ";";
                    return await _database.GetDatabase().ExecuteAsync(cmd);
                }
                catch (Exception e)
                {
                    return 0;
                }

            }
            return 0;
        }

        private string CheckStringIsEmptyReturnNull(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return "null";
            }
            else
            {
                return $"'{s}'";
            }
        }
        #endregion
    }
}
