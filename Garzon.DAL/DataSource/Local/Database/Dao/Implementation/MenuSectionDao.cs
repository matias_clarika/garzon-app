﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;
using System.Linq;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public class MenuSectionDao : GenericDao<MenuSectionEntity>, IMenuSectionDao
    {
        #region constants
        private const string TABLE_NAME = "[MenuSectionEntity]";
        private const string ORDER_BY = "ORDER BY [Sequence]";
        private const string ID_COLUMN = "[Id]";
        private const string NAME_COLUMN = "[Name]";
        private const string MENU_COLUMN = "[MenuId]";
        private const string PARENT_COLUMN = "[Parent]";
        private const string PRODUCT_ID_COLUMN = "[ProductId]";
        private const string IMAGE_COLUMN = "[Image]";
        private const string ICON_COLUMN = "[Icon]";
        private const string DEEP_LINK_COLUMN = "[DeepLink]";
        private const string FAST_CHARGE_COLUMN = "[FastCharge]";
        private const string SEQUENCE_COLUMN = "[Sequence]";
        private const string AVAILABLE_BY_SCHEDULE_COLUMN = "[AvailableBySchedule]";

        private string WITHOUT_PARENT_CONDITION = $"({PARENT_COLUMN} {IS_NULL} or {PARENT_COLUMN} <= 0)";
        #endregion

        #region constructor
        public MenuSectionDao(IDatabase database) : base(database)
        {
        }
        #endregion

        #region Implementation of IMenuSectionDao
        public Task<List<MenuSectionEntity>> Find()
        {
            string query = $"SELECT * FROM {TABLE_NAME} WHERE {WITHOUT_PARENT_CONDITION} and {PRODUCT_ID_COLUMN} <= 0 {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<MenuSectionEntity>(query);
        }

        public Task<List<MenuSectionEntity>> FindByParent(int parentId)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {PARENT_COLUMN} = {parentId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<MenuSectionEntity>(query);
        }

        public Task<int> DeleteAll()
        {
            return Task<int>.Factory.StartNew(() =>
            {
                try
                {
                    string query = $"DELETE FROM {TABLE_NAME}";
                    int result = _database.GetDatabase().GetConnection().CreateCommand(query).ExecuteNonQuery();
                    return result;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return 0;
                }
            });


        }

        public async Task<MenuSectionEntity> FindById(int id)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {ID_COLUMN} = {id}";
            return (await _database.GetDatabase().QueryAsync<MenuSectionEntity>(query)).FirstOrDefault();
        }

        public Task<List<MenuSectionEntity>> FindListByProductId(int productId)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {PRODUCT_ID_COLUMN} = {productId} {ORDER_BY}";
            return _database.GetDatabase().QueryAsync<MenuSectionEntity>(query);
        }

        public async Task<MenuSectionEntity> FindByProductId(int productId)
        {
            string query = $"SELECT * FROM {TABLE_NAME} where {PRODUCT_ID_COLUMN} = {productId} {ORDER_BY}";
            return (await _database.GetDatabase().QueryAsync<MenuSectionEntity>(query)).FirstOrDefault();
        }

        public async Task<MenuSectionEntity> InsertOrReplace(MenuSectionEntity menuSectionEntity)
        {
            await _database.GetDatabase().InsertOrReplaceAsync(menuSectionEntity);
            return menuSectionEntity;
        }

        public override async Task<int> InsertOrReplace(List<MenuSectionEntity> entities)
        {
            if (entities.Any())
            {
                try
                {
                    var cmd = $"INSERT OR REPLACE INTO {TABLE_NAME} ({ID_COLUMN},{NAME_COLUMN},{MENU_COLUMN}, {PARENT_COLUMN}, {IMAGE_COLUMN}," +
                        $" {ICON_COLUMN}, {PRODUCT_ID_COLUMN}, {DEEP_LINK_COLUMN}, {FAST_CHARGE_COLUMN}, {SEQUENCE_COLUMN}, {AVAILABLE_BY_SCHEDULE_COLUMN})";
                    cmd += " VALUES ";
                    bool first = true;
                    foreach (var entity in entities)
                    {
                        if (!first)
                        {
                            cmd += ", ";
                        }
                        cmd += $"({entity.Id}, '{entity.Name}', {entity.MenuId}, {entity.Parent}, '{entity.Image}', '{entity.Icon}', {entity.ProductId}, " +
                            $" {entity.DeepLink}, {(entity.FastCharge ? 1 : 0)}, {entity.Sequence}, {(entity.AvailableBySchedule ? 1 : 0)})";
                        first = false;
                    }
                    cmd += ";";
                    return await _database.GetDatabase().ExecuteAsync(cmd);
                }
                catch (Exception e)
                {
                    return 0;
                }

            }
            return 0;
        }
        #endregion
    }
}
