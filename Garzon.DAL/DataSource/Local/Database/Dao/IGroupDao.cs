﻿namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
    using Garzon.DAL.DataSource.Local.Entity;

    public interface IGroupDao : IGenericDao<GroupEntity>
    {
        /// <summary>
        /// Finds the list by product identifier.
        /// </summary>
        /// <returns>The list by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<GroupEntity>> FindListByProductId(int productId, int offset = 0, int limit = 20);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();

        /// <summary>
        /// Finds the by server identifier.
        /// </summary>
        /// <returns>The by server identifier.</returns>
        /// <param name="serverId">Server identifier.</param>
        /// <param name="productId">Product identifier.</param>
        Task<GroupEntity> FindByServerId(int serverId, int productId);
    }
}
