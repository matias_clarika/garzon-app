﻿namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
    using Garzon.DAL.DataSource.Local.Entity;

    public interface IProductGroupDao : IGenericDao<ProductGroupEntity>
    {
        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();

        /// <summary>
        /// Finds the by group identifier.
        /// </summary>
        /// <returns>The by group identifier.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<ProductGroupEntity>> FindByGroupId(int groupId,
                                                            int offset = 0,
                                                            int limit = 20);
    }
}
