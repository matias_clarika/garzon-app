﻿using System;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Local.Dao;
using Garzon.DAL.DataSource.Local.Entity;

namespace Garzon.DAL.DataSource.Local.Database.Dao
{
    public interface IOrderDao : IGenericDao<OrderEntity>
    {
        /// <summary>
        /// Finds the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task<OrderEntity> FindCurrentOrder();
        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<OrderEntity> FindById(int id);

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns>The all.</returns>
        Task<int> DeleteAll();
    }
}
