﻿namespace Garzon.DAL.DataSource.Local
{
    using System.Threading.Tasks;
    using Garzon.DataObjects;

    public interface IGroupLocalDataSource : IGroupDataSource
    {
        /// <summary>
        /// Finds the by identifier and product identifier.
        /// </summary>
        /// <returns>The by identifier and product identifier.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="productId">Product identifier.</param>
        Task<Group> GetByIdAndProductId(long id, long productId);
    }
}
