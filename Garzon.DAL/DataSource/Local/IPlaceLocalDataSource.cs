﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IPlaceLocalDataSource : IPlaceDataSource
    {
        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="places">Places.</param>
        Task<IEnumerable<Place>> InsertOrUpdate(List<Place> places); 
    }
}
