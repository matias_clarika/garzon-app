﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Local
{
    public interface IMenuSectionLocalDataSource : IMenuSectionDataSource
    {
        /// <summary>
        /// Finds the list by product identifier.
        /// </summary>
        /// <returns>The list by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<IEnumerable<MenuSection>> FindListByProductId(long productId);

        /// <summary>
        /// Finds the by product identifier.
        /// </summary>
        /// <returns>The by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<MenuSection> FindByProductId(long productId);
    }
}
