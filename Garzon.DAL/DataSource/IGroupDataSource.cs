﻿namespace Garzon.DAL.DataSource
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DataObjects;

    public interface IGroupDataSource : IDataSource<Group>
    {
    }

    public static class GroupParams
    {
        public const string PRODUCT_ID = "product_id";
    }
}
