﻿using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface IMenuDataSource : IDataSource<Menu>
    {
       
    }

    public static class MenuParams{
        public const string MENU_SECTION_ID = "menu_section_id";
    }
}
