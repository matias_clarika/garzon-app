﻿using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface IOrderDataSource : IDataSource<Order>
    {
    }

    public abstract class OrderParams
    {
        public const string ID = "id";
    }
}
