﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface IWaiterDataSource : IDataSource<WaiterCallOption>
    {
    }

    public static class WaiterParams
    {
        public const string PLACE_ID = "place_id";
    }
}
