﻿using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface IUserDataSource: IDataSource<User>
    {

        /// <summary>
        /// Sets the device token.
        /// </summary>
        /// <returns>The device token.</returns>
        /// <param name="deviceToken">Device token.</param>
        Task SetDeviceToken(string deviceToken);

        /// <summary>
        /// Gets the device token.
        /// </summary>
        /// <returns>The device token.</returns>
        Task<string> GetDeviceToken();

        /// <summary>
        /// Removes the current user logged.
        /// </summary>
        /// <returns>The current user logged.</returns>
        Task<bool> LogoutCurrentUser();


    }
}
