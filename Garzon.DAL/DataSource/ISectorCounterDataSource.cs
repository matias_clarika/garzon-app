﻿namespace Garzon.DAL.DataSource
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DataObjects;

    public interface ISectorCounterDataSource : IDataSource<SectorCounter>
    {
    }
}
