﻿namespace Garzon.DAL.DataSource.Remote
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Garzon.DataObjects;

    public interface ISectorCounterRemoteDataSource : ISectorCounterDataSource
    {
        /// <summary>
        /// Chooses the sectors.
        /// </summary>
        /// <returns>The sectors.</returns>
        /// <param name="sectors">Sectors.</param>
        /// <param name="force">If set to <c>true</c> force.</param>
        Task<bool> ChooseSectors(List<SectorCounter> sectors, bool force = false);
    }
}
