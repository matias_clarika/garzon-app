﻿using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote.Implementation
{
    public class WaiterCounterRemoteDataSourcce : RemoteDataSource<WaiterCounter>, IWaiterCounterRemoteDataSource
    {
        #region API
        const string API_ENDPOINT = "waitercallcounter";
        #endregion
        public WaiterCounterRemoteDataSourcce(IOAuthSettings oAuthSettings,
                                              IServerInfo serverInfo,
                                              IPollyStrategy pollyStrategy) : base(oAuthSettings,
                                                                                   serverInfo,
                                                                                   API_ENDPOINT,
                                                                                   pollyStrategy)
        {
        }
    }
}
