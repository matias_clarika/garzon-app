﻿namespace Garzon.DAL.DataSource.Remote.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Clarika.Xamarin.Base.DataObjects;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Garzon.DataObjects;

    public class OrderCounterRemoteDataSource : RemoteDataSource<OrderCounter>, IOrderCounterRemoteDataSource
    {
        #region constants
        const string API_ENDPOINT = "ordercounter";
        const string WAITER_CALL_RECEIVED_API_ENDPOINT = "waitercall";
        const string ORDER_RECEIVED_API_ENDOPOINT = "order";
        const string CHANGE_TABLE_NUMBER_API_ENDPOINT = "change-table";
        const string BLOCK_USER_API_ENDPOINT = "block-users";
        #endregion
        public OrderCounterRemoteDataSource(IOAuthSettings oAuthSettings,
                                            IServerInfo serverInfo) : base(oAuthSettings, serverInfo, API_ENDPOINT)
        {
        }

        #region implementation of IOrderCounterRemoteDataSource
        public async Task<bool> MarkReceived(OrderCounterReceived orderCounterReceived)
        {
            string entityUrl = GetEntityUrlByReceivedAction(orderCounterReceived);

            try
            {
                var url = GenerateBaseURL(entityUrl);
                var apiResponse = await Put($"{url}{orderCounterReceived.Id}/", orderCounterReceived);
                return apiResponse.IsOk();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }

        }

        public async Task<bool> ChangeTableNumber(long sectorId,
                                                  int tableNumber,
                                                  long orderId)
        {
            try
            {
                var url = GenerateBaseURL(CHANGE_TABLE_NUMBER_API_ENDPOINT);
                var apiResponse = await Post(url, CounterChangeTableNumber.Builder(sectorId, tableNumber, orderId));
                return apiResponse.IsOk();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<bool> BlockUsersByOrderId(long orderId)
        {
            try
            {
                var url = GenerateBaseURL(BLOCK_USER_API_ENDPOINT);
                var apiResponse = await Post(url, BlockUser.Build(orderId));
                return apiResponse.IsOk();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        #endregion

        /// <summary>
        /// Gets the entity URL by received action.
        /// </summary>
        /// <returns>The entity URL by received action.</returns>
        /// <param name="orderCounterReceived">Order counter received.</param>
        private string GetEntityUrlByReceivedAction(OrderCounterReceived orderCounterReceived)
        {
            string entityUrl = "";
            switch (orderCounterReceived.ReceivedAction)
            {
                case ReceivedAction.WAITER:
                    entityUrl = WAITER_CALL_RECEIVED_API_ENDPOINT;
                    break;
                case ReceivedAction.CHECK_ORDER:
                    entityUrl = ORDER_RECEIVED_API_ENDOPOINT;
                    break;
            }

            return entityUrl;
        }


    }

    #region utils
    internal class CounterChangeTableNumber : BaseDataObject
    {

        public long OrderId
        {
            get;
            set;
        }

        public long SectorId
        {
            get;
            set;
        }

        public int TableNumber
        {
            get;
            set;
        }

        public static CounterChangeTableNumber Builder(long sectorId, int tableNumber, long orderId)
        {
            return new CounterChangeTableNumber
            {
                SectorId = sectorId,
                TableNumber = tableNumber,
                OrderId = orderId
            };
        }
    }

    internal class BlockUser : BaseDataObject
    {
        public long OrderId
        {
            get;
            set;
        }

        internal static BlockUser Build(long orderId)
        {
            return new BlockUser { OrderId = orderId };
        }
    }
    #endregion
}
