﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Auth;
using Garzon.DataObjects;
using System.Linq;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Clarika.Xamarin.Base.Persistence.DataSource;

namespace Garzon.DAL.DataSource.Remote
{
    public class MenuRemoteDataSource : RemoteDataSource<Menu>, IMenuRemoteDataSource
    {
        #region constants
        private const string API_ENDPOINT = "menu";
        private const string PLACE_PARAM = "place";
        #endregion

        #region constructor
        public MenuRemoteDataSource(IOAuthSettings oAuthSettings,
                                    IServerInfo serverInfo,
                                    IPollyStrategy pollyStrategy) : base(oAuthSettings, serverInfo, API_ENDPOINT, pollyStrategy)
        {
        }
        #endregion

        #region Implementation of IMenuRemoteDataSource
        public async Task<Menu> FindMenuByPlace(long placeId)
        {
            BaseQueryFilter queryFilter = BaseQueryFilter.Builder()
                                                        .WithParam(PLACE_PARAM, placeId.ToString())
                                                        .Unique();

            return (await Get(queryFilter)).FirstOrDefault();
        }
        #endregion
    }
}
