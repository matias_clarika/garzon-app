﻿using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote.Implementation
{
    public class OrderPayCounterRemoteDataSource : RemoteDataSource<OrderPayCounter>, IOrderPayCounterRemoteDataSource
    {
        #region API
        const string API_ENDPOINT = "orderpaycounter";
        #endregion
        public OrderPayCounterRemoteDataSource(IOAuthSettings oAuthSettings,
                                               IServerInfo serverInfo,
                                               IPollyStrategy pollyStrategy) : base(oAuthSettings,
                                                                                    serverInfo,
                                                                                    API_ENDPOINT,
                                                                                    pollyStrategy)
        {
        }
    }
}
