﻿using System;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.DataObjects;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DAL.DataSource.Remote.Exceptions;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;
using Newtonsoft.Json;

namespace Garzon.DAL.DataSource.Remote.Implementation
{
    public class UserRemoteDataSource : RemoteDataSource<User>, IUserRemoteDataSource
    {

        #region API
        const string API_ENDPOINT = "user";
        const string API_LOGIN_SOCIAL_FACEBOOK_ENDPOINT = "login/token";
        const string API_LOGIN_SOCIAL_GOOGLE_ENDPOINT = "login/social/token_user";
        const string API_USER_DEVICE_ENDPOINT = "devices";
        const string API_LOGIN_COUNTER = "counter-auth-token";
        const string API_COUNTER_ONLINE = "counter";
        #endregion

        #region dependency
        readonly IDevice _device;
        #endregion
        public UserRemoteDataSource(IOAuthSettings oAuthSettings,
                                    IServerInfo serverInfo,
                                    IDevice device,
                                    IPollyStrategy pollyStrategy) : base(oAuthSettings, serverInfo, API_ENDPOINT, pollyStrategy)
        {
            _device = device;
        }

        #region Implementation of IUserRemoteDataSource
        public async Task<User> SignInSocial(SocialProviders provider,
                                             SocialUser socialUser)
        {
            try
            {
                switch (provider)
                {
                    case SocialProviders.FACEBOOK:
                        return await SignInFacebook(socialUser);
                    case SocialProviders.GOOGLE:
                        return await SignInGoogle(socialUser);
                    default:
                        throw new Exception("Provider not implemented");
                }
            }
            catch (ApiResponseException e)
            {
                throw UserSessionException.Build(e);
            }
        }
        #endregion

        #region private
        /// <summary>
        /// Signs the in facebook.
        /// </summary>
        /// <returns>The in facebook.</returns>
        /// <param name="socialUser">Social user.</param>
        private async Task<User> SignInFacebook(SocialUser socialUser)
        {
            User user = await Insert<User, FacebookUser>(FacebookUser.Builder()
                                                        .WithSocialUser(socialUser),
                                                              API_LOGIN_SOCIAL_FACEBOOK_ENDPOINT);
            ProcessToken(user.Token);
            return user;
        }

        /// <summary>
        /// Signs the in google.
        /// </summary>
        /// <returns>The in google.</returns>
        /// <param name="socialUser">Social user.</param>
        private async Task<User> SignInGoogle(SocialUser socialUser)
        {
            User user = await Insert<User, GoogleUser>(GoogleUser.Builder()
                                                    .WithSocialUser(socialUser),
                                                  API_LOGIN_SOCIAL_GOOGLE_ENDPOINT);
            ProcessToken(user.Token);
            return user;
        }

        /// <summary>
        /// Processes the token.
        /// </summary>
        /// <param name="token">Token.</param>
        private void ProcessToken(string token)
        {
            _oAuthSettings.SetToken(token);
        }

        public Task<string> GetDeviceToken()
        {
            return Task.Factory.StartNew<string>(() => null);
        }

        public async Task SetDeviceToken(string deviceToken)
        {
            await Insert<UserDevice, UserDevice>(UserDevice.Build(deviceToken,
                                                                   _device.GetPlatform()),
                                                API_USER_DEVICE_ENDPOINT);
        }

        public async Task<bool> LogoutCurrentUser()
        {
            _oAuthSettings.SetToken(null);
            return true;
        }

        public async Task<Counter> SignInCounter(string userName,
                                       string password)
        {
            try
            {
                var signInAuth = await Insert<SignInAuth, SignInAuth>(SignInAuth.Build(userName,
                                                                           password), API_LOGIN_COUNTER);
                var user = signInAuth.GetCounter();
                ProcessToken(user.Token);
                return user;
            }
            catch (ApiResponseException e)
            {
                switch (e.Code)
                {
                    case ApiCode.USER_INVALID_USER_OR_PASSWORD:
                        throw UserAuthException.Build(e);
                    default:
                        throw e;
                }
            }
        }

        public async Task<bool> IsCounterOnline(long counterId, bool isOnline)
        {
            try
            {
                var url = $"{GenerateBaseURL(API_COUNTER_ONLINE)}{counterId}/";
                var response = await Put<CounterUserOnline>(url, new CounterUserOnline { Online = isOnline });
                return response.IsOk();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        #endregion
    }

    #region utils

    public class SignInAuth : IBaseDataObject
    {
        public string Username
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? Id
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Token
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? CounterId
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Main
        {
            get;
            set;
        }

        long IBaseDataObject.Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public static SignInAuth Build(string userName,
                                       string password)
        {
            return new SignInAuth
            {
                Username = userName,
                Password = password
            };
        }

        public Counter GetCounter()
        {
            return new Counter
            {
                Id = this.CounterId.Value,
                PlaceId = this.Id.Value,
                Name = this.Name,
                Token = this.Token,
                Main = this.Main.HasValue && this.Main.Value
            };
        }

    }
    #region facebook
    public class FacebookUser
    {
        #region fields

        public string Provider
        {
            get;
            set;
        }

        public string Token
        {
            get;
            set;
        }
        #endregion

        private FacebookUser()
        {
            Provider = "facebook";
        }

        #region builder
        public static FacebookUser Builder()
        {
            return new FacebookUser();
        }

        public FacebookUser WithSocialUser(SocialUser socialUser)
        {
            Token = socialUser.Token;
            return this;
        }
        #endregion
    }
    #endregion

    #region google
    public class GoogleUser
    {
        #region fields

        public string Provider
        {
            get;
            set;
        }

        public string Code
        {
            get;
            set;
        }
        #endregion

        private GoogleUser()
        {
            Provider = "google-oauth2";
        }
        #region builder
        public static GoogleUser Builder()
        {
            return new GoogleUser();
        }

        public GoogleUser WithSocialUser(SocialUser socialUser)
        {
            Code = socialUser.Token;
            return this;
        }
        #endregion
    }
    #endregion

    #region device token
    public class UserDevice : IBaseDataObject
    {
        #region fields

        public string RegistrationId
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        [JsonIgnore]
        public long Id
        {
            get;
            set;
        }
        #endregion
        //required to serialize response
        public UserDevice()
        {

        }

        private UserDevice(string registrationId, string type)
        {
            RegistrationId = registrationId;
            Type = type;
        }

        #region builder
        public static UserDevice Build(string registrationId, Platform type)
        {
            return new UserDevice(registrationId,
                                  type == Platform.ANDROID ? "android" : "ios");
        }
        #endregion
    }
    #endregion

    public class CounterUserOnline : BaseDataObject
    {
        public bool Online
        {
            get;
            set;
        }
    }
    #endregion

}
