﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DAL.DataSource.Remote.Exceptions;
using Garzon.DAL.DataSource.Remote.Implementation;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote
{
    public class PlaceRemoteDataSource : RemoteDataSource<Place>, IPlaceRemoteDataSource
    {
        #region API
        const string API_ENDPOINT = "place";
        #endregion

        #region constructor  
        public PlaceRemoteDataSource(IOAuthSettings oAuthSettings,
                                     IServerInfo serverInfo,
                                     IPollyStrategy pollyStrategy)
            : base(oAuthSettings, serverInfo, API_ENDPOINT, pollyStrategy)
        {
        }

        #endregion


        #region override
        public override async Task<IEnumerable<Place>> Get(BaseQueryFilter queryFilter = null)
        {
            try
            {
                return await base.Get(queryFilter);
            }
            catch (ApiResponseException e)
            {
                switch (e.Code)
                {
                    case ApiCode.USER_BLOCK_PERMANENT:
                    case ApiCode.USER_BLOCK_TEMPORALY:
                        throw UserSessionException.Build(e);
                }
                throw e;
            }

        }
        #endregion  

    }
}
