﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DAL.DataSource.Remote.Exceptions;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote.Implementation
{
    public class WaiterRemoteDataSource : RemoteDataSource<WaiterCallOption>, IWaiterRemoteDataSource
    {

        #region API
        const string API_ENDPOINT = "waitercall";
        #endregion

        public WaiterRemoteDataSource(IOAuthSettings oAuthSettings,
                                      IServerInfo serverInfo) : base(oAuthSettings, serverInfo, API_ENDPOINT)
        {
        }

        #region Implementation of IWaiterRemoteDataSource
        public async Task<bool> CallWaiter(long placeId,
                                           long sectorId,
                                           int tableNumber,
                                           List<WaiterCallOption> options)
        {

            try
            {

                var waiterCall = new WaiterCall
                {
                    PlaceId = (int)placeId,
                    SectorId = sectorId,
                    TableNumber = tableNumber,
                };

                waiterCall.SetOptions(options);
                await Insert<WaiterCall, WaiterCall>(waiterCall);
                return true;
            }
            catch (ApiResponseException e)
            {
                switch (e.Code)
                {
                    case ApiCode.WAITER_CALLED_EXIST:
                        throw new WaiterCallExistException(e);
                    case ApiCode.WAITER_CALLED_TABLE_NUMBER_NOT_FOUND:
                        throw new TableNumberNotExistException(e);
                    case ApiCode.USER_BLOCK_PERMANENT:
                    case ApiCode.USER_BLOCK_TEMPORALY:
                        throw UserSessionException.Build(e);
                }
                return false;
            }
        }
        #endregion
    }



}
