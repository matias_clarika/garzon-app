﻿namespace Garzon.DAL.DataSource.Remote.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Clarika.Xamarin.Base.DataObjects;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Garzon.DataObjects;
    using Newtonsoft.Json;

    public class SectorCounterRemoteDataSource : RemoteDataSource<SectorCounter>, ISectorCounterRemoteDataSource
    {
        #region constants
        private const string API_ENDPOINT = "sector";
        private const string API_CHOOSE_SECTOR_ENDPOINT = "choose-sector";
        private const int CODE_SECTOR_LINKED_OTHER_COUNTER = 406;
        #endregion
        #region constructor
        public SectorCounterRemoteDataSource(IOAuthSettings oAuthSettings,
                                             IServerInfo serverInfo,
                                             IPollyStrategy pollyStrategy = null,
                                             JsonSerializerSettings serializerSettings = null) : base(oAuthSettings, serverInfo, API_ENDPOINT, pollyStrategy, serializerSettings)
        {
        }
        #endregion

        #region ISectorCounterRemoteDataSource
        public async Task<bool> ChooseSectors(List<SectorCounter> sectors, bool force = false)
        {
            var chooseSector = ChooseSector.Build(sectors, force);
            try
            {
                var response = await Insert<ChooseSector, ChooseSector>(chooseSector, API_CHOOSE_SECTOR_ENDPOINT);
                return true;
            }
            catch (ApiResponseException apiResponseException)
            {
                if (apiResponseException.Code == CODE_SECTOR_LINKED_OTHER_COUNTER)
                {
                    throw new SectorLinkedOtherCounterException(apiResponseException.Message);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }

        }
        #endregion
    }

    public class ChooseSector : BaseDataObject
    {
        public List<SectorCounter> Sectors
        {
            get;
            set;
        }

        public bool Force
        {
            get;
            set;
        }

        public static ChooseSector Build(List<SectorCounter> sectors, bool force = false)
        {
            FormatSectorsToSend(sectors);
            return new ChooseSector
            {
                Sectors = sectors,
                Force = force
            };
        }

        private static void FormatSectorsToSend(List<SectorCounter> sectors)
        {
            foreach (var sector in sectors)
            {
                sector.Name = null;
            }
        }
    }
}
