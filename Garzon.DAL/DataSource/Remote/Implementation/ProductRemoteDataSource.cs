﻿using Clarika.Xamarin.Base.Auth;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote
{
    public class ProductRemoteDataSource : RemoteDataSource<Product>, IProductRemoteDataSource
    {
        #region constants
        private const string API_ENDPOINT = "product";
        #endregion
        #region constructor
        public ProductRemoteDataSource(IOAuthSettings oAuthSettings,
                                       IServerInfo serverInfo) : base(oAuthSettings, serverInfo, API_ENDPOINT)
        {
        }
        #endregion
    }
}
