﻿namespace Garzon.DAL.DataSource.Remote.Implementation
{
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Garzon.Config;
    using Garzon.DataObjects;

    public class CheckVersionRemoteDataSource : RemoteDataSource<CheckVersion>, ICheckVersionRemoteDataSource
    {
        #region constants
        private const string API_ENDPOINT = "check-version";
        #endregion
        #region dependency
        readonly IAppConfig appConfig;
        #endregion
        public CheckVersionRemoteDataSource(IOAuthSettings oAuthSettings,
                                               IServerInfo serverInfo,
                                               IAppConfig appConfig) : base(oAuthSettings, serverInfo, API_ENDPOINT)
        {
            this.appConfig = appConfig;
        }
        #region ICheckVersionRemoteDataSource
        public async Task<bool> ValidateAppVersion()
        {
            try
            {
                await base.Insert(new CheckVersion { Version = this.appConfig.MinAppVersion });
                return true;
            }
            catch (ApiResponseException e)
            {
                switch (e.Code)
                {
                    case ApiCode.CHECK_VERSION_INCORRECT:
                        return false;
                    default:
                        throw e;
                }
            }
        }
        #endregion
    }
}
