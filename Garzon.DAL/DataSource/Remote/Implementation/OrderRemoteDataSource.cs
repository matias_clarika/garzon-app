﻿namespace Garzon.DAL.DataSource.Remote
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Auth;
    using Clarika.Xamarin.Base.DataObjects;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
    using Garzon.DataObjects;
    using System.Linq;
    using Clarika.Xamarin.Base.Persistence.DataSource;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Newtonsoft.Json;

    public class OrderRemoteDataSource : RemoteDataSource<Order>, IOrderRemoteDataSource
    {
        #region api
        const string API_ENDPOINT = "order";
        const string API_ACTIVE_ORDERS_ENDPOINT = "orderopen";
        const string API_CHECK_ORDER_ENDPOINT = "orderpaycall";
        const string API_CREATE_TAKE_AWAY = "create-take-away";
        #endregion
        public OrderRemoteDataSource(IOAuthSettings oAuthSettings,
                                     IServerInfo serverInfo,
                                     IPollyStrategy pollyStrategy) : base(oAuthSettings, serverInfo, API_ENDPOINT, pollyStrategy)
        {
        }

        #region IOrderRemoteDataSource
        public async Task<Order> CheckOrder(Order order, bool force)
        {
            try
            {
                this._pollyStrategy.Enable = false;
                var send = OrderPayCall.Builder(order, force);
                var result = await Insert<OrderPayCall, OrderPayCall>(send, API_CHECK_ORDER_ENDPOINT);
                if (result != null)
                {
                    order.RedirectUrl = result.RedirectUrl;
                    return order;
                }
                else
                {
                    return null;
                }
            }
            catch (ApiResponseException e)
            {
                switch (e.Code)
                {
                    case ApiCode.CHECK_ORDER_EXIST:
                        throw new CheckOrderExistException(e);
                    case ApiCode.CHECK_ORDER_EXIST_PENDING_DETAILS:
                        throw new CheckOrderPendingException(e);
                    case ApiCode.CHECK_ORDER_EXIST_PENDING_REQUEST_DETAILS:
                        throw new CheckOrderPendingRequestException(e);
                    case ApiCode.USER_BLOCK_TEMPORALY:
                    case ApiCode.USER_BLOCK_PERMANENT:
                        throw UserSessionException.Build(e);
                }
                return null;
            }
            finally
            {
                this._pollyStrategy.Enable = true;
            }
        }

        public Task<bool> ClosedOrder(Order order)
        {
            //HACK: add details because the api required details field, when the api don't required this field to delete order remove this line
            order.Details = new List<OrderDetail>();

            var orderByKitchen = new OrderByKitchen(order)
            {
                Status = (int)OrderStatus.CLOSED
            };
            return SendOrderByKitchen(orderByKitchen);
        }

        public async Task<bool> ObservedOrder(Order order)
        {
            return await SendOrderByKitchen(order);
        }

        public async Task<bool> SendOrderConfirmation(Order order)
        {
            return await SendOrderByKitchen(order);
        }

        public async Task<Order> SendToKitchen(Order order)
        {
            try
            {
                this._pollyStrategy.Enable = false;
                order = FormatOrderToSend(order);
                var json = SerializeObject(order);
                order = await Insert(order);

                return order;
            }
            catch (ApiResponseException ae)
            {
                throw SentToKitchenException.BuildByApiResponseException(ae);
            }
            catch (Exception e)
            {
                throw SentToKitchenException.BuildByException(e);
            }
            finally
            {
                this._pollyStrategy.Enable = true;
            }
        }

        public override async Task<IEnumerable<Order>> Get(BaseQueryFilter queryFilter = null)
        {
            try
            {
                return await base.Get(queryFilter);
            }
            catch (ApiResponseException e)
            {

                switch (e.Code)
                {
                    case ApiCode.USER_BLOCK_PERMANENT:
                    case ApiCode.USER_BLOCK_TEMPORALY:
                        throw UserSessionException.Build(e);
                }
                throw e;
            }

        }
        #endregion

        /// <summary>
        /// Sends the order by kitchen.
        /// </summary>
        /// <returns>The order by kitchen.</returns>
        /// <param name="order">Order.</param>
        private Task<bool> SendOrderByKitchen(Order order)
        {
            var orderByKitchen = new OrderByKitchen(order);
            return SendOrderByKitchen(orderByKitchen);
        }

        /// <summary>
        /// Formats the order to send.
        /// </summary>
        /// <returns>The order to send.</returns>
        /// <param name="order">Order.</param>
        private Order FormatOrderToSend(Order order)
        {
            if (order != null)
            {
                order.UserId = null;
                order.Place = null;
                FormatDetailsToSend(order.Details);
            }
            return order;
        }

        /// <summary>
        /// Formats the details to send.
        /// </summary>
        /// <param name="details">Details.</param>
        private static void FormatDetailsToSend(List<OrderDetail> details)
        {
            if (details != null)
            {
                foreach (var detail in details)
                {
                    detail.UserId = null;
                    detail.SendLater = null;
                    detail.GroupName = null;
                    detail.ParentId = null;

                    if (!detail.Details.Any() || detail.Details.FirstOrDefault(filter => !(filter.SendLater.HasValue && filter.SendLater.Value)) != null)
                    {
                        detail.ServerId = null;
                    }

                    FormatDetailProduct(detail);
                    if (detail.Details.Any())
                    {
                        FormatDetailsToSend(detail.Details);
                    }
                    else
                    {
                        detail.Details = null;
                    }
                }
            }
        }

        /// <summary>
        /// Formats the detail product.
        /// </summary>
        /// <param name="detail">Detail.</param>
        private static void FormatDetailProduct(OrderDetail detail)
        {
            if (detail.Product != null)
            {
                detail.ProductId = detail.Product.Id;
                detail.Product = null;
            }
        }

        /// <summary>
        /// Sends the order by kitchen.
        /// </summary>
        /// <returns>The order by kitchen.</returns>
        /// <param name="orderByKitchen">Order by kitchen.</param>
        private async Task<bool> SendOrderByKitchen(OrderByKitchen orderByKitchen)
        {
            var url = $"{GenerateBaseURL(API_ENDPOINT)}{orderByKitchen.Id}/";

            var apiResponse = await Put(url, orderByKitchen);
            if (apiResponse.Code == ApiCode.ORDER_COUNTER_WAS_PROCESSED)
            {
                throw OrderCounterWasProcessedException.Build(apiResponse.Message);
            }
            if (apiResponse.Code == ApiCode.PAYMENT_REJECTED)
            {
                throw OrderCounterPaymentRejectedException.Build(apiResponse.Response);
            }
            return apiResponse.IsOk();
        }

        public Task<bool> MarkWithdraw(Order order)
        {
            order.Details = new List<OrderDetail>();

            var orderByKitchen = new OrderByKitchen(order)
            {
                Status = (int)OrderStatus.READY_FOR_WITHDRAW
            };
            return SendOrderByKitchen(orderByKitchen);
        }

        public Task<bool> MarkDelivered(Order order)
        {
            order.Details = new List<OrderDetail>();

            var orderByKitchen = new OrderByKitchen(order)
            {
                Status = (int)OrderStatus.CLOSED
            };
            return SendOrderByKitchen(orderByKitchen);
        }

        public async Task<bool> SendTakeAwayExistOrder(long placeId, int orderNumber)
        {
            var takeAwayExistOrder = TakeAwayExistOrder.Build(placeId, orderNumber);
            var order = await this.Insert<Order, TakeAwayExistOrder>(takeAwayExistOrder, API_CREATE_TAKE_AWAY);
            return order != null && order.NumberToWithdraw.HasValue && order.NumberToWithdraw.Value == orderNumber;
        }

        public async Task<IEnumerable<Order>> GetActives(BaseQueryFilter queryFilter)
        {
            var url = $"{GenerateBaseURL(API_ACTIVE_ORDERS_ENDPOINT)}";
            var apiResponse = await this.Get(url);
            if (apiResponse.IsOk())
            {
                var remoteResponse = DeserializeObject<RemoteResponse<Order>>(apiResponse.Response);
                return remoteResponse.Results;
            }
            else
            {
                throw new ApiResponseException(apiResponse);
            }
        }
    }

    #region utils
    public class OrderPayCall : IBaseDataObject
    {

        public long OrderId
        {
            get;
            set;
        }

        public long Id { get; set; }

        public bool Force
        {
            get;
            set;
        }

        [JsonIgnore]
        public Order Order
        {
            get;
            set;
        }

        public string RedirectUrl
        {
            get;
            set;
        }

        public static OrderPayCall Builder(Order order, bool force = false)
        {
            return new OrderPayCall
            {
                OrderId = (int)order.ServerId,
                Force = force
            };
        }

        public Order GetOrder()
        {
            if (this.Order != null)
            {
                this.Order.RedirectUrl = this.RedirectUrl;
                return this.Order;
            }
            else
            {
                return null;
            }
        }
    }


    public class OrderByKitchen : BaseDataObject
    {
        public List<OrderDetail> Details
        {
            get;
            set;
        }

        public long? OrderCounterId
        {
            get;
            set;
        }

        public int? Status
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Mode
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public int? NumberToWithdraw
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeDelay
        {
            get;
            set;
        }


        public OrderByKitchen()
        {

        }

        public OrderByKitchen(Order order)
        {
            Details = order.Details;
            if (order.OrderCounterId != null)
            {
                OrderCounterId = order.OrderCounterId.Value;
            }
            Id = order.ServerId.Value;
            NumberToWithdraw = order.NumberToWithdraw;
            TimeDelay = order.TimeDelay;
            if (order.Mode > 0)
            {
                Mode = order.Mode;
            }

        }
    }

    public class TakeAwayExistOrder : IBaseDataObject
    {
        [JsonIgnore]
        public long Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public long PlaceId
        {
            get;
            set;
        }

        public int NumberToWithdraw
        {
            get;
            set;
        }

        /// <summary>
        /// Build the specified placeId and orderNumber.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="orderNumber">Order number.</param>
        public static TakeAwayExistOrder Build(long placeId, int orderNumber)
        {
            return new TakeAwayExistOrder
            {
                PlaceId = placeId,
                NumberToWithdraw = orderNumber
            };
        }
    }

    #endregion
}
