﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote
{
    public interface IOrderRemoteDataSource : IOrderDataSource
    {
        /// <summary>
        /// Checks the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        /// <param name="force">If set to <c>true</c> force.</param>
        Task<Order> CheckOrder(Order order, bool force);
        /// <summary>
        /// Sends to kitchen.
        /// </summary>
        /// <returns>The to kitchen.</returns>
        /// <param name="order">Order.</param>
        Task<Order> SendToKitchen(Order order);

        /// <summary>
        /// Observeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        Task<bool> ObservedOrder(Order order);

        /// <summary>
        /// Sends the order confirmation.
        /// </summary>
        /// <returns>The order confirmation.</returns>
        /// <param name="order">Order.</param>
        Task<bool> SendOrderConfirmation(Order order);

        /// <summary>
        /// Closeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        Task<bool> ClosedOrder(Order order);

        /// <summary>
        /// Marks the withdraw.
        /// </summary>
        /// <returns>The withdraw.</returns>
        /// <param name="order">Order.</param>
        Task<bool> MarkWithdraw(Order order);

        /// <summary>
        /// Marks the delivered.
        /// </summary>
        /// <returns>The delivered.</returns>
        /// <param name="order">Order.</param>
        Task<bool> MarkDelivered(Order order);

        /// <summary>
        /// Sends the take away exist order.
        /// </summary>
        /// <returns>The take away exist order.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="orderNumber">Order number.</param>
        Task<bool> SendTakeAwayExistOrder(long placeId, int orderNumber);

        /// <summary>
        /// Gets the actives.
        /// </summary>
        /// <returns>The actives.</returns>
        /// <param name="queryFilter">Query filter.</param>
        Task<IEnumerable<Order>> GetActives(BaseQueryFilter queryFilter);
    }
}
