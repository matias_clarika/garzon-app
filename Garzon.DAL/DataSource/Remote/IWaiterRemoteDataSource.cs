﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote
{
    public interface IWaiterRemoteDataSource : IWaiterDataSource
    {
        /// <summary>
        /// Calls the waiter.
        /// </summary>
        /// <returns>The waiter.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="tableNumber">Table number.</param>
        /// <param name="options">Options.</param>
        Task<bool> CallWaiter(long placeId,
                              long sectorId,
                              int tableNumber,
                              List<WaiterCallOption> options);
    }


    public class WaiterCallExistException : Exception
    {
        private ApiResponseException e;

        public WaiterCallExistException(ApiResponseException e): base(e.Message)
        {
            this.e = e;
        }
    }

    public class TableNumberNotExistException : Exception
    {
        private ApiResponseException e;

        public TableNumberNotExistException(ApiResponseException e) : base(e.Message)
        {
            this.e = e;
        }
    }
}
