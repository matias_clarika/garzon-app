﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource.Remote
{
    public interface IOrderCounterRemoteDataSource : IOrderCounterDataSource
    {
        /// <summary>
        /// Marks the received.
        /// </summary>
        /// <returns>The received.</returns>
        /// <param name="orderCounterReceived">Order counter received.</param>
        Task<bool> MarkReceived(OrderCounterReceived orderCounterReceived);
        /// <summary>
        /// Changes the table number.
        /// </summary>
        /// <returns>The table number.</returns>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="tableNumber">Table number.</param>
        /// <param name="orderId">Order identifier.</param>
        Task<bool> ChangeTableNumber(long sectorId, int tableNumber, long orderId);
        /// <summary>
        /// Blocks the users by order identifier.
        /// </summary>
        /// <returns>The users by order identifier.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<bool> BlockUsersByOrderId(long orderId);
    }
}
