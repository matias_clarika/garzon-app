﻿using System;
namespace Garzon.DAL.DataSource.Remote
{
    public static class ApiCode
    {

        #region user auth
        public const int USER_INVALID_USER_OR_PASSWORD = 400;

        #endregion
        #region user
        public const int USER_BLOCK_PERMANENT = 401;
        public const int USER_BLOCK_TEMPORALY = 406;
        #endregion

        #region check order
        public const int CHECK_ORDER_EXIST = 403;
        public const int CHECK_ORDER_EXIST_PENDING_DETAILS = 409;
        public const int CHECK_ORDER_EXIST_PENDING_REQUEST_DETAILS = 412;
        #endregion

        #region order
        public const int ORDER_INVALID_PIN = 403;
        public const int ORDER_TABLE_NOT_FOUND = 404;
        public const int ORDER_QUANTITY_DINERS_REQUIRED = 409;
        public const int ORDER_INVALID_PRODUCTS = 412;
        #endregion

        #region waiter called
        public const int WAITER_CALLED_EXIST = 403;
        public const int WAITER_CALLED_TABLE_NUMBER_NOT_FOUND = 404;
        #endregion

        #region validate app version
        public const int CHECK_VERSION_INCORRECT = 406;
        #endregion

        #region order counter
        public const int ORDER_COUNTER_WAS_PROCESSED = 406;
        #endregion

        #region payment
        public const int PAYMENT_REJECTED = 402;
        public const int PAYMENT_INVALID_CARD = 413;
        #endregion



    }
}
