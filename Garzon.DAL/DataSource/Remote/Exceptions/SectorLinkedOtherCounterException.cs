﻿namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    using System;

    public class SectorLinkedOtherCounterException : Exception
    {
        
        public SectorLinkedOtherCounterException(string message) : base(message)
        {
            
        }
    }
}