﻿using System;
using System.Collections.Generic;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;
using Garzon.DataObjects;
using Newtonsoft.Json;

namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    public class SentToKitchenException : Exception
    {
        #region inmutables
        private readonly ERROR _error;
        private readonly string _message;
        #endregion

        public ERROR Error
        {
            get => _error;
        }
        public enum ERROR
        {
            TABLE_NOT_FOUND,
            PIN,
            INTERNAL_EXCEPTION,
            QUANTITY_REQUIRED,
            BLOCK_USER,
            ORDER_INVALID_PRODUCTS,
            PAYMENT_REJECTED,
            PAYMENT_INVALID_CARD
        }

        public SentToKitchenException(ERROR error, string message = "")
        {
            _error = error;
            _message = message;
        }

        JsonSerializerSettings serializerSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            ContractResolver = new UnderscorePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };

        public List<Product> InvalidProducts
        {
            get
            {
                if (this.Error == ERROR.ORDER_INVALID_PRODUCTS && !string.IsNullOrEmpty(this.Message))
                {
                    return JsonConvert.DeserializeObject<List<Product>>(this.Message, this.serializerSettings);
                }
                else
                {
                    return default(List<Product>);
                }
            }
        }
        #region builders
        /// <summary>
        /// Builds the by API response exception.
        /// </summary>
        /// <returns>The by API response exception.</returns>
        /// <param name="ae">Ae.</param>
        public static SentToKitchenException BuildByApiResponseException(ApiResponseException ae)
        {
            ERROR error = ERROR.INTERNAL_EXCEPTION;
            switch (ae.Code)
            {
                case ApiCode.USER_BLOCK_PERMANENT:
                case ApiCode.USER_BLOCK_TEMPORALY:
                    error = ERROR.BLOCK_USER;
                    break;
                case ApiCode.ORDER_INVALID_PIN:
                    error = ERROR.PIN;
                    break;
                case ApiCode.ORDER_TABLE_NOT_FOUND:
                    error = ERROR.TABLE_NOT_FOUND;
                    break;
                case ApiCode.ORDER_INVALID_PRODUCTS:
                    error = ERROR.ORDER_INVALID_PRODUCTS;
                    break;
                case ApiCode.ORDER_QUANTITY_DINERS_REQUIRED:
                    error = ERROR.QUANTITY_REQUIRED;
                    break;
                case ApiCode.PAYMENT_REJECTED:
                    error = ERROR.PAYMENT_REJECTED;
                    break;
                case ApiCode.PAYMENT_INVALID_CARD:
                    error = ERROR.PAYMENT_INVALID_CARD;
                    break;
            }
            return new SentToKitchenException(error, ae.Response);
        }
        /// <summary>
        /// Builds the by exception.
        /// </summary>
        /// <returns>The by exception.</returns>
        /// <param name="e">E.</param>
        public static SentToKitchenException BuildByException(Exception e)
        {
            return new SentToKitchenException(ERROR.INTERNAL_EXCEPTION, e.Message);
        }
        #endregion

        #region overrides
        public override string Message
        {
            get
            {
                return _message;
            }
        }
        #endregion


    }
}
