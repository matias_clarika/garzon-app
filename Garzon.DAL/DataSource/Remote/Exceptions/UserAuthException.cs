﻿namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    using System;
    using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

    public class UserAuthException : Exception
    {
        private readonly ApiResponseException apiResponseException;

        public int Code{
            get{
                return apiResponseException.Code;
            }
        }

        private UserAuthException(ApiResponseException apiResponseException)
        {
            this.apiResponseException = apiResponseException;
        }

        public static UserAuthException Build(ApiResponseException apiResponseException){
            return new UserAuthException(apiResponseException);
        }

    }
}
