﻿using System;
using System.Text.RegularExpressions;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    public class UserSessionException : Exception
    {

        private ApiResponseException _apiResponseException;
        private ERROR _error;

        public ERROR Error
        {
            get
            {
                AnalyzeError();
                return _error;
            }
        }

        private int? _pendingTime;
        public int? PendingTime
        {
            get
            {
                if (!_pendingTime.HasValue)
                {
                    CalculatePendingTime();
                }
                return _pendingTime.Value;
            }
        }

        private UserSessionException(ApiResponseException apiResponseException)
        {
            _apiResponseException = apiResponseException;
        }

        /// <summary>
        /// Build the specified apiResponseException.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="apiResponseException">API response exception.</param>
        public static UserSessionException Build(ApiResponseException apiResponseException)
        {
            return new UserSessionException(apiResponseException);
        }

        public enum ERROR
        {
            BLOCK_USER_TEMPORALY, BLOCK_USER_PERMANENT, INVALID_USER, INTERNAL_ERROR, GENERAL_ERROR
        }

        /// <summary>
        /// Analyzes the error.
        /// </summary>
        private void AnalyzeError()
        {
            switch (_apiResponseException.Code)
            {
                case ApiCode.USER_BLOCK_PERMANENT:
                    _error = ERROR.BLOCK_USER_PERMANENT;
                    break;
                case ApiCode.USER_BLOCK_TEMPORALY:
                    _error = ERROR.BLOCK_USER_TEMPORALY;
                    break;
                default:
                    _error = ERROR.GENERAL_ERROR;
                    break;
            }
        }

        /// <summary>
        /// Gets the seconds in response.
        /// </summary>
        private void CalculatePendingTime()
        {
            if (_apiResponseException != null
                                   && !String.IsNullOrEmpty(_apiResponseException.Response))
            {
                int seconds;
                try{
                    var stringNumber = Regex.Unescape(_apiResponseException.Response.Replace("\\", "").Replace("\"", ""));
                    seconds = int.Parse(stringNumber);
                }catch(Exception){
                    seconds = 0;
                }
                _pendingTime = seconds;
            }
        }
    }
}
