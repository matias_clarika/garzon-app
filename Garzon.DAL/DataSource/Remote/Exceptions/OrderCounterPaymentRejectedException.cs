﻿namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    using System;

    public class OrderCounterPaymentRejectedException : Exception
    {
        private OrderCounterPaymentRejectedException(string message) : base(message)
        {

        }

        public static OrderCounterPaymentRejectedException Build(string message)
        {
            return new OrderCounterPaymentRejectedException(message);
        }
    }
}
