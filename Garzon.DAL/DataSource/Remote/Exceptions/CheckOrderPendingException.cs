﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    public class CheckOrderPendingException : Exception
    {
        private ApiResponseException e;

        public CheckOrderPendingException(ApiResponseException e) : base(e.Message)
        {
            this.e = e;
        }
    }
}
