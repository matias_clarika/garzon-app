﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    public class CheckOrderExistException : Exception
    {
        private ApiResponseException e;

        public CheckOrderExistException(ApiResponseException e) : base(e.Message)
        {
            this.e = e;
        }
    }
}
