﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource.Remote;

namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    public class CheckOrderPendingRequestException : Exception
    {
        private ApiResponseException e;

        public CheckOrderPendingRequestException(ApiResponseException e) : base(e.Message)
        {
            this.e = e;
        }
    }
}
