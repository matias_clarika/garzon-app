﻿namespace Garzon.DAL.DataSource.Remote.Exceptions
{
    using System;

    public class OrderCounterWasProcessedException : Exception
    {
        private OrderCounterWasProcessedException(string message) : base(message)
        {
        }

        /// <summary>
        /// Build the specified message.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="message">Message.</param>
        public static OrderCounterWasProcessedException Build(string message)
        {
            return new OrderCounterWasProcessedException(message);
        }
    }
}
