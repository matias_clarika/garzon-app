﻿using System.Threading.Tasks;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;

namespace Garzon.DAL.DataSource.Remote
{
    public interface IUserRemoteDataSource : IUserDataSource
    {
        /// <summary>
        /// Signs the in social.
        /// </summary>
        /// <returns>The in social.</returns>
        /// <param name="provider">Provider.</param>
        /// <param name="socialUser">Social user.</param>
        Task<User> SignInSocial(SocialProviders provider,
                                SocialUser socialUser);

        /// <summary>
        /// Signs the in counter.
        /// </summary>
        /// <returns>The in counter.</returns>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        Task<Counter> SignInCounter(string userName,
                          string password);
        Task<bool> IsCounterOnline(long counterId, bool isOnline);
    }

    public enum SocialProviders
    {
        FACEBOOK = 1, GOOGLE = 2
    }
}
