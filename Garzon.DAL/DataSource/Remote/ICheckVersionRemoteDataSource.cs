﻿using System;
using System.Threading.Tasks;

namespace Garzon.DAL.DataSource.Remote
{
    public interface ICheckVersionRemoteDataSource
    {
        /// <summary>
        /// Validates the app version.
        /// </summary>
        /// <returns>The app version.</returns>
        Task<bool> ValidateAppVersion();
    }
}
