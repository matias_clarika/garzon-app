﻿using System;
using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface ISectorDataSource : IDataSource<Sector>
    {
    }

    public static class SectorParams
    {
        public const string PLACE_ID = "place_id";
    }
}
