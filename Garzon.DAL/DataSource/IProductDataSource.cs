﻿using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface IProductDataSource : IDataSource<Product>
    {
        
    }

    public static class ProductParams
    {
        public const string MENU_SECTION = "menu_section";
        public const string GROUP = "GROUP";
    }
}
