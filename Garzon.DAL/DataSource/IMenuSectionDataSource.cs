﻿using Clarika.Xamarin.Base.Persistence.DataSource;
using Garzon.DataObjects;

namespace Garzon.DAL.DataSource
{
    public interface IMenuSectionDataSource : IDataSource<MenuSection>
    {
    }
}
