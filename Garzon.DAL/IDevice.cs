﻿namespace Garzon.DAL
{
    public interface IDevice
    {
        Platform GetPlatform();
    }

    public enum Platform{
        ANDROID = 1,
        IOS = 2
    }
}
