﻿using System;
using System.Threading.Tasks;
using Garzon.BL.Common;
using Garzon.DAL.Repository;

namespace Garzon.BL.Implementation.Common
{
    public class CheckVersionBL : ICheckVersionBL
    {
        #region dependency
        readonly ICheckVersionRepository checkVersionRepository;
        #endregion
        public CheckVersionBL(ICheckVersionRepository checkVersionRepository)
        {
            this.checkVersionRepository = checkVersionRepository;
        }

        #region ICheckVersionBL

        public Task<bool> ValidateAppVersion()
        {
            return this.checkVersionRepository.ValidateAppVersion();
        }
        #endregion
    }
}
