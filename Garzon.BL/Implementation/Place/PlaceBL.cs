﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DAL.Repository;
using Garzon.DataObjects;

namespace Garzon.BL.Implementation
{
    public class PlaceBL : IPlaceBL
    {
        #region dependency
        readonly IPlaceRepository _placeRepository;
        #endregion
        #region constructor
        public PlaceBL(IPlaceRepository placeRepository)
        {
            _placeRepository = placeRepository;
        }
        #endregion

        #region Implementation of IPlaceBL
        public async Task<List<Place>> Find(IPaggination paggination = null, double? latitude = null, double? longitude = null)
        {
            return await _placeRepository.Find(paggination, latitude, longitude);
        }
        #endregion

    }
}
