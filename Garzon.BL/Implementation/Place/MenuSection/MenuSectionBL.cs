﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DAL.Repository;
using Garzon.DataObjects;

namespace Garzon.BL.Implementation
{
    public class MenuSectionBL : IMenuSectionBL
    {
        #region dependency
        IMenuSectionRepository _menuSectionRepository;
        #endregion

        #region constructor
        public MenuSectionBL(IMenuSectionRepository menuSectionRepository)
        {
            _menuSectionRepository = menuSectionRepository;
        }
        #endregion

        #region implementation of IMenuSectionBL
        public async Task<List<MenuSection>> Find(IPaggination paggination = null, long menuSectionId = -1)
        {
            
            return await _menuSectionRepository.Find(menuSectionId, paggination);
        }

        public Task<MenuSection> FindById(long menuSectionId)
        {
            return _menuSectionRepository.FindById(menuSectionId);
        }

        public Task<MenuSection> FindByProductId(long productId)
        {
            return _menuSectionRepository.FindByProductId(productId);
        }

        #endregion


    }
}
