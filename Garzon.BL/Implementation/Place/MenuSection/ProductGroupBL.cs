﻿namespace Garzon.BL.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DAL.Repository;
    using Garzon.DataObjects;
    using System.Linq;

    public class ProductGroupBL : IProductGroupBL
    {
        #region dependency
        readonly IProductRepository productRepository;
        #endregion
        public ProductGroupBL(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        #region IProductGroupBL
        public async Task<List<Product>> FindListByGroupId(long groupId, OrderDetail pendingOrderDetail = null, IPaggination paggination = null)
        {
            var products = await productRepository.FindListByGroupId(groupId, paggination);
            if (pendingOrderDetail != null && !pendingOrderDetail.IsObserved())
            {
                if (pendingOrderDetail.ParentProductId.HasValue)
                {
                    var parentProduct = await this.productRepository.FindById(pendingOrderDetail.ParentProductId.Value);
                    if (parentProduct.IsCompoundPromoEqual())
                    {
                        products = products.FindAll((filter) => filter.Id == pendingOrderDetail.ProductId);
                    }
                }
            }

            return products;
        }
        #endregion
    }
}
