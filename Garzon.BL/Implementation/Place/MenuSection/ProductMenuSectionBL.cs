﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DAL.Repository;
using Garzon.DataObjects;

namespace Garzon.BL.Implementation
{
    public class ProductMenuSectionBL : IProductMenuSectionBL
    {
        #region dependency
        readonly IProductRepository _productRepository;
        #endregion
        #region constructor
        public ProductMenuSectionBL(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        #endregion

        #region Implementation of IProductMenuSectionBL
        public async Task<List<Product>> Find(long menuSectionId, IPaggination paggination)
        {
            return await _productRepository.FindBySectionMenu(menuSectionId, paggination);
        }

        public Task<Product> FindByID(long productId)
        {
            return _productRepository.FindById(productId);
        }

        #endregion
    }
}
