﻿namespace Garzon.BL.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DAL.Repository;
    using Garzon.DataObjects;

    public class GroupBL : IGroupBL
    {
        #region dependency
        readonly IGroupRepository groupRepository;
        #endregion
        public GroupBL(IGroupRepository groupRepository)
        {
            this.groupRepository = groupRepository;
        }

        #region IGroupBL
        public Task<List<Group>> FindListByProductId(long productId, IPaggination paggination = null)
        {
            return this.groupRepository.FindListByProductId(productId, paggination);
        }


        public Task<Group> FindById(long id, long productId)
        {
            return this.groupRepository.FindById(id, productId);
        }
        #endregion
    }
}
