﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DAL.Repository;
using Garzon.DataObjects;
using Garzon.BL;

namespace Garzon.BL.Implementation
{
    public class ProductRecommendedBL : IProductRecommendedBL
    {
        #region dependency
        readonly IProductRepository _productRepository;
        #endregion

        #region constructor
        public ProductRecommendedBL(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        #endregion

        #region implementation of IProductRecommendedBL
        public async Task<List<Product>> Find(long placeId, IPaggination paggination = null)
        {
            return await _productRepository.FindRecommended(placeId, paggination);
        }

        #endregion
    }
}
