﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.Repository;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public class MenuBL : IMenuBL
    {
        #region dependency
        readonly IMenuRepository _menuRepository;
        #endregion
        public MenuBL(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }

        #region Implementation of IMenuBL
        public Task<Menu> FindMenuByPlace(long placeId)
        {
            return _menuRepository.FindMenuByPlace(placeId);
        }
        #endregion
    }
}
