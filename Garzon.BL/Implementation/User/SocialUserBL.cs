﻿using System.Threading.Tasks;
using Garzon.DAL;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;

namespace Garzon.BL.Implementation
{
    public class SocialUserBL : ISocialUserBL
    {
        #region dependencies
        readonly IUserRepository _userRepository;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Garzon.BL.Implementation.UserBL"/> class.
        /// </summary>
        /// <param name="userRepository">User repository.</param>
        public SocialUserBL(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        #region Implementation of IUserBL

        public async Task<User> SignInFacebook(SocialUser socialUser)
        {
            await _userRepository.LogoutCurrentUser();
            return await _userRepository.SignInFacebook(socialUser);
        }

        public async Task<User> SignInGoogle(SocialUser socialUser)
        {
            await _userRepository.LogoutCurrentUser();
            return await _userRepository.SignInGoogle(socialUser);
        }

        public Task<User> SignInLocal(User user)
        {
            return _userRepository.SignInLocalUser(user);
        }
        #endregion
    }
}
