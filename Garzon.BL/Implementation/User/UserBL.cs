﻿using System;
using System.Threading.Tasks;
using Garzon.DAL;
using Garzon.DataObjects;

namespace Garzon.BL.Implementation
{
    public class UserBL : IUserBL
    {
        #region dependency
        readonly IUserRepository userRepository;
        #endregion
        public UserBL(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        #region Implementation of IUserBL
        public Task<User> FindCurrentUser()
        {
            return userRepository.GetLoggedUser();
        }

        public async Task<Counter> GetCurrentCounter()
        {
            var user = await this.FindCurrentUser();
            bool isMain = await this.userRepository.GetCurrentCounterIsMain();
            long placeId = await this.userRepository.GetCurrentCounterPlaceId();
            return Counter.Build(user, isMain, placeId);

        }

        public Task<bool> Logout()
        {
            return userRepository.LogoutCurrentUser();
        }

        public Task SetDeviceToken(string deviceToken)
        {
            return userRepository.SetDeviceToken(deviceToken);
        }

        public async Task<Counter> SignInCounter(string userName, string password)
        {
            var counter = await userRepository.SignInCounter(userName, password);
            await this.userRepository.SetForceLogout(false);
            await IsCounterOnline(true);
            return counter;
        }


        public async Task<bool> IsCounterOnline(bool isOnline = false)
        {
            try
            {
                var userLogged = await this.userRepository.GetLoggedUser();
                if (userLogged != null && userLogged.Id > 0)
                {
                    return await this.userRepository.IsCounterOnline(userLogged.Id, isOnline);
                }
                return false;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<bool> ForceLogout()
        {
            bool forceLogout = await this.userRepository.GetForceLogout();
            if (forceLogout)
            {
                await this.Logout();
                await this.userRepository.SetForceLogout(false);
                return true;
            }
            return false;
        }
        #endregion
    }
}
