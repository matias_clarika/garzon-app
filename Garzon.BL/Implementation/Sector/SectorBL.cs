﻿namespace Garzon.BL.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Garzon.BL.Abstraction.Exceptions;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Garzon.DAL.Repository;
    using Garzon.DataObjects;

    public class SectorBL : ISectorBL
    {
        #region dependency
        protected ISectorRepository sectorRepository;
        #endregion
        #region constructor
        public SectorBL(ISectorRepository sectorRepository)
        {
            this.sectorRepository = sectorRepository;
        }

        #endregion
        #region ISectorBL
        public Task<List<SectorCounter>> FindSectors()
        {
            return this.sectorRepository.FindSectors();
        }

        public async Task<bool> ChooseSectors(List<SectorCounter> sectors, bool force)
        {
            try
            {
                return await this.sectorRepository.ChooseSectors(sectors, force);
            }
            catch (SectorLinkedOtherCounterException e)
            {
                throw new ChooseSectorLinkedException(e.Message);
            }
            catch (Exception e1)
            {
                throw e1;
            }
        }

        #endregion
    }
}