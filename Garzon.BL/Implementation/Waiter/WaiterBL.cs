﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Remote;
using Garzon.DAL.Repository;
using Garzon.DataObjects;

namespace Garzon.BL.Implementation
{
    public class WaiterBL : IWaiterBL
    {
        #region dependency
        readonly IWaiterRepository _waiterRepository;
        #endregion
        public WaiterBL(IWaiterRepository waiterRepository)
        {
            _waiterRepository = waiterRepository;
        }

        #region Implementation of IWaiterBL

        public Task<List<WaiterCallOption>> FindByPlaceId(long placeId)
        {
            return _waiterRepository.FindByPlaceId(placeId);
        }

        public async Task<bool> Call(long placeId,
                                     long sectorId,
                                     int tableNumber,
                                     List<WaiterCallOption> options)
        {
            try
            {
                return await _waiterRepository.CallWaiter(placeId,
                                                          sectorId,
                                                          tableNumber,
                                                          options);
            }
            catch (WaiterCallExistException e1)
            {
                throw new WaiterCallBLException(e1);
            }catch(TableNumberNotExistException e){
                throw new TableNumberNotExistBLException(e);
            }

        }
        #endregion
    }
}
