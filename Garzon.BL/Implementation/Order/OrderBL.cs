﻿using System;
using System.Threading.Tasks;
using Garzon.Config;
using Garzon.DAL.Repository;
using Garzon.DataObjects;
using System.Linq;
using Garzon.DAL.DataSource.Remote.Exceptions;
using System.Collections.Generic;
using Clarika.Xamarin.Base.Helpers;

namespace Garzon.BL.Implementation
{
    public class OrderBL : IOrderBL
    {
        #region dependency
        readonly IOrderRepository orderRepository;
        readonly AppDefaultConfig _appConfig;
        #endregion
        public OrderBL(IOrderRepository orderRepository,
                       AppDefaultConfig appConfig)
        {
            this.orderRepository = orderRepository;
            _appConfig = appConfig;
        }

        #region Implementation of IOrderBL
        public Task<Order> FindCurrentOrder()
        {
            return orderRepository.GetCurrentOrder();
        }

        public async Task<Order> SendToKitchen(Order order)
        {
            FormatDetailsToSend(order);
            return await orderRepository.SendToKitchen(order);
        }

        public async Task<bool> IsConfirmByKitchen(long orderId)
        {
            Order order;
            do
            {
                await Task.Delay(_appConfig.TimeToRefreshOrderStatus);

                order = await orderRepository.GetOrderById(orderId);
                if (order == null)
                {
                    throw new OrderCancelByKitchenException();
                }

            } while (order.InKitchen == null || !order.InKitchen.Value);

            return true;
        }

        public async Task<Order> CheckOrder(Order order, bool force)
        {
            try
            {
                return await orderRepository.CheckOrder(order, force);
            }
            catch (CheckOrderExistException e)
            {
                throw new CheckOrderBLExistException(e);
            }
            catch (CheckOrderPendingException e1)
            {
                throw new CheckOrderBLPendingException(e1);
            }
            catch (CheckOrderPendingRequestException e2)
            {
                throw new CheckOrderBLPendingRequestException(e2);
            }
        }
        public async Task<Order> CheckOrderStatus(long orderId)
        {
            Order order = await orderRepository.GetOrderById(orderId);
            return order;
        }

        public Task<Order> SyncOrder(string data)
        {
            return orderRepository.SyncOrder(data);
        }

        public Task<bool> ClosedOrder(Order order)
        {
            return orderRepository.ClosedOrder(order);
        }

        public Task<bool> ClearCurrentOrder()
        {
            return orderRepository.ClearCurrentOrder();
        }


        #endregion

        private void FormatDetailsToSend(Order order)
        {
            var parentList = order.Details.Where(filter => !filter.ParentId.HasValue);

            foreach (var parentDetail in parentList)
            {
                var childrenList = order.Details.Where(filter => filter.ParentId == parentDetail.Id);
                if (childrenList.Any())
                {
                    parentDetail.Details = childrenList.ToList();
                }
            }
            order.Details = parentList.ToList();
        }

        public Task<Order> GetSyncOrder(string data)
        {
            return orderRepository.GetSyncOrder(data);
        }

        public Task<bool> SendTakeAwayExistOrder(long placeId, int orderNumber)
        {
            return orderRepository.SendTakeAwayExistOrder(placeId, orderNumber);
        }

        public Task<List<Order>> GetMyOrderHistory(int offset, int limit)
        {
            return orderRepository.GetMyOrderHistory(offset, limit);
        }
    }

    public class OrderCancelByKitchenException : Exception
    {
    }

}
