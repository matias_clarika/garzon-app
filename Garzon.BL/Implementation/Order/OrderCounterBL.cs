﻿namespace Garzon.BL
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.BL.Abstraction.Exceptions;
    using Garzon.DAL.DataSource.Remote.Exceptions;
    using Garzon.DAL.Repository;
    using Garzon.DataObjects;

    public class OrderCounterBL : IOrderCounterBL
    {
        #region dependency
        protected readonly IOrderCounterRepository orderCounterRepository;
        #endregion
        public OrderCounterBL(IOrderCounterRepository orderCounterRepository)
        {
            this.orderCounterRepository = orderCounterRepository;
        }
        #region implementation of IOrderCounterBL
        public Task<bool> MarkWaiterCallReceived(long id)
        {
            return MarkReceived(new OrderCounterReceived
            {
                Id = id,
                ReceivedAction = ReceivedAction.WAITER
            });
        }

        public Task<bool> ClosedOrder(OrderPayCounter orderPayCounter)
        {
            return orderCounterRepository.ClosedOrder(orderPayCounter.ToOrder());
        }

        public Task<bool> ClosedOrder(Order order)
        {
            return orderCounterRepository.ClosedOrder(order);
        }

        public Task<bool> MarkReceived(OrderCounterReceived orderCounterReceived)
        {
            return orderCounterRepository.MarkReceived(orderCounterReceived);
        }

        public async Task<bool> SendObservation(OrderCounter orderCounter)
        {
            try
            {
                return await orderCounterRepository.SendObservation(orderCounter);
            }
            catch (OrderCounterWasProcessedException e)
            {
                throw OrderCounterWasProcessedBLException.Build(e.Message);
            }
            catch (OrderCounterPaymentRejectedException e1)
            {
                throw OrderCounterPaymentRejectedBLException.Build(e1.Message);
            }
        }

        public async Task<bool> SendConfirmation(OrderCounter orderCounter)
        {
            MarkInKitchen(orderCounter);
            try
            {
                return await orderCounterRepository.SendConfirmation(orderCounter);
            }
            catch (OrderCounterWasProcessedException e)
            {
                throw OrderCounterPaymentRejectedBLException.Build(e.Message);
            }
        }

        private static void MarkInKitchen(OrderCounter orderCounter)
        {
            foreach (var orderDetail in orderCounter.Details)
            {
                orderDetail.SetInKitchen();
                if (orderDetail.Details.Any())
                {
                    foreach (var children in orderDetail.Details)
                    {
                        children.SetInKitchen();
                    }
                }
            }
        }

        public Task<List<OrderCounter>> FindOrderCounter(IPaggination paggination = null)
        {
            return orderCounterRepository.Find(paggination);
        }

        public Task<List<WaiterCounter>> FindWaiterCounter(IPaggination paggination = null)
        {
            return orderCounterRepository.FindWaiterCalled(paggination);
        }

        public Task<List<OrderPayCounter>> FindOrderPayCounter(IPaggination paggination = null)
        {
            return orderCounterRepository.FindOrderPayRequest(paggination);
        }

        public Task<List<Order>> GetOrders()
        {
            return orderCounterRepository.FindActiveOrders((int)PlaceMode.ServerPlaceModeIds.RESTO);
        }

        public Task<bool> ChangeTableNumberByOrderId(long sectorId, int tableNumber, long orderId)
        {
            return orderCounterRepository.ChangeTableNumberByOrderId(sectorId, tableNumber, orderId);
        }

        public Task<bool> BlockUsersByOrder(Order order)
        {
            return orderCounterRepository.BlockUsersByOrderId(order.ServerId.Value);
        }

        public Task<List<Order>> GetOrdersTakeAway()
        {
            return orderCounterRepository.FindOrders((int)PlaceMode.ServerPlaceModeIds.TAKE_AWAY);
        }


        public Task<bool> MarkWithdraw(Order order)
        {
            return orderCounterRepository.MarkWithdraw(order);
        }

        public Task<bool> MarkDelivered(Order order)
        {
            return orderCounterRepository.MarkDelivered(order);
        }
        #endregion
    }
}
