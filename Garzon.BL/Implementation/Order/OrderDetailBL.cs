﻿namespace Garzon.BL.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Garzon.DAL.Repository;
    using Garzon.DataObjects;

    public class OrderDetailBL : IOrderDetailBL
    {
        #region dependency
        readonly IOrderDetailRepository orderDetailRepository;
        #endregion

        #region constructor
        public OrderDetailBL(IOrderDetailRepository orderDetailRepository)
        {
            this.orderDetailRepository = orderDetailRepository;
        }
        #endregion

        #region Implementation of IOrderDetailBL

        public Task<List<OrderDetail>> FindByOrderId(long orderId)
        {
            return orderDetailRepository.FindByOrderId(orderId);
        }

        public Task<List<OrderDetail>> FindPendingByUser(User user)
        {
            if (user != null)
            {
                return orderDetailRepository.FindPendingByUserId(user.Id);
            }
            else
            {
                return orderDetailRepository.FindPending();
            }
        }

        public Task<List<OrderDetail>> FindConfirmed()
        {
            return orderDetailRepository.FindConfirmed();
        }

        public async Task<OrderDetail> InsertOrUpdate(OrderDetail ordetDetail)
        {
            return await orderDetailRepository.InsertOrUpdate(ordetDetail);

        }

        public Task<List<OrderDetail>> UpdatePendingToUser(long userId)
        {
            return orderDetailRepository.UpdatePendingToUser(userId);
        }

        public Task<List<OrderDetail>> FindConfirmedByUserId(long userId)
        {
            return orderDetailRepository.FindConfirmedByUserId(userId);
        }

        public Task<bool> Delete(IEnumerable<OrderDetail> orderDetails)
        {
            return orderDetailRepository.Delete(orderDetails);
        }

        public Task<OrderDetail> FindPendingByProductId(long productId,
                                                        User ownerUser = null)
        {
            return orderDetailRepository.FindPendingByProductId(productId,
                                                                 ownerUser);
        }

        public Task<List<OrderDetail>> FindWaitingOrderDetails(long userId)
        {
            return orderDetailRepository.FindWaitingOrderDetails(userId);
        }

        public async Task Delete(OrderDetail orderDetail)
        {
            await orderDetailRepository.Delete(new List<OrderDetail>{
                orderDetail
            });
        }

        public Task<List<OrderDetail>> FindPendingRequestListByUserId(long userId)
        {
            return orderDetailRepository.FindPendingRequestListByUserId(userId);
        }
        #endregion

        #region private
        private async Task SavedChildrenToOrderDetail(OrderDetail orderDetail)
        {
            List<OrderDetail> details = new List<OrderDetail>();

            foreach (var detail in orderDetail.Details)
            {
                detail.ParentId = orderDetail.Id;
                details.Add(await orderDetailRepository.InsertOrUpdate(detail));
            }

            orderDetail.Details = details;
        }

        public async Task<OrderDetail> SelectProductByPendingOrderDetail(Product product, OrderDetail orderDetail)
        {
            var parentDetail = await this.orderDetailRepository.FindById(orderDetail.ParentId.Value);
            parentDetail.SetPending();
            //clear comment
            parentDetail.Comment = string.Empty;
            await this.orderDetailRepository.InsertOrUpdate(parentDetail);

            orderDetail.Quantity = 1;
            orderDetail.Product = product;

            orderDetail.SetPending();
            await this.orderDetailRepository.InsertOrUpdate(orderDetail);
            return orderDetail;
        }

        public Task<bool> RemovePendingOrderDetailByProductId(long productId)
        {
            return this.orderDetailRepository.RemovePendingOrderDetailByProductId(productId);
        }

        public async Task DeletePendingOrderDetails()
        {
            await this.orderDetailRepository.DeletePendingOrderDetails();
        }

        #endregion
    }
}
