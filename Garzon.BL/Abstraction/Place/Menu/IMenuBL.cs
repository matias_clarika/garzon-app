﻿using System;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IMenuBL
    {
        /// <summary>
        /// Finds the menu by place.
        /// </summary>
        /// <returns>The menu by place.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<Menu> FindMenuByPlace(long placeId);
    }
}
