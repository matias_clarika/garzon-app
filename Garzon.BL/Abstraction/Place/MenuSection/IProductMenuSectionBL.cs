﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Garzon.DataObjects;
using Clarika.Xamarin.Base.Helpers;

namespace Garzon.BL
{
    public interface IProductMenuSectionBL
    {
        /// <summary>
        /// Find the specified menuSectionId and paggination.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="menuSectionId">Menu section identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Product>> Find(long menuSectionId, IPaggination paggination = null);

        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<Product> FindByID(long productId);
    }
}
