﻿
namespace Garzon.BL
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DataObjects;

    public interface IProductGroupBL
    {
        /// <summary>
        /// Finds the list by group identifier.
        /// </summary>
        /// <returns>The list by group identifier.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="pendingOrderDetail">Pending order detail.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Product>> FindListByGroupId(long groupId,
                                              OrderDetail pendingOrderDetail = null,
                                              IPaggination paggination = null);
    }
}
