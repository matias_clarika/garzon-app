﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IProductRecommendedBL
    {
        /// <summary>
        /// Find the specified placeId and paggination.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="paggination">Paggination.</param>
        Task<List<Product>> Find(long placeId, IPaggination paggination = null);

    }
}
