﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IMenuSectionBL
    {   
        /// <summary>
        /// Find the specified paggination and menuSectionId.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="paggination">Paggination.</param>
        /// <param name="menuSectionId">Menu section identifier.</param>
        Task<List<MenuSection>> Find(IPaggination paggination = null, long menuSectionId = -1);

        /// <summary>
        /// Finds the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="menuSectionId">Menu section identifier.</param>
        Task<MenuSection> FindById(long menuSectionId);
        Task<MenuSection> FindByProductId(long id);
    }
}
