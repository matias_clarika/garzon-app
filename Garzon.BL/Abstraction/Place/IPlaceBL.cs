﻿namespace Garzon.BL
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Clarika.Xamarin.Base.Helpers;
    using Garzon.DataObjects;

    public interface IPlaceBL
    {
        /// <summary>
        /// Find the specified paggination, latitude and longitude.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="paggination">Paggination.</param>
        /// <param name="latitude">Latitude.</param>
        /// <param name="longitude">Longitude.</param>
        Task<List<Place>> Find(IPaggination paggination = null, double? latitude = null, double? longitude = null);
    }
}
