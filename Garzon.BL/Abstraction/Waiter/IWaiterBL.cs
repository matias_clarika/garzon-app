﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Remote;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IWaiterBL
    {
        /// <summary>
        /// Finds the by place identifier.
        /// </summary>
        /// <returns>The by place identifier.</returns>
        /// <param name="placeId">Place identifier.</param>
        Task<List<WaiterCallOption>> FindByPlaceId(long placeId);

        /// <summary>
        /// Call the specified placeId, sectorId, tableNumber and options.
        /// </summary>
        /// <returns>The call.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="tableNumber">Table number.</param>
        /// <param name="options">Options.</param>
        Task<bool> Call(long placeId,
                        long sectorId,
                        int tableNumber,
                        List<WaiterCallOption> options);
    }

    public class WaiterCallBLException : Exception
    {
        private WaiterCallExistException wce;

        public WaiterCallBLException(WaiterCallExistException wce) : base(wce.Message)
        {
            this.wce = wce;
        }
    }

    public class TableNumberNotExistBLException : Exception
    {
        private TableNumberNotExistException tne;

        public TableNumberNotExistBLException(TableNumberNotExistException tne) : base(tne.Message)
        {
            this.tne = tne;
        }
    }
}
