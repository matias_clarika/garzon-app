﻿namespace Garzon.BL.Abstraction.Exceptions
{
    using System;

    public class OrderCounterPaymentRejectedBLException : Exception
    {
        private OrderCounterPaymentRejectedBLException(string message) : base(message)
        {
        }

        /// <summary>
        /// Build the specified message.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="message">Message.</param>
        public static OrderCounterPaymentRejectedBLException Build(string message){
            return new OrderCounterPaymentRejectedBLException(message);
        }
    }
}
