﻿namespace Garzon.BL.Abstraction.Exceptions
{
    using System;

    public class OrderCounterWasProcessedBLException : Exception
    {
        private OrderCounterWasProcessedBLException(string message) : base(message)
        {
        }

        /// <summary>
        /// Build the specified message.
        /// </summary>
        /// <returns>The build.</returns>
        /// <param name="message">Message.</param>
        public static OrderCounterWasProcessedBLException Build(string message){
            return new OrderCounterWasProcessedBLException(message);
        }
    }
}
