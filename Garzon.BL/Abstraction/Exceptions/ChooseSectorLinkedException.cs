﻿namespace Garzon.BL.Abstraction.Exceptions
{
    using System;

    public class ChooseSectorLinkedException : Exception
    {
        public ChooseSectorLinkedException(string message) : base(message)
        {
        }
    }
}
