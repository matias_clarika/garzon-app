﻿namespace Garzon.BL.Common
{
    using System;
    using System.Threading.Tasks;

    public interface ICheckVersionBL
    {
        Task<bool> ValidateAppVersion();
    }
}
