﻿using System;
namespace Garzon.BL.Currency
{
    public interface ICurrentCurrency
    {
        double Ratio();

        string Symbol();

    }
}
