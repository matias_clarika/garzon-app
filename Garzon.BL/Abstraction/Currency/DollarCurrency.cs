﻿using System;
namespace Garzon.BL.Currency
{
    public class DollarCurrency : ICurrentCurrency
    {
        public double Ratio()
        {
            return 1;
        }

        public string Symbol()
        {
            return "$";
        }
    }
}
