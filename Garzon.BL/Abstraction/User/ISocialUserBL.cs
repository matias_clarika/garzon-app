﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;
using Garzon.DataObjects.Social;

namespace Garzon.BL
{
    public interface ISocialUserBL
    {
        /// <summary>
        /// Signs the in facebook.
        /// </summary>
        /// <returns>The in facebook.</returns>
        /// <param name="socialUser">Social user.</param>
        Task<User> SignInFacebook(SocialUser socialUser);
        /// <summary>
        /// Signs the in google.
        /// </summary>
        /// <returns>The in google.</returns>
        /// <param name="socialUser">Social user.</param>
        Task<User> SignInGoogle(SocialUser socialUser);

        /// <summary>
        /// Signs the in local.
        /// </summary>
        /// <returns>The in local.</returns>
        /// <param name="user">User.</param>
        Task<User> SignInLocal(User user);

    }
}
