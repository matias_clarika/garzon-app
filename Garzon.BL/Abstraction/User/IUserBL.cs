﻿
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IUserBL
    {
        /// <summary>
        /// Finds the current user.
        /// </summary>
        /// <returns>The current user.</returns>
        Task<User> FindCurrentUser();

        /// <summary>
        /// Logout this instance.
        /// </summary>
        /// <returns>The logout.</returns>
        Task<bool> Logout();

        /// <summary>
        /// Sets the device token.
        /// </summary>
        /// <returns>The device token.</returns>
        /// <param name="deviceToken">Device token.</param>
        Task SetDeviceToken(string deviceToken);

        /// <summary>
        /// Signs the in counter.
        /// </summary>
        /// <returns>The in counter.</returns>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        Task<Counter> SignInCounter(string userName,
                          string password);

        /// <summary>
        /// Gets the current counter.
        /// </summary>
        /// <returns>The current counter.</returns>
        Task<Counter> GetCurrentCounter();

        /// <summary>
        /// Ises the user online.
        /// </summary>
        /// <returns>The user online.</returns>
        /// <param name="isOnline">If set to <c>true</c> is online.</param>
        Task<bool> IsCounterOnline(bool isOnline = false);

        /// <summary>
        /// Forces the logout.
        /// </summary>
        /// <returns>The logout.</returns>
        Task<bool> ForceLogout();
    }
}
