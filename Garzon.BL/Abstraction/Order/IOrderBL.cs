﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DAL.DataSource.Remote;
using Garzon.DAL.DataSource.Remote.Exceptions;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IOrderBL
    {
        /// <summary>
        /// Finds the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task<Order> FindCurrentOrder();

        /// <summary>
        /// Sends to kitchen.
        /// </summary>
        /// <returns>The to kitchen.</returns>
        /// <param name="order">Order.</param>
        Task<Order> SendToKitchen(Order order);

        /// <summary>
        /// Ises the confirm by kitchen.
        /// </summary>
        /// <returns>The confirm by kitchen.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<bool> IsConfirmByKitchen(long orderId);

        /// <summary>
        /// Checks the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        /// <param name="force">If set to <c>true</c> force.</param>
        Task<Order> CheckOrder(Order order, bool force);

        /// <summary>
        /// Checks the order status.
        /// </summary>
        /// <returns>The order status.</returns>
        Task<Order> CheckOrderStatus(long orderId);

        /// <summary>
        /// Syncs the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="data">Data.</param>
        Task<Order> SyncOrder(string data);
        /// <summary>
        /// Closeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="currentOrder">Current order.</param>
        Task<bool> ClosedOrder(Order currentOrder);
        /// <summary>
        /// Clears the current order.
        /// </summary>
        /// <returns>The current order.</returns>
        Task<bool> ClearCurrentOrder();

        /// <summary>
        /// Gets the sync order.
        /// </summary>
        /// <returns>The sync order.</returns>
        /// <param name="data">Data.</param>
        Task<Order> GetSyncOrder(string data);

        /// <summary>
        /// Sends the takeaway exist order.
        /// </summary>
        /// <returns>The takeaway exist order.</returns>
        /// <param name="placeId">Place identifier.</param>
        /// <param name="orderNumber">Order number.</param>
        Task<bool> SendTakeAwayExistOrder(long placeId, int orderNumber);

        /// <summary>
        /// Gets my order history.
        /// </summary>
        /// <returns>The my order history.</returns>
        /// <param name="offset">Offset.</param>
        /// <param name="limit">Limit.</param>
        Task<List<Order>> GetMyOrderHistory(int offset, int limit);
    }

    public class CheckOrderBLExistException : Exception
    {
        private CheckOrderExistException e;

        public CheckOrderBLExistException(CheckOrderExistException e) : base(e.Message)
        {
            this.e = e;
        }
    }

    public class CheckOrderBLPendingException : Exception
    {
        private CheckOrderPendingException e;

        public CheckOrderBLPendingException(CheckOrderPendingException e) : base(e.Message)
        {
            this.e = e;
        }
    }

    public class CheckOrderBLPendingRequestException : Exception
    {
        private CheckOrderPendingRequestException e;

        public CheckOrderBLPendingRequestException(CheckOrderPendingRequestException e) : base(e.Message)
        {
            this.e = e;
        }
    }
}
