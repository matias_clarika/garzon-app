﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Xamarin.Base.Helpers;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IOrderCounterBL
    {
        /// <summary>
        /// Finds the order counter.
        /// </summary>
        /// <returns>The order counter.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<OrderCounter>> FindOrderCounter(IPaggination paggination = null);

        /// <summary>
        /// Finds the waiter counter.
        /// </summary>
        /// <returns>The waiter counter.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<WaiterCounter>> FindWaiterCounter(IPaggination paggination = null);

        /// <summary>
        /// Finds the order pay counter.
        /// </summary>
        /// <returns>The order pay counter.</returns>
        /// <param name="paggination">Paggination.</param>
        Task<List<OrderPayCounter>> FindOrderPayCounter(IPaggination paggination = null);

        /// <summary>
        /// Marks the waiter call received.
        /// </summary>
        /// <returns>The waiter call received.</returns>
        /// <param name="id">Identifier.</param>
        Task<bool> MarkWaiterCallReceived(long id);

       /// <summary>
       /// Closeds the order.
       /// </summary>
       /// <returns>The order.</returns>
       /// <param name="orderPayCounter">Order pay counter.</param>
        Task<bool> ClosedOrder(OrderPayCounter orderPayCounter);

        /// <summary>
        /// Closeds the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        Task<bool> ClosedOrder(Order order);

        /// <summary>
        /// Marks the received.
        /// </summary>
        /// <returns>The received.</returns>
        /// <param name="orderCounterReceived">Order counter received.</param>
        Task<bool> MarkReceived(OrderCounterReceived orderCounterReceived);

        /// <summary>
        /// Sends the observation.
        /// </summary>
        /// <returns>The observation.</returns>
        /// <param name="orderCounter">Order counter.</param>
        Task<bool> SendObservation(OrderCounter orderCounter);

        /// <summary>
        /// Sends the confirmation.
        /// </summary>
        /// <returns>The confirmation.</returns>
        /// <param name="orderCounter">Order counter.</param>
        Task<bool> SendConfirmation(OrderCounter orderCounter);


        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <returns>The orders.</returns>
        Task<List<Order>> GetOrders();
        /// <summary>
        /// Changes the table number by order identifier.
        /// </summary>
        /// <returns>The table number by order identifier.</returns>
        /// <param name="sectorId">Sector identifier.</param>
        /// <param name="tableNumber">Table number.</param>
        /// <param name="orderId">Order identifier.</param>
        Task<bool> ChangeTableNumberByOrderId(long sectorId,
                                              int tableNumber,
                                              long orderId);
        /// <summary>
        /// Blocks the users by order.
        /// </summary>
        /// <returns>The users by order.</returns>
        /// <param name="order">Order.</param>
        Task<bool> BlockUsersByOrder(Order order);

        /// <summary>
        /// Gets the orders take away.
        /// </summary>
        /// <returns>The orders take away.</returns>
        Task<List<Order>> GetOrdersTakeAway();

        /// <summary>
        /// Marks the delivered.
        /// </summary>
        /// <returns>The delivered.</returns>
        /// <param name="order">Order.</param>
        Task<bool> MarkDelivered(Order order);

        /// <summary>
        /// Marks the withdraw.
        /// </summary>
        /// <returns>The withdraw.</returns>
        /// <param name="order">Order.</param>
        Task<bool> MarkWithdraw(Order order);
    }
}
