﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Garzon.DataObjects;

namespace Garzon.BL
{
    public interface IOrderDetailBL
    {
        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <returns>The or update.</returns>
        /// <param name="ordetDetail">Ordet detail.</param>
        Task<OrderDetail> InsertOrUpdate(OrderDetail ordetDetail);

        /// <summary>
        /// Delete the specified orderDetail.
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="orderDetail">Order detail.</param>
        Task Delete(OrderDetail orderDetail);

        /// <summary>
        /// Finds the by order identifier.
        /// </summary>
        /// <returns>The by order identifier.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<List<OrderDetail>> FindByOrderId(long orderId);

        /// <summary>
        /// Finds the pending by user.
        /// </summary>
        /// <returns>The pending by user.</returns>
        /// <param name="user">User.</param>
        Task<List<OrderDetail>> FindPendingByUser(User user);
        /// <summary>
        /// Updates the pending to user.
        /// </summary>
        /// <returns>The pending to user.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> UpdatePendingToUser(long userId);

        /// <summary>
        /// Finds the confirmed.
        /// </summary>
        /// <returns>The confirmed.</returns>
        Task<List<OrderDetail>> FindConfirmed();

        /// <summary>
        /// Finds the confirmed by user identifier.
        /// </summary>
        /// <returns>The confirmed by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> FindConfirmedByUserId(long userId);

        /// <summary>
        /// Delete the specified orderDetails.
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="orderDetails">Order details.</param>
        Task<bool> Delete(IEnumerable<OrderDetail> orderDetails);

        /// <summary>
        /// Finds the pending by product identifier.
        /// </summary>
        /// <returns>The pending by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        /// <param name="ownerUser">Owner user.</param>
        Task<OrderDetail> FindPendingByProductId(long productId,
                                                 User ownerUser = null);
        /// <summary>
        /// Finds the waiting order details.
        /// </summary>
        /// <returns>The waiting order details.</returns>
        /// <param name="id">Identifier.</param>
        Task<List<OrderDetail>> FindWaitingOrderDetails(long id);

        /// <summary>
        /// Finds the pending request list by user identifier.
        /// </summary>
        /// <returns>The pending request list by user identifier.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<OrderDetail>> FindPendingRequestListByUserId(long userId);

        /// <summary>
        /// Selects the product by pending order detail.
        /// </summary>
        /// <returns>The product by pending order detail.</returns>
        /// <param name="product">Product.</param>
        /// <param name="orderDetail">Order detail.</param>
        Task<OrderDetail> SelectProductByPendingOrderDetail(Product product, OrderDetail orderDetail);

        /// <summary>
        /// Removes the pending order detail by product identifier.
        /// </summary>
        /// <returns>The pending order detail by product identifier.</returns>
        /// <param name="productId">Product identifier.</param>
        Task<bool> RemovePendingOrderDetailByProductId(long productId);

        /// <summary>
        /// Deletes the pending order details.
        /// </summary>
        /// <returns>The pending order details.</returns>
        Task DeletePendingOrderDetails();
    }
}
