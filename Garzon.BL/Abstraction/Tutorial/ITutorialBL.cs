﻿namespace Garzon.BL
{
    using System;
    using System.Threading.Tasks;

    public interface ITutorialBL
    {
        /// <summary>
        /// Gets the how to call counter tutorial.
        /// </summary>
        /// <returns>The how to call counter tutorial.</returns>
        Task<bool> GetHowToCallCounterTutorial();

        /// <summary>
        /// Gets the pin number and check order tutorial.
        /// </summary>
        /// <returns>The pin number and check order tutorial.</returns>
        Task<bool> GetPinNumberAndCheckOrderTutorial();
    }
}
