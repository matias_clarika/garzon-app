﻿using System;
using System.Threading.Tasks;
using Garzon.DAL.Repository;

namespace Garzon.BL.Tutorial
{
    public class TutorialBL : ITutorialBL
    {
        readonly ITutorialRepository tutorialRepository;

        public TutorialBL(ITutorialRepository tutorialRepository)
        {
            this.tutorialRepository = tutorialRepository;
        }

        public async Task<bool> GetHowToCallCounterTutorial()
        {
            bool result = await this.tutorialRepository.GetHowToCallCounterTutorial();
            if (result)
            {
                await this.tutorialRepository.SetHowToCallCounterTutorial(false);
            }
            return result;
        }

        public async Task<bool> GetPinNumberAndCheckOrderTutorial()
        {
            bool result = await this.tutorialRepository.GetPinNumberAndCheckOrderTutorial();
            if (result)
            {
                await this.tutorialRepository.SetPinNumberAndCheckOrderTutorial(false);
            }
            return result;
        }
    }
}
